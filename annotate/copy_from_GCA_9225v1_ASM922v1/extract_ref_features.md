::: {.cell .code execution_count="6"}
``` {.python}
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord
import gffutils as gff
from BCBio import GFF
import copy
```
:::

::: {.cell .markdown}
Load reference data (genome sequences and annotation)
-----------------------------------------------------
:::

::: {.cell .markdown}
#### Current working dir. {#current-working-dir}
:::

::: {.cell .code execution_count="29"}
``` {.python}
pwd
```

::: {.output .execute_result execution_count="29"}
    '/home/grotec/Repositories/sbw25_newgenome/annotate/copy_from_GCA_9225v1_ASM922v1'
:::
:::

::: {.cell .markdown}
### Read the annotations
:::

::: {.cell .code execution_count="13"}
``` {.python}
ref_gff = "../../reference_annotations/GCA_000009225.1_ASM922v1_genomic.gff"
```
:::

::: {.cell .code execution_count="30"}
``` {.python}
with open(ref_gff, 'r') as fh:
    records = GFF.parse(fh)
    records = copy.deepcopy([rec for rec in records])
```
:::

::: {.cell .markdown}
All in one record:
:::

::: {.cell .code execution_count="32"}
``` {.python}
len(records)
```

::: {.output .execute_result execution_count="32"}
    1
:::
:::

::: {.cell .markdown}
### Get the annotations as features. {#get-the-annotations-as-features}
:::

::: {.cell .code execution_count="33"}
``` {.python}
ref_features = records[0].features
```
:::

::: {.cell .code execution_count="34"}
``` {.python}
len(ref_features)
```

::: {.output .execute_result execution_count="34"}
    26478
:::
:::

::: {.cell .markdown}
### Read the genome sequence. {#read-the-genome-sequence}
:::

::: {.cell .code execution_count="57"}
``` {.python}
ref_fasta = "/home/grotec/data/sync/sbw25/genbank2020/GCA_000009225.1_ASM922v1_genomic.fna"
ref_records = next(SeqIO.parse(open(ref_fasta, 'r'),'fasta'))
```
:::

::: {.cell .markdown}
### Get the sequence proper. {#get-the-sequence-proper}
:::

::: {.cell .code execution_count="57"}
``` {.python}
ref_seq = ref_records.seq
```
:::

::: {.cell .markdown}
Write each feature as a separate fasta file. {#write-each-feature-as-a-separate-fasta-file}
--------------------------------------------

#### Write fw and rev. strands into sep. directories. {#write-fw-and-rev-strands-into-sep-directories}
:::

::: {.cell .code execution_count="57"}
``` {.python}
!mkdir reference_features/plus
```
:::

::: {.cell .code execution_count="57"}
``` {.python}
!mkdir reference_features/minus
```
:::

::: {.cell .code execution_count="57"}
``` {.python}
# Map to get the strand right.
int_to_strand = {-1:"minus", 1:"plus"}
```
:::

::: {.cell .markdown}
### Loop over all features

-   construct one record per feature
-   insert id and annotations.
-   write to appropriate strand directory.
:::

::: {.cell .code execution_count="57"}
``` {.python}
for i,feature in enumerate(ref_features):
    seq = feature.extract(ref_seq)
    seq_record = SeqRecord(seq,
                           id=feature.id,
                           description="",
                           name="",
                           annotations=feature.qualifiers)
        
    strand = feature.location.strand
    
    with open('reference_features/{}/{}.fasta'.format(int_to_strand.get(feature.location.strand),feature.id), 'w') as fh:
        SeqIO.write([seq_record], fh, "fasta")
```
:::

::: {.cell .markdown}
Blast all features against new genome assembly
----------------------------------------------
:::

::: {.cell .code execution_count="57"}
```` {.python}
```shell
for fa in *.fasta; do echo $fa; blastn -task blastn -db ../Pseudomonas_fluorescens_SBW25_28C -query $fa -outfmt "10 qacc qstart qend sstart send sstrand nident pident mismatch positive gaps bitscore score evalue" -strand "minus" -evalue 1e-2 >> blastn_features_to_new_assembly_minus.csv; done
```

```shell
for fa in *.fasta; do echo $fa; blastn -task blastn -db ../Pseudomonas_fluorescens_SBW25_28C -query $fa -outfmt "10 qacc qstart qend sstart send sstrand nident pident mismatch positive gaps bitscore score evalue" -strand "plus" -evalue 1e-2 >> blastn_features_to_new_assembly_plus.csv; done
```
````
:::

::: {.cell .markdown}
Import alignment results
========================
:::

::: {.cell .code execution_count="491"}
``` {.python}
import pandas, numpy
```
:::

::: {.cell .markdown}
Plus strand
-----------
:::

::: {.cell .code execution_count="492"}
``` {.python}
algn_plus = pandas.read_csv("reference_features/plus/blastn_features_to_new_assembly_plus.csv", index_col=0)
```
:::

::: {.cell .markdown}
### Filter out pident \< 100 {#filter-out-pident--100}
:::

::: {.cell .code execution_count="493"}
``` {.python}
algn_plus100 = algn_plus[algn_plus['pident']==100.0]
```
:::

::: {.cell .code execution_count="494"}
``` {.python}
algn_plus100
```

::: {.output .execute_result execution_count="494"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0001</th>
      <td>1</td>
      <td>1506</td>
      <td>1</td>
      <td>1506</td>
      <td>plus</td>
      <td>1506</td>
      <td>100.0</td>
      <td>0</td>
      <td>1506</td>
      <td>0</td>
      <td>2717.0</td>
      <td>3012</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0002</th>
      <td>1</td>
      <td>1104</td>
      <td>1545</td>
      <td>2648</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0003</th>
      <td>1</td>
      <td>1104</td>
      <td>2669</td>
      <td>3772</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0004</th>
      <td>1</td>
      <td>2418</td>
      <td>3777</td>
      <td>6194</td>
      <td>plus</td>
      <td>2418</td>
      <td>100.0</td>
      <td>0</td>
      <td>2418</td>
      <td>0</td>
      <td>4361.0</td>
      <td>4836</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0006</th>
      <td>1</td>
      <td>957</td>
      <td>7284</td>
      <td>8240</td>
      <td>plus</td>
      <td>957</td>
      <td>100.0</td>
      <td>0</td>
      <td>957</td>
      <td>0</td>
      <td>1727.0</td>
      <td>1914</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>8</td>
      <td>26</td>
      <td>2029209</td>
      <td>2029227</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>8</td>
      <td>26</td>
      <td>2376022</td>
      <td>2376040</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885447</td>
      <td>885520</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885699</td>
      <td>885772</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:894195..894267</th>
      <td>1</td>
      <td>73</td>
      <td>894196</td>
      <td>894268</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
  </tbody>
</table>
<p>108853 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Genes
-----
:::

::: {.cell .code execution_count="495"}
``` {.python}
gene_plus = algn_plus100.loc[algn_plus100.index.str.startswith("gene")]
```
:::

::: {.cell .code execution_count="496"}
``` {.python}
gene_plus
```

::: {.output .execute_result execution_count="496"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0001</th>
      <td>1</td>
      <td>1506</td>
      <td>1</td>
      <td>1506</td>
      <td>plus</td>
      <td>1506</td>
      <td>100.0</td>
      <td>0</td>
      <td>1506</td>
      <td>0</td>
      <td>2717.0</td>
      <td>3012</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0002</th>
      <td>1</td>
      <td>1104</td>
      <td>1545</td>
      <td>2648</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0003</th>
      <td>1</td>
      <td>1104</td>
      <td>2669</td>
      <td>3772</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0004</th>
      <td>1</td>
      <td>2418</td>
      <td>3777</td>
      <td>6194</td>
      <td>plus</td>
      <td>2418</td>
      <td>100.0</td>
      <td>0</td>
      <td>2418</td>
      <td>0</td>
      <td>4361.0</td>
      <td>4836</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0006</th>
      <td>1</td>
      <td>957</td>
      <td>7284</td>
      <td>8240</td>
      <td>plus</td>
      <td>957</td>
      <td>100.0</td>
      <td>0</td>
      <td>957</td>
      <td>0</td>
      <td>1727.0</td>
      <td>1914</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>gene-PFLU_6111</th>
      <td>1</td>
      <td>5805</td>
      <td>6688924</td>
      <td>6694728</td>
      <td>plus</td>
      <td>5805</td>
      <td>100.0</td>
      <td>0</td>
      <td>5805</td>
      <td>0</td>
      <td>10469.0</td>
      <td>11610</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6112</th>
      <td>1</td>
      <td>2301</td>
      <td>6694709</td>
      <td>6697009</td>
      <td>plus</td>
      <td>2301</td>
      <td>100.0</td>
      <td>0</td>
      <td>2301</td>
      <td>0</td>
      <td>4150.0</td>
      <td>4602</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6130</th>
      <td>1</td>
      <td>519</td>
      <td>6713810</td>
      <td>6714328</td>
      <td>plus</td>
      <td>519</td>
      <td>100.0</td>
      <td>0</td>
      <td>519</td>
      <td>0</td>
      <td>937.0</td>
      <td>1038</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6131</th>
      <td>1</td>
      <td>945</td>
      <td>6714328</td>
      <td>6715272</td>
      <td>plus</td>
      <td>945</td>
      <td>100.0</td>
      <td>0</td>
      <td>945</td>
      <td>0</td>
      <td>1705.0</td>
      <td>1890</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6132</th>
      <td>1</td>
      <td>2439</td>
      <td>6715365</td>
      <td>6717803</td>
      <td>plus</td>
      <td>2439</td>
      <td>100.0</td>
      <td>0</td>
      <td>2439</td>
      <td>0</td>
      <td>4399.0</td>
      <td>4878</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
<p>3050 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
### Discard duplicates. From each set of duplicates, keep the first {#discard-duplicates-from-each-set-of-duplicates-keep-the-first}
:::

::: {.cell .code execution_count="497"}
``` {.python}
gene_plus.loc[gene_plus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="497"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0013</th>
      <td>1</td>
      <td>888</td>
      <td>15784</td>
      <td>16671</td>
      <td>plus</td>
      <td>888</td>
      <td>100.0</td>
      <td>0</td>
      <td>888</td>
      <td>0</td>
      <td>1602.0</td>
      <td>1776</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0013</th>
      <td>522</td>
      <td>542</td>
      <td>5468445</td>
      <td>5468465</td>
      <td>plus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>gene-PFLU_0049</th>
      <td>1</td>
      <td>276</td>
      <td>49251</td>
      <td>49526</td>
      <td>plus</td>
      <td>276</td>
      <td>100.0</td>
      <td>0</td>
      <td>276</td>
      <td>0</td>
      <td>499.0</td>
      <td>552</td>
      <td>1.000000e-141</td>
    </tr>
    <tr>
      <th>gene-PFLU_0049</th>
      <td>52</td>
      <td>71</td>
      <td>4477981</td>
      <td>4478000</td>
      <td>plus</td>
      <td>20</td>
      <td>100.0</td>
      <td>0</td>
      <td>20</td>
      <td>0</td>
      <td>37.4</td>
      <td>40</td>
      <td>1.000000e-02</td>
    </tr>
    <tr>
      <th>gene-PFLU_0368</th>
      <td>1</td>
      <td>1410</td>
      <td>407245</td>
      <td>408654</td>
      <td>plus</td>
      <td>1410</td>
      <td>100.0</td>
      <td>0</td>
      <td>1410</td>
      <td>0</td>
      <td>2544.0</td>
      <td>2820</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>gene-PFLU_5821</th>
      <td>93</td>
      <td>114</td>
      <td>6161322</td>
      <td>6161343</td>
      <td>plus</td>
      <td>22</td>
      <td>100.0</td>
      <td>0</td>
      <td>22</td>
      <td>0</td>
      <td>41.0</td>
      <td>44</td>
      <td>2.000000e-03</td>
    </tr>
    <tr>
      <th>gene-PFLU_6036</th>
      <td>1</td>
      <td>891</td>
      <td>6603976</td>
      <td>6604866</td>
      <td>plus</td>
      <td>891</td>
      <td>100.0</td>
      <td>0</td>
      <td>891</td>
      <td>0</td>
      <td>1608.0</td>
      <td>1782</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6036</th>
      <td>411</td>
      <td>435</td>
      <td>6183048</td>
      <td>6183072</td>
      <td>plus</td>
      <td>25</td>
      <td>100.0</td>
      <td>0</td>
      <td>25</td>
      <td>0</td>
      <td>46.4</td>
      <td>50</td>
      <td>6.000000e-05</td>
    </tr>
    <tr>
      <th>gene-PFLU_6067</th>
      <td>1</td>
      <td>681</td>
      <td>6640415</td>
      <td>6641095</td>
      <td>plus</td>
      <td>681</td>
      <td>100.0</td>
      <td>0</td>
      <td>681</td>
      <td>0</td>
      <td>1229.0</td>
      <td>1362</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6067</th>
      <td>259</td>
      <td>279</td>
      <td>6673689</td>
      <td>6673709</td>
      <td>plus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>7.000000e-03</td>
    </tr>
  </tbody>
</table>
<p>205 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
In all groups of identical ids, keep the first one. In this way, the
pflu ids keep their order.
:::

::: {.cell .code execution_count="498"}
``` {.python}
gene_plus_unique = gene_plus.loc[numpy.logical_not(gene_plus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="499"}
``` {.python}
gene_plus_unique
```

::: {.output .execute_result execution_count="499"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0001</th>
      <td>1</td>
      <td>1506</td>
      <td>1</td>
      <td>1506</td>
      <td>plus</td>
      <td>1506</td>
      <td>100.0</td>
      <td>0</td>
      <td>1506</td>
      <td>0</td>
      <td>2717.0</td>
      <td>3012</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0002</th>
      <td>1</td>
      <td>1104</td>
      <td>1545</td>
      <td>2648</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0003</th>
      <td>1</td>
      <td>1104</td>
      <td>2669</td>
      <td>3772</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0004</th>
      <td>1</td>
      <td>2418</td>
      <td>3777</td>
      <td>6194</td>
      <td>plus</td>
      <td>2418</td>
      <td>100.0</td>
      <td>0</td>
      <td>2418</td>
      <td>0</td>
      <td>4361.0</td>
      <td>4836</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_0006</th>
      <td>1</td>
      <td>957</td>
      <td>7284</td>
      <td>8240</td>
      <td>plus</td>
      <td>957</td>
      <td>100.0</td>
      <td>0</td>
      <td>957</td>
      <td>0</td>
      <td>1727.0</td>
      <td>1914</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>gene-PFLU_6111</th>
      <td>1</td>
      <td>5805</td>
      <td>6688924</td>
      <td>6694728</td>
      <td>plus</td>
      <td>5805</td>
      <td>100.0</td>
      <td>0</td>
      <td>5805</td>
      <td>0</td>
      <td>10469.0</td>
      <td>11610</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6112</th>
      <td>1</td>
      <td>2301</td>
      <td>6694709</td>
      <td>6697009</td>
      <td>plus</td>
      <td>2301</td>
      <td>100.0</td>
      <td>0</td>
      <td>2301</td>
      <td>0</td>
      <td>4150.0</td>
      <td>4602</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6130</th>
      <td>1</td>
      <td>519</td>
      <td>6713810</td>
      <td>6714328</td>
      <td>plus</td>
      <td>519</td>
      <td>100.0</td>
      <td>0</td>
      <td>519</td>
      <td>0</td>
      <td>937.0</td>
      <td>1038</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6131</th>
      <td>1</td>
      <td>945</td>
      <td>6714328</td>
      <td>6715272</td>
      <td>plus</td>
      <td>945</td>
      <td>100.0</td>
      <td>0</td>
      <td>945</td>
      <td>0</td>
      <td>1705.0</td>
      <td>1890</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>gene-PFLU_6132</th>
      <td>1</td>
      <td>2439</td>
      <td>6715365</td>
      <td>6717803</td>
      <td>plus</td>
      <td>2439</td>
      <td>100.0</td>
      <td>0</td>
      <td>2439</td>
      <td>0</td>
      <td>4399.0</td>
      <td>4878</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
<p>2902 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Gene subfeatures
----------------
:::

::: {.cell .code execution_count="500"}
``` {.python}
gene_plus_sub  = algn_plus100.loc[algn_plus100.index.str.startswith("id-PFLU")]
```
:::

::: {.cell .code execution_count="501"}
``` {.python}
gene_plus_sub
```

::: {.output .execute_result execution_count="501"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-PFLU_0001-2</th>
      <td>1</td>
      <td>24</td>
      <td>616</td>
      <td>639</td>
      <td>plus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>id-PFLU_0001-3</th>
      <td>1</td>
      <td>210</td>
      <td>1225</td>
      <td>1434</td>
      <td>plus</td>
      <td>210</td>
      <td>100.0</td>
      <td>0</td>
      <td>210</td>
      <td>0</td>
      <td>379.0</td>
      <td>420</td>
      <td>5.000000e-106</td>
    </tr>
    <tr>
      <th>id-PFLU_0001-4</th>
      <td>1</td>
      <td>60</td>
      <td>1375</td>
      <td>1434</td>
      <td>plus</td>
      <td>60</td>
      <td>100.0</td>
      <td>0</td>
      <td>60</td>
      <td>0</td>
      <td>109.0</td>
      <td>120</td>
      <td>3.000000e-25</td>
    </tr>
    <tr>
      <th>id-PFLU_0001</th>
      <td>1</td>
      <td>657</td>
      <td>496</td>
      <td>1152</td>
      <td>plus</td>
      <td>657</td>
      <td>100.0</td>
      <td>0</td>
      <td>657</td>
      <td>0</td>
      <td>1186.0</td>
      <td>1314</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_0002-2</th>
      <td>1</td>
      <td>348</td>
      <td>1929</td>
      <td>2276</td>
      <td>plus</td>
      <td>348</td>
      <td>100.0</td>
      <td>0</td>
      <td>348</td>
      <td>0</td>
      <td>628.0</td>
      <td>696</td>
      <td>1.000000e-180</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_6130</th>
      <td>1</td>
      <td>201</td>
      <td>6713858</td>
      <td>6714058</td>
      <td>plus</td>
      <td>201</td>
      <td>100.0</td>
      <td>0</td>
      <td>201</td>
      <td>0</td>
      <td>363.0</td>
      <td>402</td>
      <td>4.000000e-101</td>
    </tr>
    <tr>
      <th>id-PFLU_6131</th>
      <td>1</td>
      <td>285</td>
      <td>6714640</td>
      <td>6714924</td>
      <td>plus</td>
      <td>285</td>
      <td>100.0</td>
      <td>0</td>
      <td>285</td>
      <td>0</td>
      <td>515.0</td>
      <td>570</td>
      <td>1.000000e-146</td>
    </tr>
    <tr>
      <th>id-PFLU_6132-2</th>
      <td>1</td>
      <td>324</td>
      <td>6715842</td>
      <td>6716165</td>
      <td>plus</td>
      <td>324</td>
      <td>100.0</td>
      <td>0</td>
      <td>324</td>
      <td>0</td>
      <td>585.0</td>
      <td>648</td>
      <td>1.000000e-167</td>
    </tr>
    <tr>
      <th>id-PFLU_6132-3</th>
      <td>1</td>
      <td>720</td>
      <td>6717081</td>
      <td>6717800</td>
      <td>plus</td>
      <td>720</td>
      <td>100.0</td>
      <td>0</td>
      <td>720</td>
      <td>0</td>
      <td>1299.0</td>
      <td>1440</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_6132</th>
      <td>1</td>
      <td>156</td>
      <td>6715557</td>
      <td>6715712</td>
      <td>plus</td>
      <td>156</td>
      <td>100.0</td>
      <td>0</td>
      <td>156</td>
      <td>0</td>
      <td>282.0</td>
      <td>312</td>
      <td>8.000000e-77</td>
    </tr>
  </tbody>
</table>
<p>8659 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="502"}
``` {.python}
gene_plus_sub.loc[gene_plus_sub.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="502"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-PFLU_0007_3</th>
      <td>1</td>
      <td>60</td>
      <td>9237</td>
      <td>9296</td>
      <td>plus</td>
      <td>60</td>
      <td>100.0</td>
      <td>0</td>
      <td>60</td>
      <td>0</td>
      <td>109.0</td>
      <td>120</td>
      <td>3.000000e-25</td>
    </tr>
    <tr>
      <th>id-PFLU_0007_3</th>
      <td>21</td>
      <td>39</td>
      <td>5512737</td>
      <td>5512755</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>5.000000e-03</td>
    </tr>
    <tr>
      <th>id-PFLU_0013</th>
      <td>1</td>
      <td>831</td>
      <td>15811</td>
      <td>16641</td>
      <td>plus</td>
      <td>831</td>
      <td>100.0</td>
      <td>0</td>
      <td>831</td>
      <td>0</td>
      <td>1499.0</td>
      <td>1662</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_0013</th>
      <td>495</td>
      <td>515</td>
      <td>5468445</td>
      <td>5468465</td>
      <td>plus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-PFLU_0033_7</th>
      <td>1</td>
      <td>60</td>
      <td>36639</td>
      <td>36698</td>
      <td>plus</td>
      <td>60</td>
      <td>100.0</td>
      <td>0</td>
      <td>60</td>
      <td>0</td>
      <td>109.0</td>
      <td>120</td>
      <td>3.000000e-25</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_6067-2</th>
      <td>10</td>
      <td>30</td>
      <td>6673689</td>
      <td>6673709</td>
      <td>plus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>2.000000e-03</td>
    </tr>
    <tr>
      <th>id-PFLU_6076</th>
      <td>1</td>
      <td>69</td>
      <td>6654166</td>
      <td>6654234</td>
      <td>plus</td>
      <td>69</td>
      <td>100.0</td>
      <td>0</td>
      <td>69</td>
      <td>0</td>
      <td>125.0</td>
      <td>138</td>
      <td>5.000000e-30</td>
    </tr>
    <tr>
      <th>id-PFLU_6076</th>
      <td>13</td>
      <td>31</td>
      <td>4124329</td>
      <td>4124347</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>6.000000e-03</td>
    </tr>
    <tr>
      <th>id-PFLU_6090-2_6</th>
      <td>1</td>
      <td>69</td>
      <td>6667851</td>
      <td>6667919</td>
      <td>plus</td>
      <td>69</td>
      <td>100.0</td>
      <td>0</td>
      <td>69</td>
      <td>0</td>
      <td>125.0</td>
      <td>138</td>
      <td>5.000000e-30</td>
    </tr>
    <tr>
      <th>id-PFLU_6090-2_6</th>
      <td>14</td>
      <td>33</td>
      <td>4738241</td>
      <td>4738260</td>
      <td>plus</td>
      <td>20</td>
      <td>100.0</td>
      <td>0</td>
      <td>20</td>
      <td>0</td>
      <td>37.4</td>
      <td>40</td>
      <td>2.000000e-03</td>
    </tr>
  </tbody>
</table>
<p>721 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="503"}
``` {.python}
gene_plus_sub_unique = gene_plus_sub.loc[numpy.logical_not(gene_plus_sub.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="504"}
``` {.python}
gene_plus_sub_unique
```

::: {.output .execute_result execution_count="504"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-PFLU_0001-2</th>
      <td>1</td>
      <td>24</td>
      <td>616</td>
      <td>639</td>
      <td>plus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>id-PFLU_0001-3</th>
      <td>1</td>
      <td>210</td>
      <td>1225</td>
      <td>1434</td>
      <td>plus</td>
      <td>210</td>
      <td>100.0</td>
      <td>0</td>
      <td>210</td>
      <td>0</td>
      <td>379.0</td>
      <td>420</td>
      <td>5.000000e-106</td>
    </tr>
    <tr>
      <th>id-PFLU_0001-4</th>
      <td>1</td>
      <td>60</td>
      <td>1375</td>
      <td>1434</td>
      <td>plus</td>
      <td>60</td>
      <td>100.0</td>
      <td>0</td>
      <td>60</td>
      <td>0</td>
      <td>109.0</td>
      <td>120</td>
      <td>3.000000e-25</td>
    </tr>
    <tr>
      <th>id-PFLU_0001</th>
      <td>1</td>
      <td>657</td>
      <td>496</td>
      <td>1152</td>
      <td>plus</td>
      <td>657</td>
      <td>100.0</td>
      <td>0</td>
      <td>657</td>
      <td>0</td>
      <td>1186.0</td>
      <td>1314</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_0002-2</th>
      <td>1</td>
      <td>348</td>
      <td>1929</td>
      <td>2276</td>
      <td>plus</td>
      <td>348</td>
      <td>100.0</td>
      <td>0</td>
      <td>348</td>
      <td>0</td>
      <td>628.0</td>
      <td>696</td>
      <td>1.000000e-180</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_6130</th>
      <td>1</td>
      <td>201</td>
      <td>6713858</td>
      <td>6714058</td>
      <td>plus</td>
      <td>201</td>
      <td>100.0</td>
      <td>0</td>
      <td>201</td>
      <td>0</td>
      <td>363.0</td>
      <td>402</td>
      <td>4.000000e-101</td>
    </tr>
    <tr>
      <th>id-PFLU_6131</th>
      <td>1</td>
      <td>285</td>
      <td>6714640</td>
      <td>6714924</td>
      <td>plus</td>
      <td>285</td>
      <td>100.0</td>
      <td>0</td>
      <td>285</td>
      <td>0</td>
      <td>515.0</td>
      <td>570</td>
      <td>1.000000e-146</td>
    </tr>
    <tr>
      <th>id-PFLU_6132-2</th>
      <td>1</td>
      <td>324</td>
      <td>6715842</td>
      <td>6716165</td>
      <td>plus</td>
      <td>324</td>
      <td>100.0</td>
      <td>0</td>
      <td>324</td>
      <td>0</td>
      <td>585.0</td>
      <td>648</td>
      <td>1.000000e-167</td>
    </tr>
    <tr>
      <th>id-PFLU_6132-3</th>
      <td>1</td>
      <td>720</td>
      <td>6717081</td>
      <td>6717800</td>
      <td>plus</td>
      <td>720</td>
      <td>100.0</td>
      <td>0</td>
      <td>720</td>
      <td>0</td>
      <td>1299.0</td>
      <td>1440</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_6132</th>
      <td>1</td>
      <td>156</td>
      <td>6715557</td>
      <td>6715712</td>
      <td>plus</td>
      <td>156</td>
      <td>100.0</td>
      <td>0</td>
      <td>156</td>
      <td>0</td>
      <td>282.0</td>
      <td>312</td>
      <td>8.000000e-77</td>
    </tr>
  </tbody>
</table>
<p>8187 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Other subfeatures
-----------------
:::

::: {.cell .code execution_count="505"}
``` {.python}
misc_plus  = algn_plus100.loc[algn_plus100.index.str.startswith("id-AM")]
```
:::

::: {.cell .code execution_count="506"}
``` {.python}
misc_plus
```

::: {.output .execute_result execution_count="506"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1001373..1001465</th>
      <td>1</td>
      <td>93</td>
      <td>1001583</td>
      <td>1001675</td>
      <td>plus</td>
      <td>93</td>
      <td>100.0</td>
      <td>0</td>
      <td>93</td>
      <td>0</td>
      <td>168.0</td>
      <td>186</td>
      <td>6.000000e-43</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465</th>
      <td>44</td>
      <td>64</td>
      <td>4331246</td>
      <td>4331266</td>
      <td>plus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>8.000000e-04</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465</th>
      <td>74</td>
      <td>93</td>
      <td>2465053</td>
      <td>2465072</td>
      <td>plus</td>
      <td>20</td>
      <td>100.0</td>
      <td>0</td>
      <td>20</td>
      <td>0</td>
      <td>37.4</td>
      <td>40</td>
      <td>3.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1002303..1002333</th>
      <td>1</td>
      <td>31</td>
      <td>1002513</td>
      <td>1002543</td>
      <td>plus</td>
      <td>31</td>
      <td>100.0</td>
      <td>0</td>
      <td>31</td>
      <td>0</td>
      <td>57.2</td>
      <td>62</td>
      <td>4.000000e-10</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1004997..1005021</th>
      <td>1</td>
      <td>25</td>
      <td>1005207</td>
      <td>1005231</td>
      <td>plus</td>
      <td>25</td>
      <td>100.0</td>
      <td>0</td>
      <td>25</td>
      <td>0</td>
      <td>46.4</td>
      <td>50</td>
      <td>4.000000e-07</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968638..968665</th>
      <td>7</td>
      <td>24</td>
      <td>4387198</td>
      <td>4387215</td>
      <td>plus</td>
      <td>18</td>
      <td>100.0</td>
      <td>0</td>
      <td>18</td>
      <td>0</td>
      <td>33.7</td>
      <td>36</td>
      <td>4.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968638..968665</th>
      <td>7</td>
      <td>24</td>
      <td>5445291</td>
      <td>5445308</td>
      <td>plus</td>
      <td>18</td>
      <td>100.0</td>
      <td>0</td>
      <td>18</td>
      <td>0</td>
      <td>33.7</td>
      <td>36</td>
      <td>4.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968666..1001372</th>
      <td>1</td>
      <td>32707</td>
      <td>968875</td>
      <td>1001582</td>
      <td>plus</td>
      <td>32707</td>
      <td>100.0</td>
      <td>0</td>
      <td>32707</td>
      <td>1</td>
      <td>58976.0</td>
      <td>65406</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968666..1001372</th>
      <td>4846</td>
      <td>4869</td>
      <td>3629793</td>
      <td>3629816</td>
      <td>plus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>8.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:985333..985355</th>
      <td>1</td>
      <td>23</td>
      <td>985543</td>
      <td>985565</td>
      <td>plus</td>
      <td>23</td>
      <td>100.0</td>
      <td>0</td>
      <td>23</td>
      <td>0</td>
      <td>42.8</td>
      <td>46</td>
      <td>5.000000e-06</td>
    </tr>
  </tbody>
</table>
<p>97038 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="507"}
``` {.python}
misc_plus.loc[misc_plus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="507"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1001373..1001465</th>
      <td>1</td>
      <td>93</td>
      <td>1001583</td>
      <td>1001675</td>
      <td>plus</td>
      <td>93</td>
      <td>100.0</td>
      <td>0</td>
      <td>93</td>
      <td>0</td>
      <td>168.0</td>
      <td>186</td>
      <td>6.000000e-43</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465</th>
      <td>44</td>
      <td>64</td>
      <td>4331246</td>
      <td>4331266</td>
      <td>plus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>8.000000e-04</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465</th>
      <td>74</td>
      <td>93</td>
      <td>2465053</td>
      <td>2465072</td>
      <td>plus</td>
      <td>20</td>
      <td>100.0</td>
      <td>0</td>
      <td>20</td>
      <td>0</td>
      <td>37.4</td>
      <td>40</td>
      <td>3.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1013126..1013220</th>
      <td>1</td>
      <td>95</td>
      <td>1013336</td>
      <td>1013430</td>
      <td>plus</td>
      <td>95</td>
      <td>100.0</td>
      <td>0</td>
      <td>95</td>
      <td>0</td>
      <td>172.0</td>
      <td>190</td>
      <td>5.000000e-44</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1013126..1013220</th>
      <td>5</td>
      <td>34</td>
      <td>2891736</td>
      <td>2891765</td>
      <td>plus</td>
      <td>30</td>
      <td>100.0</td>
      <td>0</td>
      <td>30</td>
      <td>0</td>
      <td>55.4</td>
      <td>60</td>
      <td>1.000000e-08</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968638..968665</th>
      <td>5</td>
      <td>22</td>
      <td>2206764</td>
      <td>2206781</td>
      <td>plus</td>
      <td>18</td>
      <td>100.0</td>
      <td>0</td>
      <td>18</td>
      <td>0</td>
      <td>33.7</td>
      <td>36</td>
      <td>4.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968638..968665</th>
      <td>7</td>
      <td>24</td>
      <td>4387198</td>
      <td>4387215</td>
      <td>plus</td>
      <td>18</td>
      <td>100.0</td>
      <td>0</td>
      <td>18</td>
      <td>0</td>
      <td>33.7</td>
      <td>36</td>
      <td>4.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968638..968665</th>
      <td>7</td>
      <td>24</td>
      <td>5445291</td>
      <td>5445308</td>
      <td>plus</td>
      <td>18</td>
      <td>100.0</td>
      <td>0</td>
      <td>18</td>
      <td>0</td>
      <td>33.7</td>
      <td>36</td>
      <td>4.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968666..1001372</th>
      <td>1</td>
      <td>32707</td>
      <td>968875</td>
      <td>1001582</td>
      <td>plus</td>
      <td>32707</td>
      <td>100.0</td>
      <td>0</td>
      <td>32707</td>
      <td>1</td>
      <td>58976.0</td>
      <td>65406</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968666..1001372</th>
      <td>4846</td>
      <td>4869</td>
      <td>3629793</td>
      <td>3629816</td>
      <td>plus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>8.000000e-03</td>
    </tr>
  </tbody>
</table>
<p>96548 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="508"}
``` {.python}
misc_plus_unique = misc_plus.loc[numpy.logical_not(misc_plus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="509"}
``` {.python}
misc_plus_unique
```

::: {.output .execute_result execution_count="509"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1001373..1001465</th>
      <td>1</td>
      <td>93</td>
      <td>1001583</td>
      <td>1001675</td>
      <td>plus</td>
      <td>93</td>
      <td>100.0</td>
      <td>0</td>
      <td>93</td>
      <td>0</td>
      <td>168.0</td>
      <td>186</td>
      <td>6.000000e-43</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1002303..1002333</th>
      <td>1</td>
      <td>31</td>
      <td>1002513</td>
      <td>1002543</td>
      <td>plus</td>
      <td>31</td>
      <td>100.0</td>
      <td>0</td>
      <td>31</td>
      <td>0</td>
      <td>57.2</td>
      <td>62</td>
      <td>4.000000e-10</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1004997..1005021</th>
      <td>1</td>
      <td>25</td>
      <td>1005207</td>
      <td>1005231</td>
      <td>plus</td>
      <td>25</td>
      <td>100.0</td>
      <td>0</td>
      <td>25</td>
      <td>0</td>
      <td>46.4</td>
      <td>50</td>
      <td>4.000000e-07</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1008696..1008719</th>
      <td>1</td>
      <td>24</td>
      <td>1008906</td>
      <td>1008929</td>
      <td>plus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1013126..1013220</th>
      <td>1</td>
      <td>95</td>
      <td>1013336</td>
      <td>1013430</td>
      <td>plus</td>
      <td>95</td>
      <td>100.0</td>
      <td>0</td>
      <td>95</td>
      <td>0</td>
      <td>172.0</td>
      <td>190</td>
      <td>5.000000e-44</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-AM181176.4:966597..966623</th>
      <td>1</td>
      <td>27</td>
      <td>966806</td>
      <td>966832</td>
      <td>plus</td>
      <td>27</td>
      <td>100.0</td>
      <td>0</td>
      <td>27</td>
      <td>0</td>
      <td>50.0</td>
      <td>54</td>
      <td>5.000000e-08</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968665</th>
      <td>1</td>
      <td>92</td>
      <td>968783</td>
      <td>968874</td>
      <td>plus</td>
      <td>92</td>
      <td>100.0</td>
      <td>0</td>
      <td>92</td>
      <td>0</td>
      <td>167.0</td>
      <td>184</td>
      <td>2.000000e-42</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968638..968665</th>
      <td>1</td>
      <td>28</td>
      <td>968847</td>
      <td>968874</td>
      <td>plus</td>
      <td>28</td>
      <td>100.0</td>
      <td>0</td>
      <td>28</td>
      <td>0</td>
      <td>51.8</td>
      <td>56</td>
      <td>2.000000e-08</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968666..1001372</th>
      <td>1</td>
      <td>32707</td>
      <td>968875</td>
      <td>1001582</td>
      <td>plus</td>
      <td>32707</td>
      <td>100.0</td>
      <td>0</td>
      <td>32707</td>
      <td>1</td>
      <td>58976.0</td>
      <td>65406</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-AM181176.4:985333..985355</th>
      <td>1</td>
      <td>23</td>
      <td>985543</td>
      <td>985565</td>
      <td>plus</td>
      <td>23</td>
      <td>100.0</td>
      <td>0</td>
      <td>23</td>
      <td>0</td>
      <td>42.8</td>
      <td>46</td>
      <td>5.000000e-06</td>
    </tr>
  </tbody>
</table>
<p>1884 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
rna
---
:::

::: {.cell .code execution_count="510"}
``` {.python}
rna_plus  = algn_plus100.loc[algn_plus100.index.str.startswith("rna-")]
```
:::

::: {.cell .code execution_count="511"}
``` {.python}
rna_plus
```

::: {.output .execute_result execution_count="511"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>rna-AM181176.4:1040892..1041184</th>
      <td>1</td>
      <td>293</td>
      <td>1041102</td>
      <td>1041394</td>
      <td>plus</td>
      <td>293</td>
      <td>100.0</td>
      <td>0</td>
      <td>293</td>
      <td>0</td>
      <td>529.0</td>
      <td>586</td>
      <td>6.000000e-151</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:119713..121250</th>
      <td>1</td>
      <td>1538</td>
      <td>119714</td>
      <td>121251</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:119713..121250</th>
      <td>1</td>
      <td>1538</td>
      <td>768526</td>
      <td>770063</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:119713..121250</th>
      <td>1</td>
      <td>1538</td>
      <td>2235130</td>
      <td>2236667</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:121338..121411</th>
      <td>1</td>
      <td>74</td>
      <td>121339</td>
      <td>121412</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>8</td>
      <td>26</td>
      <td>2029209</td>
      <td>2029227</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>8</td>
      <td>26</td>
      <td>2376022</td>
      <td>2376040</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885447</td>
      <td>885520</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885699</td>
      <td>885772</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:894195..894267</th>
      <td>1</td>
      <td>73</td>
      <td>894196</td>
      <td>894268</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
  </tbody>
</table>
<p>106 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="512"}
``` {.python}
rna_plus.loc[rna_plus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="512"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>rna-AM181176.4:119713..121250</th>
      <td>1</td>
      <td>1538</td>
      <td>119714</td>
      <td>121251</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:119713..121250</th>
      <td>1</td>
      <td>1538</td>
      <td>768526</td>
      <td>770063</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:119713..121250</th>
      <td>1</td>
      <td>1538</td>
      <td>2235130</td>
      <td>2236667</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:121338..121411</th>
      <td>1</td>
      <td>74</td>
      <td>121339</td>
      <td>121412</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:121338..121411</th>
      <td>1</td>
      <td>74</td>
      <td>770151</td>
      <td>770224</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>8</td>
      <td>26</td>
      <td>2028950</td>
      <td>2028968</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>8</td>
      <td>26</td>
      <td>2029209</td>
      <td>2029227</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>8</td>
      <td>26</td>
      <td>2376022</td>
      <td>2376040</td>
      <td>plus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885447</td>
      <td>885520</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885699</td>
      <td>885772</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
  </tbody>
</table>
<p>87 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="513"}
``` {.python}
rna_plus_unique = rna_plus.loc[numpy.logical_not(rna_plus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="514"}
``` {.python}
rna_plus.loc["rna-AM181176.4:1299177..1299340"]
```

::: {.output .execute_result execution_count="514"}
    qstart            1
    qend            164
    sstart      1299387
    send        1299550
    sstrand        plus
    nident          164
    pident        100.0
    mismatch          0
    positive        164
    gaps              0
    bitscore      297.0
    score           328
    evalue          0.0
    Name: rna-AM181176.4:1299177..1299340, dtype: object
:::
:::

::: {.cell .code execution_count="515"}
``` {.python}
rna_plus_unique
```

::: {.output .execute_result execution_count="515"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>rna-AM181176.4:1040892..1041184</th>
      <td>1</td>
      <td>293</td>
      <td>1041102</td>
      <td>1041394</td>
      <td>plus</td>
      <td>293</td>
      <td>100.0</td>
      <td>0</td>
      <td>293</td>
      <td>0</td>
      <td>529.0</td>
      <td>586</td>
      <td>6.000000e-151</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:119713..121250</th>
      <td>1</td>
      <td>1538</td>
      <td>119714</td>
      <td>121251</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:121338..121411</th>
      <td>1</td>
      <td>74</td>
      <td>121339</td>
      <td>121412</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:121445..121517</th>
      <td>1</td>
      <td>73</td>
      <td>121446</td>
      <td>121518</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:121764..124664</th>
      <td>1</td>
      <td>2901</td>
      <td>121765</td>
      <td>124665</td>
      <td>plus</td>
      <td>2901</td>
      <td>100.0</td>
      <td>0</td>
      <td>2901</td>
      <td>0</td>
      <td>5232.0</td>
      <td>5802</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:124811..124930</th>
      <td>1</td>
      <td>120</td>
      <td>124812</td>
      <td>124931</td>
      <td>plus</td>
      <td>120</td>
      <td>100.0</td>
      <td>0</td>
      <td>120</td>
      <td>0</td>
      <td>217.0</td>
      <td>240</td>
      <td>2.000000e-57</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:125036..125155</th>
      <td>1</td>
      <td>120</td>
      <td>125037</td>
      <td>125156</td>
      <td>plus</td>
      <td>120</td>
      <td>100.0</td>
      <td>0</td>
      <td>120</td>
      <td>0</td>
      <td>217.0</td>
      <td>240</td>
      <td>2.000000e-57</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:1299177..1299340</th>
      <td>1</td>
      <td>164</td>
      <td>1299387</td>
      <td>1299550</td>
      <td>plus</td>
      <td>164</td>
      <td>100.0</td>
      <td>0</td>
      <td>164</td>
      <td>0</td>
      <td>297.0</td>
      <td>328</td>
      <td>4.000000e-81</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:1624965..1625051</th>
      <td>1</td>
      <td>87</td>
      <td>1625175</td>
      <td>1625261</td>
      <td>plus</td>
      <td>87</td>
      <td>100.0</td>
      <td>0</td>
      <td>87</td>
      <td>0</td>
      <td>158.0</td>
      <td>174</td>
      <td>1.000000e-39</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2028653..2028725</th>
      <td>1</td>
      <td>73</td>
      <td>2028863</td>
      <td>2028935</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2028733..2028805</th>
      <td>1</td>
      <td>73</td>
      <td>2028943</td>
      <td>2029015</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2028913..2028985</th>
      <td>1</td>
      <td>73</td>
      <td>2028863</td>
      <td>2028935</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2028992..2029064</th>
      <td>1</td>
      <td>73</td>
      <td>2028943</td>
      <td>2029015</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2049983..2050055</th>
      <td>1</td>
      <td>73</td>
      <td>2050193</td>
      <td>2050265</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2050078..2050151</th>
      <td>1</td>
      <td>74</td>
      <td>2050288</td>
      <td>2050361</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2050331..2050403</th>
      <td>1</td>
      <td>73</td>
      <td>2050193</td>
      <td>2050265</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2050426..2050499</th>
      <td>1</td>
      <td>74</td>
      <td>2050288</td>
      <td>2050361</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2234920..2236457</th>
      <td>1</td>
      <td>1538</td>
      <td>119714</td>
      <td>121251</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2236545..2236618</th>
      <td>1</td>
      <td>74</td>
      <td>121339</td>
      <td>121412</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2236652..2236724</th>
      <td>1</td>
      <td>73</td>
      <td>121446</td>
      <td>121518</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2236971..2239871</th>
      <td>1</td>
      <td>2901</td>
      <td>121765</td>
      <td>124665</td>
      <td>plus</td>
      <td>2901</td>
      <td>100.0</td>
      <td>0</td>
      <td>2901</td>
      <td>0</td>
      <td>5232.0</td>
      <td>5802</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2240018..2240137</th>
      <td>1</td>
      <td>120</td>
      <td>124812</td>
      <td>124931</td>
      <td>plus</td>
      <td>120</td>
      <td>100.0</td>
      <td>0</td>
      <td>120</td>
      <td>0</td>
      <td>217.0</td>
      <td>240</td>
      <td>2.000000e-57</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2375805..2375877</th>
      <td>1</td>
      <td>73</td>
      <td>2028943</td>
      <td>2029015</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2944182..2944381</th>
      <td>1</td>
      <td>200</td>
      <td>2944392</td>
      <td>2944591</td>
      <td>plus</td>
      <td>200</td>
      <td>100.0</td>
      <td>0</td>
      <td>200</td>
      <td>0</td>
      <td>361.0</td>
      <td>400</td>
      <td>1.000000e-100</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:3221272..3221355</th>
      <td>1</td>
      <td>84</td>
      <td>3221482</td>
      <td>3221565</td>
      <td>plus</td>
      <td>84</td>
      <td>100.0</td>
      <td>0</td>
      <td>84</td>
      <td>0</td>
      <td>152.0</td>
      <td>168</td>
      <td>4.000000e-38</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4296931..4297080</th>
      <td>1</td>
      <td>150</td>
      <td>4296791</td>
      <td>4296940</td>
      <td>plus</td>
      <td>150</td>
      <td>100.0</td>
      <td>0</td>
      <td>150</td>
      <td>0</td>
      <td>271.0</td>
      <td>300</td>
      <td>1.000000e-73</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4663279..4663474</th>
      <td>1</td>
      <td>196</td>
      <td>4663139</td>
      <td>4663334</td>
      <td>plus</td>
      <td>196</td>
      <td>100.0</td>
      <td>0</td>
      <td>196</td>
      <td>0</td>
      <td>354.0</td>
      <td>392</td>
      <td>2.000000e-98</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5226269..5226438</th>
      <td>1</td>
      <td>170</td>
      <td>5226129</td>
      <td>5226298</td>
      <td>plus</td>
      <td>170</td>
      <td>100.0</td>
      <td>0</td>
      <td>170</td>
      <td>0</td>
      <td>307.0</td>
      <td>340</td>
      <td>2.000000e-84</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5488630..5488703</th>
      <td>1</td>
      <td>74</td>
      <td>5488490</td>
      <td>5488563</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5489022..5489095</th>
      <td>1</td>
      <td>74</td>
      <td>5488490</td>
      <td>5488563</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5577099..5577180</th>
      <td>1</td>
      <td>82</td>
      <td>5576959</td>
      <td>5577040</td>
      <td>plus</td>
      <td>82</td>
      <td>100.0</td>
      <td>0</td>
      <td>82</td>
      <td>0</td>
      <td>149.0</td>
      <td>164</td>
      <td>5.000000e-37</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:557909..558014</th>
      <td>1</td>
      <td>106</td>
      <td>557910</td>
      <td>558015</td>
      <td>plus</td>
      <td>106</td>
      <td>100.0</td>
      <td>0</td>
      <td>106</td>
      <td>0</td>
      <td>192.0</td>
      <td>212</td>
      <td>7.000000e-50</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5717037..5717156</th>
      <td>1</td>
      <td>120</td>
      <td>5716897</td>
      <td>5717016</td>
      <td>plus</td>
      <td>120</td>
      <td>100.0</td>
      <td>0</td>
      <td>120</td>
      <td>0</td>
      <td>217.0</td>
      <td>240</td>
      <td>2.000000e-57</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5720717..5722254</th>
      <td>1</td>
      <td>1538</td>
      <td>5720577</td>
      <td>5722114</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5735279..5735433</th>
      <td>1</td>
      <td>155</td>
      <td>5735139</td>
      <td>5735293</td>
      <td>plus</td>
      <td>155</td>
      <td>100.0</td>
      <td>0</td>
      <td>155</td>
      <td>0</td>
      <td>280.0</td>
      <td>310</td>
      <td>3.000000e-76</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5798308..5798701</th>
      <td>1</td>
      <td>394</td>
      <td>5798168</td>
      <td>5798561</td>
      <td>plus</td>
      <td>394</td>
      <td>100.0</td>
      <td>0</td>
      <td>394</td>
      <td>0</td>
      <td>711.0</td>
      <td>788</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6068492..6068611</th>
      <td>1</td>
      <td>120</td>
      <td>6068352</td>
      <td>6068471</td>
      <td>plus</td>
      <td>120</td>
      <td>100.0</td>
      <td>0</td>
      <td>120</td>
      <td>0</td>
      <td>217.0</td>
      <td>240</td>
      <td>2.000000e-57</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6068758..6071658</th>
      <td>1</td>
      <td>2901</td>
      <td>5717163</td>
      <td>5720063</td>
      <td>plus</td>
      <td>2901</td>
      <td>100.0</td>
      <td>0</td>
      <td>2901</td>
      <td>0</td>
      <td>5232.0</td>
      <td>5802</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072180..6073709</th>
      <td>1</td>
      <td>1530</td>
      <td>5720585</td>
      <td>5722114</td>
      <td>plus</td>
      <td>1530</td>
      <td>100.0</td>
      <td>0</td>
      <td>1530</td>
      <td>0</td>
      <td>2760.0</td>
      <td>3060</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6129999..6130072</th>
      <td>1</td>
      <td>74</td>
      <td>6129859</td>
      <td>6129932</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6438562..6438739</th>
      <td>1</td>
      <td>178</td>
      <td>6438422</td>
      <td>6438599</td>
      <td>plus</td>
      <td>178</td>
      <td>100.0</td>
      <td>0</td>
      <td>178</td>
      <td>0</td>
      <td>322.0</td>
      <td>356</td>
      <td>1.000000e-88</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:768525..770062</th>
      <td>1</td>
      <td>1538</td>
      <td>119714</td>
      <td>121251</td>
      <td>plus</td>
      <td>1538</td>
      <td>100.0</td>
      <td>0</td>
      <td>1538</td>
      <td>0</td>
      <td>2774.0</td>
      <td>3076</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:770150..770223</th>
      <td>1</td>
      <td>74</td>
      <td>121339</td>
      <td>121412</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:770257..770329</th>
      <td>1</td>
      <td>73</td>
      <td>121446</td>
      <td>121518</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:770576..773476</th>
      <td>1</td>
      <td>2901</td>
      <td>121765</td>
      <td>124665</td>
      <td>plus</td>
      <td>2901</td>
      <td>100.0</td>
      <td>0</td>
      <td>2901</td>
      <td>0</td>
      <td>5232.0</td>
      <td>5802</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:773623..773742</th>
      <td>1</td>
      <td>120</td>
      <td>124812</td>
      <td>124931</td>
      <td>plus</td>
      <td>120</td>
      <td>100.0</td>
      <td>0</td>
      <td>120</td>
      <td>0</td>
      <td>217.0</td>
      <td>240</td>
      <td>2.000000e-57</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885361..885433</th>
      <td>1</td>
      <td>73</td>
      <td>885362</td>
      <td>885434</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885446..885519</th>
      <td>1</td>
      <td>74</td>
      <td>885447</td>
      <td>885520</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>1</td>
      <td>73</td>
      <td>885529</td>
      <td>885601</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885447</td>
      <td>885520</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:894195..894267</th>
      <td>1</td>
      <td>73</td>
      <td>894196</td>
      <td>894268</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
  </tbody>
</table>
</div>
```
:::
:::

::: {.cell .code execution_count="516"}
``` {.python}
len(rna_plus_unique)
```

::: {.output .execute_result execution_count="516"}
    51
:::
:::

::: {.cell .code execution_count="517"}
``` {.python}
plus_unique = pandas.concat([gene_plus_unique, gene_plus_sub_unique, misc_plus_unique, rna_plus_unique], axis=0)
```
:::

::: {.cell .code execution_count="518"}
``` {.python}
plus_unique
```

::: {.output .execute_result execution_count="518"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0001</th>
      <td>1</td>
      <td>1506</td>
      <td>1</td>
      <td>1506</td>
      <td>plus</td>
      <td>1506</td>
      <td>100.0</td>
      <td>0</td>
      <td>1506</td>
      <td>0</td>
      <td>2717.0</td>
      <td>3012</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0002</th>
      <td>1</td>
      <td>1104</td>
      <td>1545</td>
      <td>2648</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0003</th>
      <td>1</td>
      <td>1104</td>
      <td>2669</td>
      <td>3772</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0004</th>
      <td>1</td>
      <td>2418</td>
      <td>3777</td>
      <td>6194</td>
      <td>plus</td>
      <td>2418</td>
      <td>100.0</td>
      <td>0</td>
      <td>2418</td>
      <td>0</td>
      <td>4361.0</td>
      <td>4836</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0006</th>
      <td>1</td>
      <td>957</td>
      <td>7284</td>
      <td>8240</td>
      <td>plus</td>
      <td>957</td>
      <td>100.0</td>
      <td>0</td>
      <td>957</td>
      <td>0</td>
      <td>1727.0</td>
      <td>1914</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885361..885433</th>
      <td>1</td>
      <td>73</td>
      <td>885362</td>
      <td>885434</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885446..885519</th>
      <td>1</td>
      <td>74</td>
      <td>885447</td>
      <td>885520</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885528..885600</th>
      <td>1</td>
      <td>73</td>
      <td>885529</td>
      <td>885601</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:885698..885771</th>
      <td>1</td>
      <td>74</td>
      <td>885447</td>
      <td>885520</td>
      <td>plus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:894195..894267</th>
      <td>1</td>
      <td>73</td>
      <td>894196</td>
      <td>894268</td>
      <td>plus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
  </tbody>
</table>
<p>13024 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Minus strand
------------
:::

::: {.cell .code execution_count="519"}
``` {.python}
algn_minus = pandas.read_csv("reference_features/minus/blastn_features_to_new_assembly_minus.csv", index_col=0)
```
:::

::: {.cell .markdown}
### Filter out pident \< 100 {#filter-out-pident--100}
:::

::: {.cell .code execution_count="520"}
``` {.python}
algn_minus100 = algn_minus[algn_minus['pident']==100.0]
```
:::

::: {.cell .code execution_count="521"}
``` {.python}
algn_minus100
```

::: {.output .execute_result execution_count="521"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0005</th>
      <td>1</td>
      <td>723</td>
      <td>7021</td>
      <td>6299</td>
      <td>minus</td>
      <td>723</td>
      <td>100.0</td>
      <td>0</td>
      <td>723</td>
      <td>0</td>
      <td>1305.0</td>
      <td>1446</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0008</th>
      <td>1</td>
      <td>771</td>
      <td>11476</td>
      <td>10706</td>
      <td>minus</td>
      <td>771</td>
      <td>100.0</td>
      <td>0</td>
      <td>771</td>
      <td>0</td>
      <td>1391.0</td>
      <td>1542</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0009</th>
      <td>1</td>
      <td>537</td>
      <td>12095</td>
      <td>11559</td>
      <td>minus</td>
      <td>537</td>
      <td>100.0</td>
      <td>0</td>
      <td>537</td>
      <td>0</td>
      <td>969.0</td>
      <td>1074</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0010</th>
      <td>1</td>
      <td>2055</td>
      <td>14152</td>
      <td>12098</td>
      <td>minus</td>
      <td>2055</td>
      <td>100.0</td>
      <td>0</td>
      <td>2055</td>
      <td>0</td>
      <td>3707.0</td>
      <td>4110</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0011</th>
      <td>1</td>
      <td>954</td>
      <td>15102</td>
      <td>14149</td>
      <td>minus</td>
      <td>954</td>
      <td>100.0</td>
      <td>0</td>
      <td>954</td>
      <td>0</td>
      <td>1721.0</td>
      <td>1908</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>5720501</td>
      <td>5720428</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>6071956</td>
      <td>6071883</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6141773..6141890</th>
      <td>1</td>
      <td>118</td>
      <td>6141750</td>
      <td>6141633</td>
      <td>minus</td>
      <td>118</td>
      <td>100.0</td>
      <td>0</td>
      <td>118</td>
      <td>0</td>
      <td>214.0</td>
      <td>236</td>
      <td>2.000000e-56</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:816719..816792</th>
      <td>1</td>
      <td>74</td>
      <td>816793</td>
      <td>816720</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:834266..834337</th>
      <td>1</td>
      <td>72</td>
      <td>834338</td>
      <td>834267</td>
      <td>minus</td>
      <td>72</td>
      <td>100.0</td>
      <td>0</td>
      <td>72</td>
      <td>0</td>
      <td>131.0</td>
      <td>144</td>
      <td>1.000000e-31</td>
    </tr>
  </tbody>
</table>
<p>97048 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Genes {#genes}
-----
:::

::: {.cell .code execution_count="522"}
``` {.python}
gene_minus = algn_minus100.loc[algn_minus100.index.str.startswith("gene")]
```
:::

::: {.cell .code execution_count="523"}
``` {.python}
gene_minus
```

::: {.output .execute_result execution_count="523"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0005</th>
      <td>1</td>
      <td>723</td>
      <td>7021</td>
      <td>6299</td>
      <td>minus</td>
      <td>723</td>
      <td>100.0</td>
      <td>0</td>
      <td>723</td>
      <td>0</td>
      <td>1305.0</td>
      <td>1446</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0008</th>
      <td>1</td>
      <td>771</td>
      <td>11476</td>
      <td>10706</td>
      <td>minus</td>
      <td>771</td>
      <td>100.0</td>
      <td>0</td>
      <td>771</td>
      <td>0</td>
      <td>1391.0</td>
      <td>1542</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0009</th>
      <td>1</td>
      <td>537</td>
      <td>12095</td>
      <td>11559</td>
      <td>minus</td>
      <td>537</td>
      <td>100.0</td>
      <td>0</td>
      <td>537</td>
      <td>0</td>
      <td>969.0</td>
      <td>1074</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0010</th>
      <td>1</td>
      <td>2055</td>
      <td>14152</td>
      <td>12098</td>
      <td>minus</td>
      <td>2055</td>
      <td>100.0</td>
      <td>0</td>
      <td>2055</td>
      <td>0</td>
      <td>3707.0</td>
      <td>4110</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0011</th>
      <td>1</td>
      <td>954</td>
      <td>15102</td>
      <td>14149</td>
      <td>minus</td>
      <td>954</td>
      <td>100.0</td>
      <td>0</td>
      <td>954</td>
      <td>0</td>
      <td>1721.0</td>
      <td>1908</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>gene-PFLU_6129</th>
      <td>1</td>
      <td>1896</td>
      <td>6713259</td>
      <td>6711364</td>
      <td>minus</td>
      <td>1896</td>
      <td>100.0</td>
      <td>0</td>
      <td>1896</td>
      <td>0</td>
      <td>3420.0</td>
      <td>3792</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6133</th>
      <td>1</td>
      <td>1371</td>
      <td>6719257</td>
      <td>6717887</td>
      <td>minus</td>
      <td>1371</td>
      <td>100.0</td>
      <td>0</td>
      <td>1371</td>
      <td>0</td>
      <td>2473.0</td>
      <td>2742</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6134</th>
      <td>1</td>
      <td>1683</td>
      <td>6721019</td>
      <td>6719337</td>
      <td>minus</td>
      <td>1683</td>
      <td>100.0</td>
      <td>0</td>
      <td>1683</td>
      <td>0</td>
      <td>3036.0</td>
      <td>3366</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6135</th>
      <td>1</td>
      <td>405</td>
      <td>6721664</td>
      <td>6721260</td>
      <td>minus</td>
      <td>405</td>
      <td>100.0</td>
      <td>0</td>
      <td>405</td>
      <td>0</td>
      <td>731.0</td>
      <td>810</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6136</th>
      <td>1</td>
      <td>135</td>
      <td>6721815</td>
      <td>6721681</td>
      <td>minus</td>
      <td>135</td>
      <td>100.0</td>
      <td>0</td>
      <td>135</td>
      <td>0</td>
      <td>244.0</td>
      <td>270</td>
      <td>2.000000e-65</td>
    </tr>
  </tbody>
</table>
<p>3336 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
### Discard duplicates. From each set of duplicates, keep the first {#discard-duplicates-from-each-set-of-duplicates-keep-the-first}
:::

::: {.cell .code execution_count="524"}
``` {.python}
gene_minus.loc[gene_minus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="524"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0027</th>
      <td>1</td>
      <td>1515</td>
      <td>31387</td>
      <td>29873</td>
      <td>minus</td>
      <td>1515</td>
      <td>100.0</td>
      <td>0</td>
      <td>1515</td>
      <td>0</td>
      <td>2733.0</td>
      <td>3030</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0027</th>
      <td>1018</td>
      <td>1041</td>
      <td>225746</td>
      <td>225723</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>4.000000e-04</td>
    </tr>
    <tr>
      <th>gene-PFLU_0074</th>
      <td>1</td>
      <td>786</td>
      <td>75195</td>
      <td>74410</td>
      <td>minus</td>
      <td>786</td>
      <td>100.0</td>
      <td>0</td>
      <td>786</td>
      <td>0</td>
      <td>1418.0</td>
      <td>1572</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0074</th>
      <td>118</td>
      <td>138</td>
      <td>6330185</td>
      <td>6330165</td>
      <td>minus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>8.000000e-03</td>
    </tr>
    <tr>
      <th>gene-PFLU_0173</th>
      <td>1</td>
      <td>843</td>
      <td>196527</td>
      <td>195685</td>
      <td>minus</td>
      <td>843</td>
      <td>100.0</td>
      <td>0</td>
      <td>843</td>
      <td>0</td>
      <td>1521.0</td>
      <td>1686</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>gene-PFLU_6009</th>
      <td>454</td>
      <td>476</td>
      <td>6602140</td>
      <td>6602118</td>
      <td>minus</td>
      <td>23</td>
      <td>100.0</td>
      <td>0</td>
      <td>23</td>
      <td>0</td>
      <td>42.8</td>
      <td>46</td>
      <td>6.000000e-04</td>
    </tr>
    <tr>
      <th>gene-PFLU_6033</th>
      <td>1</td>
      <td>1149</td>
      <td>6603013</td>
      <td>6601865</td>
      <td>minus</td>
      <td>1149</td>
      <td>100.0</td>
      <td>0</td>
      <td>1149</td>
      <td>0</td>
      <td>2073.0</td>
      <td>2298</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6033</th>
      <td>874</td>
      <td>896</td>
      <td>6565561</td>
      <td>6565539</td>
      <td>minus</td>
      <td>23</td>
      <td>100.0</td>
      <td>0</td>
      <td>23</td>
      <td>0</td>
      <td>42.8</td>
      <td>46</td>
      <td>1.000000e-03</td>
    </tr>
    <tr>
      <th>gene-PFLU_6053</th>
      <td>1</td>
      <td>246</td>
      <td>6622163</td>
      <td>6621918</td>
      <td>minus</td>
      <td>246</td>
      <td>100.0</td>
      <td>0</td>
      <td>246</td>
      <td>0</td>
      <td>444.0</td>
      <td>492</td>
      <td>2.000000e-125</td>
    </tr>
    <tr>
      <th>gene-PFLU_6053</th>
      <td>135</td>
      <td>156</td>
      <td>1187210</td>
      <td>1187189</td>
      <td>minus</td>
      <td>22</td>
      <td>100.0</td>
      <td>0</td>
      <td>22</td>
      <td>0</td>
      <td>41.0</td>
      <td>44</td>
      <td>7.000000e-04</td>
    </tr>
  </tbody>
</table>
<p>312 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
In all groups of identical ids, keep the first one. In this way, the
pflu ids keep their order.
:::

::: {.cell .code execution_count="525"}
``` {.python}
gene_minus_unique = gene_minus.loc[numpy.logical_not(gene_minus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="526"}
``` {.python}
gene_minus_unique
```

::: {.output .execute_result execution_count="526"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0005</th>
      <td>1</td>
      <td>723</td>
      <td>7021</td>
      <td>6299</td>
      <td>minus</td>
      <td>723</td>
      <td>100.0</td>
      <td>0</td>
      <td>723</td>
      <td>0</td>
      <td>1305.0</td>
      <td>1446</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0008</th>
      <td>1</td>
      <td>771</td>
      <td>11476</td>
      <td>10706</td>
      <td>minus</td>
      <td>771</td>
      <td>100.0</td>
      <td>0</td>
      <td>771</td>
      <td>0</td>
      <td>1391.0</td>
      <td>1542</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0009</th>
      <td>1</td>
      <td>537</td>
      <td>12095</td>
      <td>11559</td>
      <td>minus</td>
      <td>537</td>
      <td>100.0</td>
      <td>0</td>
      <td>537</td>
      <td>0</td>
      <td>969.0</td>
      <td>1074</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0010</th>
      <td>1</td>
      <td>2055</td>
      <td>14152</td>
      <td>12098</td>
      <td>minus</td>
      <td>2055</td>
      <td>100.0</td>
      <td>0</td>
      <td>2055</td>
      <td>0</td>
      <td>3707.0</td>
      <td>4110</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0011</th>
      <td>1</td>
      <td>954</td>
      <td>15102</td>
      <td>14149</td>
      <td>minus</td>
      <td>954</td>
      <td>100.0</td>
      <td>0</td>
      <td>954</td>
      <td>0</td>
      <td>1721.0</td>
      <td>1908</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>gene-PFLU_6129</th>
      <td>1</td>
      <td>1896</td>
      <td>6713259</td>
      <td>6711364</td>
      <td>minus</td>
      <td>1896</td>
      <td>100.0</td>
      <td>0</td>
      <td>1896</td>
      <td>0</td>
      <td>3420.0</td>
      <td>3792</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6133</th>
      <td>1</td>
      <td>1371</td>
      <td>6719257</td>
      <td>6717887</td>
      <td>minus</td>
      <td>1371</td>
      <td>100.0</td>
      <td>0</td>
      <td>1371</td>
      <td>0</td>
      <td>2473.0</td>
      <td>2742</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6134</th>
      <td>1</td>
      <td>1683</td>
      <td>6721019</td>
      <td>6719337</td>
      <td>minus</td>
      <td>1683</td>
      <td>100.0</td>
      <td>0</td>
      <td>1683</td>
      <td>0</td>
      <td>3036.0</td>
      <td>3366</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6135</th>
      <td>1</td>
      <td>405</td>
      <td>6721664</td>
      <td>6721260</td>
      <td>minus</td>
      <td>405</td>
      <td>100.0</td>
      <td>0</td>
      <td>405</td>
      <td>0</td>
      <td>731.0</td>
      <td>810</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_6136</th>
      <td>1</td>
      <td>135</td>
      <td>6721815</td>
      <td>6721681</td>
      <td>minus</td>
      <td>135</td>
      <td>100.0</td>
      <td>0</td>
      <td>135</td>
      <td>0</td>
      <td>244.0</td>
      <td>270</td>
      <td>2.000000e-65</td>
    </tr>
  </tbody>
</table>
<p>3116 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Gene subfeatures {#gene-subfeatures}
----------------
:::

::: {.cell .code execution_count="527"}
``` {.python}
gene_minus_sub  = algn_minus100.loc[algn_minus100.index.str.startswith("id-PFLU")]
```
:::

::: {.cell .code execution_count="528"}
``` {.python}
gene_minus_sub
```

::: {.output .execute_result execution_count="528"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-PFLU_0005-2</th>
      <td>1</td>
      <td>45</td>
      <td>6517</td>
      <td>6473</td>
      <td>minus</td>
      <td>45</td>
      <td>100.0</td>
      <td>0</td>
      <td>45</td>
      <td>0</td>
      <td>82.4</td>
      <td>90</td>
      <td>2.000000e-17</td>
    </tr>
    <tr>
      <th>id-PFLU_0005-3</th>
      <td>1</td>
      <td>360</td>
      <td>7021</td>
      <td>6662</td>
      <td>minus</td>
      <td>360</td>
      <td>100.0</td>
      <td>0</td>
      <td>360</td>
      <td>0</td>
      <td>650.0</td>
      <td>720</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_0005</th>
      <td>1</td>
      <td>231</td>
      <td>6559</td>
      <td>6329</td>
      <td>minus</td>
      <td>231</td>
      <td>100.0</td>
      <td>0</td>
      <td>231</td>
      <td>0</td>
      <td>417.0</td>
      <td>462</td>
      <td>2.000000e-117</td>
    </tr>
    <tr>
      <th>id-PFLU_0008-2</th>
      <td>1</td>
      <td>69</td>
      <td>11449</td>
      <td>11381</td>
      <td>minus</td>
      <td>69</td>
      <td>100.0</td>
      <td>0</td>
      <td>69</td>
      <td>0</td>
      <td>125.0</td>
      <td>138</td>
      <td>5.000000e-30</td>
    </tr>
    <tr>
      <th>id-PFLU_0008</th>
      <td>1</td>
      <td>381</td>
      <td>11293</td>
      <td>10913</td>
      <td>minus</td>
      <td>381</td>
      <td>100.0</td>
      <td>0</td>
      <td>381</td>
      <td>0</td>
      <td>688.0</td>
      <td>762</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_6134-2</th>
      <td>1</td>
      <td>69</td>
      <td>6719909</td>
      <td>6719841</td>
      <td>minus</td>
      <td>69</td>
      <td>100.0</td>
      <td>0</td>
      <td>69</td>
      <td>0</td>
      <td>125.0</td>
      <td>138</td>
      <td>5.000000e-30</td>
    </tr>
    <tr>
      <th>id-PFLU_6134</th>
      <td>1</td>
      <td>552</td>
      <td>6719915</td>
      <td>6719364</td>
      <td>minus</td>
      <td>552</td>
      <td>100.0</td>
      <td>0</td>
      <td>552</td>
      <td>0</td>
      <td>996.0</td>
      <td>1104</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_6135</th>
      <td>1</td>
      <td>327</td>
      <td>6721652</td>
      <td>6721326</td>
      <td>minus</td>
      <td>327</td>
      <td>100.0</td>
      <td>0</td>
      <td>327</td>
      <td>0</td>
      <td>590.0</td>
      <td>654</td>
      <td>3.000000e-169</td>
    </tr>
    <tr>
      <th>id-PFLU_6136-2</th>
      <td>1</td>
      <td>60</td>
      <td>6721812</td>
      <td>6721753</td>
      <td>minus</td>
      <td>60</td>
      <td>100.0</td>
      <td>0</td>
      <td>60</td>
      <td>0</td>
      <td>109.0</td>
      <td>120</td>
      <td>3.000000e-25</td>
    </tr>
    <tr>
      <th>id-PFLU_6136</th>
      <td>1</td>
      <td>132</td>
      <td>6721815</td>
      <td>6721684</td>
      <td>minus</td>
      <td>132</td>
      <td>100.0</td>
      <td>0</td>
      <td>132</td>
      <td>0</td>
      <td>239.0</td>
      <td>264</td>
      <td>7.000000e-64</td>
    </tr>
  </tbody>
</table>
<p>9190 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="529"}
``` {.python}
gene_minus_sub.loc[gene_minus_sub.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="529"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-PFLU_0027</th>
      <td>1</td>
      <td>1245</td>
      <td>31384</td>
      <td>30140</td>
      <td>minus</td>
      <td>1245</td>
      <td>100.0</td>
      <td>0</td>
      <td>1245</td>
      <td>0</td>
      <td>2246.0</td>
      <td>2490</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>id-PFLU_0027</th>
      <td>1015</td>
      <td>1038</td>
      <td>225746</td>
      <td>225723</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>0.000300</td>
    </tr>
    <tr>
      <th>id-PFLU_0074-3</th>
      <td>1</td>
      <td>24</td>
      <td>75084</td>
      <td>75061</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>0.000001</td>
    </tr>
    <tr>
      <th>id-PFLU_0074-3</th>
      <td>7</td>
      <td>24</td>
      <td>6330185</td>
      <td>6330168</td>
      <td>minus</td>
      <td>18</td>
      <td>100.0</td>
      <td>0</td>
      <td>18</td>
      <td>0</td>
      <td>33.7</td>
      <td>36</td>
      <td>0.002000</td>
    </tr>
    <tr>
      <th>id-PFLU_0074-3</th>
      <td>5</td>
      <td>21</td>
      <td>1479738</td>
      <td>1479722</td>
      <td>minus</td>
      <td>17</td>
      <td>100.0</td>
      <td>0</td>
      <td>17</td>
      <td>0</td>
      <td>31.9</td>
      <td>34</td>
      <td>0.008000</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_6053</th>
      <td>39</td>
      <td>60</td>
      <td>1187210</td>
      <td>1187189</td>
      <td>minus</td>
      <td>22</td>
      <td>100.0</td>
      <td>0</td>
      <td>22</td>
      <td>0</td>
      <td>41.0</td>
      <td>44</td>
      <td>0.000400</td>
    </tr>
    <tr>
      <th>id-PFLU_6097</th>
      <td>1</td>
      <td>837</td>
      <td>6674474</td>
      <td>6673638</td>
      <td>minus</td>
      <td>837</td>
      <td>100.0</td>
      <td>0</td>
      <td>837</td>
      <td>0</td>
      <td>1510.0</td>
      <td>1674</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>id-PFLU_6097</th>
      <td>766</td>
      <td>786</td>
      <td>6640693</td>
      <td>6640673</td>
      <td>minus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>0.009000</td>
    </tr>
    <tr>
      <th>id-PFLU_6114-2</th>
      <td>1</td>
      <td>402</td>
      <td>6698412</td>
      <td>6698011</td>
      <td>minus</td>
      <td>402</td>
      <td>100.0</td>
      <td>0</td>
      <td>402</td>
      <td>0</td>
      <td>726.0</td>
      <td>804</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>id-PFLU_6114-2</th>
      <td>89</td>
      <td>109</td>
      <td>2508082</td>
      <td>2508062</td>
      <td>minus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>0.004000</td>
    </tr>
  </tbody>
</table>
<p>961 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="530"}
``` {.python}
gene_minus_sub_unique = gene_minus_sub.loc[numpy.logical_not(gene_minus_sub.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="531"}
``` {.python}
gene_minus_sub_unique
```

::: {.output .execute_result execution_count="531"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-PFLU_0005-2</th>
      <td>1</td>
      <td>45</td>
      <td>6517</td>
      <td>6473</td>
      <td>minus</td>
      <td>45</td>
      <td>100.0</td>
      <td>0</td>
      <td>45</td>
      <td>0</td>
      <td>82.4</td>
      <td>90</td>
      <td>2.000000e-17</td>
    </tr>
    <tr>
      <th>id-PFLU_0005-3</th>
      <td>1</td>
      <td>360</td>
      <td>7021</td>
      <td>6662</td>
      <td>minus</td>
      <td>360</td>
      <td>100.0</td>
      <td>0</td>
      <td>360</td>
      <td>0</td>
      <td>650.0</td>
      <td>720</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_0005</th>
      <td>1</td>
      <td>231</td>
      <td>6559</td>
      <td>6329</td>
      <td>minus</td>
      <td>231</td>
      <td>100.0</td>
      <td>0</td>
      <td>231</td>
      <td>0</td>
      <td>417.0</td>
      <td>462</td>
      <td>2.000000e-117</td>
    </tr>
    <tr>
      <th>id-PFLU_0008-2</th>
      <td>1</td>
      <td>69</td>
      <td>11449</td>
      <td>11381</td>
      <td>minus</td>
      <td>69</td>
      <td>100.0</td>
      <td>0</td>
      <td>69</td>
      <td>0</td>
      <td>125.0</td>
      <td>138</td>
      <td>5.000000e-30</td>
    </tr>
    <tr>
      <th>id-PFLU_0008</th>
      <td>1</td>
      <td>381</td>
      <td>11293</td>
      <td>10913</td>
      <td>minus</td>
      <td>381</td>
      <td>100.0</td>
      <td>0</td>
      <td>381</td>
      <td>0</td>
      <td>688.0</td>
      <td>762</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_6134-2</th>
      <td>1</td>
      <td>69</td>
      <td>6719909</td>
      <td>6719841</td>
      <td>minus</td>
      <td>69</td>
      <td>100.0</td>
      <td>0</td>
      <td>69</td>
      <td>0</td>
      <td>125.0</td>
      <td>138</td>
      <td>5.000000e-30</td>
    </tr>
    <tr>
      <th>id-PFLU_6134</th>
      <td>1</td>
      <td>552</td>
      <td>6719915</td>
      <td>6719364</td>
      <td>minus</td>
      <td>552</td>
      <td>100.0</td>
      <td>0</td>
      <td>552</td>
      <td>0</td>
      <td>996.0</td>
      <td>1104</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>id-PFLU_6135</th>
      <td>1</td>
      <td>327</td>
      <td>6721652</td>
      <td>6721326</td>
      <td>minus</td>
      <td>327</td>
      <td>100.0</td>
      <td>0</td>
      <td>327</td>
      <td>0</td>
      <td>590.0</td>
      <td>654</td>
      <td>3.000000e-169</td>
    </tr>
    <tr>
      <th>id-PFLU_6136-2</th>
      <td>1</td>
      <td>60</td>
      <td>6721812</td>
      <td>6721753</td>
      <td>minus</td>
      <td>60</td>
      <td>100.0</td>
      <td>0</td>
      <td>60</td>
      <td>0</td>
      <td>109.0</td>
      <td>120</td>
      <td>3.000000e-25</td>
    </tr>
    <tr>
      <th>id-PFLU_6136</th>
      <td>1</td>
      <td>132</td>
      <td>6721815</td>
      <td>6721684</td>
      <td>minus</td>
      <td>132</td>
      <td>100.0</td>
      <td>0</td>
      <td>132</td>
      <td>0</td>
      <td>239.0</td>
      <td>264</td>
      <td>7.000000e-64</td>
    </tr>
  </tbody>
</table>
<p>8557 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code}
``` {.python}
```
:::

::: {.cell .markdown}
Other subfeatures {#other-subfeatures}
-----------------
:::

::: {.cell .code execution_count="532"}
``` {.python}
misc_minus  = algn_minus100.loc[algn_minus100.index.str.startswith("id-AM")]
```
:::

::: {.cell .code execution_count="533"}
``` {.python}
misc_minus
```

::: {.output .execute_result execution_count="533"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1001373..1001465-2</th>
      <td>1</td>
      <td>93</td>
      <td>1001675</td>
      <td>1001583</td>
      <td>minus</td>
      <td>93</td>
      <td>100.0</td>
      <td>0</td>
      <td>93</td>
      <td>0</td>
      <td>168.0</td>
      <td>186</td>
      <td>6.000000e-43</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465-2</th>
      <td>30</td>
      <td>50</td>
      <td>4331266</td>
      <td>4331246</td>
      <td>minus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>8.000000e-04</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465-2</th>
      <td>1</td>
      <td>20</td>
      <td>2465072</td>
      <td>2465053</td>
      <td>minus</td>
      <td>20</td>
      <td>100.0</td>
      <td>0</td>
      <td>20</td>
      <td>0</td>
      <td>37.4</td>
      <td>40</td>
      <td>3.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1002520..1002550</th>
      <td>1</td>
      <td>31</td>
      <td>1002760</td>
      <td>1002730</td>
      <td>minus</td>
      <td>31</td>
      <td>100.0</td>
      <td>0</td>
      <td>31</td>
      <td>0</td>
      <td>57.2</td>
      <td>62</td>
      <td>4.000000e-10</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1004987..1005010</th>
      <td>1</td>
      <td>24</td>
      <td>207911</td>
      <td>207888</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>75</td>
      <td>93</td>
      <td>5249822</td>
      <td>5249804</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>7</td>
      <td>25</td>
      <td>6366043</td>
      <td>6366025</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>6</td>
      <td>24</td>
      <td>6414065</td>
      <td>6414047</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>6</td>
      <td>24</td>
      <td>6516708</td>
      <td>6516690</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:985357..985379</th>
      <td>1</td>
      <td>23</td>
      <td>985589</td>
      <td>985567</td>
      <td>minus</td>
      <td>23</td>
      <td>100.0</td>
      <td>0</td>
      <td>23</td>
      <td>0</td>
      <td>42.8</td>
      <td>46</td>
      <td>5.000000e-06</td>
    </tr>
  </tbody>
</table>
<p>84437 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="534"}
``` {.python}
misc_minus.loc[misc_minus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="534"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1001373..1001465-2</th>
      <td>1</td>
      <td>93</td>
      <td>1001675</td>
      <td>1001583</td>
      <td>minus</td>
      <td>93</td>
      <td>100.0</td>
      <td>0</td>
      <td>93</td>
      <td>0</td>
      <td>168.0</td>
      <td>186</td>
      <td>6.000000e-43</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465-2</th>
      <td>30</td>
      <td>50</td>
      <td>4331266</td>
      <td>4331246</td>
      <td>minus</td>
      <td>21</td>
      <td>100.0</td>
      <td>0</td>
      <td>21</td>
      <td>0</td>
      <td>39.2</td>
      <td>42</td>
      <td>8.000000e-04</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1001373..1001465-2</th>
      <td>1</td>
      <td>20</td>
      <td>2465072</td>
      <td>2465053</td>
      <td>minus</td>
      <td>20</td>
      <td>100.0</td>
      <td>0</td>
      <td>20</td>
      <td>0</td>
      <td>37.4</td>
      <td>40</td>
      <td>3.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1004987..1005010</th>
      <td>1</td>
      <td>24</td>
      <td>207911</td>
      <td>207888</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1004987..1005010</th>
      <td>1</td>
      <td>24</td>
      <td>626253</td>
      <td>626230</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>6</td>
      <td>24</td>
      <td>4794023</td>
      <td>4794005</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>75</td>
      <td>93</td>
      <td>5249822</td>
      <td>5249804</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>7</td>
      <td>25</td>
      <td>6366043</td>
      <td>6366025</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>6</td>
      <td>24</td>
      <td>6414065</td>
      <td>6414047</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>6</td>
      <td>24</td>
      <td>6516708</td>
      <td>6516690</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>9.000000e-03</td>
    </tr>
  </tbody>
</table>
<p>84163 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="535"}
``` {.python}
misc_minus_unique = misc_minus.loc[numpy.logical_not(misc_minus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="536"}
``` {.python}
misc_minus_unique
```

::: {.output .execute_result execution_count="536"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1001373..1001465-2</th>
      <td>1</td>
      <td>93</td>
      <td>1001675</td>
      <td>1001583</td>
      <td>minus</td>
      <td>93</td>
      <td>100.0</td>
      <td>0</td>
      <td>93</td>
      <td>0</td>
      <td>168.0</td>
      <td>186</td>
      <td>6.000000e-43</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1002520..1002550</th>
      <td>1</td>
      <td>31</td>
      <td>1002760</td>
      <td>1002730</td>
      <td>minus</td>
      <td>31</td>
      <td>100.0</td>
      <td>0</td>
      <td>31</td>
      <td>0</td>
      <td>57.2</td>
      <td>62</td>
      <td>4.000000e-10</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1004987..1005010</th>
      <td>1</td>
      <td>24</td>
      <td>207911</td>
      <td>207888</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1008686..1008710</th>
      <td>1</td>
      <td>25</td>
      <td>1008920</td>
      <td>1008896</td>
      <td>minus</td>
      <td>25</td>
      <td>100.0</td>
      <td>0</td>
      <td>25</td>
      <td>0</td>
      <td>46.4</td>
      <td>50</td>
      <td>4.000000e-07</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1013127..1013153</th>
      <td>1</td>
      <td>27</td>
      <td>67795</td>
      <td>67769</td>
      <td>minus</td>
      <td>27</td>
      <td>100.0</td>
      <td>0</td>
      <td>27</td>
      <td>0</td>
      <td>50.0</td>
      <td>54</td>
      <td>5.000000e-08</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-AM181176.4:95718..95744</th>
      <td>1</td>
      <td>27</td>
      <td>95745</td>
      <td>95719</td>
      <td>minus</td>
      <td>27</td>
      <td>100.0</td>
      <td>0</td>
      <td>27</td>
      <td>0</td>
      <td>50.0</td>
      <td>54</td>
      <td>5.000000e-08</td>
    </tr>
    <tr>
      <th>id-AM181176.4:966641..966666</th>
      <td>1</td>
      <td>26</td>
      <td>966875</td>
      <td>966850</td>
      <td>minus</td>
      <td>26</td>
      <td>100.0</td>
      <td>0</td>
      <td>26</td>
      <td>0</td>
      <td>48.2</td>
      <td>52</td>
      <td>1.000000e-07</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968573..968596</th>
      <td>1</td>
      <td>24</td>
      <td>968805</td>
      <td>968782</td>
      <td>minus</td>
      <td>24</td>
      <td>100.0</td>
      <td>0</td>
      <td>24</td>
      <td>0</td>
      <td>44.6</td>
      <td>48</td>
      <td>1.000000e-06</td>
    </tr>
    <tr>
      <th>id-AM181176.4:968574..968666</th>
      <td>1</td>
      <td>93</td>
      <td>968875</td>
      <td>968783</td>
      <td>minus</td>
      <td>93</td>
      <td>100.0</td>
      <td>0</td>
      <td>93</td>
      <td>0</td>
      <td>168.0</td>
      <td>186</td>
      <td>6.000000e-43</td>
    </tr>
    <tr>
      <th>id-AM181176.4:985357..985379</th>
      <td>1</td>
      <td>23</td>
      <td>985589</td>
      <td>985567</td>
      <td>minus</td>
      <td>23</td>
      <td>100.0</td>
      <td>0</td>
      <td>23</td>
      <td>0</td>
      <td>42.8</td>
      <td>46</td>
      <td>5.000000e-06</td>
    </tr>
  </tbody>
</table>
<p>1565 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code}
``` {.python}
```
:::

::: {.cell .markdown}
rna {#rna}
---
:::

::: {.cell .code execution_count="537"}
``` {.python}
rna_minus  = algn_minus100.loc[algn_minus100.index.str.startswith("rna-")]
```
:::

::: {.cell .code execution_count="538"}
``` {.python}
rna_minus
```

::: {.output .execute_result execution_count="538"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>rna-AM181176.4:1019665..1019806</th>
      <td>1</td>
      <td>142</td>
      <td>1020016</td>
      <td>1019875</td>
      <td>minus</td>
      <td>142</td>
      <td>100.0</td>
      <td>0</td>
      <td>142</td>
      <td>0</td>
      <td>257.0</td>
      <td>284</td>
      <td>3.000000e-69</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:1576792..1576862</th>
      <td>1</td>
      <td>71</td>
      <td>1577072</td>
      <td>1577002</td>
      <td>minus</td>
      <td>71</td>
      <td>100.0</td>
      <td>0</td>
      <td>71</td>
      <td>0</td>
      <td>129.0</td>
      <td>142</td>
      <td>4.000000e-31</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:203174..203247</th>
      <td>1</td>
      <td>74</td>
      <td>203248</td>
      <td>203175</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2943100..2943306</th>
      <td>1</td>
      <td>207</td>
      <td>2943516</td>
      <td>2943310</td>
      <td>minus</td>
      <td>207</td>
      <td>100.0</td>
      <td>0</td>
      <td>207</td>
      <td>0</td>
      <td>374.0</td>
      <td>414</td>
      <td>2.000000e-104</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:325857..325929</th>
      <td>1</td>
      <td>73</td>
      <td>325930</td>
      <td>325858</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>5720501</td>
      <td>5720428</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>6071956</td>
      <td>6071883</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6141773..6141890</th>
      <td>1</td>
      <td>118</td>
      <td>6141750</td>
      <td>6141633</td>
      <td>minus</td>
      <td>118</td>
      <td>100.0</td>
      <td>0</td>
      <td>118</td>
      <td>0</td>
      <td>214.0</td>
      <td>236</td>
      <td>2.000000e-56</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:816719..816792</th>
      <td>1</td>
      <td>74</td>
      <td>816793</td>
      <td>816720</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:834266..834337</th>
      <td>1</td>
      <td>72</td>
      <td>834338</td>
      <td>834267</td>
      <td>minus</td>
      <td>72</td>
      <td>100.0</td>
      <td>0</td>
      <td>72</td>
      <td>0</td>
      <td>131.0</td>
      <td>144</td>
      <td>1.000000e-31</td>
    </tr>
  </tbody>
</table>
<p>85 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="539"}
``` {.python}
rna_minus.loc[rna_minus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="539"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>rna-AM181176.4:325857..325929</th>
      <td>1</td>
      <td>73</td>
      <td>325930</td>
      <td>325858</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:325857..325929</th>
      <td>8</td>
      <td>26</td>
      <td>4338650</td>
      <td>4338632</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:325857..325929</th>
      <td>8</td>
      <td>26</td>
      <td>4338945</td>
      <td>4338927</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:325857..325929</th>
      <td>8</td>
      <td>26</td>
      <td>6065683</td>
      <td>6065665</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:3708738..3708808</th>
      <td>1</td>
      <td>71</td>
      <td>3708668</td>
      <td>3708598</td>
      <td>minus</td>
      <td>71</td>
      <td>100.0</td>
      <td>0</td>
      <td>71</td>
      <td>0</td>
      <td>129.0</td>
      <td>142</td>
      <td>4.000000e-31</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6065758..6065830</th>
      <td>8</td>
      <td>26</td>
      <td>5385233</td>
      <td>5385215</td>
      <td>minus</td>
      <td>19</td>
      <td>100.0</td>
      <td>0</td>
      <td>19</td>
      <td>0</td>
      <td>35.6</td>
      <td>38</td>
      <td>7.000000e-03</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6071917..6071989</th>
      <td>1</td>
      <td>73</td>
      <td>5720394</td>
      <td>5720322</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6071917..6071989</th>
      <td>1</td>
      <td>73</td>
      <td>6071849</td>
      <td>6071777</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>5720501</td>
      <td>5720428</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>6071956</td>
      <td>6071883</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
  </tbody>
</table>
<p>62 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="540"}
``` {.python}
rna_minus_unique = rna_minus.loc[numpy.logical_not(rna_minus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="541"}
``` {.python}
rna_minus_unique
```

::: {.output .execute_result execution_count="541"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>rna-AM181176.4:1019665..1019806</th>
      <td>1</td>
      <td>142</td>
      <td>1020016</td>
      <td>1019875</td>
      <td>minus</td>
      <td>142</td>
      <td>100.0</td>
      <td>0</td>
      <td>142</td>
      <td>0</td>
      <td>257.0</td>
      <td>284</td>
      <td>3.000000e-69</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:1576792..1576862</th>
      <td>1</td>
      <td>71</td>
      <td>1577072</td>
      <td>1577002</td>
      <td>minus</td>
      <td>71</td>
      <td>100.0</td>
      <td>0</td>
      <td>71</td>
      <td>0</td>
      <td>129.0</td>
      <td>142</td>
      <td>4.000000e-31</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:203174..203247</th>
      <td>1</td>
      <td>74</td>
      <td>203248</td>
      <td>203175</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:2943100..2943306</th>
      <td>1</td>
      <td>207</td>
      <td>2943516</td>
      <td>2943310</td>
      <td>minus</td>
      <td>207</td>
      <td>100.0</td>
      <td>0</td>
      <td>207</td>
      <td>0</td>
      <td>374.0</td>
      <td>414</td>
      <td>2.000000e-104</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:325857..325929</th>
      <td>1</td>
      <td>73</td>
      <td>325930</td>
      <td>325858</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:354633..354705</th>
      <td>1</td>
      <td>73</td>
      <td>354706</td>
      <td>354634</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:3708738..3708808</th>
      <td>1</td>
      <td>71</td>
      <td>3708668</td>
      <td>3708598</td>
      <td>minus</td>
      <td>71</td>
      <td>100.0</td>
      <td>0</td>
      <td>71</td>
      <td>0</td>
      <td>129.0</td>
      <td>142</td>
      <td>4.000000e-31</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4163618..4163705</th>
      <td>1</td>
      <td>88</td>
      <td>4163565</td>
      <td>4163478</td>
      <td>minus</td>
      <td>88</td>
      <td>100.0</td>
      <td>0</td>
      <td>88</td>
      <td>0</td>
      <td>159.0</td>
      <td>176</td>
      <td>3.000000e-40</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4338725..4338797</th>
      <td>1</td>
      <td>73</td>
      <td>4338657</td>
      <td>4338585</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4338878..4338959</th>
      <td>1</td>
      <td>82</td>
      <td>4338819</td>
      <td>4338738</td>
      <td>minus</td>
      <td>82</td>
      <td>100.0</td>
      <td>0</td>
      <td>82</td>
      <td>0</td>
      <td>149.0</td>
      <td>164</td>
      <td>5.000000e-37</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4339020..4339092</th>
      <td>1</td>
      <td>73</td>
      <td>4338657</td>
      <td>4338585</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4339128..4339201</th>
      <td>1</td>
      <td>74</td>
      <td>4339061</td>
      <td>4338988</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4584206..4584279</th>
      <td>1</td>
      <td>74</td>
      <td>4584139</td>
      <td>4584066</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4615247..4615320</th>
      <td>1</td>
      <td>74</td>
      <td>4615180</td>
      <td>4615107</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4616965..4617038</th>
      <td>1</td>
      <td>74</td>
      <td>4616898</td>
      <td>4616825</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4617147..4617220</th>
      <td>1</td>
      <td>74</td>
      <td>4616898</td>
      <td>4616825</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4617279..4617351</th>
      <td>1</td>
      <td>73</td>
      <td>4617211</td>
      <td>4617139</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4648707..4648779</th>
      <td>1</td>
      <td>73</td>
      <td>4648639</td>
      <td>4648567</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4648860..4648932</th>
      <td>1</td>
      <td>73</td>
      <td>4648792</td>
      <td>4648720</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4648986..4649058</th>
      <td>1</td>
      <td>73</td>
      <td>4648639</td>
      <td>4648567</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4649140..4649212</th>
      <td>1</td>
      <td>73</td>
      <td>4648792</td>
      <td>4648720</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4745921..4745993</th>
      <td>1</td>
      <td>73</td>
      <td>4745853</td>
      <td>4745781</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:4959120..4959373</th>
      <td>1</td>
      <td>254</td>
      <td>4959233</td>
      <td>4958980</td>
      <td>minus</td>
      <td>254</td>
      <td>100.0</td>
      <td>0</td>
      <td>254</td>
      <td>0</td>
      <td>459.0</td>
      <td>508</td>
      <td>8.000000e-130</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5051832..5051931</th>
      <td>1</td>
      <td>100</td>
      <td>5051791</td>
      <td>5051692</td>
      <td>minus</td>
      <td>100</td>
      <td>100.0</td>
      <td>0</td>
      <td>100</td>
      <td>0</td>
      <td>181.0</td>
      <td>200</td>
      <td>1.000000e-46</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5134937..5135023</th>
      <td>1</td>
      <td>87</td>
      <td>5134883</td>
      <td>5134797</td>
      <td>minus</td>
      <td>87</td>
      <td>100.0</td>
      <td>0</td>
      <td>87</td>
      <td>0</td>
      <td>158.0</td>
      <td>174</td>
      <td>1.000000e-39</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5216596..5216669</th>
      <td>1</td>
      <td>74</td>
      <td>5216529</td>
      <td>5216456</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5220126..5220199</th>
      <td>1</td>
      <td>74</td>
      <td>5220059</td>
      <td>5219986</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5220275..5220348</th>
      <td>1</td>
      <td>74</td>
      <td>5220059</td>
      <td>5219986</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5220460..5220547</th>
      <td>1</td>
      <td>88</td>
      <td>5220407</td>
      <td>5220320</td>
      <td>minus</td>
      <td>88</td>
      <td>100.0</td>
      <td>0</td>
      <td>88</td>
      <td>0</td>
      <td>159.0</td>
      <td>176</td>
      <td>3.000000e-40</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5385308..5385380</th>
      <td>1</td>
      <td>73</td>
      <td>5385240</td>
      <td>5385168</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5720462..5720534</th>
      <td>1</td>
      <td>73</td>
      <td>5720394</td>
      <td>5720322</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5720568..5720641</th>
      <td>1</td>
      <td>74</td>
      <td>5720501</td>
      <td>5720428</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5770280..5770353</th>
      <td>1</td>
      <td>74</td>
      <td>5770213</td>
      <td>5770140</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5770453..5770535</th>
      <td>1</td>
      <td>83</td>
      <td>5770395</td>
      <td>5770313</td>
      <td>minus</td>
      <td>83</td>
      <td>100.0</td>
      <td>0</td>
      <td>83</td>
      <td>0</td>
      <td>150.0</td>
      <td>166</td>
      <td>1.000000e-37</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:601394..601477</th>
      <td>1</td>
      <td>84</td>
      <td>601478</td>
      <td>601395</td>
      <td>minus</td>
      <td>84</td>
      <td>100.0</td>
      <td>0</td>
      <td>84</td>
      <td>0</td>
      <td>152.0</td>
      <td>168</td>
      <td>4.000000e-38</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:601630..601713</th>
      <td>1</td>
      <td>84</td>
      <td>601478</td>
      <td>601395</td>
      <td>minus</td>
      <td>84</td>
      <td>100.0</td>
      <td>0</td>
      <td>84</td>
      <td>0</td>
      <td>152.0</td>
      <td>168</td>
      <td>4.000000e-38</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6065521..6065593</th>
      <td>1</td>
      <td>73</td>
      <td>6065453</td>
      <td>6065381</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6065758..6065830</th>
      <td>1</td>
      <td>73</td>
      <td>6065690</td>
      <td>6065618</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6065860..6065930</th>
      <td>1</td>
      <td>71</td>
      <td>6065790</td>
      <td>6065720</td>
      <td>minus</td>
      <td>71</td>
      <td>100.0</td>
      <td>0</td>
      <td>71</td>
      <td>0</td>
      <td>129.0</td>
      <td>142</td>
      <td>4.000000e-31</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6065957..6066038</th>
      <td>1</td>
      <td>82</td>
      <td>6065898</td>
      <td>6065817</td>
      <td>minus</td>
      <td>82</td>
      <td>100.0</td>
      <td>0</td>
      <td>82</td>
      <td>0</td>
      <td>149.0</td>
      <td>164</td>
      <td>5.000000e-37</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6071917..6071989</th>
      <td>1</td>
      <td>73</td>
      <td>5720394</td>
      <td>5720322</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>5720501</td>
      <td>5720428</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6141773..6141890</th>
      <td>1</td>
      <td>118</td>
      <td>6141750</td>
      <td>6141633</td>
      <td>minus</td>
      <td>118</td>
      <td>100.0</td>
      <td>0</td>
      <td>118</td>
      <td>0</td>
      <td>214.0</td>
      <td>236</td>
      <td>2.000000e-56</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:816719..816792</th>
      <td>1</td>
      <td>74</td>
      <td>816793</td>
      <td>816720</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:834266..834337</th>
      <td>1</td>
      <td>72</td>
      <td>834338</td>
      <td>834267</td>
      <td>minus</td>
      <td>72</td>
      <td>100.0</td>
      <td>0</td>
      <td>72</td>
      <td>0</td>
      <td>131.0</td>
      <td>144</td>
      <td>1.000000e-31</td>
    </tr>
  </tbody>
</table>
</div>
```
:::
:::

::: {.cell .code execution_count="542"}
``` {.python}
len(rna_minus_unique)
```

::: {.output .execute_result execution_count="542"}
    45
:::
:::

::: {.cell .code execution_count="543"}
``` {.python}
minus_unique = pandas.concat([gene_minus_unique, gene_minus_sub_unique, misc_minus_unique, rna_minus_unique], axis=0)
```
:::

::: {.cell .code execution_count="544"}
``` {.python}
minus_unique
```

::: {.output .execute_result execution_count="544"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0005</th>
      <td>1</td>
      <td>723</td>
      <td>7021</td>
      <td>6299</td>
      <td>minus</td>
      <td>723</td>
      <td>100.0</td>
      <td>0</td>
      <td>723</td>
      <td>0</td>
      <td>1305.0</td>
      <td>1446</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0008</th>
      <td>1</td>
      <td>771</td>
      <td>11476</td>
      <td>10706</td>
      <td>minus</td>
      <td>771</td>
      <td>100.0</td>
      <td>0</td>
      <td>771</td>
      <td>0</td>
      <td>1391.0</td>
      <td>1542</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0009</th>
      <td>1</td>
      <td>537</td>
      <td>12095</td>
      <td>11559</td>
      <td>minus</td>
      <td>537</td>
      <td>100.0</td>
      <td>0</td>
      <td>537</td>
      <td>0</td>
      <td>969.0</td>
      <td>1074</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0010</th>
      <td>1</td>
      <td>2055</td>
      <td>14152</td>
      <td>12098</td>
      <td>minus</td>
      <td>2055</td>
      <td>100.0</td>
      <td>0</td>
      <td>2055</td>
      <td>0</td>
      <td>3707.0</td>
      <td>4110</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0011</th>
      <td>1</td>
      <td>954</td>
      <td>15102</td>
      <td>14149</td>
      <td>minus</td>
      <td>954</td>
      <td>100.0</td>
      <td>0</td>
      <td>954</td>
      <td>0</td>
      <td>1721.0</td>
      <td>1908</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6071917..6071989</th>
      <td>1</td>
      <td>73</td>
      <td>5720394</td>
      <td>5720322</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>5720501</td>
      <td>5720428</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6141773..6141890</th>
      <td>1</td>
      <td>118</td>
      <td>6141750</td>
      <td>6141633</td>
      <td>minus</td>
      <td>118</td>
      <td>100.0</td>
      <td>0</td>
      <td>118</td>
      <td>0</td>
      <td>214.0</td>
      <td>236</td>
      <td>2.000000e-56</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:816719..816792</th>
      <td>1</td>
      <td>74</td>
      <td>816793</td>
      <td>816720</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:834266..834337</th>
      <td>1</td>
      <td>72</td>
      <td>834338</td>
      <td>834267</td>
      <td>minus</td>
      <td>72</td>
      <td>100.0</td>
      <td>0</td>
      <td>72</td>
      <td>0</td>
      <td>131.0</td>
      <td>144</td>
      <td>1.000000e-31</td>
    </tr>
  </tbody>
</table>
<p>13283 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Concat plus and minus strand. {#concat-plus-and-minus-strand}
-----------------------------
:::

::: {.cell .code execution_count="545"}
``` {.python}
all_unique = pandas.concat([plus_unique, minus_unique])
```
:::

::: {.cell .code execution_count="546"}
``` {.python}
all_unique
```

::: {.output .execute_result execution_count="546"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0001</th>
      <td>1</td>
      <td>1506</td>
      <td>1</td>
      <td>1506</td>
      <td>plus</td>
      <td>1506</td>
      <td>100.0</td>
      <td>0</td>
      <td>1506</td>
      <td>0</td>
      <td>2717.0</td>
      <td>3012</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0002</th>
      <td>1</td>
      <td>1104</td>
      <td>1545</td>
      <td>2648</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0003</th>
      <td>1</td>
      <td>1104</td>
      <td>2669</td>
      <td>3772</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0004</th>
      <td>1</td>
      <td>2418</td>
      <td>3777</td>
      <td>6194</td>
      <td>plus</td>
      <td>2418</td>
      <td>100.0</td>
      <td>0</td>
      <td>2418</td>
      <td>0</td>
      <td>4361.0</td>
      <td>4836</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0006</th>
      <td>1</td>
      <td>957</td>
      <td>7284</td>
      <td>8240</td>
      <td>plus</td>
      <td>957</td>
      <td>100.0</td>
      <td>0</td>
      <td>957</td>
      <td>0</td>
      <td>1727.0</td>
      <td>1914</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6071917..6071989</th>
      <td>1</td>
      <td>73</td>
      <td>5720394</td>
      <td>5720322</td>
      <td>minus</td>
      <td>73</td>
      <td>100.0</td>
      <td>0</td>
      <td>73</td>
      <td>0</td>
      <td>132.0</td>
      <td>146</td>
      <td>3.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6072023..6072096</th>
      <td>1</td>
      <td>74</td>
      <td>5720501</td>
      <td>5720428</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:6141773..6141890</th>
      <td>1</td>
      <td>118</td>
      <td>6141750</td>
      <td>6141633</td>
      <td>minus</td>
      <td>118</td>
      <td>100.0</td>
      <td>0</td>
      <td>118</td>
      <td>0</td>
      <td>214.0</td>
      <td>236</td>
      <td>2.000000e-56</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:816719..816792</th>
      <td>1</td>
      <td>74</td>
      <td>816793</td>
      <td>816720</td>
      <td>minus</td>
      <td>74</td>
      <td>100.0</td>
      <td>0</td>
      <td>74</td>
      <td>0</td>
      <td>134.0</td>
      <td>148</td>
      <td>1.000000e-32</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:834266..834337</th>
      <td>1</td>
      <td>72</td>
      <td>834338</td>
      <td>834267</td>
      <td>minus</td>
      <td>72</td>
      <td>100.0</td>
      <td>0</td>
      <td>72</td>
      <td>0</td>
      <td>131.0</td>
      <td>144</td>
      <td>1.000000e-31</td>
    </tr>
  </tbody>
</table>
<p>26307 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
We have 26307 unique blasted features but 26478 features in the gff.
Which ones are missing?
:::

::: {.cell .code execution_count="547"}
``` {.python}
all_keys = []
for i,f in enumerate(ref_features):
    keys = f.qualifiers.keys()
    for key in keys:
        if key not in all_keys:
            all_keys.append(key)
```
:::

::: {.cell .code execution_count="548"}
``` {.python}
ref_features_df = pandas.DataFrame(
    index=[feature.id for feature in ref_features],
    columns=["start", "end", "strand"] + all_keys
)
```
:::

::: {.cell .code execution_count="549"}
``` {.python}
ref_features_dict = {}

for i,f in enumerate(ref_features):
    if f.id == "AM181176.4:1..6722539":
        continue
#     print(f)
    ref_features_dict[f.id] = {"start":f.location.start.position+1,
                               "end":f.location.end.position,
                               "strand": f.location.strand}
    for k,v in f.qualifiers.items():
        ref_features_dict[f.id][k] = v[0]
```
:::

::: {.cell .code execution_count="550"}
``` {.python}
ref_features_df = pandas.DataFrame(ref_features_dict).T
```
:::

::: {.cell .code execution_count="551"}
``` {.python}
ref_features_df.head()
```

::: {.output .execute_result execution_count="551"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>start</th>
      <th>end</th>
      <th>strand</th>
      <th>ID</th>
      <th>gbkey</th>
      <th>standard_name</th>
      <th>source</th>
      <th>Name</th>
      <th>gene</th>
      <th>gene_biotype</th>
      <th>...</th>
      <th>partial</th>
      <th>bound_moiety</th>
      <th>anticodon</th>
      <th>product</th>
      <th>pseudo</th>
      <th>old_locus_tag</th>
      <th>Dbxref</th>
      <th>rpt_type</th>
      <th>end_range</th>
      <th>start_range</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1..16700</th>
      <td>1</td>
      <td>16700</td>
      <td>1</td>
      <td>id-AM181176.4:1..16700</td>
      <td>misc_feature</td>
      <td>ReD1</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>gene-PFLU_0001</th>
      <td>1</td>
      <td>1506</td>
      <td>1</td>
      <td>gene-PFLU_0001</td>
      <td>Gene</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>dnaA</td>
      <td>dnaA</td>
      <td>protein_coding</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_0001</th>
      <td>496</td>
      <td>1152</td>
      <td>1</td>
      <td>id-PFLU_0001</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>dnaA</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_0001-2</th>
      <td>616</td>
      <td>639</td>
      <td>1</td>
      <td>id-PFLU_0001-2</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>dnaA</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_0001-3</th>
      <td>1225</td>
      <td>1434</td>
      <td>1</td>
      <td>id-PFLU_0001-3</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>dnaA</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 25 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="552"}
``` {.python}
ref_features_df.index.difference(all_unique.index)
```

::: {.output .execute_result execution_count="552"}
    Index(['id-AM181176.4:1208297..1208322', 'id-AM181176.4:1274453..1274468',
           'id-AM181176.4:1274484..1274499', 'id-AM181176.4:1291908..1291922',
           'id-AM181176.4:1291925..1291939', 'id-AM181176.4:1292095..1292110',
           'id-AM181176.4:1292142..1292157', 'id-AM181176.4:1298935..1298948',
           'id-AM181176.4:1298952..1298966', 'id-AM181176.4:1334210..1334223',
           ...
           'id-PFLU_4001-2_9', 'id-PFLU_4089-2_4', 'id-PFLU_4261-3',
           'id-PFLU_4375', 'id-PFLU_4868', 'id-PFLU_5513-2', 'id-PFLU_5542',
           'id-PFLU_5908', 'id-PFLU_5979-2', 'rna-AM181176.4:5717303..5720203'],
          dtype='object', length=170)
:::
:::

::: {.cell .code execution_count="553"}
``` {.python}
algn = pandas.concat([algn_plus, algn_minus])
```
:::

::: {.cell .code execution_count="554"}
``` {.python}
ref_features_df.index.difference(algn.index)
```

::: {.output .execute_result execution_count="554"}
    Index(['id-AM181176.4:1208297..1208322', 'id-AM181176.4:1274453..1274468',
           'id-AM181176.4:1274484..1274499', 'id-AM181176.4:1291908..1291922',
           'id-AM181176.4:1291925..1291939', 'id-AM181176.4:1292095..1292110',
           'id-AM181176.4:1292142..1292157', 'id-AM181176.4:1298935..1298948',
           'id-AM181176.4:1298952..1298966', 'id-AM181176.4:1334210..1334223',
           ...
           'id-PFLU_3982', 'id-PFLU_4001-2_9', 'id-PFLU_4089-2_4',
           'id-PFLU_4261-3', 'id-PFLU_4375', 'id-PFLU_4868', 'id-PFLU_5513-2',
           'id-PFLU_5542', 'id-PFLU_5908', 'id-PFLU_5979-2'],
          dtype='object', length=169)
:::
:::

::: {.cell .code execution_count="555"}
``` {.python}
# Plus strand 
algn_plus.index.intersection(ref_features_df.index.difference(algn.index))
```

::: {.output .execute_result execution_count="555"}
    Index([], dtype='object')
:::
:::

::: {.cell .code execution_count="556"}
``` {.python}
ref_features_plus = pandas.Index([f.id for f in ref_features if f.location.strand==1])
```
:::

::: {.cell .code execution_count="557"}
``` {.python}
ref_features_plus
```

::: {.output .execute_result execution_count="557"}
    Index(['AM181176.4:1..6722539', 'id-AM181176.4:1..16700', 'gene-PFLU_0001',
           'id-PFLU_0001', 'id-PFLU_0001-2', 'id-PFLU_0001-3', 'id-PFLU_0001-4',
           'gene-PFLU_0002', 'id-PFLU_0002', 'id-PFLU_0002-2',
           ...
           'id-AM181176.4:6713408..6713920', 'gene-PFLU_6130', 'id-PFLU_6130',
           'id-PFLU_6130-2', 'gene-PFLU_6131', 'id-PFLU_6131', 'gene-PFLU_6132',
           'id-PFLU_6132', 'id-PFLU_6132-2', 'id-PFLU_6132-3'],
          dtype='object', length=13121)
:::
:::

::: {.cell .code execution_count="558"}
``` {.python}
missing = ref_features_df.loc[ref_features_df.index.difference(algn.index)]
```
:::

::: {.cell .code execution_count="559"}
``` {.python}
missing
```

::: {.output .execute_result execution_count="559"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>start</th>
      <th>end</th>
      <th>strand</th>
      <th>ID</th>
      <th>gbkey</th>
      <th>standard_name</th>
      <th>source</th>
      <th>Name</th>
      <th>gene</th>
      <th>gene_biotype</th>
      <th>...</th>
      <th>partial</th>
      <th>bound_moiety</th>
      <th>anticodon</th>
      <th>product</th>
      <th>pseudo</th>
      <th>old_locus_tag</th>
      <th>Dbxref</th>
      <th>rpt_type</th>
      <th>end_range</th>
      <th>start_range</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1208297..1208322</th>
      <td>1208297</td>
      <td>1208322</td>
      <td>1</td>
      <td>id-AM181176.4:1208297..1208322</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>1274453</td>
      <td>1274468</td>
      <td>1</td>
      <td>id-AM181176.4:1274453..1274468</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274484..1274499</th>
      <td>1274484</td>
      <td>1274499</td>
      <td>1</td>
      <td>id-AM181176.4:1274484..1274499</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1291908..1291922</th>
      <td>1291908</td>
      <td>1291922</td>
      <td>1</td>
      <td>id-AM181176.4:1291908..1291922</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1291925..1291939</th>
      <td>1291925</td>
      <td>1291939</td>
      <td>-1</td>
      <td>id-AM181176.4:1291925..1291939</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_4868</th>
      <td>5348685</td>
      <td>5348696</td>
      <td>-1</td>
      <td>id-PFLU_4868</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_5513-2</th>
      <td>6041726</td>
      <td>6041737</td>
      <td>-1</td>
      <td>id-PFLU_5513-2</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>rpsH</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_5542</th>
      <td>6066639</td>
      <td>6066650</td>
      <td>-1</td>
      <td>id-PFLU_5542</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_5908</th>
      <td>6463295</td>
      <td>6463306</td>
      <td>-1</td>
      <td>id-PFLU_5908</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_5979-2</th>
      <td>6537203</td>
      <td>6537214</td>
      <td>-1</td>
      <td>id-PFLU_5979-2</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>rpmG</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>169 rows × 25 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="560"}
``` {.python}
missing[missing.strand==-1].to_csv("reference_features/minus/missing_short_minus.csv", header=None, index=True)
```
:::

::: {.cell .code execution_count="561"}
``` {.python}
length = missing.end - missing.start
```
:::

::: {.cell .code execution_count="562"}
``` {.python}
length.sort_values()
```

::: {.output .execute_result execution_count="562"}
    id-PFLU_5979-2                    11
    id-AM181176.4:4622516..4622527    11
    id-AM181176.4:4989762..4989773    11
    id-AM181176.4:6249906..6249917    11
    id-PFLU_0504                      11
                                      ..
    id-AM181176.4:1208297..1208322    25
    id-PFLU_1401-3                    65
    id-PFLU_4001-2_9                  68
    id-PFLU_4089-2_4                  68
    id-PFLU_4375                      68
    Length: 169, dtype: object
:::
:::

::: {.cell .markdown}
### Missing hits result from evalue cutoff being too low for very short features. {#missing-hits-result-from-evalue-cutoff-being-too-low-for-very-short-features}

These were blasted again with blast-short and evalue cutoff = 1.
:::

::: {.cell .markdown}
Short blasted plus strand
=========================
:::

::: {.cell .code execution_count="565"}
``` {.python}
short_plus = pandas.read_csv("reference_features/plus/shorts_plus.csv",header=None, index_col=0)
```
:::

::: {.cell .code execution_count="566"}
``` {.python}
short_plus
```

::: {.output .execute_result execution_count="566"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <th>8</th>
      <th>9</th>
      <th>10</th>
      <th>11</th>
      <th>12</th>
      <th>13</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1208297..1208322</th>
      <td>1</td>
      <td>26</td>
      <td>1208507</td>
      <td>1208532</td>
      <td>plus</td>
      <td>26</td>
      <td>100.0</td>
      <td>0</td>
      <td>26</td>
      <td>0</td>
      <td>52.0</td>
      <td>26</td>
      <td>2.000000e-08</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>1</td>
      <td>16</td>
      <td>1274663</td>
      <td>1274678</td>
      <td>plus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>5.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>3</td>
      <td>16</td>
      <td>3775775</td>
      <td>3775788</td>
      <td>plus</td>
      <td>14</td>
      <td>100.0</td>
      <td>0</td>
      <td>14</td>
      <td>0</td>
      <td>28.2</td>
      <td>14</td>
      <td>8.400000e-02</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>4</td>
      <td>16</td>
      <td>5967107</td>
      <td>5967119</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>3.300000e-01</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>2</td>
      <td>14</td>
      <td>6709296</td>
      <td>6709308</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>3.300000e-01</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_2360</th>
      <td>1</td>
      <td>12</td>
      <td>2572012</td>
      <td>2572023</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3330</th>
      <td>1</td>
      <td>12</td>
      <td>3688450</td>
      <td>3688461</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>4399274</td>
      <td>4399285</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>5644609</td>
      <td>5644620</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_4261-3</th>
      <td>1</td>
      <td>12</td>
      <td>4706418</td>
      <td>4706429</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
  </tbody>
</table>
<p>4034 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="567"}
``` {.python}
short_plus.columns = algn.columns
```
:::

::: {.cell .code execution_count="568"}
``` {.python}
short_plus
```

::: {.output .execute_result execution_count="568"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1208297..1208322</th>
      <td>1</td>
      <td>26</td>
      <td>1208507</td>
      <td>1208532</td>
      <td>plus</td>
      <td>26</td>
      <td>100.0</td>
      <td>0</td>
      <td>26</td>
      <td>0</td>
      <td>52.0</td>
      <td>26</td>
      <td>2.000000e-08</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>1</td>
      <td>16</td>
      <td>1274663</td>
      <td>1274678</td>
      <td>plus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>5.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>3</td>
      <td>16</td>
      <td>3775775</td>
      <td>3775788</td>
      <td>plus</td>
      <td>14</td>
      <td>100.0</td>
      <td>0</td>
      <td>14</td>
      <td>0</td>
      <td>28.2</td>
      <td>14</td>
      <td>8.400000e-02</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>4</td>
      <td>16</td>
      <td>5967107</td>
      <td>5967119</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>3.300000e-01</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>2</td>
      <td>14</td>
      <td>6709296</td>
      <td>6709308</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>3.300000e-01</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_2360</th>
      <td>1</td>
      <td>12</td>
      <td>2572012</td>
      <td>2572023</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3330</th>
      <td>1</td>
      <td>12</td>
      <td>3688450</td>
      <td>3688461</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>4399274</td>
      <td>4399285</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>5644609</td>
      <td>5644620</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_4261-3</th>
      <td>1</td>
      <td>12</td>
      <td>4706418</td>
      <td>4706429</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
  </tbody>
</table>
<p>4034 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
### Filter out pident \< 100 {#filter-out-pident--100}
:::

::: {.cell .code execution_count="569"}
``` {.python}
short_plus100 = short_plus[short_plus['pident']==100.0]
```
:::

::: {.cell .code execution_count="570"}
``` {.python}
short_plus100
```

::: {.output .execute_result execution_count="570"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1208297..1208322</th>
      <td>1</td>
      <td>26</td>
      <td>1208507</td>
      <td>1208532</td>
      <td>plus</td>
      <td>26</td>
      <td>100.0</td>
      <td>0</td>
      <td>26</td>
      <td>0</td>
      <td>52.0</td>
      <td>26</td>
      <td>2.000000e-08</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>1</td>
      <td>16</td>
      <td>1274663</td>
      <td>1274678</td>
      <td>plus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>5.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>3</td>
      <td>16</td>
      <td>3775775</td>
      <td>3775788</td>
      <td>plus</td>
      <td>14</td>
      <td>100.0</td>
      <td>0</td>
      <td>14</td>
      <td>0</td>
      <td>28.2</td>
      <td>14</td>
      <td>8.400000e-02</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>4</td>
      <td>16</td>
      <td>5967107</td>
      <td>5967119</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>3.300000e-01</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>2</td>
      <td>14</td>
      <td>6709296</td>
      <td>6709308</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>3.300000e-01</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_2360</th>
      <td>1</td>
      <td>12</td>
      <td>2572012</td>
      <td>2572023</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3330</th>
      <td>1</td>
      <td>12</td>
      <td>3688450</td>
      <td>3688461</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>4399274</td>
      <td>4399285</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>5644609</td>
      <td>5644620</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_4261-3</th>
      <td>1</td>
      <td>12</td>
      <td>4706418</td>
      <td>4706429</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
  </tbody>
</table>
<p>4032 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Genes {#genes}
-----
:::

::: {.cell .markdown}
### Discard duplicates. From each set of duplicates, keep the first {#discard-duplicates-from-each-set-of-duplicates-keep-the-first}
:::

::: {.cell .code execution_count="571"}
``` {.python}
short_plus.loc[short_plus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="571"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>1</td>
      <td>16</td>
      <td>1274663</td>
      <td>1274678</td>
      <td>plus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>0.005</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>3</td>
      <td>16</td>
      <td>3775775</td>
      <td>3775788</td>
      <td>plus</td>
      <td>14</td>
      <td>100.0</td>
      <td>0</td>
      <td>14</td>
      <td>0</td>
      <td>28.2</td>
      <td>14</td>
      <td>0.084</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>4</td>
      <td>16</td>
      <td>5967107</td>
      <td>5967119</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.330</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>2</td>
      <td>14</td>
      <td>6709296</td>
      <td>6709308</td>
      <td>plus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.330</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298935..1298948</th>
      <td>1</td>
      <td>14</td>
      <td>1299145</td>
      <td>1299158</td>
      <td>plus</td>
      <td>14</td>
      <td>100.0</td>
      <td>0</td>
      <td>14</td>
      <td>0</td>
      <td>28.2</td>
      <td>14</td>
      <td>0.042</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_2252-3</th>
      <td>4</td>
      <td>15</td>
      <td>2953848</td>
      <td>2953859</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
    <tr>
      <th>id-PFLU_2252-3</th>
      <td>4</td>
      <td>15</td>
      <td>4029569</td>
      <td>4029580</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
    <tr>
      <th>id-PFLU_2252-3</th>
      <td>4</td>
      <td>15</td>
      <td>5369501</td>
      <td>5369512</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>4399274</td>
      <td>4399285</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>5644609</td>
      <td>5644620</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
  </tbody>
</table>
<p>4008 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
In all groups of identical ids, keep the first one. In this way, the
pflu ids keep their order.
:::

::: {.cell .code execution_count="572"}
``` {.python}
short_plus_unique = short_plus.loc[numpy.logical_not(short_plus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="573"}
``` {.python}
short_plus_unique
```

::: {.output .execute_result execution_count="573"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1208297..1208322</th>
      <td>1</td>
      <td>26</td>
      <td>1208507</td>
      <td>1208532</td>
      <td>plus</td>
      <td>26</td>
      <td>100.0</td>
      <td>0</td>
      <td>26</td>
      <td>0</td>
      <td>52.0</td>
      <td>26</td>
      <td>2.000000e-08</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274453..1274468</th>
      <td>1</td>
      <td>16</td>
      <td>1274663</td>
      <td>1274678</td>
      <td>plus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>5.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1274484..1274499</th>
      <td>1</td>
      <td>16</td>
      <td>1274694</td>
      <td>1274709</td>
      <td>plus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>5.000000e-03</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1291908..1291922</th>
      <td>1</td>
      <td>15</td>
      <td>1292118</td>
      <td>1292132</td>
      <td>plus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>1.600000e-02</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1292095..1292110</th>
      <td>1</td>
      <td>16</td>
      <td>1292305</td>
      <td>1292320</td>
      <td>plus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>5.000000e-03</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_2252-3</th>
      <td>1</td>
      <td>15</td>
      <td>2442766</td>
      <td>2442780</td>
      <td>plus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>1.600000e-02</td>
    </tr>
    <tr>
      <th>id-PFLU_2360</th>
      <td>1</td>
      <td>12</td>
      <td>2572012</td>
      <td>2572023</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3330</th>
      <td>1</td>
      <td>12</td>
      <td>3688450</td>
      <td>3688461</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_3982</th>
      <td>1</td>
      <td>12</td>
      <td>4399274</td>
      <td>4399285</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
    <tr>
      <th>id-PFLU_4261-3</th>
      <td>1</td>
      <td>12</td>
      <td>4706418</td>
      <td>4706429</td>
      <td>plus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>6.600000e-01</td>
    </tr>
  </tbody>
</table>
<p>92 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code}
``` {.python}
```
:::

::: {.cell .markdown}
Short blasted minus strand
==========================
:::

::: {.cell .code execution_count="574"}
``` {.python}
short_minus = pandas.read_csv("reference_features/minus/shorts_minus.csv",header=None, index_col=0)
```
:::

::: {.cell .code execution_count="575"}
``` {.python}
short_minus
```

::: {.output .execute_result execution_count="575"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <th>8</th>
      <th>9</th>
      <th>10</th>
      <th>11</th>
      <th>12</th>
      <th>13</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1291925..1291939</th>
      <td>1</td>
      <td>15</td>
      <td>1292149</td>
      <td>1292135</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1292142..1292157</th>
      <td>1</td>
      <td>16</td>
      <td>1292367</td>
      <td>1292352</td>
      <td>minus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>0.005</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>15</td>
      <td>1299176</td>
      <td>1299162</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>3</td>
      <td>15</td>
      <td>448580</td>
      <td>448568</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>13</td>
      <td>5477441</td>
      <td>5477429</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_4868</th>
      <td>1</td>
      <td>12</td>
      <td>5348556</td>
      <td>5348545</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5513-2</th>
      <td>1</td>
      <td>12</td>
      <td>6041597</td>
      <td>6041586</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5542</th>
      <td>1</td>
      <td>12</td>
      <td>6066510</td>
      <td>6066499</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5908</th>
      <td>1</td>
      <td>12</td>
      <td>6463166</td>
      <td>6463155</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5979-2</th>
      <td>1</td>
      <td>12</td>
      <td>6537074</td>
      <td>6537063</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
  </tbody>
</table>
<p>2855 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="576"}
``` {.python}
short_minus.columns = algn.columns
```
:::

::: {.cell .code execution_count="577"}
``` {.python}
short_minus
```

::: {.output .execute_result execution_count="577"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1291925..1291939</th>
      <td>1</td>
      <td>15</td>
      <td>1292149</td>
      <td>1292135</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1292142..1292157</th>
      <td>1</td>
      <td>16</td>
      <td>1292367</td>
      <td>1292352</td>
      <td>minus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>0.005</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>15</td>
      <td>1299176</td>
      <td>1299162</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>3</td>
      <td>15</td>
      <td>448580</td>
      <td>448568</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>13</td>
      <td>5477441</td>
      <td>5477429</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_4868</th>
      <td>1</td>
      <td>12</td>
      <td>5348556</td>
      <td>5348545</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5513-2</th>
      <td>1</td>
      <td>12</td>
      <td>6041597</td>
      <td>6041586</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5542</th>
      <td>1</td>
      <td>12</td>
      <td>6066510</td>
      <td>6066499</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5908</th>
      <td>1</td>
      <td>12</td>
      <td>6463166</td>
      <td>6463155</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5979-2</th>
      <td>1</td>
      <td>12</td>
      <td>6537074</td>
      <td>6537063</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
  </tbody>
</table>
<p>2855 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
### Filter out pident \< 100 {#filter-out-pident--100}
:::

::: {.cell .code execution_count="578"}
``` {.python}
short_minus100 = short_minus[short_minus['pident']==100.0]
```
:::

::: {.cell .code execution_count="579"}
``` {.python}
short_minus100
```

::: {.output .execute_result execution_count="579"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1291925..1291939</th>
      <td>1</td>
      <td>15</td>
      <td>1292149</td>
      <td>1292135</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1292142..1292157</th>
      <td>1</td>
      <td>16</td>
      <td>1292367</td>
      <td>1292352</td>
      <td>minus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>0.005</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>15</td>
      <td>1299176</td>
      <td>1299162</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>3</td>
      <td>15</td>
      <td>448580</td>
      <td>448568</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>13</td>
      <td>5477441</td>
      <td>5477429</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_4868</th>
      <td>1</td>
      <td>12</td>
      <td>5348556</td>
      <td>5348545</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5513-2</th>
      <td>1</td>
      <td>12</td>
      <td>6041597</td>
      <td>6041586</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5542</th>
      <td>1</td>
      <td>12</td>
      <td>6066510</td>
      <td>6066499</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5908</th>
      <td>1</td>
      <td>12</td>
      <td>6463166</td>
      <td>6463155</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5979-2</th>
      <td>1</td>
      <td>12</td>
      <td>6537074</td>
      <td>6537063</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
  </tbody>
</table>
<p>2855 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
### Discard duplicates. From each set of duplicates, keep the first {#discard-duplicates-from-each-set-of-duplicates-keep-the-first}
:::

::: {.cell .code execution_count="580"}
``` {.python}
short_minus.loc[short_minus.index.duplicated(keep=False)]
```

::: {.output .execute_result execution_count="580"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>15</td>
      <td>1299176</td>
      <td>1299162</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>3</td>
      <td>15</td>
      <td>448580</td>
      <td>448568</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>13</td>
      <td>5477441</td>
      <td>5477429</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>13</td>
      <td>5631003</td>
      <td>5630991</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>2</td>
      <td>14</td>
      <td>6163308</td>
      <td>6163296</td>
      <td>minus</td>
      <td>13</td>
      <td>100.0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>26.3</td>
      <td>13</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_2271-3</th>
      <td>1</td>
      <td>12</td>
      <td>652259</td>
      <td>652248</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
    <tr>
      <th>id-PFLU_2271-3</th>
      <td>1</td>
      <td>12</td>
      <td>1359761</td>
      <td>1359750</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
    <tr>
      <th>id-PFLU_2271-3</th>
      <td>1</td>
      <td>12</td>
      <td>2397364</td>
      <td>2397353</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
    <tr>
      <th>id-PFLU_2271-3</th>
      <td>1</td>
      <td>12</td>
      <td>4747201</td>
      <td>4747190</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
    <tr>
      <th>id-PFLU_2271-3</th>
      <td>1</td>
      <td>12</td>
      <td>5550483</td>
      <td>5550472</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.990</td>
    </tr>
  </tbody>
</table>
<p>2830 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
In all groups of identical ids, keep the first one. In this way, the
pflu ids keep their order.
:::

::: {.cell .code execution_count="581"}
``` {.python}
short_minus_unique = short_minus.loc[numpy.logical_not(short_minus.index.duplicated(keep='first'))]
```
:::

::: {.cell .code execution_count="582"}
``` {.python}
short_minus_unique
```

::: {.output .execute_result execution_count="582"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
    <tr>
      <th>0</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1291925..1291939</th>
      <td>1</td>
      <td>15</td>
      <td>1292149</td>
      <td>1292135</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1292142..1292157</th>
      <td>1</td>
      <td>16</td>
      <td>1292367</td>
      <td>1292352</td>
      <td>minus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>0.005</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1298952..1298966</th>
      <td>1</td>
      <td>15</td>
      <td>1299176</td>
      <td>1299162</td>
      <td>minus</td>
      <td>15</td>
      <td>100.0</td>
      <td>0</td>
      <td>15</td>
      <td>0</td>
      <td>30.2</td>
      <td>15</td>
      <td>0.016</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1334227..1334240</th>
      <td>1</td>
      <td>14</td>
      <td>1334450</td>
      <td>1334437</td>
      <td>minus</td>
      <td>14</td>
      <td>100.0</td>
      <td>0</td>
      <td>14</td>
      <td>0</td>
      <td>28.2</td>
      <td>14</td>
      <td>0.042</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1368150..1368165</th>
      <td>1</td>
      <td>16</td>
      <td>1368375</td>
      <td>1368360</td>
      <td>minus</td>
      <td>16</td>
      <td>100.0</td>
      <td>0</td>
      <td>16</td>
      <td>0</td>
      <td>32.2</td>
      <td>16</td>
      <td>0.005</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_4868</th>
      <td>1</td>
      <td>12</td>
      <td>5348556</td>
      <td>5348545</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5513-2</th>
      <td>1</td>
      <td>12</td>
      <td>6041597</td>
      <td>6041586</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5542</th>
      <td>1</td>
      <td>12</td>
      <td>6066510</td>
      <td>6066499</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5908</th>
      <td>1</td>
      <td>12</td>
      <td>6463166</td>
      <td>6463155</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
    <tr>
      <th>id-PFLU_5979-2</th>
      <td>1</td>
      <td>12</td>
      <td>6537074</td>
      <td>6537063</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.660</td>
    </tr>
  </tbody>
</table>
<p>70 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="583"}
``` {.python}
all_unique = all_unique.append(short_plus_unique).append(short_minus_unique)
```
:::

::: {.cell .code execution_count="584"}
``` {.python}
all_unique
```

::: {.output .execute_result execution_count="584"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>qstart</th>
      <th>qend</th>
      <th>sstart</th>
      <th>send</th>
      <th>sstrand</th>
      <th>nident</th>
      <th>pident</th>
      <th>mismatch</th>
      <th>positive</th>
      <th>gaps</th>
      <th>bitscore</th>
      <th>score</th>
      <th>evalue</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gene-PFLU_0001</th>
      <td>1</td>
      <td>1506</td>
      <td>1</td>
      <td>1506</td>
      <td>plus</td>
      <td>1506</td>
      <td>100.0</td>
      <td>0</td>
      <td>1506</td>
      <td>0</td>
      <td>2717.0</td>
      <td>3012</td>
      <td>0.00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0002</th>
      <td>1</td>
      <td>1104</td>
      <td>1545</td>
      <td>2648</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0003</th>
      <td>1</td>
      <td>1104</td>
      <td>2669</td>
      <td>3772</td>
      <td>plus</td>
      <td>1104</td>
      <td>100.0</td>
      <td>0</td>
      <td>1104</td>
      <td>0</td>
      <td>1992.0</td>
      <td>2208</td>
      <td>0.00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0004</th>
      <td>1</td>
      <td>2418</td>
      <td>3777</td>
      <td>6194</td>
      <td>plus</td>
      <td>2418</td>
      <td>100.0</td>
      <td>0</td>
      <td>2418</td>
      <td>0</td>
      <td>4361.0</td>
      <td>4836</td>
      <td>0.00</td>
    </tr>
    <tr>
      <th>gene-PFLU_0006</th>
      <td>1</td>
      <td>957</td>
      <td>7284</td>
      <td>8240</td>
      <td>plus</td>
      <td>957</td>
      <td>100.0</td>
      <td>0</td>
      <td>957</td>
      <td>0</td>
      <td>1727.0</td>
      <td>1914</td>
      <td>0.00</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>id-PFLU_4868</th>
      <td>1</td>
      <td>12</td>
      <td>5348556</td>
      <td>5348545</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.66</td>
    </tr>
    <tr>
      <th>id-PFLU_5513-2</th>
      <td>1</td>
      <td>12</td>
      <td>6041597</td>
      <td>6041586</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.66</td>
    </tr>
    <tr>
      <th>id-PFLU_5542</th>
      <td>1</td>
      <td>12</td>
      <td>6066510</td>
      <td>6066499</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.66</td>
    </tr>
    <tr>
      <th>id-PFLU_5908</th>
      <td>1</td>
      <td>12</td>
      <td>6463166</td>
      <td>6463155</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.66</td>
    </tr>
    <tr>
      <th>id-PFLU_5979-2</th>
      <td>1</td>
      <td>12</td>
      <td>6537074</td>
      <td>6537063</td>
      <td>minus</td>
      <td>12</td>
      <td>100.0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>24.3</td>
      <td>12</td>
      <td>0.66</td>
    </tr>
  </tbody>
</table>
<p>26469 rows × 13 columns</p>
</div>
```
:::
:::

::: {.cell .code execution_count="585"}
``` {.python}
len(ref_features)
```

::: {.output .execute_result execution_count="585"}
    26478
:::
:::

::: {.cell .code execution_count="586"}
``` {.python}
missing = ref_features_df.loc[ref_features_df.index.difference(all_unique.index)]
```
:::

::: {.cell .markdown}
Still a handful missing, could not be aligned anywhere in the new genome
(???)
:::

::: {.cell .code execution_count="587"}
``` {.python}
missing
```

::: {.output .execute_result execution_count="587"}
```{=html}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>start</th>
      <th>end</th>
      <th>strand</th>
      <th>ID</th>
      <th>gbkey</th>
      <th>standard_name</th>
      <th>source</th>
      <th>Name</th>
      <th>gene</th>
      <th>gene_biotype</th>
      <th>...</th>
      <th>partial</th>
      <th>bound_moiety</th>
      <th>anticodon</th>
      <th>product</th>
      <th>pseudo</th>
      <th>old_locus_tag</th>
      <th>Dbxref</th>
      <th>rpt_type</th>
      <th>end_range</th>
      <th>start_range</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>id-AM181176.4:1576708..1576723</th>
      <td>1576708</td>
      <td>1576723</td>
      <td>1</td>
      <td>id-AM181176.4:1576708..1576723</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1576730..1576745</th>
      <td>1576730</td>
      <td>1576745</td>
      <td>-1</td>
      <td>id-AM181176.4:1576730..1576745</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1748023..1748040</th>
      <td>1748023</td>
      <td>1748040</td>
      <td>1</td>
      <td>id-AM181176.4:1748023..1748040</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-AM181176.4:1751189..1751203</th>
      <td>1751189</td>
      <td>1751203</td>
      <td>-1</td>
      <td>id-AM181176.4:1751189..1751203</td>
      <td>repeat_region</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_4001-2_9</th>
      <td>4420471</td>
      <td>4420539</td>
      <td>-1</td>
      <td>id-PFLU_4001-2</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>true</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_4089-2_4</th>
      <td>4523786</td>
      <td>4523854</td>
      <td>-1</td>
      <td>id-PFLU_4089-2</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>true</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>id-PFLU_4375</th>
      <td>4823077</td>
      <td>4823145</td>
      <td>1</td>
      <td>id-PFLU_4375</td>
      <td>misc_feature</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>rna-AM181176.4:5717303..5720203</th>
      <td>5717303</td>
      <td>5720203</td>
      <td>1</td>
      <td>rna-AM181176.4:5717303..5720203</td>
      <td>rRNA</td>
      <td>NaN</td>
      <td>EMBL</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>8 rows × 25 columns</p>
</div>
```
:::
:::

::: {.cell .markdown}
Check location offsets (should never be larger than difference in base counts.) {#check-location-offsets-should-never-be-larger-than-difference-in-base-counts}
-------------------------------------------------------------------------------
:::

::: {.cell .markdown}
#### Swap start and end if feature is on minus strand
:::

::: {.cell .code execution_count="589"}
``` {.python}
tmp = copy.deepcopy(all_unique[all_unique.sstrand=="minus"].loc[:,"sstart"])
```
:::

::: {.cell .code execution_count="590"}
``` {.python}
tmp
```

::: {.output .execute_result execution_count="590"}
    gene-PFLU_0005       7021
    gene-PFLU_0008      11476
    gene-PFLU_0009      12095
    gene-PFLU_0010      14152
    gene-PFLU_0011      15102
                       ...   
    id-PFLU_4868      5348556
    id-PFLU_5513-2    6041597
    id-PFLU_5542      6066510
    id-PFLU_5908      6463166
    id-PFLU_5979-2    6537074
    Name: sstart, Length: 13353, dtype: int64
:::
:::

::: {.cell .code execution_count="591"}
``` {.python}
all_unique.loc[all_unique.sstrand=="minus","sstart"] = copy.deepcopy(all_unique.loc[all_unique.sstrand=="minus","send"])
```
:::

::: {.cell .code execution_count="592"}
``` {.python}
all_unique[all_unique.sstrand=="minus"].loc[:,"sstart"]
```

::: {.output .execute_result execution_count="592"}
    gene-PFLU_0005       6299
    gene-PFLU_0008      10706
    gene-PFLU_0009      11559
    gene-PFLU_0010      12098
    gene-PFLU_0011      14149
                       ...   
    id-PFLU_4868      5348545
    id-PFLU_5513-2    6041586
    id-PFLU_5542      6066499
    id-PFLU_5908      6463155
    id-PFLU_5979-2    6537063
    Name: sstart, Length: 13353, dtype: int64
:::
:::

::: {.cell .code execution_count="593"}
``` {.python}
all_unique.loc[all_unique.sstrand=="minus","send"] = tmp
```
:::

::: {.cell .code execution_count="594"}
``` {.python}
start_offset = all_unique.sstart - ref_features_df.loc[all_unique.index].start
```
:::

::: {.cell .code execution_count="595"}
``` {.python}
end_offset = all_unique.send - ref_features_df.loc[all_unique.index].end
```
:::

::: {.cell .code execution_count="596"}
``` {.python}
start_offset['gene-PFLU_0005']
```

::: {.output .execute_result execution_count="596"}
    0
:::
:::

::: {.cell .code execution_count="597"}
``` {.python}
all_unique.loc['gene-PFLU_0005']
```

::: {.output .execute_result execution_count="597"}
    qstart           1
    qend           723
    sstart        6299
    send          7021
    sstrand      minus
    nident         723
    pident       100.0
    mismatch         0
    positive       723
    gaps             0
    bitscore    1305.0
    score         1446
    evalue         0.0
    Name: gene-PFLU_0005, dtype: object
:::
:::

::: {.cell .code execution_count="598"}
``` {.python}
ref_features_df.loc['gene-PFLU_0005']
```

::: {.output .execute_result execution_count="598"}
    start                      6299
    end                        7021
    strand                       -1
    ID               gene-PFLU_0005
    gbkey                      Gene
    standard_name               NaN
    source                     EMBL
    Name                  PFLU_0005
    gene                        NaN
    gene_biotype     protein_coding
    locus_tag             PFLU_0005
    inference                   NaN
    Note                        NaN
    part                        NaN
    is_ordered                  NaN
    partial                     NaN
    bound_moiety                NaN
    anticodon                   NaN
    product                     NaN
    pseudo                      NaN
    old_locus_tag               NaN
    Dbxref                      NaN
    rpt_type                    NaN
    end_range                   NaN
    start_range                 NaN
    Name: gene-PFLU_0005, dtype: object
:::
:::

::: {.cell .code execution_count="599"}
``` {.python}
import matplotlib as mpl
```
:::

::: {.cell .markdown}
### Plot histogram over offsets on log-log scale
:::

::: {.cell .code execution_count="600"}
``` {.python}
fig, axs = mpl.pyplot.subplots(1)
abs(start_offset).plot(ax=axs,bins=numpy.logspace(0, 7, 32), kind='hist', loglog=True)
```

::: {.output .execute_result execution_count="600"}
    <AxesSubplot:ylabel='Frequency'>
:::

::: {.output .display_data}
![](64269156d144adf6758278f7f492f2f58702bddf.png)
:::
:::

::: {.cell .code execution_count="601"}
``` {.python}
shorts = pandas.concat([short_plus, short_minus])
```
:::

::: {.cell .markdown}
### Plot length vs. offset for each feature. {#plot-length-vs-offset-for-each-feature}
:::

::: {.cell .code execution_count="602"}
``` {.python}
mpl.pyplot.loglog(all_unique.send - all_unique.sstart, start_offset, "o")
_ = mpl.pyplot.title("Length (x) vs. offset (y)")
```

::: {.output .display_data}
![](19d75e3d87a6237068bdb7443661dee4315f2a83.png)
:::
:::

::: {.cell .markdown}
Setup the new genome annotation
-------------------------------

### Consider only genes
:::

::: {.cell .markdown}
#### Open reference annotations as db
:::

::: {.cell .code execution_count="603"}
``` {.python}
db = gff.FeatureDB('./Pseudomonas_fluorescens_sbw25_ref.db')
```
:::

::: {.cell .markdown}
### Fix gene, CDS, transcript, mRNA positions

CDS, mRNA are feature.children of gene
:::

::: {.cell .code execution_count="1"}
``` {.python}
from gffutils import biopython_integration as gffbio
```
:::

::: {.cell .code execution_count="36"}
``` {.python}
### Open the new assembly.
```
:::

::: {.cell .code execution_count="647"}
``` {.python}
with open("../../../fasta/Pseudomonas_fluorescens_SBW25_28C.fa") as fh:
    new_genome_assembly = next(SeqIO.parse(fh, 'fasta'))
```
:::

::: {.cell .code execution_count="649"}
``` {.python}
len(new_genome_assembly.seq)
```

::: {.output .execute_result execution_count="649"}
    6722400
:::
:::

::: {.cell .markdown}
#### Construct a seq record to append each feature to. {#construct-a-seq-record-to-append-each-feature-to}
:::

::: {.cell .code execution_count="655"}
``` {.python}
fixed_record = SeqRecord(new_genome_assembly.seq, "MPB ASM001")
```
:::

::: {.cell .markdown}
#### Loop over the reference annotation features (genes) and their children. {#loop-over-the-reference-annotation-features-genes-and-their-children}

-   Get the feature id.
-   Adjust start, end position using the subject positions from the
    blast aligments (stored in the dataframe `all_unique`).
-   Convert feature to SeqFeature.
-   Append to SeqRecord
:::

::: {.cell .code execution_count="668" collapsed="true" jupyter="{\"outputs_hidden\":true}"}
``` {.python}
for i,feat in enumerate(db.iter_by_parent_childs(featuretype='gene')):
    # Get id.
    feat_id = feat[0].id
    
    print("Fixing coordinates of gene {} and all its 'children'.".format(feat_id))
    # Retrieve the feature with adjusted coordinates
    fixed_id = feat_id.replace(":", "-") 
    
    # Retrieve the record from the blast alignments.
    fixed_feature = all_unique.loc[fixed_id]
    
    # Loop over all (sub)-features of this one.
    for j,f in enumerate(feat):
        feat[j].start = int(fixed_feature.sstart)
        feat[j].end = int(fixed_feature.send)
    
    # Convert to SeqFeature.
    seq_feature = bpy.to_seqfeature(feat[0])
    seq_feature.sub_features = [bpy.to_seqfeature(f) for f in feat[1:]]
    # Append to the record.     
    fixed_record.features.append(seq_feature)
```

::: {.output .stream .stdout}
    Fixing coordinates of gene gene:PFLU_0001 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0002 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0003 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0004 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0005 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0006 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0007 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0008 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0009 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0010 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0011 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0012 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0013 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0014 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0015 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0016 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0017 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0018 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0019 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0020 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0021 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0022 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0023 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0024 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0025 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0026 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0027 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0028 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0029 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0030 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0031 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0032 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0033 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0034 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0035 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0036 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0037 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0038 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0039 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0040 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0041 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0042 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0043 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0044 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0045 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0046 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0047 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0048 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0049 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0050 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0051 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0052 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0053 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0054 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0055 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0056 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0057 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0058 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0059 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0060 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0061 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0062 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0063 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0064 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0065 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0066 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0067 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0068 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0069 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0070 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0071 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0072 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0073 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0074 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0075 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0076 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0077 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0078 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0079 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0080 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0081 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0082 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0083 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0084 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0085 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0086 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0087 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0088 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0089 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0090 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0091 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0093 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0094 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0095 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0096 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0097 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0098 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0099 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0100 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0101 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0102 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0103 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0104 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0105 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0106 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0107 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0108 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0109 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0110 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0111 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0112 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0113 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0114 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0115 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0116 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0117 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0118 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0119 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0120 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0121 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0122 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0123 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0124 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0125 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0126 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0127 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0128 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0129 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0130 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0131 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0132 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0133 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0134 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0135 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0136 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0137 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0138 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0139 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0140 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0141 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0142 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0143 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0144 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0145 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0146 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0147 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0148 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0148A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0149 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0150 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0151 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0152 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0153 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0154 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0155 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0156 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0157 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0158 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0159 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0160 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0161 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0162 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0163 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0164 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0165 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0166 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0167 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0170 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0171 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0172 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0173 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0174 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0175 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0176 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0177 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0178 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0179 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0180 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0181 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0182 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0183 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0184 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0185 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0186 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0187 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0188 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0189 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0190 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0191 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0192 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0193 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0194 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0196 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0197 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0198 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0199 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0200 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0201 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0202 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0203 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0204 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0205 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0206 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0207 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0208 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0209 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0210 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0211 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0213 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0214 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0215 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0218 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0219 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0227 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0228 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0229 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0230 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0231 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0232 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0233 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0234 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0235 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0236 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0237 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0238 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0239 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0240 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0241 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0242 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0243 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0244 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0245 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0246 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0247 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0248 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0249 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0250 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0251 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0252 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0253 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0254 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0255 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0256 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0257 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0258 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0259 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0260 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0261 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0262 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0263 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0264 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0265 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0266 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0267 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0268 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0269 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0270 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0271 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0272 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0274 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0275 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0276 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0277 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0278 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0279 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0280 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0281 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0282 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0283 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0284 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0285 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0286 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0287 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0288 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0289 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0290 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0291 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0292 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0293 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0294 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0295 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0296 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0299 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0300 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0301 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0302 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0303 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0304 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0305 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0306 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0307 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0308 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0309 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0310 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0311 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0312 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0313 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0314 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0315 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0316 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0317 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0318 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0319 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0320 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0321 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0322 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0323 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0324 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0325 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0327 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0328 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0329 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0330 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0331 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0332 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0333 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0334 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0335 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0336 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0337 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0338 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0339 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0340 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0341 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0342 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0343 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0344 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0345 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0346 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0347 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0348 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0349 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0350 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0351 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0352 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0353 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0354 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0355 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0356 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0357 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0358 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0359 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0360 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0361 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0362 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0363 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0364 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0365 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0366 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0367 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0368 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0369 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0370 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0371 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0372 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0373 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0374 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0375 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0376 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0377 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0378 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0379 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0380 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0381 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0382 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0383 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0384 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0385 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0386 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0387 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0388 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0389 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0390 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0391 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0392 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0393 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0394 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0395 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0396 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0397 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0398 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0399 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0400 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0401 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0402 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0403 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0404 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0405 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0406 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0408 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0409 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0411 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0412 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0413 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0414 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0415 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0416 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0417 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0418 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0419 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0420 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0421 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0422 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0423 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0424 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0425 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0426 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0427 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0428 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0429 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0430 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0433 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0434 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0435 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0436 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0437 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0438 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0439 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0440 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0441 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0442 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0443 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0444 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0445 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0446 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0447 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0448 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0449 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0450 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0451 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0452 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0453 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0454 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0455 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0456 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0457 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0458 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0459 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0460 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0461 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0462 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0463 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0464 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0465 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0466 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0467 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0468 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0469 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0471 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0472 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0473 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0474 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0475 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0476 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0477 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0478 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0479 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0480 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0481 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0482 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0483 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0485 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0486 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0487 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0488 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0489 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0490 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0491 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0492 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0493 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0494 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0495 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0496 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0497 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0498 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0499 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0500 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0501 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0502 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0503 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0504 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0505 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0506 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0507 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0508 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0509 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0510 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0511 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0512 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0513 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0514 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0515 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0516 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0517 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0518 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0519 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0520 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0521 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0522 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0523 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0524 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0525 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0526 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0527 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0528 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0530 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0531 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0533 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0534 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0535 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0536 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0537 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0538 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0539 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0540 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0541 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0542 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0543 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0545 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0546 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0547 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0548 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0549 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0550 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0551 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0553 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0554 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0555 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0556 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0557 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0558 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0559 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0560 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0561 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0562 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0563 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0564 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0565 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0566 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0567 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0568 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0569 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0570 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0571 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0572 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0573 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0574 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0575 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0576 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0577 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0578 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0579 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0580 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0581 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0582 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0583 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0584 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0585 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0586 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0587 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0588 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0589 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0590 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0591 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0592 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0593 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0594 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0595 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0596 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0597 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0598 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0601 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0602 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0603 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0604 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0605 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0606 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0607 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0608 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0609 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0610 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0611 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0612 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0613 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0614 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0615 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0616 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0617 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0618 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0619 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0620 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0621 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0623 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0624 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0625 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0626 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0627 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0628 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0629 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0630 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0631 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0632 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0633 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0634 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0635 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0636 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0637 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0638 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0639 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0640 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0641 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0642 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0643 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0644 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0645 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0646 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0647 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0648 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0649 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0650 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0651 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0652 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0653 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0654 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0655 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0656 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0657 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0658 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0659 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0660 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0661 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0662 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0663 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0664 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0665 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0666 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0667 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0668 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0669 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0670 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0671 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0672 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0673 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0674 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0675 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0676 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0677 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0678 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0679 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0680 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0681 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0682 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0683 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0684 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0685 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0686 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0687 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0688 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0689 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0690 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0691 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0692 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0693 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0694 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0695 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0696 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0697 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0698 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0699 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0700 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0701 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0702 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0703 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0704 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0705 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0706 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0707 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0708 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0709 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0712 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0713 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0714 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0715 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0716 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0717 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0718 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0719 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0720 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0721 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0722 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0723 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0724 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0725 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0726 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0727 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0728 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0729 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0730 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0731 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0732 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0733 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0734 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0735 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0737 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0738 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0739 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0740 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0741 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0742 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0743 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0744 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0745 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0746 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0747 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0748 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0749 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0750 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0751 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0752 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0753 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0754 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0755 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0756 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0757 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0758 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0759 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0760 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0761 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0762 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0763 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0763A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0764 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0765 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0766 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0767 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0768 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0769 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0770 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0771 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0772 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0773 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0774 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0775 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0776 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0777 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0778 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0779 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0780 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0781 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0782 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0783 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0784 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0785 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0786 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0787 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0788 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0789 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0790 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0791 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0793 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0794 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0795 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0796 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0797 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0798 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0799 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0800 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0801 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0802 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0803 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0804 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0805 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0806 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0807 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0808 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0809 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0810 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0811 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0812 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0813 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0814 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0815 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0816 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0817 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0818 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0819 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0820 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0821 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0822 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0823 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0824 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0825 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0826 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0827 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0828 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0829 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0830 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0831 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0832 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0833 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0834 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0835 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0836 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0837 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0838 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0839 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0840 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0841 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0842 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0843 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0844 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0845 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0846 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0848 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0849 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0850 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0851 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0852 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0853 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0854 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0855 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0856 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0857 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0858 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0859 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0860 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0861 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0862 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0863 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0864 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0865 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0866 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0867 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0868 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0869 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0870 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0871 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0872 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0873 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0874 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0875 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0876 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0877 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0878 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0879 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0880 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0881 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0882 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0883 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0884 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0885 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0886 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0887 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0888 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0889 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0890 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0891 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0892 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0893 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0894 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0895 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0896 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0897 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0898 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0899 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0900 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0901 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0902 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0903 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0904 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0905 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0906 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0907 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0908 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0909 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0910 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0912 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0913 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0914 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0915 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0916 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0917 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0918 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0919 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0920 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0921 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0922 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0923 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0924 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0925 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0926 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0927 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0928 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0929 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0930 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0931 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0932 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0933 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0934 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0935 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0936 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0937 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0938 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0939 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0940 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0941 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0942 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0943 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0944 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0945 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0946 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0947 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0948 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0949 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0950 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0951 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0952 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0953 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0954 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0955 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0956 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0957 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0958 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0959 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0960 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0961 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0962 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0963 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0964 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0965 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0966 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0968 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0969 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0970 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0971 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0972 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0973 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0974 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0975 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0976 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0977 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0978 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0979 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0980 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0981 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0982 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0983 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0984 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0985 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0986 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0987 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0988 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0989 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0990 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0992 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0993 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0994 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0995 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0996 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0997 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0998 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_0999 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1000 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1001 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1002 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1003 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1004 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1005 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1006 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1007 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1008 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1009 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1010 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1011 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1012 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1013 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1014 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1015 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1016 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1017 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1018 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1019 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1020 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1021 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1022 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1023 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1024 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1025 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1026 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1027 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1028 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1029 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1030 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1031 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1032 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1033 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1034 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1035 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1036 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1037 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1038 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1039 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1040 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1041 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1042 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1043 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1044 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1045 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1046 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1047 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1048 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1049 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1050 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1051 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1052 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1053 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1054 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1055 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1056 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1057 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1058 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1059 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1060 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1061 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1062 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1063 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1064 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1065 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1066 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1067 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1068 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1069 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1070 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1071 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1072 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1073 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1074 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1075 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1076 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1077 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1078 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1079 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1081 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1082 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1083 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1084 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1085 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1086 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1087 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1087A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1088 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1089 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1090 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1091 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1092 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1093 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1094 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1095 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1096 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1097 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1098 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1099 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1100 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1101 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1102 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1103 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1104 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1105 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1107 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1108 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1109 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1110 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1111 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1112 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1113 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1114 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1115 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1116 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1117 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1118 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1119 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1120 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1121 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1122 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1123 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1124 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1125 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1126 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1127 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1128 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1129 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1130 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1131 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1132 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1133 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1134 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1135 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1136 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1137 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1138 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1139 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1140 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1141 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1142 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1143 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1144 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1145 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1146 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1148 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1149 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1150 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1151 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1152 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1153 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1154 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1155 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1156 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1157 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1158 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1159 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1160 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1161 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1162 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1163 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1164 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1165 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1166 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1167 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1169 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1170 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1171 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1172 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1173 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1174 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1175 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1176 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1177 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1178 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1179 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1180 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1181 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1182 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1183 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1184 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1185 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1186 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1187 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1188 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1189 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1190 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1191 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1192 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1193 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1195 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1196 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1197 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1198 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1199 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1200 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1201 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1202 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1203 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1204 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1205 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1206 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1207 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1208 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1209 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1210 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1211 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1212 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1213 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1214 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1215 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1216 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1217 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1219 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1220 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1221 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1222 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1223 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1224 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1225 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1228 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1229 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1230 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1231 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1232 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1233 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1234 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1235 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1236 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1237 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1238 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1239 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1240 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1241 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1243 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1244 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1245 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1246 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1247 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1248 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1249 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1250 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1251 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1252 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1253 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1254 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1255 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1256 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1257 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1258 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1259 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1260 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1261 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1262 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1263 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1264 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1265 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1266 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1267 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1268 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1269 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1270 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1271 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1272 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1273 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1274 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1275 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1276 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1277 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1278 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1279 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1280 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1281 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1282 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1283 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1284 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1285 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1286 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1287 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1289 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1290 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1291 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1292 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1293 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1294 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1295 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1296 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1297 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1298 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1299 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1300 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1301 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1302 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1302A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1304 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1305 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1306 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1307 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1308 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1309 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1310 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1311 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1312 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1313 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1314 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1315 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1317A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1318 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1319 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1320 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1321 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1322 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1323 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1324 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1325 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1326 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1327 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1328 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1329 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1330 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1332 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1333 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1334 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1335 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1336 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1337 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1338 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1339 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1340 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1341 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1342 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1343 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1344 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1345 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1346 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1347 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1348 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1349 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1350 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1351 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1352 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1353 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1354 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1355 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1356 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1357 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1358 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1359 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1359A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1361 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1362 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1363 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1364 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1365 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1366 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1367 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1368 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1369 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1370 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1371 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1372 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1373 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1374 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1375 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1376 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1377 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1378 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1379 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1380 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1381 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1382 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1383 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1384 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1385 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1386 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1387 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1388 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1389 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1390 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1391 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1392 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1394 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1395 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1396 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1397 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1398 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1399 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1400 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1401 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1402 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1403 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1404 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1405 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1406 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1407 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1408 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1409 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1410 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1411 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1412 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1413 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1414 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1415 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1416 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1417 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1418 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1419 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1420 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1421 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1422 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1423 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1424 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1425 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1426 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1427 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1428 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1429 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1430 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1431 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1432 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1433 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1434 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1435 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1436 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1437 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1438 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1439 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1440 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1441 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1442 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1443 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1444 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1445 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1446 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1447 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1448 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1449 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1450 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1451 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1452 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1453 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1454 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1455 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1456 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1458 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1459 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1460 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1461 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1462 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1463 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1464 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1465 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1467 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1468 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1469 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1471 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1472 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1473 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1474 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1475 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1476 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1477 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1478 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1479 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1480 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1481 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1482 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1483 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1484 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1485 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1486 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1487 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1488 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1489 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1490 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1491 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1492 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1493 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1496 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1498 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1499 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1500 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1501 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1502 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1503 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1504 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1505 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1506 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1507 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1508 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1509 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1510 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1511 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1512 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1513 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1514 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1515 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1516 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1517 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1518 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1519 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1520 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1521 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1522 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1523 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1524 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1525 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1526 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1527 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1528 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1529 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1530 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1531 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1532 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1533 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1534 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1535 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1536 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1537 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1538 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1539 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1540 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1541 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1542 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1543 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1544 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1545 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1546 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1547 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1548 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1549 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1550 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1551 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1552 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1553 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1554 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1555 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1556 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1557 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1558 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1559 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1560 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1561 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1562 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1563 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1564 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1565 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1566 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1568 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1569 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1570 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1571 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1572 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1573 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1574 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1575 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1576 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1577 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1578 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1579 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1580 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1581 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1582 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1583 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1584 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1585 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1586 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1587 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1588 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1589 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1590 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1591 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1591A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1591B and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1592 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1593 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1594 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1595 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1596 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1597 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1600 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1601 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1602 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1603 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1604 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1605 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1606 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1607 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1608 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1609 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1610 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1611 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1612 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1613 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1614 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1615 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1616 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1617 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1618 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1619 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1620 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1621 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1622 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1623 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1624 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1625 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1626 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1627 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1628 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1629 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1630 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1631 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1632 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1633 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1634 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1635 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1636 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1637 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1638 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1639 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1640 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1641 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1643 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1644 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1645 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1646 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1647 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1648 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1649 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1650 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1651 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1652 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1653 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1654 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1655 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1656 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1657 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1658 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1659 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1660 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1662 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1663 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1664 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1665 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1666 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1667 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1668 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1669 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1670 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1671 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1672 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1673 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1674 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1676 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1677 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1678 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1679 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1680 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1681 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1682 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1683 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1684 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1685 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1686 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1687 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1688 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1689 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1690 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1691 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1692 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1693 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1694 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1695 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1696 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1697 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1698 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1699 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1700 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1701 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1702 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1703 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1704 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1705 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1706 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1707 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1708 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1709 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1710 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1711 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1712 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1713 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1714 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1715 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1716 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1717 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1718 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1719 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1720 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1721 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1722 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1723 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1724 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1725 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1726 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1727 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1728 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1730 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1731 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1732 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1732A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1734 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1735 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1736 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1737 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1738 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1739 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1740 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1741 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1742 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1743 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1744 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1745 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1746 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1747 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1748 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1749 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1750 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1751 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1752 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1753 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1754 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1755 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1756 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1757 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1758 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1759 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1760 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1761 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1762 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1763 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1764 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1765 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1766 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1767 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1768 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1769 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1770 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1771 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1772 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1773 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1774 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1775 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1777 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1778 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1779 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1780 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1781 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1782 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1783 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1784 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1785 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1786 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1787 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1788 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1789 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1790 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1791 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1792 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1793 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1794 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1795 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1796 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1797 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1798 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1799 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1800 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1801 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1802 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1803 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1804 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1805 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1806 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1807 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1808 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1809 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1810 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1811 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1812 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1813 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1814 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1815 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1816 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1817 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1818 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1819 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1820 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1821 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1822 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1823 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1824 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1825 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1826 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1827 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1828 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1829 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1830 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1831 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1832 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1833 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1834 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1835 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1836 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1837 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1838 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1839 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1840 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1841 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1842 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1843 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1844 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1845 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1846 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1847 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1848 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1849 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1850 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1851 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1852 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1853 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1854 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1855 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1856 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1858 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1859 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1860 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1861 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1862 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1863 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1864 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1865 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1866 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1867 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1868 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1869 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1870 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1871 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1872 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1873 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1874 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1875 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1876 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1877 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1878 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1879 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1880 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1881 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1881A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1883 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1884 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1885 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1886 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1887 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1888 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1890 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1891 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1892 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1893 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1894 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1895 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1896 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1897 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1898 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1899 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1900 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1901 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1902 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1903 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1903A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1905 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1906 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1907 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1908 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1909 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1910 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1911 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1912 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1913 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1914 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1915 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1916 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1917 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1918 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1919 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1920 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1921 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1922 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1923 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1924 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1925 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1927 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1928 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1929 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1930 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1931 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1932 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1933 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1934 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1935 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1937 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1938 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1939 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1940 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1941 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1942 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1943 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1944 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1945 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1947 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1948 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1949 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1950 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1951 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1952 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1953 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1954 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1955 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1958 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1960 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1961 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1962 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1963 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1964 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1965 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1968 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1969 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1970 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1972 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1973 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1974 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1975 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1976 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1977 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1978 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1979 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1980 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1981 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1982 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1983 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1984 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1985 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1986 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1987 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1987A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1988 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1989 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1990 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1991 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1992 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1993 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1994 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1995 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1996 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1997 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1998 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_1999 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2000 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2001 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2002 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2003 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2004 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2009 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2010 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2011 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2012 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2013 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2014 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2015 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2016 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2019 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2020 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2021 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2022 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2023 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2024 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2025 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2026 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2027 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2028 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2029 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2030 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2031 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2032 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2033 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2034 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2035 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2036 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2037 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2038 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2039 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2040 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2041 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2042 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2043 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2044 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2045 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2046 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2047 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2048 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2049 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2050 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2051 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2052 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2053 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2054 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2055 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2056 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2057 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2058 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2060 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2061 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2062 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2063 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2064 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2065 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2066 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2067 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2068 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2069 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2070 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2071 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2072 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2073 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2074 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2075 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2076 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2077 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2078 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2079 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2080 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2081 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2082 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2084 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2085 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2086 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2087 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2088 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2089 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2090 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2091 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2092 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2093 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2094 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2095 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2096 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2097 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2098 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2099 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2100 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2101 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2102 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2104 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2105 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2106 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2107 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2108 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2109 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2110 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2111 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2112 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2113 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2114 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2115 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2116 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2118 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2119 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2120 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2121 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2122 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2123 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2124 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2125 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2126 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2127 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2128 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2129 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2130 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2131 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2132 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2133 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2134 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2135 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2136 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2137 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2138 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2139 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2140 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2141 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2142 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2143 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2144 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2145 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2146 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2147 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2148 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2149 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2150 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2151 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2152 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2153 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2154 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2155 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2156 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2157 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2158 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2159 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2160 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2161 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2162 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2163 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2164 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2165 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2167 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2168 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2169 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2170 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2171 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2172 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2173 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2174 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2175 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2176 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2177 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2178 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2179 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2180 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2181 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2182 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2183 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2184 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2185 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2186 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2187 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2188 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2189 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2190 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2191 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2193 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2195 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2196 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2197 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2198 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2199 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2200 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2201 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2202 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2203 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2204 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2205 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2206 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2207 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2208 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2209 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2210 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2211 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2212 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2213 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2214 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2215 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2216 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2218 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2219 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2220 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2221 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2222 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2223 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2224 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2225 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2226 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2227 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2228 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2229 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2230 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2231 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2232 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2233 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2234 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2235 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2236 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2237 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2238 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2239 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2240 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2242 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2243 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2244 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2245 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2246 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2248 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2249 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2250 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2251 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2252 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2253 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2254 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2255 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2257 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2258 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2260 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2261 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2262 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2263 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2264 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2265 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2266 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2267 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2268 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2269 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2270 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2271 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2272 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2273 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2274 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2275 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2276 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2277 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2278 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2279 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2280 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2281 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2282 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2283 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2284 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2285 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2286 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2287 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2288 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2289 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2290 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2291 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2292 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2293 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2294 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2295 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2296 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2297 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2298 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2299 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2300 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2301 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2302 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2303 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2304 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2305 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2306 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2307 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2308 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2309 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2310 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2313 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2315 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2316 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2317 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2318 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2319 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2320 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2321 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2322 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2323 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2324 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2325 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2326 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2327 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2328 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2329 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2330 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2331 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2332 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2333 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2334 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2335 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2336 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2337 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2338 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2339 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2340 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2341 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2342 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2343 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2344 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2345 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2346 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2347 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2348 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2349 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2350 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2351 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2352 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2353 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2354 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2355 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2356 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2357 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2358 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2359 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2360 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2361 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2362 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2363 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2364 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2365 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2366 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2367 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2368 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2369 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2370 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2371 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2372 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2373 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2374 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2375 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2376 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2378 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2379 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2380 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2381 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2382 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2383 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2384 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2385 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2386 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2387 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2389 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2390 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2391 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2392 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2393 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2394 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2395 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2396 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2397 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2398 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2399 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2400 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2402 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2403 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2404 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2405 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2406 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2407 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2408 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2409 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2410 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2411 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2412 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2413 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2414 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2415 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2416 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2417 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2418 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2419 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2420 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2421 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2422 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2423 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2424 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2425 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2427 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2428 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2432 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2433 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2434 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2435 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2436 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2437 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2438 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2439 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2440 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2441 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2442 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2443 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2444 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2445 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2446 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2447 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2448 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2449 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2450 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2451 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2452 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2453 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2454 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2455 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2456 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2457 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2458 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2459 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2460 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2461 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2462 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2463 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2465 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2466 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2467 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2468 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2469 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2470 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2471 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2472 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2473 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2474 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2475 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2477 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2478 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2479 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2480 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2481 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2482 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2482A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2483 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2486 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2487 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2488 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2489 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2490 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2493 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2493A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2495 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2496 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2497 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2498 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2499 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2500 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2502 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2504 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2505 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2506 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2507 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2508 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2509 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2509A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2511 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2512 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2513 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2515 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2515A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2516 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2517 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2518 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2520 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2521 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2522 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2523 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2524 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2525 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2526 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2527 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2528 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2529 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2530 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2532 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2533 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2534 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2535 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2536 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2537 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2538 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2539 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2540 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2541 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2542 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2543 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2544 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2545 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2546 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2547 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2548 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2549 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2550 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2551 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2552 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2553 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2555 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2556 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2557 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2558 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2559 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2561 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2562 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2563 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2564 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2565 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2566 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2567 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2568 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2569 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2570 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2571 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2572 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2573 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2574 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2575 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2576 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2577 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2578 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2579 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2580 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2581 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2582 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2583 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2584 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2585 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2586 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2587 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2588 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2589 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2590 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2591 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2592 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2593 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2594 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2595 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2596 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2597 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2598 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2599 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2600 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2601 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2602 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2603 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2604 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2605 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2606 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2607 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2608 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2609 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2610 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2611 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2612 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2613 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2614 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2615 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2616 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2617 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2618 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2619 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2620 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2621 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2622 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2623 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2624 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2625 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2626 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2627 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2628 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2629 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2630 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2631 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2632 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2633 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2634 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2635 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2636 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2637 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2638 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2639 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2640 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2642 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2643 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2644 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2645 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2646 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2647 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2648 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2649 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2650 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2651 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2652 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2653 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2654 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2655 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2656 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2657 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2658 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2659 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2662 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2663 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2664 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2665 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2666 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2667 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2668 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2669 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2670 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2671 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2672 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2673 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2674 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2675 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2676 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2677 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2678 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2679 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2680 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2681 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2682 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2683 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2684 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2685 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2686 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2688 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2689 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2690 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2691 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2692 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2693 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2694 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2695 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2696 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2697 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2698 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2699 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2700 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2701 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2702 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2703 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2704 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2705 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2706 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2707 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2708 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2709 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2710 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2711 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2712 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2713 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2714 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2715 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2716 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2717 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2718 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2719 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2720 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2721 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2722 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2723 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2724 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2725 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2726 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2727 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2728 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2729 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2730 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2731 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2732 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2733 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2734 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2736 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2737 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2738 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2739 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2740 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2741 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2742 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2743 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2744 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2745 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2746 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2747 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2748 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2749 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2750 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2751 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2752 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2753 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2754 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2755 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2756 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2757 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2759 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2760 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2761 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2762 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2763 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2764 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2765 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2766 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2767 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2768 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2769 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2770 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2771 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2772 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2773 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2774 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2775 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2776 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2777 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2778 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2779 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2780 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2781 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2782 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2783 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2784 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2785 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2786 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2787 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2788 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2789 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2790 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2791 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2792 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2793 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2795 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2796 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2797 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2798 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2799 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2800 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2801 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2802 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2803 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2804 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2805 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2806 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2807 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2808 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2809 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2810 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2811 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2812 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2813 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2814 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2815 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2816 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2817 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2818 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2819 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2820 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2821 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2822 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2823 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2824 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2824A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2826 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2827 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2828 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2829 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2830 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2832 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2833 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2834 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2835 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2836 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2837 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2838 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2839 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2841 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2842 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2843 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2844 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2844A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2845 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2846 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2847 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2848 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2849 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2850 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2851 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2852 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2853 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2854 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2855 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2856 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2857 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2857A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2858 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2860 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2861 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2862 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2863 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2864 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2865 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2866 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2867 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2868 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2869 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2870 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2871 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2872 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2873 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2874 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2875 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2876 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2877 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2878 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2879 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2880 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2881 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2882 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2883 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2884 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2885 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2886 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2887 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2888 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2888A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2890 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2891 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2892 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2892A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2892B and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2894 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2895 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2896 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2897 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2898 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2900 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2902 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2903 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2904 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2905 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2906 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2907 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2908 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2909 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2910 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2911 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2912 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2913 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2914 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2915 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2916 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2917 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2918 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2919 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2920 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2921 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2922 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2923 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2924 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2925 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2926 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2927 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2928 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2929 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2930 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2931 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2932 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2933 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2934 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2935 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2936 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2937 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2938 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2939 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2940 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2941 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2942 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2943 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2944 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2945 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2946 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2947 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2948 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2949 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2949A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2949B and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2950 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2950A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2951 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2952 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2953 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2955 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2956 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2957 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2958 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2959 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2960 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2961 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2962 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2963 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2964 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2965 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2966 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2967 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2968 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2969 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2970 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2971 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2972 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2973 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2974 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2975 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2976 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2977 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2978 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2979 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2980 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2981 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2982 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2983 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2984 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2985 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2986 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2987 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2988 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2989 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2990 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2991 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2991A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2993 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2994 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2995 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2996 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2997 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2998 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_2999 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3000 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3001 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3002 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3003 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3004 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3005 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3006 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3007 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3008 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3009 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3010 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3011 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3012 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3013 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3014 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3015 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3016 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3017 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3018 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3019 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3020 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3021 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3022 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3023 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3024 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3025 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3026 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3027 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3028 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3029 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3030 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3031 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3032 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3033 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3034 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3035 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3036 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3037 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3038 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3039 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3040 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3041 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3042 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3043 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3044 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3045 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3046 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3047 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3048 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3049 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3050 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3051.1 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3052 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3053 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3054 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3055 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3057 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3059 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3060 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3061 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3062 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3063 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3064 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3065 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3066 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3067 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3068 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3069 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3070 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3071 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3072 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3073 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3074 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3075 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3076 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3077 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3078 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3079 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3080 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3081 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3082 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3083 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3084 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3085 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3086 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3087 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3088 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3089 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3090 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3091 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3092 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3093 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3094 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3095 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3096 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3097 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3098 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3101 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3102 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3103 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3104 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3105 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3106 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3107 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3108 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3109 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3110 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3111 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3113 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3114 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3115 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3116 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3117 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3118 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3119 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3120 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3121 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3122 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3123 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3124 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3125 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3126 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3127 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3128 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3129 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3130 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3131 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3131A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3133 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3134 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3135 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3136 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3137 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3138 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3139 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3140 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3141 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3142 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3143 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3144 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3145 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3146 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3147 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3148 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3149 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3150 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3151 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3152 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3153 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3154 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3155 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3156 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3157 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3158 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3159 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3160 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3161 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3162 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3163 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3165 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3166 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3167 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3168 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3169 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3170 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3171 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3172 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3173 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3175 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3176 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3177 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3178 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3179 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3179A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3181 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3182 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3183 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3184 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3185 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3186 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3187 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3188 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3190 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3191 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3192 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3193 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3194 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3195 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3196 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3197 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3198 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3199 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3200 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3201 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3202 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3203 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3204 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3205 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3206 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3207 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3208 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3209 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3210 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3211 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3212 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3213 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3214 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3215 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3216 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3217 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3218 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3219 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3220 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3221 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3222 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3223 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3224 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3225 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3226 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3227 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3228 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3229 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3230 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3231 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3232 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3233 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3234 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3235 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3236 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3237 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3238 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3239 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3240 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3241 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3242 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3242A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3243 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3244 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3245 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3246 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3247 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3248 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3249 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3250 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3251 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3252 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3253 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3255 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3256 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3257 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3258 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3259 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3260 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3261 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3262 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3263 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3264 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3265 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3267 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3268 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3269 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3270 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3271 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3272 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3273 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3274 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3275 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3276 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3277 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3278 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3279 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3280 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3281 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3282 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3283 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3284 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3285 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3286 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3287 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3288 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3289 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3290 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3291 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3292 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3293 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3294 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3295 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3296 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3297 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3298 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3299 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3300 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3301 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3302 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3303 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3304 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3305 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3306 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3307 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3308 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3309 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3310 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3311 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3312 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3313 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3314 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3315 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3316 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3317 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3318 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3319 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3320 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3321 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3322 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3323 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3324 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3325 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3326 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3327 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3328 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3329 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3330 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3331 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3332 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3333 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3334 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3335 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3336 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3338 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3339 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3341 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3342 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3342A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3343 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3344 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3345 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3345A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3347 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3348 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3349 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3350 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3351 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3353 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3354 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3355 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3356 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3357 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3358 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3359 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3360 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3361 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3362 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3363 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3364 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3365 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3366 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3367 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3369 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3370 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3371 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3372 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3373 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3374 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3375 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3376 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3377 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3378 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3379 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3380 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3381 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3382 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3383 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3384 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3385 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3386 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3387 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3388 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3389 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3390 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3391 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3392 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3393 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3394 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3395 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3396 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3397 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3398 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3399 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3400 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3401 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3402 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3403 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3405 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3406 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3407 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3408 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3409 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3410 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3411 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3412 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3413 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3414 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3415 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3416 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3417 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3418 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3419 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3420 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3421 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3422 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3423 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3424 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3425 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3426 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3427 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3428 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3429 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3430 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3431 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3432 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3433 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3434 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3435 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3436 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3437 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3438 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3439 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3440 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3441 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3442 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3443 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3444 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3445 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3446 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3447 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3448 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3450 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3451 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3452 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3453 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3455 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3456 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3457 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3458 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3459 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3460 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3461 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3462 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3463 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3464 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3465 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3466 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3467 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3468 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3469 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3470 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3472 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3473 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3474 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3475 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3476 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3477 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3478 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3479 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3480 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3481 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3482 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3483 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3484 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3485 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3486 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3487 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3488 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3489 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3490 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3491 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3492 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3493 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3494 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3495 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3496 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3497 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3498 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3499 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3500 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3501 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3502 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3503 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3504 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3505 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3506 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3507 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3508 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3509 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3510 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3511 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3512 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3513 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3514 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3515 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3516 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3517 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3518 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3519 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3521 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3522 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3523 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3525 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3526 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3527 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3528 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3529 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3530 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3531 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3532 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3533 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3534 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3535 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3536 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3537 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3538 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3539 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3540 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3541 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3542 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3543 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3544 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3545 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3546 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3547 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3548 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3549 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3550 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3551 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3552 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3553 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3554 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3555 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3556 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3557 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3558 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3559 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3560 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3561 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3562 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3563 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3564 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3565 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3566 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3567 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3568 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3569 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3570 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3571 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3572 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3573 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3574 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3575 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3576 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3577 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3578 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3579 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3580 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3581 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3582 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3583 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3584 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3585 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3586 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3587 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3588 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3589 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3590 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3591 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3592 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3593 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3594 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3595 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3596 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3597 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3598 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3599 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3600 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3601 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3602 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3603 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3604 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3605 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3606 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3607 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3608 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3609 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3610 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3611 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3612 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3613 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3614 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3615 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3616 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3617 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3618 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3619 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3620 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3621 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3622 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3623 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3624 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3625 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3626 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3627 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3628 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3629 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3630 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3631 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3632 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3633 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3634 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3635 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3636 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3637 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3638 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3639 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3640 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3641 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3642 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3643 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3644 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3645 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3646 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3647 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3649 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3650 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3651 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3652 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3653 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3654 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3655 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3656 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3657 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3658 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3659 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3660 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3661 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3662 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3663 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3664 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3665 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3666 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3667 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3668 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3669 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3670 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3671 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3672 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3673 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3674 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3675 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3676 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3677 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3678 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3680 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3681 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3682 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3683 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3684 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3685 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3686 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3689 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3690 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3691 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3692 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3693 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3694 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3695 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3696 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3697 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3698 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3699 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3700 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3701 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3702 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3703 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3704 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3705 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3706 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3708 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3709 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3710 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3711 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3714 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3715 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3716 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3717 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3718 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3719 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3720 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3721 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3722 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3723 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3724 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3725 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3726 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3727 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3728 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3729 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3730 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3731 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3732 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3733 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3734 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3735 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3736 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3737 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3738 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3739 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3740 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3741 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3742 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3743 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3744 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3745 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3746 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3747 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3748 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3749 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3750 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3752 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3753 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3755 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3756 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3757 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3758 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3759 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3760 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3761 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3762 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3763 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3764 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3765 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3765A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3766 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3767 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3768 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3769 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3770 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3771 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3772 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3773 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3774 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3775 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3776 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3777 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3778 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3779 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3780 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3781 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3782 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3783 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3784 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3785 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3786 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3787 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3788 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3789 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3790 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3791 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3792 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3793 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3794 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3795 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3796 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3797 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3798 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3799 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3800 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3801 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3802 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3803 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3804 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3805 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3806 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3807 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3808 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3809 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3810 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3811 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3812 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3813 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3814 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3815 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3816 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3817 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3818 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3819 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3820 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3821 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3822 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3823 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3824 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3825 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3826 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3827 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3828 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3829 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3830 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3831 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3832 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3833 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3834 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3835 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3836 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3836A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3838 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3839 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3840 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3841 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3842 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3843 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3844 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3845 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3846 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3847 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3848 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3849 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3850 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3851 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3852 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3853 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3854 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3855 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3856 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3857 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3858 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3859 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3860 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3861 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3862 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3865 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3866 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3867 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3868 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3868A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3870 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3871 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3872 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3873 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3874 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3875 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3876 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3877 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3878 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3879 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3880 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3881 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3882 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3883 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3884 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3885 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3886 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3887 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3888 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3889 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3890 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3891 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3892 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3893 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3894 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3895 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3896 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3897 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3898 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3899 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3900 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3901 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3902 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3903 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3904 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3905 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3906 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3907 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3908 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3909 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3910 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3911 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3912 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3913 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3914 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3915 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3916 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3917 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3918 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3919 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3920 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3921 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3922 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3923 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3924 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3925 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3926 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3927 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3928 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3929 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3930 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3931 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3932 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3933 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3934 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3935 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3936 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3937 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3938 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3939 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3940 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3941 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3942 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3943 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3944 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3945 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3946 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3947 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3948 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3949 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3950 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3951 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3952 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3953 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3954 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3955 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3956 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3958 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3959 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3960 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3961 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3962 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3963 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3964 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3965 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3966 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3967 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3968 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3969 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3970 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3971 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3971A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3973 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3974 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3975 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3976 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3977 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3978 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3979 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3980 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3981 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3982 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3983 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3984 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3985 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3986 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3987 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3988 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3989 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3990 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3991 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3992 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3993 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3994 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3995 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3996 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3997 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3998 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_3999 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4000 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4001 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4002 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4003 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4004 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4005 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4006 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4007 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4008 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4009 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4010 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4011 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4012 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4013 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4014 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4015 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4016 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4017 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4018 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4019 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4020 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4021 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4022 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4023 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4024 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4025 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4026 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4027 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4028 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4029 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4030 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4031 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4032 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4033 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4034 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4035 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4036 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4037 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4038 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4039 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4040 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4041 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4042 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4043 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4044 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4045 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4046 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4047 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4048 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4049 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4050 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4051 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4052 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4053 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4054 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4055 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4056 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4057 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4058 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4059 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4060 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4061 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4062 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4063 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4064 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4065 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4066 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4067 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4068 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4069 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4070 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4071 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4072 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4073 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4074 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4075 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4076 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4077 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4078 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4079 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4080 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4081 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4082 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4083 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4084 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4085 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4086 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4087 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4088 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4089 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4090 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4091 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4093 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4094 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4095 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4096 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4097 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4098 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4099 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4100 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4101 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4102 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4103 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4104 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4105 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4106 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4107 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4108 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4109 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4110 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4111 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4112 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4113 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4114 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4115 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4116 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4117 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4118 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4119 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4120 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4121 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4122 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4122A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4124 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4125 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4125A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4126A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4126B and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4126C and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4127 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4128 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4128A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4128B and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4128C and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4128D and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4131 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4132 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4135 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4138 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4139 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4141 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4142 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4143 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4144 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4145 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4146 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4147 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4148 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4149 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4150 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4151 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4152 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4153 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4154 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4155 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4156 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4157 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4158 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4159 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4160 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4161 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4162 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4163 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4164 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4165 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4166 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4167 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4168 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4169 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4170 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4171 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4172 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4173 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4174 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4175 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4176 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4177 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4178 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4179 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4181 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4182 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4183 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4184 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4186 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4187 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4188 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4189 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4190 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4191 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4192 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4193 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4194 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4195 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4196 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4197 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4198 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4199 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4200 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4201 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4202 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4203 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4204 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4205 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4207 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4208 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4209 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4211 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4212 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4213 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4214 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4215 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4216 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4217 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4218 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4219 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4220 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4221 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4222 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4223 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4224 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4225 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4226 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4227 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4228 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4229 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4230 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4231 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4233 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4234 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4235 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4236 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4237 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4238 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4239 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4240 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4241 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4242 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4243 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4244 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4245 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4246 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4247 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4248 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4249 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4250 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4251 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4252 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4253 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4254 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4255 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4256 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4257 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4258 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4259 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4260 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4261 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4262 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4263 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4264 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4265 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4266 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4267 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4268 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4269 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4270 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4271 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4272 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4273 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4274 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4275 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4276 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4277 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4278 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4280 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4281 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4282 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4283 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4284 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4285 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4286 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4287 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4288 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4290 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4291 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4292 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4293 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4294 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4295 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4297 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4300 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4301 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4304 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4305 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4306 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4307 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4308 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4309 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4310 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4311 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4312 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4313 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4314 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4315 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4316 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4317 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4318 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4319 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4320 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4321 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4322 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4323 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4324 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4325 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4326 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4327 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4329 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4330 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4331 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4332 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4334 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4335 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4336 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4337 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4338 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4339 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4340 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4341 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4342 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4343 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4344 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4345 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4346 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4347 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4348 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4349 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4350 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4351 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4352 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4353 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4354 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4355 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4356 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4357 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4358 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4359 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4360 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4361 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4362 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4363 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4364 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4366 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4367 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4368 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4369 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4370 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4371 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4372 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4373 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4374 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4375 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4376 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4377 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4378 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4379 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4380 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4381 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4382 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4383 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4384 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4385 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4386 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4387 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4388 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4389 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4390 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4391 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4392 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4393 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4394 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4395 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4396 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4397 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4398 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4399 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4400 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4401 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4402 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4403 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4404 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4405 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4406 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4407 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4408 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4409 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4410 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4411 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4413 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4414 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4415 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4416 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4417 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4418 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4419 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4420 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4421 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4422 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4423 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4424 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4425 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4426 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4427 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4428 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4429 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4430 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4431 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4432 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4433 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4435 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4436 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4437 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4438 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4439 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4440 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4441 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4442 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4443 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4444 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4445 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4446 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4447 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4448 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4449 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4450 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4451 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4452 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4453 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4454 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4455 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4456 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4457 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4458 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4459 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4460 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4461 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4462 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4463 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4464 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4465 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4466 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4467 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4468 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4469 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4470 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4471 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4472 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4473 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4474 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4476 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4477 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4478 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4479 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4480 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4481 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4482 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4483 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4484 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4486 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4487 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4488 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4489 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4490 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4491 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4493 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4494 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4495 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4496 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4497 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4498 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4499 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4500 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4501 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4502 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4503 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4504 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4505 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4506 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4507 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4508 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4509 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4510 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4511 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4512 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4513 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4514 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4515 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4516 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4517 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4518 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4519 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4520 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4522 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4523 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4524 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4525 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4526 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4527 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4528 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4529 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4530 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4531 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4532 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4533 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4534 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4535 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4536 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4537 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4538 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4539 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4540 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4542 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4543 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4543A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4545 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4546 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4548 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4549 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4550 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4551 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4552 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4553 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4554 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4555 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4556 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4558 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4559 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4560 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4561 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4563 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4564 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4565 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4566 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4567 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4568 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4569 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4570 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4571 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4572A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4574 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4575 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4576 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4577 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4578 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4579 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4580 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4581 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4582 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4583 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4584 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4585 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4586 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4587 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4588 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4589 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4590 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4591 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4592 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4593 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4594 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4595 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4596 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4597 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4598 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4599 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4600 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4601 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4603 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4604 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4606 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4607 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4608 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4609 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4610 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4611 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4612 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4613 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4614 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4615 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4616 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4617 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4619 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4620 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4621 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4622 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4623 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4624 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4625 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4626 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4627 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4628 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4629 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4630 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4631 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4632 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4633 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4634 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4635 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4636 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4637 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4638 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4639 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4639A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4641 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4642 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4643 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4644 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4645 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4646 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4647 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4648 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4649 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4650 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4651 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4652 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4653 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4654 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4655 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4656 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4657 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4658 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4659 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4660 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4661 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4662 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4663 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4664 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4665 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4666 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4667 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4668 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4669 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4670 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4671 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4672 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4673 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4674 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4675 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4676 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4677 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4678 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4679 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4680 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4681 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4682 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4683 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4684 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4685 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4686 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4687 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4688 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4689 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4690 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4691 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4692 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4693 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4694 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4695 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4696 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4697 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4698 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4699 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4700 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4701 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4702 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4703 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4704 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4705 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4706 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4707 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4708 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4709 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4710 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4711 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4712 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4713 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4714 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4715 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4716 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4717 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4718 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4719 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4720 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4721 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4722 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4723 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4724 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4725 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4726 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4727 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4728 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4729 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4730 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4731 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4732 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4733 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4734 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4736 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4737 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4738 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4739 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4741 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4742 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4744 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4745 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4746 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4747 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4748 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4749 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4750 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4751 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4752 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4753 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4754 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4755 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4756 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4758 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4759 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4760 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4761 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4762 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4763 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4764 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4765 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4766 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4767 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4768 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4769 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4769A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4771 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4772 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4773 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4777 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4778 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4779 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4780 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4781 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4782 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4783 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4784 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4785 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4786 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4787 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4788 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4789 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4790 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4791 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4792 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4793 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4794 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4795 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4796 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4797 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4798 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4799 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4800 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4801 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4802 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4803 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4804 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4805 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4806 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4807 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4808 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4809 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4810 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4811 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4812 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4813 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4814 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4815 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4816 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4817 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4818 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4819 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4820 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4821 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4823 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4824 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4825 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4826 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4828 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4829 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4830 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4831 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4832 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4833 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4834 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4835 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4836 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4837 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4838 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4840 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4841 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4842 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4843 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4844 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4845 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4846 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4847 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4848 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4849 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4850 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4851 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4852 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4853 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4854 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4855 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4858 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4859 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4860 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4861 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4862 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4863 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4865 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4866 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4867 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4868 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4869 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4870 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4872 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4873 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4874 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4876 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4877 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4878 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4879 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4880 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4881 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4882 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4883 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4884 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4885 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4886 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4887 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4888 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4889 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4890 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4891 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4892 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4893 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4894 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4895 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4896 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4897 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4898 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4899 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4900 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4901 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4902 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4903 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4904 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4905 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4906 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4907 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4908 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4909 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4910 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4911 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4912 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4913 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4914 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4915 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4916 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4917 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4918 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4919 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4920 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4921 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4922 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4923 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4924 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4925 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4926 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4927 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4928 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4929 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4930 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4931 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4932 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4933 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4934 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4935 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4936 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4937 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4938 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4939 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4940 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4941 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4942 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4943 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4944 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4945 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4946 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4947 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4948 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4949 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4950 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4951 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4952 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4953 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4954 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4955 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4956 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4957 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4958 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4959 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4960 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4961 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4962 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4963 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4964 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4965 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4966 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4967 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4968 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4969 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4970 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4971 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4972 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4973 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4974 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4975 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4976 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4977 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4978 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4979 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4980 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4981 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4982 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4983 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4984 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4985 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4986 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4987 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4988 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4989 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4990 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4991 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4992 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4993 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4994 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4995 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4996 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4997 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4998 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_4999 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5000 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5001 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5002 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5003 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5005 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5006 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5007 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5008 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5009 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5010 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5011 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5012 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5013 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5014 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5015 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5016 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5017 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5018 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5019 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5020 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5021 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5022 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5023 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5024 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5025 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5026 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5027 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5028 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5029 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5030 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5031 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5033 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5034 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5035 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5037 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5038 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5039 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5040 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5041 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5042 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5043 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5044 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5045 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5046 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5047 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5048 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5049 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5050 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5051 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5052 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5053 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5054 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5055 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5056 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5057 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5058 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5059 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5060 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5061 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5062 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5063 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5064 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5065 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5066 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5067 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5068 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5069 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5070 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5071 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5072 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5073 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5074 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5075 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5076 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5077 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5078 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5079 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5080 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5081 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5082 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5082A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5083 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5083A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5084 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5085 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5086 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5087 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5088 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5089 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5090 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5091 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5092 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5093 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5094 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5095 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5096 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5097 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5098 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5099 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5100 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5101 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5102 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5103 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5104 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5105 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5106 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5107 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5108 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5109 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5110 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5111 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5112 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5113 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5114 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5115 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5116 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5117 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5118 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5119 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5120 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5123 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5124 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5124A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5126 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5127 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5128 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5129 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5130 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5131 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5132 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5133 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5134 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5135 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5136 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5137 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5138 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5139 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5140 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5141 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5142 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5143 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5144 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5145 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5146 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5147 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5148 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5149 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5150 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5151 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5152 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5153 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5154 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5155 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5156 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5157 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5158 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5159 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5160 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5161 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5162 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5163 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5164 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5165 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5166 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5167 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5168 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5169 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5170 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5171 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5172 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5173 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5174 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5175 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5176 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5177 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5178 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5179 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5180 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5181 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5182 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5183 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5184 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5185 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5186 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5187 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5188 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5189 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5190 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5191 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5192 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5192A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5193 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5194 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5195 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5196 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5197 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5198 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5199 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5200 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5202 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5203 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5204 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5205 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5206 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5207 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5208 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5209 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5210 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5211 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5212 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5213 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5214 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5215 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5216 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5217 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5218 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5219 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5220 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5221 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5222 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5223 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5224 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5225 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5226 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5227 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5228 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5229 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5230 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5231 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5232 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5233 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5234 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5235 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5236 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5237 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5239 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5240 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5241 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5242 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5243 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5244 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5245 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5246 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5247 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5248 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5249 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5250 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5251 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5252 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5253 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5254 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5255 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5256 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5257 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5258 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5259 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5260 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5261 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5262 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5263 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5264 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5265 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5266 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5267 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5268 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5269 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5270 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5271 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5272 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5273 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5274 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5275 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5276 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5277 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5278 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5279 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5280 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5281 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5282A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5283 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5284 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5285 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5287 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5288 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5290 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5292 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5293 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5294 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5295 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5296 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5297 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5298 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5299 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5300 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5301 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5302 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5303 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5304 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5305 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5306 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5307 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5308 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5309 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5310 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5311 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5312 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5313 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5314 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5315 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5316 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5317 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5318 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5319 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5320 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5321 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5322 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5323 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5324 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5325 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5326 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5327 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5328 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5329 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5330 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5331 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5332 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5333 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5334 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5335 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5336 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5337 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5338 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5339 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5340 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5341 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5342 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5343 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5344 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5345 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5346 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5347 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5348 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5349 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5350 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5351 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5352 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5353 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5354 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5355 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5356 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5357 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5358 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5359 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5361 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5362 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5363 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5364 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5365 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5366 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5367 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5368 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5369 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5370 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5371 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5372 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5373 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5374 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5375 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5376 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5377 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5378 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5379 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5380 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5381 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5382 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5383 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5384 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5385 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5386 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5387 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5388 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5389 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5390 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5391 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5392 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5393 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5394 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5395 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5396 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5397 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5398 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5399 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5400 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5401 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5402 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5403 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5404 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5405 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5406 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5407 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5408 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5409 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5410 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5411 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5412 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5413 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5414 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5415 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5416 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5417 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5418 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5419 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5420 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5421 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5422 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5423 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5424 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5425 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5426 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5427 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5428 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5429 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5430 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5431 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5432 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5433 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5434 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5435 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5436 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5437 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5438 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5439 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5440 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5441 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5442 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5443 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5444 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5445 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5446 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5447 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5448 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5449 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5453 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5454 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5455 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5456 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5457 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5458 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5459 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5460 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5461 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5462 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5463 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5464 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5465 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5466 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5467 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5468 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5469 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5470 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5471 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5472 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5473 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5474 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5475 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5476 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5477 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5478 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5479 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5480 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5481 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5482 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5483 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5484 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5485 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5486 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5487 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5488 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5489 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5490 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5491 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5492 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5493 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5494 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5495 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5496 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5497 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5498 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5499 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5500 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5501 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5502 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5503 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5504 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5505 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5506 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5507 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5508 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5509 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5510 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5511 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5512 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5513 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5514 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5515 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5516 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5517 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5518 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5519 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5520 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5521 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5522 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5523 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5524 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5525 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5526 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5527 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5528 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5529 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5530 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5531 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5532 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5533 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5534 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5535 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5536 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5537 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5538 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5539 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5540 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5541 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5542 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5543 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5544 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5545 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5546 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5547 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5548 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5549 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5550 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5551 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5552 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5553 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5554 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5555 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5556 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5557 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5558 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5559 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5560 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5561 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5562 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5563 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5564 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5565 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5566 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5567 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5568 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5569 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5570 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5571 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5572 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5573 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5574 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5575 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5576 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5577 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5578 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5579 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5580 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5581 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5582 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5583 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5584 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5585 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5586 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5587 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5588 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5589 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5590 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5591 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5592 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5593 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5594 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5595 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5596 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5597 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5598 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5599 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5600 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5601 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5602 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5603 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5604 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5605 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5606 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5607 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5608 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5609 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5610 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5611 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5612 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5613 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5614 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5615 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5616 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5617 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5618 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5619 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5621 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5622 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5623 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5624 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5625 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5626 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5627 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5628 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5629 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5630 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5631 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5632 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5633 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5634 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5635 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5636 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5637 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5638 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5639 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5640 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5641 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5642 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5643 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5644 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5645 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5646 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5647 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5648 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5649 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5650 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5651 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5652 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5653 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5654 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5656 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5657 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5658 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5659 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5660 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5661 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5662 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5663 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5664 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5665 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5666 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5667 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5668 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5669 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5670 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5671 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5672 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5673 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5674 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5675 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5676 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5677 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5678 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5679 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5680 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5681 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5682 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5683 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5684 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5685 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5686 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5687 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5688 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5689 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5690 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5691 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5692 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5693 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5694 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5695 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5696 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5697 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5698 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5699 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5700 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5701 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5702 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5704 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5705 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5706 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5707 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5708 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5709 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5710 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5711 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5712 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5713 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5714 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5715 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5716 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5717 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5718 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5719 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5720 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5721 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5722 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5723 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5724 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5725 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5726 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5727 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5728 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5729 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5730 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5731 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5732 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5733 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5734 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5735 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5736 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5737 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5738 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5739 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5740 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5741 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5742 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5743 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5744 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5747 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5748 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5749 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5750 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5751 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5752 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5753 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5754 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5755 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5756 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5757 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5758 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5759 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5760 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5761 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5762 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5763 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5764 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5765 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5766 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5767 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5768 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5769 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5770 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5771 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5772 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5773 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5774 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5775 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5776 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5777 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5778 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5779 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5780 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5782 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5783 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5784 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5785 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5786 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5787 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5788 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5789 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5790 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5791 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5792 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5793 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5794 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5795 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5796 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5797 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5798 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5799 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5800 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5801 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5802 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5803 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5804 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5805 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5806 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5807 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5808 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5809 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5810 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5811 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5812 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5813 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5814 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5815 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5816 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5817 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5818 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5819 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5820 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5821 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5822 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5823 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5824 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5825 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5826 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5827 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5828 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5829 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5830 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5831 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5832 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5833 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5834 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5835 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5836 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5837 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5838 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5839 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5839A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5841 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5842 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5843 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5844 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5845 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5846 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5847 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5848 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5849 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5851 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5852 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5853 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5854 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5855 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5856 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5857 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5858 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5859 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5860 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5862 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5863 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5864 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5865 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5866 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5867 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5868 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5869 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5870 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5871 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5872 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5873 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5874 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5875 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5876 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5877 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5878 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5879 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5880 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5881 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5882 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5883 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5885 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5886 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5887 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5888 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5889 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5890 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5891 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5892 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5893 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5894 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5895 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5896 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5897 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5898 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5899 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5900 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5901 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5902 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5903 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5904 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5905 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5906 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5907 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5908 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5909 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5910 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5911 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5912 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5913 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5914 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5915 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5916 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5917 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5918 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5919 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5920 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5921 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5922 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5923 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5924 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5925 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5926 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5927 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5928 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5929 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5930 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5931 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5932 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5933 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5934 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5935 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5936 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5937 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5938 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5939 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5940 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5941 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5942 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5943 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5944 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5945 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5946 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5947 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5948 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5949 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5950 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5951 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5952 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5953 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5954 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5955 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5956 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5957 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5958 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5959 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5960 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5961 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5962 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5963 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5964 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5965 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5966 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5967 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5968 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5969 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5970 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5971 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5972 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5973 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5974 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5975 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5976 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5978 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5979 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5980 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5981 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5982 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5983 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5984 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5986 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5987 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5988 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5989 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5990 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5991 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5992 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5993 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5994 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5995 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5996 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5997 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5998 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_5999 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6000 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6001 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6002 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6003 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6004 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6005 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6006 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6007 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6008 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6009 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6010 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6011 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6012 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6013 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6014 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6015 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6016 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6017 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6018 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6019 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6020 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6021 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6022 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6023 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6024 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6025 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6026 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6027 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6028 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6029 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6030 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6031 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6032 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6033 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6034 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6035 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6036 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6037 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6039 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6040 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6041 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6042 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6043 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6044 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6045 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6046 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6047 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6048 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6049 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6050 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6051 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6052 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6053 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6054 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6055 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6056 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6057 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6058 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6059 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6060 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6061 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6062 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6063 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6064 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6065 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6066 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6067 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6070 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6071 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6072 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6073 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6074 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6075 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6076 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6077 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6078 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6079 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6080 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6081 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6082 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6083 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6084 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6085 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6086 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6086A and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6087 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6088 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6089 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6090 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6091 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6092 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6093 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6094 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6095 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6096 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6097 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6098 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6099 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6100 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6101 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6102 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6103 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6104 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6105 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6106 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6107 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6108 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6109 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6110 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6111 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6112 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6113 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6114 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6115 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6116 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6117 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6118 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6119 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6120 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6121 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6122 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6123 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6124 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6125 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6126 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6127 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6128 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6129 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6130 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6131 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6132 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6133 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6134 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6135 and all its 'children'.
    Fixing coordinates of gene gene:PFLU_6136 and all its 'children'.
:::
:::

::: {.cell .markdown}
### Finally write the new record as gff to disk. {#finally-write-the-new-record-as-gff-to-disk}
:::

::: {.cell .code execution_count="669"}
``` {.python}
with open("MPB_ASM001_20210401.gff3", "w") as fh:
    GFF.write([fixed_record], fh)
```
:::
