from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.SeqRecord import SeqRecord
from collections import namedtuple, OrderedDict
import copy
import gffutils as gff
import pandas
import re
from gffutils import biopython_integration as gffbio
from tqdm import tqdm
import re
import distance
import time
import logging

def get_feature_counts(db):
    return  dict(
                    [
                        (
                            feature_name,
                            feature_count
                        )
                        for feature_name, feature_count in zip(
                            [
                                ft for ft in db.featuretypes()
                            ],
                            [
                                db.count_features_of_type(ft) for ft in db.featuretypes()
                            ]
                        )
                    ]
                )

def db_to_seqrecord(db, seq, name='MPBAS00001', id='MPBAS00001'):
    seq_record = SeqRecord(seq=seq, name=name, id=id)

    for feat in tqdm(db.all_features(order_by=['start', 'featuretype', 'end'])):
        seqfeat = gffbio.to_seqfeature(feat)
        seqfeat.qualifiers['seqid'] = [id]
        seq_record.features.append(seqfeat)
    return seq_record
        
def serialize(db, seq, fname, name='MPBAS00001', id='MPB00001'):
    with open(fname, 'w') as OFH:
        GFF.write([db_to_seqrecord(db, seq, name, id)], OFH, include_fasta=False)