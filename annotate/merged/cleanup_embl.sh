#! /bin/bash

patch MPBAS00001.embl reference.patch

cat MPBAS00001.embl | sed -e "s/\"\/product=//" | sed -e "s/LOCUS/misc/" | sed -e "s/PFLU_PFLU/PFLU/" |  grep -v "\/anticodon=" | gzip - > MPBAS00001.embl.gz

zcat MPBAS00001.embl.gz > MPBAS00001.embl
