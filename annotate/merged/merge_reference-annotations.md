# Merge reference annotations into the Sanger2009 + Ensembl2015 annotation

## Module imports


```python
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.SeqRecord import SeqRecord
from IPython.display import Markdown as md
from collections import namedtuple, OrderedDict
import copy
import gffutils as gff
import pandas
import re
from gffutils import biopython_integration as gffbio
from tqdm import tqdm
import re
import logging
logging.basicConfig(level=logging.INFO)
```


```python
from gffutils.helpers import merge_attributes
import copy
```

## Load genome assembly


```python
with open("gnl_MPB_PFLU_1-20210723_.fasta", 'r') as fh:
    sbw25 = SeqIO.read(fh, "fasta")
```

## Load annotation databases

### RefSeq annotation DB (RS, rs)


```python
rsdb = gff.create_db('/scratch/grotec/sbw25/transferred_annotations/refseq_GCF000009225.2/gnl_MPB_PFLU_1__refseq.gff', ":memory:",id_spec="Name", force=True, checklines=1000, merge_strategy="create_unique" )
```


```python
## Feature counts:
rsdb_feature_counts = {}
for ft in rsdb.featuretypes():
    rsdb_feature_counts[ft] = rsdb.count_features_of_type(ft)

rsdb_feature_counts=pandas.DataFrame.from_dict(rsdb_feature_counts, orient='index')
rsdb_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6069</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6153</td>
    </tr>
    <tr>
      <th>ncRNA</th>
      <td>3</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>7</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66</td>
    </tr>
    <tr>
      <th>tmRNA</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>



### Ensembl/EBI/EMBL annotation DB (EBI, ebi)


```python
ebidb = gff.create_db('gnl_MPB_PFLU_1-20210416_.gff3', ":memory:",id_spec="Name", force=True, checklines=1000, merge_strategy="create_unique" )

```


```python
## Feature counts:
ebidb_feature_counts = {}
for ft in ebidb.featuretypes():
    ebidb_feature_counts[ft] = ebidb.count_features_of_type(ft)

ebidb_feature_counts=pandas.DataFrame.from_dict(ebidb_feature_counts, orient='index')
ebidb_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>11293</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>6098</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>82</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>79</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>79</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66</td>
    </tr>
  </tbody>
</table>
</div>



### Genbank annotation DB (GB, gb)


```python
gbdb = gff.create_db('/scratch/grotec/sbw25/transferred_annotations/genbank_GCA000009225.1/gnl_MPB_PFLU_1__genbank.gff', ":memory:",id_spec="Name", force=True, checklines=1000, merge_strategy="create_unique" )

```


```python
## Feature counts:
gbdb_feature_counts = {}
for ft in gbdb.featuretypes():
    gbdb_feature_counts[ft] = gbdb.count_features_of_type(ft)

gbdb_feature_counts=pandas.DataFrame.from_dict(gbdb_feature_counts, orient='index')
gbdb_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6044</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6018</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>15</td>
    </tr>
    <tr>
      <th>misc_feature</th>
      <td>16929</td>
    </tr>
    <tr>
      <th>protein_bind</th>
      <td>1</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>14</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>3375</td>
    </tr>
    <tr>
      <th>sig_peptide</th>
      <td>1131</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>1</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66</td>
    </tr>
  </tbody>
</table>
</div>



### Pseudomonas.com annotation DB (PDC, pdc)


```python
pdcdb = gff.create_db('/scratch/grotec/sbw25/transferred_annotations/pdc_GCF000009225.2/gnl_MPB_PFLU_1__pdc.gff', ":memory:",id_spec="Name", force=True, checklines=1000, merge_strategy="create_unique" )
```


```python
## Feature counts:
pdcdb_feature_counts = {}
for ft in pdcdb.featuretypes():
    pdcdb_feature_counts[ft] = pdcdb.count_features_of_type(ft)

pdcdb_feature_counts=pandas.DataFrame.from_dict(pdcdb_feature_counts, orient='index')
pdcdb_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6106</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>15</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66</td>
    </tr>
  </tbody>
</table>
</div>



### gnl_MPB_PFLU_1  annotation DB (latest version) (gnl, mpb, GNL, MPB)


```python
gnldb = gff.create_db('gnl_MPB_PFLU_1-20210825_.gff3', ':memory:')
```


```python
## Feature counts:
gnldb_feature_counts = {}
for ft in gnldb.featuretypes():
    gnldb_feature_counts[ft] = gnldb.count_features_of_type(ft)

gnldb_feature_counts=pandas.DataFrame.from_dict(gnldb_feature_counts, orient='index')
gnldb_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>1</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>20643</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>1</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>6003</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>82</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>1</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>79</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>95</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>79</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>3389</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>61</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>2</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>15</td>
    </tr>
  </tbody>
</table>
</div>




```python
db_feature_counts = pandas.concat([rsdb_feature_counts, ebidb_feature_counts, gbdb_feature_counts, pdcdb_feature_counts, gnldb_feature_counts], axis=1, join='outer')
```


```python
db_feature_counts.columns = ['RefSeq', 'EBI', 'GenBank', 'Pseudomonas.com', 'MPB']
```


```python
db_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RefSeq</th>
      <th>EBI</th>
      <th>GenBank</th>
      <th>Pseudomonas.com</th>
      <th>MPB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6069.0</td>
      <td>5921.0</td>
      <td>6044.0</td>
      <td>5921.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6153.0</td>
      <td>5921.0</td>
      <td>6018.0</td>
      <td>6106.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA</th>
      <td>3.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>7.0</td>
      <td>NaN</td>
      <td>14.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
    </tr>
    <tr>
      <th>tmRNA</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>NaN</td>
      <td>11293.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>20643.0</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>NaN</td>
      <td>6098.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6003.0</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>NaN</td>
      <td>5921.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>NaN</td>
      <td>82.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>82.0</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>79.0</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>79.0</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
      <td>15.0</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>misc_feature</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>16929.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>protein_bind</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>3375.0</td>
      <td>NaN</td>
      <td>3389.0</td>
    </tr>
    <tr>
      <th>sig_peptide</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1131.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>pseudogenic_CDS</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>58.0</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>95.0</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>61.0</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
    </tr>
  </tbody>
</table>
</div>



## Tasks 

### CDS
- [ ] [RS](#RS-CDS)
    - I find hunderds of CDS's that differ by start or end positions from the ones in gnl. Not taking these into account for now.
- [x] [GB](#GB-CDS)
123 CDSs in GB are annotated with "pseudogene". Out of these 123, there are 58 which do not have a corresponding pseudogene in the gnl annotation.
I added all 123 pseudogene CDS as 'pseudogenic_CDS' to the gnl db.
- [x] [PDC](#Pseudomonas.com-CDS)

### Genes
- [x] [PDC](#Pseudomonas.com-genes)
- [x] [GB](#GB-genes)
    - genbank has 97 'genes' not corresponding to gene entries in gnl (the new sbw25 annotation). all of these 97 are tagged as 'pseudo'. 22 out of these 97 are neither in the gnl pseudogenes.
      -> action taken: retyped those 22 to pseudogene, create new id and add to gnldb.
    - gb has no explicit pseudogenes
    
### ncRNA
- [x] [RS](#RS-ncRNA)
    - all three ncRNA genes have no analogon in gnl. 
    -> action taken: copied 3 ncRNAs to gnl. ID updated to ncRNA_i.
    
### regulatory
- [x] [RS](#RS-regulatory)
    - 1 additional regulatory region found and copied to gnl.
- [x] [GB](#GB-regulatory)
    - gb has 14 features typed 'regulatory'. They all match to 'biological_region's in gnldb.
      -> action taken: deleted 'biological_regions's and added 'regulatory_region' (that's the correct SO term).
      
### tmRNA
- [x] [RS](#RS-tmRNA)
    - the tmRNA is already present in as a transcript. Retyped to tmRNA and merged attributes.
    
### misc RNA
- [x] [GB](#GB-misc_RNA)
- [x] [PDC](#Pseudomonas.com-misc_RNA)

### repeat_region
- [x] [GB](#GB-repeat_region)
    - All `repeat_region`s in GB have a correspondance at the same location in GNL. The GNL entries are all of type `repeat_region`.

### sig_peptide
- [x] [GB](#GB-sig_peptide)
    - all GB `sig_peptide`s have corresponding matches in gnl.
    - action taken: retyped gnl features to `sig_peptide` and updated db (old features deleted).
    
### misc_feature
- [x] [GB](#GB-misc_feature)
    - 11 `misc_features` are new to gnl. 
    - action taken: retyped to `biological_region` and updated gnldb. 
    - serialized to *gnl_MPB_PFLU_1-20220204.gff3*

## Utilities


```python
# For each CDS in RS, check if there is a CDS at the same coordinates in GNL. If yes, go on, if not, add it to the update list.
def find_new_features(refdb, ref_feattype, gnldb, gnl_feattype=None, logmatches=False):
    
    return_list = []
      
    for i, reffeat in tqdm(enumerate(refdb.features_of_type(ref_feattype))):
        
        # if i > 100: break
        
        # logging.info("Processing %s %s", ref_feattype, reffeat.id)
        new_feat = True

        start = reffeat.start
        end = reffeat.end
        strand = reffeat.strand
        
        for j, gnlfeat in enumerate(gnldb.all_features(featuretype=gnl_feattype)):
            if (gnlfeat.start == start and gnlfeat.end == end and gnlfeat.strand == strand):
                new_feat = False
                
                if logmatches:
                    logging.info("%s feature %s matches %s feature %s.", 
                        "Reference DB", str(reffeat), "Target DB", str(gnlfeat))
                break
        
        if new_feat:
            logging.info("No match found for %s. Append to features.", str(reffeat))
            return_list.append(copy.deepcopy(reffeat))
        
    return return_list 
```


```python

def serialize(db, fname):
    seq_record = SeqRecord(seq=sbw25.seq, name="gnl_MPB_PFLU_1", id="gnl_MPB_PFLU_1")

    for feat in tqdm(db.all_features()):
        seqfeat = gffbio.to_seqfeature(feat)
        seq_record.features.append(seqfeat)
        
    with open(fname, 'w') as OFH:
        GFF.write([seq_record], OFH, include_fasta=False)
```


```python
def get_tableview():
    gnldb_feature_counts = {}
    for ft in gnldb.featuretypes():
        gnldb_feature_counts[ft] = gnldb.count_features_of_type(ft)

    gnldb_feature_counts=pandas.DataFrame.from_dict(gnldb_feature_counts, orient='index')

    db_feature_counts = pandas.concat([rsdb_feature_counts, ebidb_feature_counts, gbdb_feature_counts, pdcdb_feature_counts, gnldb_feature_counts], axis=1, join='outer')
    db_feature_counts.columns = ['RefSeq', 'EBI', 'GenBank', 'Pseudomonas.com', 'MPB']
    
    return db_feature_counts
```

## GB CDS 


```python
cds_updates_from_gb = find_new_features(gbcds_updates_from_gbnldb, "CDS")
```

    213it [00:00, 209.18it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	244537	244692	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_0223;note=This CDS lacks of appropriate start and stop codonsand a frameshift after residue 52;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.37. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	244694	245125	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_0223;note=This CDS lacks of appropriate start and stop codonsand a frameshift after residue 52;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.37. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	243385	243753	.	+	0	Name=putative transposase CDS;locus_tag=PFLU_0220;note=This CDS contains two framehifts after residues 123and 142;pseudo;codon_start=1;transl_table=11;product=putative transposase;db_xref=PSEUDO:CAY46502.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.38. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	243755	243811	.	+	0	Name=putative transposase CDS;locus_tag=PFLU_0220;note=This CDS contains two framehifts after residues 123and 142;pseudo;codon_start=1;transl_table=11;product=putative transposase;db_xref=PSEUDO:CAY46502.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.38. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	243811	243951	.	+	0	Name=putative transposase CDS;locus_tag=PFLU_0220;note=This CDS contains two framehifts after residues 123and 142;pseudo;codon_start=1;transl_table=11;product=putative transposase;db_xref=PSEUDO:CAY46502.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.38. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	242103	242636	.	-	0	Name=putative transposase-like protein (pseudogene) CDS;locus_tag=PFLU_0216;note=This CDS contains two stop codons at residues 29 and 75;pseudo;codon_start=1;transl_table=11;product=putative transposase-like protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    236it [00:02, 56.75it/s] INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	244143	244364	.	+	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_0222;note=This CDS lacks of appropriate start and stop codon and has two frameshifts after residues 74 and 95;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.39. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	244370	244432	.	+	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_0222;note=This CDS lacks of appropriate start and stop codon and has two frameshifts after residues 74 and 95;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.39. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	244432	244536	.	+	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_0222;note=This CDS lacks of appropriate start and stop codon and has two frameshifts after residues 74 and 95;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.39. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	219257	219622	.	+	0	Name=putative response regulator (pseudogene) CDS;locus_tag=PFLU_0195;note=similarity reduced to the N-terminal region of the database matches;pseudo;codon_start=1;transl_table=11;product=putative response regulator (pseudogene);db_xref=PSEUDO:CAY46479.1;NCBI Feature Key=CDS. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	245910	245981	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_0225;note=This CDS lacks of an appropriate start codon and has a frameshift after residue 24;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.40. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	245980	246114	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_0225;note=This CDS lacks of an appropriate start codon and has a frameshift after residue 24;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.40. Append to features.
    290it [00:03, 52.94it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	325174	325566	.	+	0	Name=putative dehydrogenase (pseudogene) CDS;locus_tag=PFLU_0297;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative dehydrogenase (pseudogene);NCBI Feature Key=CDS. Append to features.
    372it [00:04, 81.51it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	453724	454983	.	+	0	Name=putative fimbriae biogenesis-related protein (pseudogene) CDS;locus_tag=PFLU_0410;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative fimbriae biogenesis-related protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    399it [00:05, 63.87it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	451422	452075	.	+	0	Name=putative fimbriae-related protein (pseudogene) CDS;locus_tag=PFLU_0407;pseudo;codon_start=1;transl_table=11;product=putative fimbriae-related protein (pseudogene);db_xref=PSEUDO:CAY46684.1;NCBI Feature Key=CDS. Append to features.
    423it [00:05, 57.01it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	480866	481597	.	-	0	Name=putative glycosyl-transferase (pseudogene) CDS;locus_tag=PFLU_0431;old_locus_tag=PFLU0432;note=this CDS contains a frameshift after residue 244;pseudo;codon_start=1;transl_table=11;product=putative glycosyl-transferase (pseudogene);db_xref=PSEUDO:CAY46708.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318361.81. Append to features.
    430it [00:06, 42.24it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	479931	480866	.	-	0	Name=putative glycosyl-transferase (pseudogene) CDS;locus_tag=PFLU_0431;old_locus_tag=PFLU0432;note=this CDS contains a frameshift after residue 244;pseudo;codon_start=1;transl_table=11;product=putative glycosyl-transferase (pseudogene);db_xref=PSEUDO:CAY46708.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318361.81. Append to features.
    465it [00:06, 55.57it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	531569	531736	.	+	0	Name=putative ABC transport system%2C ATP-binding protein (pseudogene) CDS;locus_tag=PFLU_0470;note=This CDS lacks of an appropriate stop codon.;pseudo;codon_start=1;transl_table=11;product=putative ABC transport system%2C ATP-binding protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    735it [00:12, 42.69it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	817800	818354	.	+	0	Name=hypothetical protein (pseudogene) CDS;locus_tag=PFLU_0710;note=the only full sequence match is to the already annotated entry%2C the rest of the similarities finish at residue 185 and are to longer CDSs;pseudo;codon_start=1;transl_table=11;product=hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    755it [00:13, 36.68it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	818357	818515	.	+	0	Name=putative type III ATP synthase (pseudogene) CDS;locus_tag=PFLU_0711;note=similarity reduced to the C-terminal region of the database matches;pseudo;codon_start=1;transl_table=11;product=putative type III ATP synthase (pseudogene);db_xref=PSEUDO:CAY46980.1;NCBI Feature Key=CDS. Append to features.
    1210it [00:28, 25.22it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1361597	1361668	.	+	0	Name=peptide chain release factor 2 (pseudogene) CDS;locus_tag=PFLU_1226;note=This CDS has a frameshift after residue 24;pseudo;codon_start=1;transl_table=11;product=peptide chain release factor 2 (pseudogene);db_xref=PSEUDO:CAY47485.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318501.217. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1361670	1362692	.	+	0	Name=peptide chain release factor 2 (pseudogene) CDS;locus_tag=PFLU_1226;note=This CDS has a frameshift after residue 24;pseudo;codon_start=1;transl_table=11;product=peptide chain release factor 2 (pseudogene);db_xref=PSEUDO:CAY47485.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318501.217. Append to features.
    1435it [00:38, 21.55it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1604767	1604958	.	-	0	Name=putatuve L-aspartate oxidase (pseudogene) CDS;locus_tag=PFLU_1457;note=this remnant lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putatuve L-aspartate oxidase (pseudogene);NCBI Feature Key=CDS. Append to features.
    1447it [00:39, 19.62it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1639428	1640786	.	+	0	Name=putative transport-related membrane protein (pseudogene) CDS;locus_tag=PFLU_1494;note=This CDS contains a stop codon at residue 224;pseudo;codon_start=1;transl_table=11;product=putative transport-related membrane protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    1609it [00:47, 18.66it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1753442	1753933	.	+	0	Name=putative integrase protein (pseudogene) CDS;locus_tag=PFLU_1599;note=This CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative integrase protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    1613it [00:47, 14.61it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1752922	1753230	.	-	0	Name=hypothetical protein (pseudogene) CDS;locus_tag=PFLU_1598;note=No significant database matches. Lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    1663it [00:50, 18.53it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1824305	1824892	.	+	0	Name=putative pilin glycosylation protein (pseudogene) CDS;locus_tag=PFLU_1661;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative pilin glycosylation protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    1747it [00:55, 17.52it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	1941952	1942254	.	+	0	Name=putative phosphoglycerate mutase (pseudogene) CDS;locus_tag=PFLU_1776;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative phosphoglycerate mutase (pseudogene);NCBI Feature Key=CDS. Append to features.
    1885it [01:03, 16.21it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2132439	2133203	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_1959;note=this CDS contains stop codons in residues 200%2C 379 and 383%2C various frameshifts after residues 255%2C 387 and 441 and lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335. Append to features.
    1887it [01:04, 12.36it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2133205	2133600	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_1959;note=this CDS contains stop codons in residues 200%2C 379 and 383%2C various frameshifts after residues 255%2C 387 and 441 and lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2133600	2133761	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_1959;note=this CDS contains stop codons in residues 200%2C 379 and 383%2C various frameshifts after residues 255%2C 387 and 441 and lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335. Append to features.
    1889it [01:04,  8.69it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2133763	2133873	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_1959;note=this CDS contains stop codons in residues 200%2C 379 and 383%2C various frameshifts after residues 255%2C 387 and 441 and lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335. Append to features.
    1913it [01:06, 16.21it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2140367	2141113	.	-	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_1967;note=this CDS contains a stop codon at residue 48;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    1915it [01:06, 12.21it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2130565	2130882	.	+	0	Name=hypothetical protein (pseudogene) CDS;locus_tag=PFLU_1956;note=this CDS contains a frameshift after residue 106;pseudo;codon_start=1;transl_table=11;product=hypothetical protein (pseudogene);db_xref=PSEUDO:CAY48200.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318634.336. Append to features.
    1917it [01:06, 10.49it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2130891	2131274	.	+	0	Name=hypothetical protein (pseudogene) CDS;locus_tag=PFLU_1956;note=this CDS contains a frameshift after residue 106;pseudo;codon_start=1;transl_table=11;product=hypothetical protein (pseudogene);db_xref=PSEUDO:CAY48200.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318634.336. Append to features.
    1925it [01:07, 13.19it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2139739	2140260	.	-	0	Name=putative pilL-related (pseudogene) CDS;locus_tag=PFLU_1966;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative pilL-related (pseudogene);NCBI Feature Key=CDS. Append to features.
    1937it [01:08, 14.89it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2110260	2110559	.	-	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_1936;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS. Append to features.
    1939it [01:08, 10.42it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2101964	2102086	.	-	0	Name=DNA-binding protein HU-beta (pseudogene) CDS;locus_tag=PFLU_1926;note=also similar to PFLU3926 and PFLU6032;pseudo;codon_start=1;transl_table=11;product=DNA-binding protein HU-beta (pseudogene);db_xref=PSEUDO:CAY48171.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318635.337. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2101809	2101958	.	-	0	Name=DNA-binding protein HU-beta (pseudogene) CDS;locus_tag=PFLU_1926;note=also similar to PFLU3926 and PFLU6032;pseudo;codon_start=1;transl_table=11;product=DNA-binding protein HU-beta (pseudogene);db_xref=PSEUDO:CAY48171.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318635.337. Append to features.
    1995it [01:12, 16.79it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176622	2176723	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176727	2176789	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    1997it [01:12, 10.17it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176793	2176984	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176965	2177000	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    1999it [01:12,  7.95it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2177000	2177041	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2177041	2177058	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    2001it [01:13,  6.78it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2177067	2177126	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    2002it [01:13,  6.32it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2177132	2177269	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2007;note=this CDS contains several stop codons and frameshifts and lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348. Append to features.
    2013it [01:14, 12.76it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176604	2176615	.	-	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2006;note=this CDS lacks of an appropriate start codon and contains frameshifts after residues 4 and 18;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.349. Append to features.
    2015it [01:14, 10.44it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176554	2176595	.	-	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2006;note=this CDS lacks of an appropriate start codon and contains frameshifts after residues 4 and 18;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.349. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176381	2176548	.	-	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2006;note=this CDS lacks of an appropriate start codon and contains frameshifts after residues 4 and 18;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.349. Append to features.
    2017it [01:15,  7.74it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2188919	2189152	.	+	0	Name=putative aminotransferase (pseudogene) CDS;locus_tag=PFLU_2017A;note=this CDS lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative aminotransferase (pseudogene);NCBI Feature Key=CDS. Append to features.
    2020it [01:15,  8.58it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2189157	2189318	.	+	0	Name=putative transport-related protein (pseudogene) CDS;locus_tag=PFLU_2018;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative transport-related protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    2022it [01:15,  8.20it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2176230	2176379	.	-	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_2005;note=this CDS lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    2077it [01:19, 15.04it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2263908	2264033	.	-	0	Name=putative ferric receptor (pseudogene) CDS;locus_tag=PFLU_2083;note=this CDS lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative ferric receptor (pseudogene);NCBI Feature Key=CDS. Append to features.
    2197it [01:28, 14.13it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2376312	2376578	.	+	0	Name=putative phage-related integrase (pseudogene) CDS;locus_tag=PFLU_2192;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative phage-related integrase (pseudogene);NCBI Feature Key=CDS. Append to features.
    2223it [01:30, 13.83it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2432013	2432144	.	+	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_2241;note=this CDS lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318692.393. Append to features.
    2225it [01:30, 10.94it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2432580	2432981	.	+	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_2241;note=this CDS lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318692.393. Append to features.
    2249it [01:32, 13.26it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2448751	2448993	.	-	0	Name=putative phage integrase (pseudogene) CDS;locus_tag=PFLU_2259;note=this CDS lacks of an appropriate start codon and has a frameshift after residue 81;pseudo;codon_start=1;transl_table=11;product=putative phage integrase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318694.394. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2448461	2448742	.	-	0	Name=putative phage integrase (pseudogene) CDS;locus_tag=PFLU_2259;note=this CDS lacks of an appropriate start codon and has a frameshift after residue 81;pseudo;codon_start=1;transl_table=11;product=putative phage integrase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318694.394. Append to features.
    2251it [01:32,  8.77it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2446076	2446537	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_2256;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    2265it [01:33, 12.66it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2517042	2518988	.	+	0	Name=putative methyl-accepting chemotaxis protein (pseudogene) CDS;locus_tag=PFLU_2312;note=this CDS contains a stop codon at residue 160;pseudo;codon_start=1;transl_table=11;product=putative methyl-accepting chemotaxis protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    2375it [01:41, 13.24it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2591038	2591109	.	-	0	Name=putative glutathione S-transferse (pseudogene) CDS;locus_tag=PFLU_2377;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative glutathione S-transferse (pseudogene);NCBI Feature Key=CDS. Append to features.
    2475it [01:49, 12.69it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2716706	2717050	.	+	0	Name=putative regulatory protein (pseudogene) CDS;locus_tag=PFLU_2503A;note=this CDS lacks of an appropriate start codon and has a frameshift after residue 115;pseudo;codon_start=1;transl_table=11;product=putative regulatory protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.435. Append to features.
    2477it [01:49, 10.36it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2717127	2717354	.	+	0	Name=putative regulatory protein (pseudogene) CDS;locus_tag=PFLU_2503A;note=this CDS lacks of an appropriate start codon and has a frameshift after residue 115;pseudo;codon_start=1;transl_table=11;product=putative regulatory protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.435. Append to features.
    2479it [01:50,  9.16it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2733911	2734486	.	-	0	Name=putative phage integrase (pseudogene) CDS;locus_tag=PFLU_2519;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative phage integrase (pseudogene);NCBI Feature Key=CDS. Append to features.
    2487it [01:50, 11.06it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2707541	2707966	.	+	0	Name=putative methyl-accepting chemotaxis protein (pseudogene) CDS;locus_tag=PFLU_2492;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative methyl-accepting chemotaxis protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    2489it [01:51,  9.64it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2699109	2699294	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_2485;note=this CDS has a frameshift after residue 62;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);db_xref=PSEUDO:CAY48722.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.436. Append to features.
    2491it [01:51,  8.82it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2699296	2699499	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_2485;note=this CDS has a frameshift after residue 62;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);db_xref=PSEUDO:CAY48722.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.436. Append to features.
    2502it [01:52, 11.94it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2744951	2745193	.	-	0	Name=putative lyase (pseudogene) CDS;locus_tag=PFLU_2531;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative lyase (pseudogene);NCBI Feature Key=CDS. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2698830	2699048	.	-	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_2484;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS. Append to features.
    2504it [01:52,  8.50it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2707394	2707519	.	-	0	Name=putative potassium uptake protein (pseudogene) CDS;locus_tag=PFLU_2491;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative potassium uptake protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    2588it [01:59, 12.57it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2931315	2932253	.	+	0	Name=conserved leucine-rich repeat domain protein (pseudogene) CDS;locus_tag=PFLU_2661;pseudo;codon_start=1;transl_table=11;product=conserved leucine-rich repeat domain protein (pseudogene);db_xref=PSEUDO:CAY48893.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318764.458. Append to features.
    2590it [02:00, 10.32it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	2932255	2933718	.	+	0	Name=conserved leucine-rich repeat domain protein (pseudogene) CDS;locus_tag=PFLU_2661;pseudo;codon_start=1;transl_table=11;product=conserved leucine-rich repeat domain protein (pseudogene);db_xref=PSEUDO:CAY48893.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318764.458. Append to features.
    2662it [02:06, 11.59it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3018548	3019549	.	+	0	Name=putative threonine dehydratase biosynthetic (pseudogene) CDS;locus_tag=PFLU_2735;pseudo;codon_start=1;transl_table=11;product=putative threonine dehydratase biosynthetic (pseudogene);NCBI Feature Key=CDS. Append to features.
    3028it [02:39, 10.84it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3334863	3335324	.	-	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_3056;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    3036it [02:39, 10.21it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3329815	3330012	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_3051.2;note=No significant database matches;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    3038it [02:40,  9.03it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3336581	3336742	.	-	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_3058;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS. Append to features.
    3067it [02:43, 10.52it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3384791	3385294	.	-	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_3099;note=No significant database matches;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);db_xref=PSEUDO:CAY49329.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318839.539. Append to features.
    3069it [02:43,  9.15it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3384324	3384785	.	-	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_3099;note=No significant database matches;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);db_xref=PSEUDO:CAY49329.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318839.539. Append to features.
    3142it [02:50, 10.38it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3466133	3466426	.	+	0	Name=putative LysR-family transcriptional regulator (pseudogene) CDS;locus_tag=PFLU_3174;pseudo;codon_start=1;transl_table=11;product=putative LysR-family transcriptional regulator (pseudogene);NCBI Feature Key=CDS. Append to features.
    3236it [03:00,  9.72it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3604646	3604756	.	+	0	Name=putative carboxylase (pseudogene) CDS;locus_tag=PFLU_3254;pseudo;codon_start=1;transl_table=11;product=putative carboxylase (pseudogene);NCBI Feature Key=CDS. Append to features.
    3352it [03:13,  9.00it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3702527	3702853	.	-	0	Name=putative modification methylase (pseudogene) CDS;locus_tag=PFLU_3346;pseudo;codon_start=1;transl_table=11;product=putative modification methylase (pseudogene);NCBI Feature Key=CDS. Append to features.
    3357it [03:13,  7.92it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	3708169	3708336	.	-	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_3352;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);db_xref=PSEUDO:CAY49578.1;NCBI Feature Key=CDS. Append to features.
    3648it [03:48,  8.34it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4074295	4074588	.	+	0	Name=putative phosphomannomutase (pseudogene) CDS;locus_tag=PFLU_3679;pseudo;codon_start=1;transl_table=11;product=putative phosphomannomutase (pseudogene);NCBI Feature Key=CDS. Append to features.
    3695it [03:54,  7.98it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4152481	4153065	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_3754;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    3704it [03:55,  8.28it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4102831	4103196	.	+	0	Name=putative AraC-family transcriptional regulator (pseudogene) CDS;locus_tag=PFLU_3707;pseudo;codon_start=1;transl_table=11;product=putative AraC-family transcriptional regulator (pseudogene);NCBI Feature Key=CDS. Append to features.
    3784it [04:05,  7.88it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4258463	4260679	.	+	0	Name=putative pH adaptation potassium efflux protein (pseudogene) CDS;locus_tag=PFLU_3864;pseudo;codon_start=1;transl_table=11;product=putative pH adaptation potassium efflux protein (pseudogene);db_xref=PSEUDO:CAY50206.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318965.668. Append to features.
    3785it [04:05,  6.68it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4260679	4261383	.	+	0	Name=putative pH adaptation potassium efflux protein (pseudogene) CDS;locus_tag=PFLU_3864;pseudo;codon_start=1;transl_table=11;product=putative pH adaptation potassium efflux protein (pseudogene);db_xref=PSEUDO:CAY50206.1;NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318965.668. Append to features.
    4077it [04:44,  7.07it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4579359	4580843	.	-	0	Name=putative phage-related tail fibre protein (pseudogene) CDS;locus_tag=PFLU_4134;pseudo;codon_start=1;transl_table=11;product=putative phage-related tail fibre protein (pseudogene);db_xref=PSEUDO:CAY50625.1;NCBI Feature Key=CDS. Append to features.
    4104it [04:48,  7.06it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4581463	4582134	.	-	0	Name=putative phage-related tail protein (pseudogene) CDS;locus_tag=PFLU_4136;pseudo;codon_start=1;transl_table=11;product=putative phage-related tail protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    4136it [04:52,  7.58it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4579012	4579233	.	-	0	Name=putative tail fiber assembly protein (pseudogene) CDS;locus_tag=PFLU_4133;pseudo;codon_start=1;transl_table=11;product=putative tail fiber assembly protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    4137it [04:52,  6.64it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4582128	4582343	.	-	0	Name=putative phage terminase (pseudogene) CDS;locus_tag=PFLU_4137;pseudo;codon_start=1;transl_table=11;product=putative phage terminase (pseudogene);NCBI Feature Key=CDS. Append to features.
    4141it [04:53,  6.97it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4583794	4583973	.	+	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_4140;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS. Append to features.
    4203it [05:01,  6.99it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4744794	4744946	.	-	0	Name=putative adhesin (pseudogene) CDS;locus_tag=PFLU_4302;note=this CDS has a frameshift after residue 51;pseudo;codon_start=1;transl_table=11;product=putative adhesin (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319036.741. Append to features.
    4204it [05:02,  6.16it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4742368	4744779	.	-	0	Name=putative adhesin (pseudogene) CDS;locus_tag=PFLU_4302;note=this CDS has a frameshift after residue 51;pseudo;codon_start=1;transl_table=11;product=putative adhesin (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319036.741. Append to features.
    4244it [05:07,  7.69it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4744934	4745563	.	-	0	Name=putative MCP-domain signal transduction protein (pseudogene) CDS;locus_tag=PFLU_4303;pseudo;codon_start=1;transl_table=11;product=putative MCP-domain signal transduction protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    4254it [05:08,  7.70it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4741076	4741228	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_4299;note=this CDS lack of an appropriate start codon%2C has two internal stop codons and two frameshifts after residues 51 and 92;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.742. Append to features.
    4255it [05:09,  6.74it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4741230	4741352	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_4299;note=this CDS lack of an appropriate start codon%2C has two internal stop codons and two frameshifts after residues 51 and 92;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.742. Append to features.
    4256it [05:09,  6.17it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4741373	4741426	.	+	0	Name=putative transposase (pseudogene) CDS;locus_tag=PFLU_4299;note=this CDS lack of an appropriate start codon%2C has two internal stop codons and two frameshifts after residues 51 and 92;pseudo;codon_start=1;transl_table=11;product=putative transposase (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.742. Append to features.
    4261it [05:10,  7.00it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4734032	4734208	.	-	0	Name=putative dehydrogenase (pseudogene) CDS;locus_tag=PFLU_4289;pseudo;codon_start=1;transl_table=11;product=putative dehydrogenase (pseudogene);NCBI Feature Key=CDS. Append to features.
    4262it [05:10,  6.29it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4740921	4741082	.	-	0	Name=putative phage integrase (pseudogene) CDS;locus_tag=PFLU_4298;pseudo;codon_start=1;transl_table=11;product=putative phage integrase (pseudogene);NCBI Feature Key=CDS. Append to features.
    4263it [05:10,  5.89it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4739720	4739821	.	+	0	Name=putative regulatory protein (pseudogene) CDS;locus_tag=PFLU_4296;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative regulatory protein (pseudogene);db_xref=PSEUDO:CAY50879.1;NCBI Feature Key=CDS. Append to features.
    4440it [05:36,  6.76it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4943934	4943954	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_4475;note=this CDS lacks of a start codon and has a frameshift after residue 7;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319084.777. Append to features.
    4441it [05:36,  6.15it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	4943953	4944216	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_4475;note=this CDS lacks of a start codon and has a frameshift after residue 7;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319084.777. Append to features.
    4555it [05:53,  6.62it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5079449	5079814	.	-	0	Name=putative invasion protein regulator (pseudogene) CDS;locus_tag=PFLU_4605;note=this CDS lacks a stop codon;pseudo;codon_start=1;transl_table=11;product=putative invasion protein regulator (pseudogene);NCBI Feature Key=CDS. Append to features.
    4709it [06:16,  6.45it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5257892	5258821	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_4776;note=this CDS lacks of an ppropriate start codon;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    4734it [06:20,  6.32it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5253976	5254164	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_4770;note=this CDS lacks of an appropriate start codon and has a frameshift after residue 63;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319131.826. Append to features.
    4735it [06:21,  5.86it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5254164	5254496	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_4770;note=this CDS lacks of an appropriate start codon and has a frameshift after residue 63;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319131.826. Append to features.
    4740it [06:21,  6.21it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5257376	5257819	.	+	0	Name=putative multidrug resistance protein (pseudogene) CDS;locus_tag=PFLU_4775;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative multidrug resistance protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    4753it [06:23,  6.57it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5257064	5257309	.	-	0	Name=putative LysR-family transcriptional regulator (pseudogene) CDS;locus_tag=PFLU_4774;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative LysR-family transcriptional regulator (pseudogene);NCBI Feature Key=CDS. Append to features.
    4993it [07:02,  6.01it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5531718	5531963	.	-	0	Name=putative PTS-related membrane protein (pseudogene) CDS;locus_tag=PFLU_5036;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative PTS-related membrane protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    5106it [07:22,  5.89it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5697240	5697923	.	+	0	Name=putative plasmid-related protein (pseudogene) CDS;locus_tag=PFLU_5201;note=This CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative plasmid-related protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    5207it [07:40,  5.64it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5807618	5808508	.	-	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_5289;note=This CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS. Append to features.
    5211it [07:40,  5.49it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5800084	5800770	.	+	0	Name=putative phage-related helicase (pseudogene) CDS;locus_tag=PFLU_5282;note=this CDS lacks of an appropriate stop codon;pseudo;codon_start=1;transl_table=11;product=putative phage-related helicase (pseudogene);NCBI Feature Key=CDS. Append to features.
    5224it [07:43,  5.70it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5804849	5805301	.	-	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_5286;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    5239it [07:45,  5.51it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5809374	5809556	.	-	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_5291A;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    5407it [08:16,  5.54it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5983888	5984061	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_5451;note=this CDS lacks of an appropriate start codon and contains a frameshift after residue 70;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319262.951. Append to features.
    5408it [08:16,  5.34it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5984064	5984492	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_5451;note=this CDS lacks of an appropriate start codon and contains a frameshift after residue 70;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319262.951. Append to features.
    5430it [08:20,  5.60it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	5983355	5983726	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_5450;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    5778it [09:24,  5.26it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	6407106	6407885	.	-	0	Name=putative integrase (pseudogene) CDS;locus_tag=PFLU_5850;note=this CDS lacks of an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=putative integrase (pseudogene);NCBI Feature Key=CDS. Append to features.
    5798it [09:28,  5.30it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	6416757	6417149	.	+	0	Name=putative transport-related substrate binding protein CDS;locus_tag=PFLU_5861;note=this CDS lacks of appropriate start and stop codonsand has an internal stop codon after residue 62;pseudo;codon_start=1;transl_table=11;product=putative transport-related substrate binding protein;NCBI Feature Key=CDS. Append to features.
    5969it [10:01,  5.01it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	6641446	6642090	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_6068;note=this CDS lacks of appropriate start and stop codons;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);NCBI Feature Key=CDS. Append to features.
    5974it [10:02,  5.11it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	CDS	6642099	6642548	.	+	0	Name=conserved hypothetical protein (pseudogene) CDS;locus_tag=PFLU_6069;note=This CDS does not have an appropriate start codon;pseudo;codon_start=1;transl_table=11;product=conserved hypothetical protein (pseudogene);db_xref=PSEUDO:CAY53645.1;NCBI Feature Key=CDS. Append to features.
    6044it [10:16,  9.81it/s]



```python
pattern = re.compile(r'pseudogene')
```


```python
len(cds_updates_from_gb)
```




    123



-> That's exactly the difference between number of CDSs in gb and in gnl

### Are they all pseudogenic CDSs?


```python
len([pattern.search(feat.attributes['product'][0]) for feat in cds_updates_from_gb])
```




    123



-> Yes!


```python
# Convert type
for feat in cds_updates_from_gb:
    feat.featuretype = 'pseudogenic_CDS'
```


```python
feat
```




    <Feature pseudogenic_CDS (gnl_MPB_PFLU_1:6642099-6642548[+]) at 0x7f31fc922a30>




```python
gb_cds_not_in_gnl_pseudogenes = []
```


```python
# Get new pseudogenic CDS as a SeqFeatures 
for tmp in cds_updates_from_gb:
    
    match = False
    for pgene in gnldb.features_of_type('pseudogene'):
        if tmp.start == pgene.start and tmp.end == pgene.end and tmp.strand == pgene.strand:
            match = True
            break
    
    if not match:
        gb_cds_not_in_gnl_pseudogenes.append(tmp)
        
    
```


```python
len(gb_cds_not_in_gnl_pseudogenes)
```




    58




```python
[print(feat.qualifiers['ID'], feat.qualifiers['locus_tag'], feat.location) for feat in gb_cds_not_in_gnl_pseudogenes.features]
```

    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.37'] ['PFLU_0223'] [244536:244692](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.37'] ['PFLU_0223'] [244693:245125](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.38'] ['PFLU_0220'] [243384:243753](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.38'] ['PFLU_0220'] [243754:243811](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318317.38'] ['PFLU_0220'] [243810:243951](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.39'] ['PFLU_0222'] [244142:244364](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.39'] ['PFLU_0222'] [244369:244432](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.39'] ['PFLU_0222'] [244431:244536](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.40'] ['PFLU_0225'] [245909:245981](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318318.40'] ['PFLU_0225'] [245979:246114](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318361.81'] ['PFLU_0431'] [480865:481597](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318361.81'] ['PFLU_0431'] [479930:480866](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318501.217'] ['PFLU_1226'] [1361596:1361668](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318501.217'] ['PFLU_1226'] [1361669:1362692](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335'] ['PFLU_1959'] [2132438:2133203](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335'] ['PFLU_1959'] [2133204:2133600](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335'] ['PFLU_1959'] [2133599:2133761](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318633.335'] ['PFLU_1959'] [2133762:2133873](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318634.336'] ['PFLU_1956'] [2130564:2130882](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318634.336'] ['PFLU_1956'] [2130890:2131274](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318635.337'] ['PFLU_1926'] [2101963:2102086](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318635.337'] ['PFLU_1926'] [2101808:2101958](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2176621:2176723](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2176726:2176789](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2176792:2176984](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2176964:2177000](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2176999:2177041](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2177040:2177058](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2177066:2177126](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.348'] ['PFLU_2007'] [2177131:2177269](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.349'] ['PFLU_2006'] [2176603:2176615](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.349'] ['PFLU_2006'] [2176553:2176595](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318641.349'] ['PFLU_2006'] [2176380:2176548](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318692.393'] ['PFLU_2241'] [2432012:2432144](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318692.393'] ['PFLU_2241'] [2432579:2432981](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318694.394'] ['PFLU_2259'] [2448750:2448993](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318694.394'] ['PFLU_2259'] [2448460:2448742](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.435'] ['PFLU_2503A'] [2716705:2717050](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.435'] ['PFLU_2503A'] [2717126:2717354](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.436'] ['PFLU_2485'] [2699108:2699294](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318738.436'] ['PFLU_2485'] [2699295:2699499](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318764.458'] ['PFLU_2661'] [2931314:2932253](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318764.458'] ['PFLU_2661'] [2932254:2933718](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318839.539'] ['PFLU_3099'] [3384790:3385294](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318839.539'] ['PFLU_3099'] [3384323:3384785](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318965.668'] ['PFLU_3864'] [4258462:4260679](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452318965.668'] ['PFLU_3864'] [4260678:4261383](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319036.741'] ['PFLU_4302'] [4744793:4744946](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319036.741'] ['PFLU_4302'] [4742367:4744779](-)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.742'] ['PFLU_4299'] [4741075:4741228](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.742'] ['PFLU_4299'] [4741229:4741352](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.742'] ['PFLU_4299'] [4741372:4741426](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319084.777'] ['PFLU_4475'] [4943933:4943954](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319084.777'] ['PFLU_4475'] [4943952:4944216](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319131.826'] ['PFLU_4770'] [5253975:5254164](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319131.826'] ['PFLU_4770'] [5254163:5254496](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319262.951'] ['PFLU_5451'] [5983887:5984061](+)
    ['H3vLTfdV07uecYH3tU9j82ABBTs.1642452319262.951'] ['PFLU_5451'] [5984063:5984492](+)





    [None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None,
     None]



### Conclusion:
Add the 123 differing CDS from GB to the gnl annotation as pseudogenic_CDS.


```python
# Make IDs unique
for i,pds in enumerate(cds_updates_from_gb):
    pds.attributes['ID'] = ['pseudogenic_cds_{}'.format(i)]
    pds.id = 'pseudogenic_cds_{}'.format(i)

    
```


```python
cds_updates_from_gb[0].__dict__
```




    {'seqid': 'gnl_MPB_PFLU_1',
     'source': 'Geneious',
     'featuretype': 'pseudogenic_CDS',
     'start': 244537,
     'end': 244692,
     'score': '.',
     'strand': '+',
     'frame': '0',
     'attributes': <gffutils.attributes.Attributes at 0x7f31fce6eb20>,
     'extra': [],
     'bin': 4682,
     'id': 'pseudogenic_cds_0',
     'dialect': {'leading semicolon': False,
      'trailing semicolon': False,
      'quoted GFF2 values': False,
      'field separator': ';',
      'keyval separator': '=',
      'multival separator': ',',
      'fmt': 'gff3',
      'repeated keys': False,
      'order': ['Name',
       'organism',
       'mol_type',
       'strain',
       'db_xref',
       'NCBI Feature Key',
       'locus_tag',
       'note',
       'gene',
       'EC_number',
       'codon_start',
       'transl_table',
       'product',
       'protein_id',
       'standard_name',
       'inference',
       'NCBI Join Type',
       'ID',
       'bound_moiety',
       'anticodon']},
     'file_order': 1087,
     'keep_order': False,
     'sort_attribute_values': False,
     'type': 'pseudogenic_CDS'}




```python
gnldb.update(cds_updates_from_gb)
```




    <gffutils.interface.FeatureDB at 0x7f3201cbcdc0>




```python
gnldb.count_features_of_type('pseudogenic_CDS')
```




    123




```python
## Feature counts:
gnldb_feature_counts = {}
for ft in gnldb.featuretypes():
    gnldb_feature_counts[ft] = gnldb.count_features_of_type(ft)

gnldb_feature_counts=pandas.DataFrame.from_dict(gnldb_feature_counts, orient='index')
gnldb_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>1</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>20643</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>1</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>6003</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>5921</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>82</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>1</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>79</td>
    </tr>
    <tr>
      <th>pseudogenic_CDS</th>
      <td>123</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>95</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>79</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>3389</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>61</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>2</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>15</td>
    </tr>
  </tbody>
</table>
</div>




```python
db_feature_counts = pandas.concat([rsdb_feature_counts, ebidb_feature_counts, gbdb_feature_counts, pdcdb_feature_counts, gnldb_feature_counts], axis=1, join='outer')

db_feature_counts.columns = ['RefSeq', 'EBI', 'GenBank', 'Pseudomonas.com', 'MPB']

db_feature_counts
```

### GB Genes


```python
gb_genes = gbdb.features_of_type('gene')
```


```python
gb_gene_updates = find_new_features(gbdb, 'gene', gnldb, 'gene')
```

    195it [00:00, 333.60it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	244537	245125	.	+	.	locus_tag=PFLU_0223;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	243385	243951	.	+	.	locus_tag=PFLU_0220;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	242103	242636	.	-	.	locus_tag=PFLU_0216;pseudo. Append to features.
    233it [00:01, 139.41it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	244143	244536	.	+	.	locus_tag=PFLU_0222;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	219257	219622	.	+	.	locus_tag=PFLU_0195;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	245910	246114	.	+	.	locus_tag=PFLU_0225;pseudo. Append to features.
    276it [00:01, 100.98it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	325174	325566	.	+	.	locus_tag=PFLU_0297;pseudo. Append to features.
    368it [00:02, 116.96it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	453724	454983	.	+	.	locus_tag=PFLU_0410;pseudo. Append to features.
    394it [00:03, 95.05it/s] INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	451422	452075	.	+	.	locus_tag=PFLU_0407;pseudo. Append to features.
    416it [00:03, 83.29it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	479931	481597	.	-	.	locus_tag=PFLU_0431;old_locus_tag=PFLU0432;pseudo. Append to features.
    456it [00:03, 77.81it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	531569	531736	.	+	.	locus_tag=PFLU_0470;pseudo. Append to features.
    729it [00:07, 65.61it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	817800	818354	.	+	.	locus_tag=PFLU_0710;pseudo. Append to features.
    750it [00:08, 55.11it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	818357	818515	.	+	.	locus_tag=PFLU_0711;pseudo. Append to features.
    1201it [00:18, 37.27it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1361597	1362692	.	+	.	locus_tag=PFLU_1226;pseudo. Append to features.
    1427it [00:25, 31.40it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1604767	1604958	.	-	.	locus_tag=PFLU_1457;pseudo. Append to features.
    1437it [00:26, 25.53it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1639428	1640786	.	+	.	locus_tag=PFLU_1494;pseudo. Append to features.
    1601it [00:32, 27.30it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1753442	1753933	.	+	.	locus_tag=PFLU_1599;pseudo. Append to features.
    1604it [00:32, 21.61it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1752922	1753230	.	-	.	locus_tag=PFLU_1598;pseudo. Append to features.
    1655it [00:34, 24.02it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1824305	1824892	.	+	.	locus_tag=PFLU_1661;pseudo. Append to features.
    1739it [00:37, 27.42it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1941952	1942254	.	+	.	locus_tag=PFLU_1776;pseudo. Append to features.
    1877it [00:43, 23.30it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2132439	2133873	.	+	.	locus_tag=PFLU_1959;pseudo. Append to features.
    1901it [00:44, 22.86it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2140367	2141113	.	-	.	locus_tag=PFLU_1967;pseudo. Append to features.
    1904it [00:45, 18.98it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2130565	2131274	.	+	.	locus_tag=PFLU_1956;pseudo. Append to features.
    1913it [00:45, 19.29it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2139739	2140260	.	-	.	locus_tag=PFLU_1966;pseudo. Append to features.
    1925it [00:46, 20.38it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2110260	2110559	.	-	.	locus_tag=PFLU_1936;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2101809	2102086	.	-	.	locus_tag=PFLU_1926;pseudo. Append to features.
    1982it [00:48, 22.41it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2176622	2176723	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2176727	2176789	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2176793	2177058	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350. Append to features.
    1985it [00:49, 13.93it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2177067	2177126	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2177132	2177269	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350. Append to features.
    1997it [00:50, 17.06it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2176381	2176615	.	-	.	locus_tag=PFLU_2006;pseudo. Append to features.
    1999it [00:50, 15.19it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2188919	2189152	.	+	.	locus_tag=PFLU_2017A;pseudo. Append to features.
    2001it [00:50, 13.86it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2189157	2189318	.	+	.	locus_tag=PFLU_2018;pseudo. Append to features.
    2004it [00:50, 13.83it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2176230	2176379	.	-	.	locus_tag=PFLU_2005;pseudo. Append to features.
    2060it [00:53, 21.70it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2263908	2264033	.	-	.	locus_tag=PFLU_2083;pseudo. Append to features.
    2179it [00:59, 20.78it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2376312	2376578	.	+	.	locus_tag=PFLU_2192;pseudo. Append to features.
    2205it [01:00, 19.89it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2432013	2432144	.	+	.	locus_tag=PFLU_2241;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318695.395. Append to features.
    2207it [01:00, 16.14it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2432580	2432981	.	+	.	locus_tag=PFLU_2241;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318695.395. Append to features.
    2231it [01:02, 19.60it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2448461	2448993	.	-	.	locus_tag=PFLU_2259;pseudo. Append to features.
    2233it [01:02, 15.77it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2446076	2446537	.	+	.	locus_tag=PFLU_2256;pseudo. Append to features.
    2245it [01:02, 18.13it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2517042	2518988	.	+	.	locus_tag=PFLU_2312;pseudo. Append to features.
    2356it [01:08, 18.74it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2591038	2591109	.	-	.	locus_tag=PFLU_2377;pseudo. Append to features.
    2456it [01:14, 18.90it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2716706	2717354	.	+	.	locus_tag=PFLU_2503A;pseudo. Append to features.
    2460it [01:14, 14.90it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2733911	2734486	.	-	.	locus_tag=PFLU_2519;pseudo. Append to features.
    2468it [01:15, 16.07it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2707541	2707966	.	+	.	locus_tag=PFLU_2492;pseudo. Append to features.
    2470it [01:15, 13.66it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2699109	2699294	.	+	.	locus_tag=PFLU_2485;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318739.437. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2699296	2699499	.	+	.	locus_tag=PFLU_2485;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318739.437. Append to features.
    2482it [01:16, 16.35it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2744951	2745193	.	-	.	locus_tag=PFLU_2531;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2698830	2699048	.	-	.	locus_tag=PFLU_2484;pseudo. Append to features.
    2484it [01:16, 11.09it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2707394	2707519	.	-	.	locus_tag=PFLU_2491;pseudo. Append to features.
    2568it [01:21, 17.01it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2931315	2933718	.	+	.	locus_tag=PFLU_2661;pseudo. Append to features.
    2642it [01:26, 16.27it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3018548	3019549	.	+	.	locus_tag=PFLU_2735;pseudo. Append to features.
    3006it [01:49, 13.78it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3334863	3335324	.	-	.	locus_tag=PFLU_3056;pseudo. Append to features.
    3016it [01:50, 14.09it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3329815	3330012	.	+	.	locus_tag=PFLU_3051.2;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3336581	3336742	.	-	.	locus_tag=PFLU_3058;pseudo. Append to features.
    3046it [01:52, 14.28it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3384324	3385294	.	-	.	locus_tag=PFLU_3099;pseudo. Append to features.
    3120it [01:58, 14.13it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3466133	3466426	.	+	.	locus_tag=PFLU_3174;pseudo. Append to features.
    3214it [02:04, 15.23it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3604646	3604756	.	+	.	locus_tag=PFLU_3254;pseudo. Append to features.
    3330it [02:13, 13.68it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3702527	3702853	.	-	.	locus_tag=PFLU_3346;pseudo. Append to features.
    3334it [02:13, 12.70it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	3708169	3708336	.	-	.	locus_tag=PFLU_3352;pseudo. Append to features.
    3626it [02:36, 12.90it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4074295	4074588	.	+	.	locus_tag=PFLU_3679;pseudo. Append to features.
    3672it [02:40, 12.21it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4152481	4153065	.	+	.	locus_tag=PFLU_3754;pseudo. Append to features.
    3682it [02:41, 11.91it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4102831	4103196	.	+	.	locus_tag=PFLU_3707;pseudo. Append to features.
    3762it [02:47, 12.02it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4258463	4261383	.	+	.	locus_tag=PFLU_3864;pseudo. Append to features.
    4054it [03:13, 10.58it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4579359	4580843	.	-	.	locus_tag=PFLU_4134;pseudo. Append to features.
    4080it [03:15, 10.80it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4581463	4582134	.	-	.	locus_tag=PFLU_4136;pseudo. Append to features.
    4112it [03:18, 11.40it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4579012	4579233	.	-	.	locus_tag=PFLU_4133;pseudo. Append to features.
    4114it [03:18, 10.09it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4582128	4582343	.	-	.	locus_tag=PFLU_4137;pseudo. Append to features.
    4118it [03:19, 10.12it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4583794	4583973	.	+	.	locus_tag=PFLU_4140;pseudo. Append to features.
    4180it [03:25, 10.84it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4744794	4744946	.	-	.	locus_tag=PFLU_4302;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.743. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4742368	4744779	.	-	.	locus_tag=PFLU_4302;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.743. Append to features.
    4221it [03:28, 10.70it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4744934	4745563	.	-	.	locus_tag=PFLU_4303;pseudo. Append to features.
    4231it [03:29, 10.71it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4741076	4741228	.	+	.	locus_tag=PFLU_4299;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319050.744. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4741230	4741352	.	+	.	locus_tag=PFLU_4299;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319050.744. Append to features.
    4233it [03:30,  9.55it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4741373	4741426	.	+	.	locus_tag=PFLU_4299;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319050.744. Append to features.
    4238it [03:30, 10.14it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4734032	4734208	.	-	.	locus_tag=PFLU_4289;pseudo. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4740921	4741082	.	-	.	locus_tag=PFLU_4298;pseudo. Append to features.
    4240it [03:30,  9.19it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4739720	4739821	.	+	.	locus_tag=PFLU_4296;pseudo. Append to features.
    4417it [03:48,  9.77it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4943934	4944216	.	+	.	locus_tag=PFLU_4475;pseudo. Append to features.
    4531it [03:59, 10.20it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5079449	5079814	.	-	.	locus_tag=PFLU_4605;pseudo. Append to features.
    4685it [04:14,  9.38it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5257892	5258821	.	+	.	locus_tag=PFLU_4776;pseudo. Append to features.
    4710it [04:17,  7.59it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5253976	5254496	.	+	.	locus_tag=PFLU_4770;pseudo. Append to features.
    4715it [04:18,  8.76it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5257376	5257819	.	+	.	locus_tag=PFLU_4775;pseudo. Append to features.
    4728it [04:19,  9.41it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5257064	5257309	.	-	.	locus_tag=PFLU_4774;pseudo. Append to features.
    4968it [04:46,  8.89it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5531718	5531963	.	-	.	locus_tag=PFLU_5036;pseudo. Append to features.
    5081it [04:59,  7.71it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5697240	5697923	.	+	.	locus_tag=PFLU_5201;pseudo. Append to features.
    5182it [05:11,  8.36it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5807618	5808508	.	-	.	locus_tag=PFLU_5289;pseudo. Append to features.
    5186it [05:11,  8.26it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5800084	5800770	.	+	.	locus_tag=PFLU_5282;pseudo. Append to features.
    5199it [05:13,  8.44it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5804849	5805301	.	-	.	locus_tag=PFLU_5286;pseudo. Append to features.
    5214it [05:15,  8.27it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5809374	5809556	.	-	.	locus_tag=PFLU_5291A;pseudo. Append to features.
    5382it [05:35,  8.35it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5983888	5984492	.	+	.	locus_tag=PFLU_5451;pseudo. Append to features.
    5404it [05:38,  8.19it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5983355	5983726	.	+	.	locus_tag=PFLU_5450;pseudo. Append to features.
    5752it [06:22,  7.81it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	6407106	6407885	.	-	.	locus_tag=PFLU_5850;pseudo. Append to features.
    5772it [06:25,  7.66it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	6416757	6417149	.	+	.	locus_tag=PFLU_5861;pseudo. Append to features.
    5943it [06:48,  7.83it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	6641446	6642090	.	+	.	locus_tag=PFLU_6068;pseudo. Append to features.
    5948it [06:48,  7.84it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	6642099	6642548	.	+	.	locus_tag=PFLU_6069;pseudo. Append to features.
    6018it [06:58, 14.38it/s]



```python
len(gb_gene_updates)
```




    97



All pseudogenes?


```python
all(['pseudo' in gene.attributes.keys() for gene in gb_gene_updates])
```




    True



### Are these 97 (pseudo)genes already in the pseudogene features of gnl?


```python
gb_pseudogene_updates = []
for gb_gene in gb_gene_updates:
    match = False
    for gnl_pseudogene in gnldb.features_of_type('pseudogene'):
        if gnl_pseudogene.start == gb_gene.start and  \
           gnl_pseudogene.end == gb_gene.end and \
           gnl_pseudogene.strand == gb_gene.strand :
            match = True
            break
    if not match:
        gb_pseudogene_updates.append(gb_gene)
    
```


```python
len(gb_pseudogene_updates)
```




    22



-> 22 new pseudogenes found.


```python
[print(psg) for psg in gb_pseudogene_updates];
```

    gnl_MPB_PFLU_1	Geneious	gene	243385	243951	.	+	.	locus_tag=PFLU_0220;pseudo
    gnl_MPB_PFLU_1	Geneious	gene	244143	244536	.	+	.	locus_tag=PFLU_0222;pseudo
    gnl_MPB_PFLU_1	Geneious	gene	245910	246114	.	+	.	locus_tag=PFLU_0225;pseudo
    gnl_MPB_PFLU_1	Geneious	gene	479931	481597	.	-	.	locus_tag=PFLU_0431;old_locus_tag=PFLU0432;pseudo
    gnl_MPB_PFLU_1	Geneious	gene	2132439	2133873	.	+	.	locus_tag=PFLU_1959;pseudo
    gnl_MPB_PFLU_1	Geneious	gene	2176622	2176723	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350
    gnl_MPB_PFLU_1	Geneious	gene	2176727	2176789	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350
    gnl_MPB_PFLU_1	Geneious	gene	2176793	2177058	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350
    gnl_MPB_PFLU_1	Geneious	gene	2177067	2177126	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350
    gnl_MPB_PFLU_1	Geneious	gene	2177132	2177269	.	+	.	locus_tag=PFLU_2007;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318642.350
    gnl_MPB_PFLU_1	Geneious	gene	2432013	2432144	.	+	.	locus_tag=PFLU_2241;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318695.395
    gnl_MPB_PFLU_1	Geneious	gene	2432580	2432981	.	+	.	locus_tag=PFLU_2241;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318695.395
    gnl_MPB_PFLU_1	Geneious	gene	2699109	2699294	.	+	.	locus_tag=PFLU_2485;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318739.437
    gnl_MPB_PFLU_1	Geneious	gene	2699296	2699499	.	+	.	locus_tag=PFLU_2485;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318739.437
    gnl_MPB_PFLU_1	Geneious	gene	4258463	4261383	.	+	.	locus_tag=PFLU_3864;pseudo
    gnl_MPB_PFLU_1	Geneious	gene	4744794	4744946	.	-	.	locus_tag=PFLU_4302;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.743
    gnl_MPB_PFLU_1	Geneious	gene	4742368	4744779	.	-	.	locus_tag=PFLU_4302;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319049.743
    gnl_MPB_PFLU_1	Geneious	gene	4741076	4741228	.	+	.	locus_tag=PFLU_4299;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319050.744
    gnl_MPB_PFLU_1	Geneious	gene	4741230	4741352	.	+	.	locus_tag=PFLU_4299;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319050.744
    gnl_MPB_PFLU_1	Geneious	gene	4741373	4741426	.	+	.	locus_tag=PFLU_4299;pseudo;NCBI Join Type=order;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319050.744
    gnl_MPB_PFLU_1	Geneious	gene	4943934	4944216	.	+	.	locus_tag=PFLU_4475;pseudo
    gnl_MPB_PFLU_1	Geneious	gene	5253976	5254496	.	+	.	locus_tag=PFLU_4770;pseudo



```python
## Assign unique IDs and set type to pseudogene
for i, gene in enumerate(gb_pseudogene_updates):
    del gene.attributes['pseudo']
    gene.featuretype = 'pseudogene'
    gene.source = 'genbank'
    gene.attributes['ID'] = 'pseudogene_{}'.format(i)
    gene.id = gene.attributes
```


```python
gb_pseudogene_updates[0]

```




    <Feature pseudogene (gnl_MPB_PFLU_1:243385-243951[+]) at 0x7f31fcb779a0>




```python
# Update gnl db
gnldb.update(gb_pseudogene_updates)
```




    <gffutils.interface.FeatureDB at 0x7f3201cbcdc0>




```python
## Feature counts:
gnldb_feature_counts = {}
for ft in gnldb.featuretypes():
    gnldb_feature_counts[ft] = gnldb.count_features_of_type(ft)

gnldb_feature_counts=pandas.DataFrame.from_dict(gnldb_feature_counts, orient='index')
```


```python
db_feature_counts = pandas.concat([rsdb_feature_counts, ebidb_feature_counts, gbdb_feature_counts, pdcdb_feature_counts, gnldb_feature_counts], axis=1, join='outer')
db_feature_counts.columns = ['RefSeq', 'EBI', 'GenBank', 'Pseudomonas.com', 'MPB']
db_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RefSeq</th>
      <th>EBI</th>
      <th>GenBank</th>
      <th>Pseudomonas.com</th>
      <th>MPB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6069.0</td>
      <td>5921.0</td>
      <td>6044.0</td>
      <td>5921.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6153.0</td>
      <td>5921.0</td>
      <td>6018.0</td>
      <td>6106.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA</th>
      <td>3.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>7.0</td>
      <td>NaN</td>
      <td>14.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
    </tr>
    <tr>
      <th>tmRNA</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>NaN</td>
      <td>11293.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>20643.0</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>NaN</td>
      <td>6098.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6003.0</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>NaN</td>
      <td>5921.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>NaN</td>
      <td>82.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>82.0</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>101.0</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>79.0</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
      <td>15.0</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>misc_feature</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>16929.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>protein_bind</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>3375.0</td>
      <td>NaN</td>
      <td>3389.0</td>
    </tr>
    <tr>
      <th>sig_peptide</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1131.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>pseudogenic_CDS</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>123.0</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>95.0</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>61.0</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
    </tr>
  </tbody>
</table>
</div>



## GB regulatory


```python
gb_regulatory_updates = find_new_features(gbdb, 'regulatory', gnldb, logmatches=True)
```

    0it [00:00, ?it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	2267085	2267105	.	+	.	Name=recognised by TAT system regulatory;regulatory_class=other;locus_tag=PFLU_2086;note=recognised by TAT system;NCBI Feature Key=regulatory. Append to features.
    1it [00:01,  1.22s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	2442766	2442780	.	+	.	regulatory_class=other;locus_tag=PFLU_2252;note=possibly recognised by TAT system for transport. Append to features.
    2it [00:02,  1.18s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	2468613	2468627	.	-	.	regulatory_class=other;locus_tag=PFLU_2271;note=possibly recognised by TAT system for transport. Append to features.
    3it [00:03,  1.18s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	2625323	2625343	.	+	.	regulatory_class=other;locus_tag=PFLU_2414;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    4it [00:04,  1.19s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	2790363	2790383	.	-	.	regulatory_class=other;locus_tag=PFLU_2549;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    5it [00:05,  1.19s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	2985170	2985190	.	-	.	regulatory_class=other;locus_tag=PFLU_2703;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    6it [00:07,  1.17s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	3077836	3077856	.	+	.	regulatory_class=other;locus_tag=PFLU_2787;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    7it [00:08,  1.15s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	3327238	3327265	.	+	.	Name=dif regulatory;regulatory_class=other;note=dif;NCBI Feature Key=regulatory. Append to features.
    8it [00:09,  1.14s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	3460962	3460982	.	+	.	regulatory_class=other;locus_tag=PFLU_3165;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    9it [00:10,  1.14s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	3826898	3826918	.	-	.	regulatory_class=other;locus_tag=PFLU_3458;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    10it [00:11,  1.15s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	3831233	3831253	.	+	.	regulatory_class=other;locus_tag=PFLU_3464;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    11it [00:12,  1.16s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	4060863	4060883	.	-	.	regulatory_class=other;locus_tag=PFLU_3667;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    12it [00:13,  1.17s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	4676876	4676896	.	+	.	regulatory_class=other;locus_tag=PFLU_4234;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    13it [00:15,  1.17s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	5739462	5739482	.	-	.	regulatory_class=other;locus_tag=PFLU_5232;note=possibly recognised by TAT system for Sec-independent transport. Append to features.
    14it [00:16,  1.17s/it]


-> all 'regulatory' features in GB are in 'biological_region's in gnl.

I'll retype them to regulatory_region (SO:0005836)

### When rerun: delete biological regions first!!!


```python
for i,feat in tqdm(enumerate(gb_regulatory_updates)):
    feat.featuretype = 'regulatory_region'
    feat.attributes['ID'] = 'regulatory_region_{}'.format(i)
    feat.id = feat.attributes['ID']
```

    14it [00:00, 56353.41it/s]



```python
gnldb.update(gb_regulatory_updates)
```




    <gffutils.interface.FeatureDB at 0x7f3201cbcdc0>




```python
## Feature counts:
gnldb_feature_counts = {}
for ft in gnldb.featuretypes():
    gnldb_feature_counts[ft] = gnldb.count_features_of_type(ft)

gnldb_feature_counts=pandas.DataFrame.from_dict(gnldb_feature_counts, orient='index')

db_feature_counts = pandas.concat([rsdb_feature_counts, ebidb_feature_counts, gbdb_feature_counts, pdcdb_feature_counts, gnldb_feature_counts], axis=1, join='outer')
db_feature_counts.columns = ['RefSeq', 'EBI', 'GenBank', 'Pseudomonas.com', 'MPB']
db_feature_counts
```


```python
for i,feat in enumerate(gb_regulatory_updates):
    for j, contained in enumerate(gnldb.region(feat)):
        print(i, j, contained)
        if(contained.start == feat.start and contained.end == feat.end and contained.strand == feat.strand and contained.featuretype != feat.featuretype):
            logging.warning("Match")
```

    0 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    0 1 gnl_MPB_PFLU_1	Geneious	sequence_difference	2265328	2296518	.	+	.	ID=ReD25 difference;Name=ReD25 difference;external_name=AM181176.4:misc_feature:2265118..2296308;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=sequence_difference;note=ReD25;seqid=gnl_MPB_PFLU_1
    0 2 gnl_MPB_PFLU_1	ena	CDS	2266152	2267195	.	+	0	Dbxref=EMBL:AM181176,InterPro:IPR002818,InterPro:IPR009057,InterPro:IPR018060,InterPro:IPR018062,InterPro:IPR029062,KEGG:pfs:PFLU_2086,Pfam:PF01965,Pfam:PF12833,STRING:216595.PFLU_2086,SUPFAM:SSF46689,SUPFAM:SSF52317,eggNOG:COG4977,Pubmed:19432983;ID=CDS:CAY48327;Ontology_term=GO:0003700,GO:0043565;Parent=transcript:CAY48327;codon_start=1;confidence_level=3;description=Putative AraC-family regulatory protein;features=Domain (1);frame=0;ft_domain=DOMAIN 232..330%3B  /note%3D"HTH araC/xylS-type"%3B  /evidence%3D"ECO:0000259|PROSITE:PS01124";gene=PFLU_2086;inference=Predicted;locus_tag=PFLU_2086;ncbi_feature_key=CDS;product=putative AraC-family regulatory protein;protein_id=CAY48327;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B with%3DUniProt:Q62E24%3B Burkholderia mallei (Pseudomonas mallei).%3B Transcriptional regulator%2C AraC family.%3B length%3D343%3B id 65.476%25%3B ungapped id 67.073%25%3B E()%3D6.2e-83%3B 336 aa overlap%3B query 5-332%3B subject 2-337,fasta%3B with%3DUniProt:Q84H26%3B Pseudomonas aurantiaca.%3B darR%3B DarR.%3B length%3D330%3B id 78.086%25%3B ungapped id 78.086%25%3B E()%3D1e-96%3B 324 aa overlap%3B query 16-339%3B subject 6-329;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    0 3 gnl_MPB_PFLU_1	ena	exon	2266152	2267195	.	+	.	ID=exon_2077;Name=CAY48327-1;Parent=transcript:CAY48327;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY48327-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    0 4 gnl_MPB_PFLU_1	ena	gene	2266152	2267195	.	+	.	ID=gene:PFLU_2086;biotype=protein_coding;description=putative AraC-family regulatory protein;frame=.;gene_id=PFLU_2086;logic_name=ena;seqid=gnl_MPB_PFLU_1
    0 5 gnl_MPB_PFLU_1	ena	mRNA	2266152	2267195	.	+	.	ID=transcript:CAY48327;Parent=gene:PFLU_2086;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY48327
    0 6 gnl_MPB_PFLU_1	Geneious	biological_region	2266995	2267123	.	+	.	ID=misc_1444;Name=misc;external_name=PS00041 Bacterial regulatory proteins%2C araC family signature.;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=misc_feature;note=PS00041 Bacterial regulatory proteins%2C araC family signature.;seqid=gnl_MPB_PFLU_1
    0 7 gnl_MPB_PFLU_1	Geneious	biological_region	2267004	2267138	.	+	.	ID=HMMPfam_2138;Name=HMMPfam;domain=HMMPfam:PF00165%3BBacterial regulatory helix-turn-helix prote%3B3.1e-05%3Bcodon 285-329;external_name=PFLU_2086%3B AM181176.4:misc_feature:2266794..2266928;frame=.;locus_tag=PFLU_2086;logic_name=ena_misc_feature;ncbi_feature_key=misc_feature;note=HMMPfam hit to PF00165%2C Bacterial regulatory helix-turn-helix prote%2C score 3.1e-05;seqid=gnl_MPB_PFLU_1
    0 8 gnl_MPB_PFLU_1	Geneious	regulatory_region	2267085	2267105	.	+	.	Name=recognised by TAT system regulatory;regulatory_class=other;locus_tag=PFLU_2086;note=recognised by TAT system;NCBI Feature Key=regulatory;ID=regulatory_region_0
    1 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    1 1 gnl_MPB_PFLU_1	Geneious	sequence_difference	2438997	2489613	.	+	.	ID=ReD28 difference;Name=ReD28 difference;external_name=AM181176.4:misc_feature:2438787..2489403;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=sequence_difference;note=ReD28;seqid=gnl_MPB_PFLU_1
    1 2 gnl_MPB_PFLU_1	ena	CDS	2442661	2443104	.	+	0	Dbxref=EMBL:AM181176,InterPro:IPR021994,KEGG:pfs:PFLU_2252,OrthoDB:1627731at2,Pfam:PF12158,RefSeq:WP_012723483.1,STRING:216595.PFLU_2252,eggNOG:ENOG5033HK0,Pubmed:19432983;ID=CDS:CAY48487;Ontology_term=GO:0016021;Parent=transcript:CAY48487;codon_start=1;confidence_level=4;description=Putative membrane protein;features=Transmembrane (2);frame=0;gene=PFLU_2252;inference=Predicted;locus_tag=PFLU_2252;ncbi_feature_key=CDS;product=putative membrane protein;protein_id=CAY48487;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B with%3DUniProt:Q88B72%3B Pseudomonas syringae (pv. tomato).%3B Hypothetical protein.%3B length%3D143%3B id 47.826%25%3B ungapped id 48.889%25%3B E()%3D3.8e-17%3B 138 aa overlap%3B query 5-140%3B subject 6-142,fasta%3B with%3DUniProt:Q9I068%3B Pseudomonas aeruginosa.%3B Hypothetical protein.%3B length%3D145%3B id 55.814%25%3B ungapped id 55.814%25%3B E()%3D1e-20%3B 129 aa overlap%3B query 1-129%3B subject 1-129;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    1 3 gnl_MPB_PFLU_1	ena	exon	2442661	2443104	.	+	.	ID=exon_2239;Name=CAY48487-1;Parent=transcript:CAY48487;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY48487-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    1 4 gnl_MPB_PFLU_1	ena	gene	2442661	2443104	.	+	.	ID=gene:PFLU_2252;biotype=protein_coding;description=putative membrane protein;frame=.;gene_id=PFLU_2252;logic_name=ena;seqid=gnl_MPB_PFLU_1
    1 5 gnl_MPB_PFLU_1	ena	mRNA	2442661	2443104	.	+	.	ID=transcript:CAY48487;Parent=gene:PFLU_2252;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY48487
    1 6 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2442694	2443050	.	+	.	ID=biological_region_4064;external_name=2 probable transmembrane helices predicted for PFLU_2252 by TMHMM2.0 at aa 12-34 and 108-130;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    1 7 gnl_MPB_PFLU_1	Geneious	regulatory_region	2442766	2442780	.	+	.	regulatory_class=other;locus_tag=PFLU_2252;note=possibly recognised by TAT system for transport;ID=regulatory_region_1
    2 0 gnl_MPB_PFLU_1	ena	CDS	2466453	2468684	.	-	0	Dbxref=EMBL:AM181176,InterPro:IPR000674,InterPro:IPR006311,InterPro:IPR008274,InterPro:IPR012368,InterPro:IPR037165,KEGG:pfs:PFLU_2271,OrthoDB:337328at2,PRIDE:C3KAR0,Pfam:PF02738,RefSeq:WP_012723500.1,STRING:216595.PFLU_2271,SUPFAM:SSF56003,eggNOG:COG1529,Pubmed:19432983;ID=CDS:CAY48506;Ontology_term=GO:0016491;Parent=transcript:CAY48506;codon_start=1;confidence_level=4;description=Putative exported isoquinoline 1-oxidoreductase%2C beta subunit;features=Domain (1);frame=0;ft_domain=DOMAIN 222..301%3B  /note%3D"Ald_Xan_dh_C"%3B  /evidence%3D"ECO:0000259|SMART:SM01008";gene=PFLU_2271;inference=Predicted;locus_tag=PFLU_2271;ncbi_feature_key=CDS;product=putative exported isoquinoline 1-oxidoreductase%2Cbeta subunit;protein_id=CAY48506;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B with%3DUniProt:Q51698 (EMBL:PDIORAB)%3B Brevundimonas diminuta (Pseudomonas diminuta).%3B iorB%3B Isoquinoline 1-oxidoreductase beta subunit (EC 1.3.99.16).%3B length%3D781%3B id 31.137%25%3B ungapped id 34.040%25%3B E()%3D9.2e-41%3B 774 aa overlap%3B query 14-737%3B subject 10-767,fasta%3B with%3DUniProt:Q88GU5%3B Pseudomonas putida (strain KT2440).%3B Isoquinoline 1-oxidoreductase%2C beta subunit%2C putative.%3B length%3D751%3B id 84.873%25%3B ungapped id 85.330%25%3B E()%3D0%3B 747 aa overlap%3B query 1-743%3B subject 5-751;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    2 1 gnl_MPB_PFLU_1	ena	exon	2466453	2468684	.	-	.	ID=exon_2259;Name=CAY48506-1;Parent=transcript:CAY48506;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY48506-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    2 2 gnl_MPB_PFLU_1	ena	gene	2466453	2468684	.	-	.	ID=gene:PFLU_2271;biotype=protein_coding;description=putative exported isoquinoline 1-oxidoreductase%2C beta subunit;frame=.;gene_id=PFLU_2271;logic_name=ena;seqid=gnl_MPB_PFLU_1
    2 3 gnl_MPB_PFLU_1	ena	mRNA	2466453	2468684	.	-	.	ID=transcript:CAY48506;Parent=gene:PFLU_2271;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY48506
    2 4 gnl_MPB_PFLU_1	Geneious	biological_region	2468544	2468684	.	-	.	ID=misc_1562;Name=misc;algorithm=SignalP 2.0 HMM;cleavage=0.733;coord=47%2C48;frame=.;locus_tag=PFLU_2271;ncbi_feature_key=misc_feature;note=Signal peptide predicted for PFLU_2271 by SignalP 2.0 HMM (Signal peptide probability 0.752) with cleavage site probability 0.733 between residues 47 and 48;seqid=gnl_MPB_PFLU_1;signal=0.752;type=signalp
    2 5 gnl_MPB_PFLU_1	Geneious	regulatory_region	2468613	2468627	.	-	.	regulatory_class=other;locus_tag=PFLU_2271;note=possibly recognised by TAT system for transport;ID=regulatory_region_2
    3 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    3 1 gnl_MPB_PFLU_1	ena	CDS	2625089	2626015	.	+	0	Dbxref=EMBL:AM181176,GeneID:58734012,InterPro:IPR006860,InterPro:IPR012373,InterPro:IPR032623,KEGG:pfs:PFLU_2414,OrthoDB:1381035at2,PANTHER:PTHR30273,Pfam:PF04773,Pfam:PF16220,RefSeq:WP_012723636.1,STRING:216595.PFLU_2414,eggNOG:COG3712,Pubmed:19432983;ID=CDS:CAY48645;Parent=transcript:CAY48645;codon_start=1;confidence_level=4;description=Putative iron siderophore-related protein;features=Domain (2);frame=0;ft_domain=DOMAIN 19..61%3B  /note%3D"DUF4880"%3B  /evidence%3D"ECO:0000259|Pfam:PF16220"%3B DOMAIN 111..195%3B  /note%3D"FecR"%3B  /evidence%3D"ECO:0000259|Pfam:PF04773";gene=PFLU_2414;inference=Predicted;locus_tag=PFLU_2414;ncbi_feature_key=CDS;product=putative iron siderophore-related protein;protein_id=CAY48645;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B with%3DUniProt:P23485 (EMBL:ECFECIR)%3B Escherichia coli.%3B fecR%3B Protein fecR.%3B length%3D317%3B id 23.101%25%3B ungapped id 25.795%25%3B E()%3D0.0067%3B 316 aa overlap%3B query 1-296%3B subject 1-303,fasta%3B with%3DUniProt:Q6NA51%3B Rhodopseudomonas palustris.%3B Probable FecR%2C iron siderophore sensor protein.%3B length%3D328%3B id 41.587%25%3B ungapped id 44.710%25%3B E()%3D6.4e-27%3B 315 aa overlap%3B query 15-308%3B subject 15-328;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    3 2 gnl_MPB_PFLU_1	ena	exon	2625089	2626015	.	+	.	ID=exon_2398;Name=CAY48645-1;Parent=transcript:CAY48645;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY48645-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    3 3 gnl_MPB_PFLU_1	ena	gene	2625089	2626015	.	+	.	ID=gene:PFLU_2414;biotype=protein_coding;description=putative iron siderophore-related protein;frame=.;gene_id=PFLU_2414;logic_name=ena;seqid=gnl_MPB_PFLU_1
    3 4 gnl_MPB_PFLU_1	ena	mRNA	2625089	2626015	.	+	.	ID=transcript:CAY48645;Parent=gene:PFLU_2414;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY48645
    3 5 gnl_MPB_PFLU_1	Geneious	regulatory_region	2625323	2625343	.	+	.	regulatory_class=other;locus_tag=PFLU_2414;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_3
    4 0 gnl_MPB_PFLU_1	ena	CDS	2789118	2790389	.	-	0	Dbxref=EMBL:AM181176,InterPro:IPR000192,InterPro:IPR006311,InterPro:IPR015421,InterPro:IPR015422,InterPro:IPR015424,InterPro:IPR019546,InterPro:IPR020578,KEGG:pfs:PFLU_2549,OrthoDB:446447at2,Pfam:PF00266,RefSeq:WP_012723753.1,STRING:216595.PFLU_2549,SUPFAM:SSF53383,TIGRFAMs:TIGR01409,eggNOG:COG0520,Pubmed:19432983;ID=CDS:CAY48785;Ontology_term=GO:0008483;Parent=transcript:CAY48785;codon_start=1;confidence_level=4;description=Putative aminotransferase;features=Chain (1)%3B Domain (1)%3B Signal peptide (1);frame=0;ft_domain=DOMAIN 62..390%3B  /note%3D"Aminotran_5"%3B  /evidence%3D"ECO:0000259|Pfam:PF00266";gene=PFLU_2549;inference=Inferred from homology;locus_tag=PFLU_2549;ncbi_feature_key=CDS;product=putative aminotransferase;protein_families=Class-V pyridoxal-phosphate-dependent aminotransferase family;protein_id=CAY48785;seqid=gnl_MPB_PFLU_1;similarity=SIMILARITY: Belongs to the class-V pyridoxal-phosphate-dependent aminotransferase family. {ECO:0000256|RuleBase:RU004075}.,fasta%3B with%3DUniProt:Q884D8 (EMBL:AE016853%3B)%3B Pseudomonas syringae pv. tomato.%3B Aminotransferase%2C class V.%3B length%3D427%3B id 72.066%25%3B ungapped id 72.749%25%3B E()%3D2.5e-122%3B 426 aa overlap%3B query 1-422%3B subject 1-426,fasta%3B with%3DUniProt:Q89UX9 (EMBL:BA000040%3B)%3B Bradyrhizobium japonicum.%3B Blr1280 protein.%3B length%3D481%3B id 27.045%25%3B ungapped id 29.750%25%3B E()%3D2.2e-11%3B 440 aa overlap%3B query 2-419%3B subject 51-472;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    4 1 gnl_MPB_PFLU_1	ena	exon	2789118	2790389	.	-	.	ID=exon_2529;Name=CAY48785-1;Parent=transcript:CAY48785;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY48785-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    4 2 gnl_MPB_PFLU_1	ena	gene	2789118	2790389	.	-	.	ID=gene:PFLU_2549;biotype=protein_coding;description=putative aminotransferase;frame=.;gene_id=PFLU_2549;logic_name=ena;seqid=gnl_MPB_PFLU_1
    4 3 gnl_MPB_PFLU_1	ena	mRNA	2789118	2790389	.	-	.	ID=transcript:CAY48785;Parent=gene:PFLU_2549;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY48785
    4 4 gnl_MPB_PFLU_1	Geneious	biological_region	2790300	2790389	.	-	.	ID=misc_1759;Name=misc;algorithm=SignalP 2.0 HMM;cleavage=0.774;coord=30%2C31;frame=.;locus_tag=PFLU_2549;ncbi_feature_key=misc_feature;note=Signal peptide predicted for PFLU_2549 by SignalP 2.0 HMM (Signal peptide probability 1.000) with cleavage site probability 0.774 between residues 30 and 31;seqid=gnl_MPB_PFLU_1;signal=1.000;type=signalp
    4 5 gnl_MPB_PFLU_1	Geneious	regulatory_region	2790363	2790383	.	-	.	regulatory_class=other;locus_tag=PFLU_2549;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_4
    5 0 gnl_MPB_PFLU_1	ena	CDS	2980985	2986072	.	-	0	Dbxref=EMBL:AM181176,KEGG:pfs:PFLU_2703,OrthoDB:4635at2,RefSeq:WP_012723893.1,STRING:216595.PFLU_2703,eggNOG:ENOG5031TID,Pubmed:19432983;ID=CDS:CAY48934;Parent=transcript:CAY48934;codon_start=1;confidence_level=4;description=Uncharacterized protein;features=Coiled coil (1);frame=0;gene=PFLU_2703;inference=Predicted;locus_tag=PFLU_2703;ncbi_feature_key=CDS;product=conserved hypothetical protein;protein_id=CAY48934;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B with%3DUniProt:Q87YT5 (EMBL:AE016853%3B)%3B Pseudomonas syringae pv. tomato.%3B Hypothetical protein.%3B length%3D1743%3B id 24.345%25%3B ungapped id 27.025%25%3B E()%3D5e-08%3B 1795 aa overlap%3B query 69-1748%3B subject 6-1737;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    5 1 gnl_MPB_PFLU_1	ena	exon	2980985	2986072	.	-	.	ID=exon_2679;Name=CAY48934-1;Parent=transcript:CAY48934;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY48934-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    5 2 gnl_MPB_PFLU_1	ena	gene	2980985	2986072	.	-	.	ID=gene:PFLU_2703;biotype=protein_coding;description=conserved hypothetical protein;frame=.;gene_id=PFLU_2703;logic_name=ena;seqid=gnl_MPB_PFLU_1
    5 3 gnl_MPB_PFLU_1	ena	mRNA	2980985	2986072	.	-	.	ID=transcript:CAY48934;Parent=gene:PFLU_2703;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY48934
    5 4 gnl_MPB_PFLU_1	Geneious	regulatory_region	2985170	2985190	.	-	.	regulatory_class=other;locus_tag=PFLU_2703;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_5
    6 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    6 1 gnl_MPB_PFLU_1	ena	CDS	3077455	3077973	.	+	0	Dbxref=EMBL:AM181176,InterPro:IPR007627,InterPro:IPR013249,InterPro:IPR013324,InterPro:IPR013325,InterPro:IPR014284,InterPro:IPR036388,InterPro:IPR039425,KEGG:pfs:PFLU_2787,OrthoDB:1914729at2,PANTHER:PTHR43133,Pfam:PF04542,Pfam:PF08281,RefSeq:WP_012723972.1,STRING:216595.PFLU_2787,SUPFAM:SSF88659,SUPFAM:SSF88946,TIGRFAMs:TIGR02937,eggNOG:COG1595,Pubmed:19432983;ID=CDS:CAY49017;Ontology_term=GO:0003677,GO:0006352,GO:0016987;Parent=transcript:CAY49017;codon_start=1;confidence_level=2;description=Heme uptake regulator;features=Domain (2);frame=0;ft_domain=DOMAIN 18..84%3B  /note%3D"Sigma70_r2"%3B  /evidence%3D"ECO:0000259|Pfam:PF04542"%3B DOMAIN 114..167%3B  /note%3D"Sigma70_r4_2"%3B  /evidence%3D"ECO:0000259|Pfam:PF08281";gene=PFLU_2787;inference=Inferred from homology;locus_tag=PFLU_2787;ncbi_feature_key=CDS;note=%3B evidence%3DIEA%3B db_xref%3Dbbronchiseptica:BB4653%3B date%3D20060221%3B method%3Dautomatic:reciprocal fasta;ortholog=GeneDB_bavium:BAV3136,GeneDB_bbronchiseptica:BB4653,GeneDB_bparapertussis:BPP4183,GeneDB_bpertussis:BP0349,GeneDB_ecarotovora_atroseptica:ECA1079,GeneDB_ecoli042:Ec042-4782;primary_name=hurI;product=heme uptake regulator;protein_families=Sigma-70 factor family%2C ECF subfamily;protein_id=CAY49017;seqid=gnl_MPB_PFLU_1;similarity=SIMILARITY: Belongs to the sigma-70 factor family. ECF subfamily. {ECO:0000256|ARBA:ARBA00010641}.,fasta%3B BAV3136%3B bavium%3B heme uptake RNA polymerase sigma-70 factor%3B length 171 aa%3B id%3D55.6%25%3B %3B E()%3D2.4e-32%3B 153 aa overlap%3B query 18-170 aa%3B subject 15-167 aa,fasta%3B BB4653%3B bbronchiseptica%3B heme uptake regulator%3B length 169 aa%3B id%3D55.8%25%3B %3B E()%3D7.2e-33%3B 165 aa overlap%3B query 6-170 aa%3B subject 3-167 aa,fasta%3B BP0349%3B bpertussis%3B heme uptake regulator%3B length 169 aa%3B id%3D55.8%25%3B %3B E()%3D3.1e-33%3B 165 aa overlap%3B query 6-170 aa%3B subject 3-167 aa,fasta%3B BPP4183%3B bparapertussis%3B heme uptake regulator%3B length 169 aa%3B id%3D55.2%25%3B %3B E()%3D1.2e-32%3B 165 aa overlap%3B query 6-170 aa%3B subject 3-167 aa,fasta%3B ECA1079%3B ecarotovora_atroseptica%3B RNA polymerase sigma factor%3B length 173 aa%3B id%3D49.4%25%3B %3B E()%3D1.7e-28%3B 166 aa overlap%3B query 7-172 aa%3B subject 5-169 aa,fasta%3B Ec042-4782%3B ecoli042%3B KpLE2 phage-likeelement\%3B sigma 19 factor of RNA polymerase%3B length 173aa%3B id%3D50.0%25%3B %3B E()%3D4.1e-30%3B 172 aa overlap%3B query 1-172aa%3B subject 1-169 aa;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    6 2 gnl_MPB_PFLU_1	ena	exon	3077455	3077973	.	+	.	ID=exon_2762;Name=CAY49017-1;Parent=transcript:CAY49017;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY49017-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    6 3 gnl_MPB_PFLU_1	ena	gene	3077455	3077973	.	+	.	ID=gene:PFLU_2787;biotype=protein_coding;description=heme uptake regulator;frame=.;gene_id=PFLU_2787;logic_name=ena;seqid=gnl_MPB_PFLU_1
    6 4 gnl_MPB_PFLU_1	ena	mRNA	3077455	3077973	.	+	.	ID=transcript:CAY49017;Parent=gene:PFLU_2787;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY49017
    6 5 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	3077794	3077955	.	+	.	ID=biological_region_5083;external_name=PFLU_2787%3B AM181176.4:misc_feature:3077584..3077745;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    6 6 gnl_MPB_PFLU_1	Geneious	regulatory_region	3077836	3077856	.	+	.	regulatory_class=other;locus_tag=PFLU_2787;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_6
    7 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    7 1 gnl_MPB_PFLU_1	Geneious	sequence_difference	3321875	3396611	.	+	.	ID=ReD33 difference;Name=ReD33 difference;external_name=AM181176.4:misc_feature:3321665..3396401;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=sequence_difference;note=ReD33;seqid=gnl_MPB_PFLU_1
    7 2 gnl_MPB_PFLU_1	Geneious	regulatory_region	3327238	3327265	.	+	.	Name=dif regulatory;regulatory_class=other;note=dif;NCBI Feature Key=regulatory;ID=regulatory_region_7
    8 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    8 1 gnl_MPB_PFLU_1	Geneious	sequence_difference	3451692	3626373	.	+	.	ID=ReD35 difference;Name=ReD35 difference;external_name=AM181176.4:misc_feature:3451484..3626165;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=sequence_difference;note=ReD35;seqid=gnl_MPB_PFLU_1
    8 2 gnl_MPB_PFLU_1	ena	CDS	3460515	3461204	.	+	0	Dbxref=EMBL:AM181176,InterPro:IPR015414,InterPro:IPR032816,KEGG:pfs:PFLU_3165,PANTHER:PTHR12677,Pfam:PF09335,STRING:216595.PFLU_3165,eggNOG:COG0398,Pubmed:19432983;ID=CDS:CAY49392;Ontology_term=GO:0005886,GO:0016021;Parent=transcript:CAY49392;codon_start=1;confidence_level=3;description=TVP38/TMEM64 family membrane protein;features=Transmembrane (4);frame=0;gene=PFLU_3165;inference=Inferred from homology;locus_tag=PFLU_3165;ncbi_feature_key=CDS;note=%3B evidence%3DIEA%3B db_xref%3Dcbotulinum:CBO0356%3B date%3D20060221%3B method%3Dautomatic:top fasta hit;product=putative membrane protein;protein_families=TVP38/TMEM64 family;protein_id=CAY49392;seqid=gnl_MPB_PFLU_1;similarity=SIMILARITY: Belongs to the TVP38/TMEM64 family. {ECO:0000256|RuleBase:RU366058}.,fasta%3B CBO0356%3B cbotulinum%3B putative membraneprotein%3B length 224 aa%3B id%3D33.3%25%3B %3B E()%3D1.1e-13%3B 150 aaoverlap%3B query 25-173 aa%3B subject 35-182 aa;uniprot_annotation_score=2 out of 5;uniprot_review_status=unreviewed
    8 3 gnl_MPB_PFLU_1	ena	exon	3460515	3461204	.	+	.	ID=exon_3139;Name=CAY49392-1;Parent=transcript:CAY49392;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY49392-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    8 4 gnl_MPB_PFLU_1	ena	gene	3460515	3461204	.	+	.	ID=gene:PFLU_3165;biotype=protein_coding;description=putative membrane protein;frame=.;gene_id=PFLU_3165;logic_name=ena;seqid=gnl_MPB_PFLU_1
    8 5 gnl_MPB_PFLU_1	ena	mRNA	3460515	3461204	.	+	.	ID=transcript:CAY49392;Parent=gene:PFLU_3165;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY49392
    8 6 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	3460617	3461159	.	+	.	ID=biological_region_5676;external_name=4 probable transmembrane helices predicted for PFLU_3165 by TMHMM2.0 at aa 35-57%2C 72-94%2C 154-176 and 196-215;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    8 7 gnl_MPB_PFLU_1	Geneious	biological_region	3460620	3461081	.	+	.	ID=HMMPfam_3216;Name=HMMPfam;domain=HMMPfam:PF00597%3BDedA family%3B4.4e-05%3Bcodon 36-189;frame=.;locus_tag=PFLU_3165;ncbi_feature_key=misc_feature;note=HMMPfam hit to PF00597%2C DedA family%2C score 4.4e-05;seqid=gnl_MPB_PFLU_1
    8 8 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	3460680	3461039	.	+	.	ID=biological_region_5677;external_name=PFLU_3165%3B AM181176.4:misc_feature:3460472..3460831;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    8 9 gnl_MPB_PFLU_1	Geneious	regulatory_region	3460962	3460982	.	+	.	regulatory_class=other;locus_tag=PFLU_3165;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_8
    8 10 gnl_MPB_PFLU_1	Geneious	biological_region	3460974	3461042	.	+	.	ID=H3vLTfdV07uecYH3tU9j82ABBTs.1627027358010.548.2;Name=misc;algorithm=TMHMM 2.0;coord=35%2C57,72%2C94,154%2C176,196%2C215;frame=.;locus_tag=PFLU_3165;ncbi_feature_key=misc_feature;ncbi_join_type=join;note=4 probable transmembrane helices predicted for PFLU_3165 by TMHMM2.0 at aa 35-57%2C 72-94%2C 154-176 and 196-215;seqid=gnl_MPB_PFLU_1;type=TMHMM
    9 0 gnl_MPB_PFLU_1	ena	CDS	3825857	3826954	.	-	0	Dbxref=EMBL:AM181176,InterPro:IPR005511,InterPro:IPR006311,InterPro:IPR011042,InterPro:IPR013658,KEGG:pfs:PFLU_3458,OrthoDB:966287at2,PRIDE:C3KCG5,PRINTS:PR01790,Pfam:PF08450,RefSeq:WP_012724612.1,STRING:216595.PFLU_3458,eggNOG:COG3386,Pubmed:19432983;ID=CDS:CAY49680;Parent=transcript:CAY49680;codon_start=1;confidence_level=3;description=Putative gluconolactonase;features=Domain (1);frame=0;ft_domain=DOMAIN 79..347%3B  /note%3D"SGL"%3B  /evidence%3D"ECO:0000259|Pfam:PF08450";gene=PFLU_3458;inference=Predicted;locus_tag=PFLU_3458;ncbi_feature_key=CDS;note=%3B evidence%3DIEA%3B db_xref%3Dbpseudomallei:BPSS0035%3B date%3D20060221%3B method%3Dautomatic:reciprocal fasta;ortholog=GeneDB_bpseudomallei:BPSS0035,GeneDB_rleguminosarum:RL4240,GeneDB_scoelicolor:SCO0524;product=putative gluconolactonase;protein_id=CAY49680;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B BPSS0035%3B bpseudomallei%3B putative gluconolactonase%3B length 316 aa%3B id%3D55.3%25%3B %3B E()%3D1.6e-70%3B 329 aa overlap%3B query 37-364 aa%3B subject 1-316 aa,fasta%3B RL4240%3B rleguminosarum%3B conserved hypothetical protein%3B length 303 aa%3B id%3D43.7%25%3B %3B E()%3D6.6e-41%3B 318 aa overlap%3B query 50-363 aa%3B subject 4-302 aa,fasta%3B SCO0524%3B scoelicolor%3B possible gluconolactonase precursor%3B length 302 aa%3B id%3D42.9%25%3B %3B E()%3D1.2e-38%3B 315 aa overlap%3B query 50-361 aa%3B subject 4-299 aa;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    9 1 gnl_MPB_PFLU_1	ena	exon	3825857	3826954	.	-	.	ID=exon_3428;Name=CAY49680-1;Parent=transcript:CAY49680;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY49680-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    9 2 gnl_MPB_PFLU_1	ena	gene	3825857	3826954	.	-	.	ID=gene:PFLU_3458;biotype=protein_coding;description=putative gluconolactonase;frame=.;gene_id=PFLU_3458;logic_name=ena;seqid=gnl_MPB_PFLU_1
    9 3 gnl_MPB_PFLU_1	ena	mRNA	3825857	3826954	.	-	.	ID=transcript:CAY49680;Parent=gene:PFLU_3458;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY49680
    9 4 gnl_MPB_PFLU_1	Geneious	biological_region	3826841	3826954	.	-	.	ID=misc_2315;Name=misc;algorithm=SignalP 2.0 HMM;cleavage=0.537;coord=38%2C39;frame=.;locus_tag=PFLU_3458;ncbi_feature_key=misc_feature;note=Signal peptide predicted for PFLU_3458 by SignalP 2.0 HMM (Signal peptide probability 0.893) with cleavage site probability 0.537 between residues 38 and 39;seqid=gnl_MPB_PFLU_1;signal=0.893;type=signalp
    9 5 gnl_MPB_PFLU_1	Geneious	regulatory_region	3826898	3826918	.	-	.	regulatory_class=other;locus_tag=PFLU_3458;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_9
    10 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    10 1 gnl_MPB_PFLU_1	ena	CDS	3831158	3832045	.	+	0	Dbxref=EMBL:AM181176,InterPro:IPR002925,InterPro:IPR006311,InterPro:IPR029058,KEGG:pfs:PFLU_3464,OrthoDB:1889569at2,PRIDE:C3KCH1,Pfam:PF01738,RefSeq:WP_012724618.1,STRING:216595.PFLU_3464,SUPFAM:SSF53474,eggNOG:COG0412,Pubmed:19432983;ID=CDS:CAY49686;Ontology_term=EC:3.1.1.45,GO:0008806,GO:0016021;Parent=transcript:CAY49686;codon_start=1;confidence_level=3;description=Putative carboxymethylenebutenolidase (EC 3.1.1.45);features=Domain (1)%3B Transmembrane (1);frame=0;ft_domain=DOMAIN 85..293%3B  /note%3D"DLH"%3B  /evidence%3D"ECO:0000259|Pfam:PF01738";gene=PFLU_3464;inference=Predicted;locus_tag=PFLU_3464;ncbi_feature_key=CDS;note=%3B evidence%3DIEA%3B db_xref%3Decarotovora_atroseptica:ECA0184%3B date%3D20060221%3B method%3Dautomatic:reciprocal fasta;ortholog=GeneDB_bbronchiseptica:BB1332,GeneDB_bparapertussis:BPP1116,GeneDB_bpseudomallei:BPSS1767,GeneDB_ecarotovora_atroseptica:ECA0184,GeneDB_sbongori:SBG3500,GeneDB_senteritidisPT4:SEN3762,GeneDB_styphi:STY3592,GeneDB_styphimuriumDT104:SDT3974;product=putative carboxymethylenebutenolidase;protein_id=CAY49686;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B BB1332%3B bbronchiseptica%3B putative hydrolase%3B length 301 aa%3B id%3D30.7%25%3B %3B E()%3D0.00018%3B 274 aa overlap%3B query 41-293 aa%3B subject 28-297 aa,fasta%3B BPP1116%3B bparapertussis%3B putative hydrolase%3B length 301 aa%3B id%3D30.7%25%3B %3B E()%3D0.00023%3B 274 aa overlap%3B query 41-293 aa%3B subject 28-297 aa,fasta%3B BPSS1767%3B bpseudomallei%3B putative carboxymethylenebutenolidase%3B length 291 aa%3B id%3D32.6%25%3B %3B E()%3D1.3e-10%3B 291 aa overlap%3B query 22-293 aa%3B subject 12-288 aa,fasta%3B ECA0184%3B ecarotovora_atroseptica%3B putative carboxymethylenebutenolidase%3B length 275 aa%3B id%3D37.5%25%3B %3B E()%3D1.8e-15%3B 240 aa overlap%3B query 68-292 aa%3B subject 35-271 aa,fasta%3B SBG3500%3B sbongori%3B putative hydrolase%3Blength 270 aa%3B id%3D34.0%25%3B %3B E()%3D2.5e-07%3B 265 aa overlap%3Bquery 45-293 aa%3B subject 7-265 aa,fasta%3B SDT3974%3B styphimuriumDT104%3B putative hydrolase%3B length 270 aa%3B id%3D34.2%25%3B %3B E()%3D4.5e-07%3B 266 aa overlap%3B query 45-293 aa%3B subject 7-265 aa,fasta%3B SEN3762%3B senteritidisPT4%3B putative hydrolase%3B length 270 aa%3B id%3D34.2%25%3B %3B E()%3D4.8e-07%3B 266 aa overlap%3B query 45-293 aa%3B subject 7-265 aa,fasta%3B STY3592%3B styphi%3B putative hydrolase%3B length 270 aa%3B id%3D33.8%25%3B %3B E()%3D1.5e-06%3B 266 aa overlap%3B query 45-293 aa%3B subject 7-265 aa;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    10 2 gnl_MPB_PFLU_1	ena	exon	3831158	3832045	.	+	.	ID=exon_3434;Name=CAY49686-1;Parent=transcript:CAY49686;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY49686-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    10 3 gnl_MPB_PFLU_1	ena	gene	3831158	3832045	.	+	.	ID=gene:PFLU_3464;biotype=protein_coding;description=putative carboxymethylenebutenolidase;frame=.;gene_id=PFLU_3464;logic_name=ena;seqid=gnl_MPB_PFLU_1
    10 4 gnl_MPB_PFLU_1	ena	mRNA	3831158	3832045	.	+	.	ID=transcript:CAY49686;Parent=gene:PFLU_3464;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY49686
    10 5 gnl_MPB_PFLU_1	Geneious	regulatory_region	3831233	3831253	.	+	.	regulatory_class=other;locus_tag=PFLU_3464;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_10
    11 0 gnl_MPB_PFLU_1	ena	CDS	4060533	4061510	.	-	0	Dbxref=EMBL:AM181176,GeneID:58734580,HAMAP:MF_00956,InterPro:IPR001509,InterPro:IPR028614,InterPro:IPR036291,KEGG:pfs:PFLU_3667,OrthoDB:1558163at2,Pfam:PF01370,RefSeq:WP_012724814.1,STRING:216595.PFLU_3667,SUPFAM:SSF51735,UniPathway:UPA00128%3BUER00191,eggNOG:COG0451,Pubmed:19432983;ID=CDS:CAY49920;Ontology_term=EC:1.1.1.271,EC:5.1.3.-,GO:0016853,GO:0042351,GO:0050577,GO:0070401;Parent=transcript:CAY49920;codon_start=1;confidence_level=2;description=GDP-L-fucose synthase (EC 1.1.1.271) (GDP-4-keto-6-deoxy-D-mannose-3%2C5-epimerase-4-reductase);features=Active site (1)%3B Binding site (6)%3B Domain (1)%3B Nucleotide binding (3)%3B Site (2);frame=0;ft_domain=DOMAIN 9..232%3B  /note%3D"Epimerase"%3B  /evidence%3D"ECO:0000259|Pfam:PF01370";gene=PFLU_3667;inference=Inferred from homology;locus_tag=PFLU_3667;ncbi_feature_key=CDS;note=%3B evidence%3DIEA%3B db_xref%3DstyphimuriumDT104:SDT2164%3Bdate%3D20060221%3B method%3Dautomatic:reciprocal fasta;ortholog=GeneDB_Cms:CMS1580,GeneDB_bfragilisNCTC9343:BF1887,GeneDB_cjejuni:Cj1428c,GeneDB_ecoli042:Ec042-2289,GeneDB_mtuberculosis:Rv1512,GeneDB_rleguminosarum:RL0826,GeneDB_sbongori:SBG1927,GeneDB_senteritidisPT4:SEN2104,GeneDB_styphi:STY2320,GeneDB_styphimuriumDT104:SDT2164,GeneDB_yenterocolitica:YE3074;pathway=PATHWAY: Nucleotide-sugar biosynthesis%3B GDP-L-fucose biosynthesis via de novo pathway%3B GDP-L-fucose from GDP-alpha-D-mannose: step 2/2. {ECO:0000256|HAMAP-Rule:MF_00956}.;primary_name=fcl;product=GDP-fucose synthetase;protein_families=NAD(P)-dependent epimerase/dehydratase family%2C Fucose synthase subfamily;protein_id=CAY49920;seqid=gnl_MPB_PFLU_1;similarity=SIMILARITY: Belongs to the NAD(P)-dependent epimerase/dehydratase family. Fucose synthase subfamily. {ECO:0000256|ARBA:ARBA00005959%2C ECO:0000256|HAMAP-Rule:MF_00956}.,fasta%3B BF1887%3B bfragilisNCTC9343%3B putative GDP-L-fucose synthetase%3B length 360 aa%3B id%3D47.5%25%3B %3B E()%3D5.6e-44%3B 354 aa overlap%3B query 4-316 aa%3B subject 6-359 aa,fasta%3B CMS1580%3B Cms%3B GDP-l-fucose synthetase%3Blength 334 aa%3B id%3D55.6%25%3B %3B E()%3D1.4e-51%3B 320 aa overlap%3Bquery 4-323 aa%3B subject 23-333 aa,fasta%3B Cj1428c%3B cjejuni%3B putative fucose synthetase%3B length 346 aa%3B id%3D36.2%25%3B %3B E()%3D1.9e-29%3B 340 aa overlap%3B query 6-315 aa%3B subject 4-342 aa,fasta%3B Ec042-2289%3B ecoli042%3B bifunctional GDP-fucose synthetase: GDP-4-dehydro-6-dexoy-D-mannose epimerase\%3B GDP-4-dehydro-6-L-deoxygalactose reductase%2C has NAD(P)-binding site%2C colanic acid synthesis%3B length 321 aa%3B id%3D67.8%25%3B %3B E()%3D2.9e-87%3B 317 aa overlap%3B query 8-324 aa%3B subject 5-321 aa,fasta%3B RL0826%3B rleguminosarum%3B putative GDP-L-fucose synthetase%3B length 326 aa%3B id%3D60.8%25%3B %3B E()%3D1.4e-71%3B 316 aa overlap%3B query 8-317 aa%3B subject 6-321 aa,fasta%3B Rv1512%3B mtuberculosis%3B probable nucleotide-sugar epimerase epiA%3B length 322 aa%3B id%3D50.8%25%3B %3B E()%3D2.9e-44%3B 313 aa overlap%3B query 4-315 aa%3B subject 11-313 aa,fasta%3B SBG1927%3B sbongori%3B GDP-fucose synthetase%3B length 321 aa%3B id%3D68.1%25%3B %3B E()%3D1.8e-87%3B 317 aa overlap%3B query 8-324 aa%3B subject 5-321 aa,fasta%3B SDT2164%3B styphimuriumDT104%3B GDP-fucosesynthetase%3B length 321 aa%3B id%3D69.1%25%3B %3B E()%3D1.9e-89%3B 317 aaoverlap%3B query 8-324 aa%3B subject 5-321 aa,fasta%3B SEN2104%3B senteritidisPT4%3B GDP-fucose synthetase%3B length 321 aa%3B id%3D69.1%25%3B %3B E()%3D2.5e-87%3B 317 aa overlap%3B query 8-324 aa%3B subject 5-321 aa,fasta%3B STY2320%3B styphi%3B GDP-fucose synthetase%3B length 321 aa%3B id%3D69.1%25%3B %3B E()%3D6.9e-90%3B 317 aa overlap%3B query 8-324 aa%3B subject 5-321 aa,fasta%3B YE3074%3B yenterocolitica%3B GDP-fucose synthetase%3B length 321 aa%3B id%3D67.0%25%3B %3B E()%3D7.7e-86%3B 309 aa overlap%3B query 8-316 aa%3B subject 5-313 aa;uniprot_annotation_score=3 out of 5;uniprot_review_status=unreviewed
    11 1 gnl_MPB_PFLU_1	ena	exon	4060533	4061510	.	-	.	ID=exon_3633;Name=CAY49920-1;Parent=transcript:CAY49920;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY49920-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    11 2 gnl_MPB_PFLU_1	ena	gene	4060533	4061510	.	-	.	ID=gene:PFLU_3667;biotype=protein_coding;description=GDP-fucose synthetase;frame=.;gene_id=PFLU_3667;logic_name=ena;seqid=gnl_MPB_PFLU_1
    11 3 gnl_MPB_PFLU_1	ena	mRNA	4060533	4061510	.	-	.	ID=transcript:CAY49920;Parent=gene:PFLU_3667;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY49920
    11 4 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	4060764	4061486	.	-	.	ID=biological_region_6600;external_name=PFLU_3667%3B AM181176.4:misc_feature:complement(4060904..4061626);frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    11 5 gnl_MPB_PFLU_1	Geneious	regulatory_region	4060863	4060883	.	-	.	regulatory_class=other;locus_tag=PFLU_3667;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_11
    12 0 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	2050710	4749278	.	+	.	ID=biological_region_3485;external_name=1/3 vs 2/3;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    12 1 gnl_MPB_PFLU_1	ena	CDS	4676669	4678183	.	+	0	Dbxref=EMBL:AM181176,InterPro:IPR011701,InterPro:IPR020846,InterPro:IPR036259,KEGG:pfs:PFLU_4234,OrthoDB:1108236at2,Pfam:PF07690,RefSeq:WP_015885011.1,STRING:216595.PFLU_4234,SUPFAM:SSF103473,eggNOG:COG0477,Pubmed:19432983;ID=CDS:CAY50775;Ontology_term=GO:0016021,GO:0022857;Parent=transcript:CAY50775;codon_start=1;confidence_level=3;description=Putative membrane protein;features=Domain (1)%3B Transmembrane (13);frame=0;ft_domain=DOMAIN 7..496%3B  /note%3D"MFS"%3B  /evidence%3D"ECO:0000259|PROSITE:PS50850";gene=PFLU_4234;inference=Predicted;locus_tag=PFLU_4234;ncbi_feature_key=CDS;note=%3B evidence%3DIEA%3B db_xref%3Dbpseudomallei:BPSS0216%3B date%3D20060221%3B method%3Dautomatic:reciprocal fasta;ortholog=GeneDB_Cms:CMS0961,GeneDB_bbronchiseptica:BB2411,GeneDB_bparapertussis:BPP1345,GeneDB_bpertussis:BP2133,GeneDB_bpseudomallei:BPSS0216,GeneDB_cdiphtheriae:DIP1082,GeneDB_scoelicolor:SCO2498;product=putative membrane protein;protein_id=CAY50775;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B BB2411%3B bbronchiseptica%3B putative transmembrane efflux protein%3B length 479 aa%3B id%3D33.5%25%3B %3B E()%3D3.3e-32%3B 507 aa overlap%3B query 2-503 aa%3B subject 8-475 aa,fasta%3B BP2133%3B bpertussis%3B putative transmembrane efflux protein%3B length 479 aa%3B id%3D33.7%25%3B %3B E()%3D6.5e-34%3B 507 aa overlap%3B query 2-503 aa%3B subject 8-475 aa,fasta%3B BPP1345%3B bparapertussis%3B putative transmembrane efflux protein%3B length 479 aa%3B id%3D33.5%25%3B %3B E()%3D1.3e-32%3B 507 aa overlap%3B query 2-503 aa%3B subject 8-475 aa,fasta%3B BPSS0216%3B bpseudomallei%3B putative membrane protein%3B length 545 aa%3B id%3D47.5%25%3B %3B E()%3D5.1e-66%3B 488 aa overlap%3B query 8-492 aa%3B subject 50-537 aa,fasta%3B CMS0961%3B Cms%3B putative drug efflux protein%3B length 511 aa%3B id%3D31.3%25%3B %3B E()%3D9e-20%3B 504 aa overlap%3B query 2-456 aa%3B subject 12-509 aa,fasta%3B DIP1082%3B cdiphtheriae%3B transmembrane efflux protein%2C major facilitator family%3B length 471 aa%3B id%3D31.4%25%3B %3B E()%3D1.8e-34%3B 458 aa overlap%3B query 3-445 aa%3B subject 20-470 aa,fasta%3B SCO2498%3B scoelicolor%3B putative efflux membrane protein%3B length 481 aa%3B id%3D37.0%25%3B %3B E()%3D1.7e-40%3B 440 aa overlap%3B query 2-434 aa%3B subject 7-435 aa;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    12 2 gnl_MPB_PFLU_1	ena	exon	4676669	4678183	.	+	.	ID=exon_4206;Name=CAY50775-1;Parent=transcript:CAY50775;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY50775-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    12 3 gnl_MPB_PFLU_1	ena	gene	4676669	4678183	.	+	.	ID=gene:PFLU_4234;biotype=protein_coding;description=putative membrane protein;frame=.;gene_id=PFLU_4234;logic_name=ena;seqid=gnl_MPB_PFLU_1
    12 4 gnl_MPB_PFLU_1	ena	mRNA	4676669	4678183	.	+	.	ID=transcript:CAY50775;Parent=gene:PFLU_4234;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY50775
    12 5 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	4676687	4678141	.	+	.	ID=biological_region_7669;external_name=14 probable transmembrane helices predicted for PFLU_4234 by TMHMM2.0 at aa 7-29%2C 44-65%2C 72-94%2C 98-120%2C 133-155%2C 160-182%2C 195-214%2C 219-241%2C 261-283%2C 293-312%2C 325-347%2C 352-369%2C 397-419 and 469-491;frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    12 6 gnl_MPB_PFLU_1	Geneious	biological_region	4676699	4677904	.	+	.	ID=HMMPfam_4367;Name=HMMPfam;domain=HMMPfam:PF07690%3BMajor Facilitator Superfamily%3B1.6e-55%3Bcodon 11-412;external_name=PFLU_4234%3B AM181176.4:misc_feature:4676839..4678044;frame=.;locus_tag=PFLU_4234;logic_name=ena_misc_feature;ncbi_feature_key=misc_feature;note=HMMPfam hit to PF07690%2C Major Facilitator Superfamily%2C score 1.6e-55;seqid=gnl_MPB_PFLU_1
    12 7 gnl_MPB_PFLU_1	Geneious	biological_region	4676849	4676899	.	+	.	ID=misc_2881;Name=misc;external_name=PS00216 Sugar transport proteins signature 1.;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=misc_feature;note=PS00216 Sugar transport proteins signature 1.;seqid=gnl_MPB_PFLU_1
    12 8 gnl_MPB_PFLU_1	Geneious	regulatory_region	4676876	4676896	.	+	.	regulatory_class=other;locus_tag=PFLU_4234;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_12
    12 9 gnl_MPB_PFLU_1	Geneious	biological_region	4676882	4676950	.	+	.	ID=H3vLTfdV07uecYH3tU9j82ABBTs.1627027358278.724.2;Name=misc;algorithm=TMHMM 2.0;coord=7%2C29,44%2C65,72%2C94,98%2C120,133%2C155,160%2C182,195%2C214,219%2C241,261%2C283,293%2C312,325%2C347,352%2C369,397%2C419,469%2C491;frame=.;locus_tag=PFLU_4234;ncbi_feature_key=misc_feature;ncbi_join_type=join;note=14 probable transmembrane helices predicted for PFLU_4234 by TMHMM2.0 at aa 7-29%2C 44-65%2C 72-94%2C 98-120%2C 133-155%2C 160-182%2C 195-214%2C 219-241%2C 261-283%2C 293-312%2C 325-347%2C 352-369%2C 397-419 and 469-491;seqid=gnl_MPB_PFLU_1;type=TMHMM
    13 0 gnl_MPB_PFLU_1	ena	CDS	5739195	5740367	.	-	0	Dbxref=EMBL:AM181176,GeneID:58735227,InterPro:IPR004838,InterPro:IPR004839,InterPro:IPR015421,InterPro:IPR015424,KEGG:pfs:PFLU_5232,OrthoDB:554560at2,PRIDE:C3K239,Pfam:PF00155,RefSeq:WP_015885900.1,STRING:216595.PFLU_5232,SUPFAM:SSF53383,eggNOG:COG0436,Pubmed:19432983;ID=CDS:CAY52319;Ontology_term=EC:2.6.1.-,GO:0008483,GO:0009058,GO:0030170;Parent=transcript:CAY52319;codon_start=1;confidence_level=4;description=Aminotransferase (EC 2.6.1.-);features=Domain (1);frame=0;ft_domain=DOMAIN 33..381%3B  /note%3D"Aminotran_1_2"%3B  /evidence%3D"ECO:0000259|Pfam:PF00155";gene=PFLU_5232;inference=Inferred from homology;locus_tag=PFLU_5232;ncbi_feature_key=CDS;note=%3B evidence%3DIEA%3B db_xref%3Dbpseudomallei:BPSL2629%3B date%3D20060221%3B method%3Dautomatic:reciprocal fasta;ortholog=GeneDB_bavium:BAV0857,GeneDB_bbronchiseptica:BB1374,GeneDB_bparapertussis:BPP1158,GeneDB_bpertussis:BP1062,GeneDB_bpseudomallei:BPSL2629,GeneDB_cdiphtheriae:DIP1929,GeneDB_mbovis:Mb3595,GeneDB_mtuberculosis:Rv3565,GeneDB_rleguminosarum:RL2092;product=putative putative aminotransferase;protein_families=Class-I pyridoxal-phosphate-dependent aminotransferase family;protein_id=CAY52319;seqid=gnl_MPB_PFLU_1;similarity=SIMILARITY: Belongs to the class-I pyridoxal-phosphate-dependent aminotransferase family. {ECO:0000256|RuleBase:RU000481}.,fasta%3B BAV0857%3B bavium%3B probable aminotransferase%3B length 384 aa%3B id%3D46.1%25%3B %3B E()%3D8.4e-62%3B 380 aa overlap%3B query 6-385 aa%3B subject 5-384 aa,fasta%3B BB1374%3B bbronchiseptica%3B probable aminotransferase%3B length 384 aa%3B id%3D49.5%25%3B %3B E()%3D7.8e-65%3B 380 aa overlap%3B query 6-385 aa%3B subject 5-384 aa,fasta%3B BP1062%3B bpertussis%3B probable aminotransferase%3B length 384 aa%3B id%3D49.2%25%3B %3B E()%3D2.2e-63%3B 380 aa overlap%3B query 6-385 aa%3B subject 5-384 aa,fasta%3B BPP1158%3B bparapertussis%3B probable aminotransferase%3B length 384 aa%3B id%3D49.5%25%3B %3B E()%3D1.8e-65%3B 380 aa overlap%3B query 6-385 aa%3B subject 5-384 aa,fasta%3B BPSL2629%3B bpseudomallei%3B putative aminotransferase%3B length 416 aa%3B id%3D51.6%25%3B %3B E()%3D2.6e-69%3B 382 aa overlap%3B query 6-383 aa%3B subject 31-412 aa,fasta%3B DIP1929%3B cdiphtheriae%3B Putative aspartate aminotransferase%3B length 378 aa%3B id%3D36.1%25%3B %3B E()%3D6e-43%3B 379 aa overlap%3B query 6-381 aa%3B subject 4-373 aa,fasta%3B Mb3595%3B mbovis%3B POSSIBLE ASPARTATE AMINOTRANSFERASE ASPB (TRANSAMINASE A) (ASPAT) (GLUTAMIC--OXALOACETIC TRANSAMINASE) (GLUTAMIC--ASPARTIC TRANSAMINASE)%3B length 388 aa%3B id%3D38.9%25%3B %3B E()%3D1.7e-47%3B 378 aa overlap%3B query 12-387 aa%3B subject 11-387 aa,fasta%3B RL2092%3B rleguminosarum%3B putative aspartate aminotransferase%3B length 395 aa%3B id%3D40.9%25%3B %3B E()%3D2.1e-49%3B 384 aa overlap%3B query 6-386 aa%3B subject 15-395 aa,fasta%3B Rv3565%3B mtuberculosis%3B POSSIBLE ASPARTATE AMINOTRANSFERASE ASPB (TRANSAMINASE A) (ASPAT) (GLUTAMIC--OXALOACETIC TRANSAMINASE) (GLUTAMIC--ASPARTIC TRANSAMINASE)%3B length 388 aa%3B id%3D38.9%25%3B %3B E()%3D2.2e-47%3B 378 aa overlap%3B query 12-387 aa%3B subject 11-387 aa;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    13 1 gnl_MPB_PFLU_1	ena	exon	5739195	5740367	.	-	.	ID=exon_5192;Name=CAY52319-1;Parent=transcript:CAY52319;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY52319-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    13 2 gnl_MPB_PFLU_1	ena	gene	5739195	5740367	.	-	.	ID=gene:PFLU_5232;biotype=protein_coding;description=putative putative aminotransferase;frame=.;gene_id=PFLU_5232;logic_name=ena;seqid=gnl_MPB_PFLU_1
    13 3 gnl_MPB_PFLU_1	ena	mRNA	5739195	5740367	.	-	.	ID=transcript:CAY52319;Parent=gene:PFLU_5232;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY52319
    13 4 gnl_MPB_PFLU_1	Geneious	biological_region	5739213	5740130	.	-	.	ID=HMMPfam_5481;Name=HMMPfam;domain=HMMPfam:PF00155%3BAminotransferase class I and II%3B2.2e-31%3Bcodon 80-385;frame=.;locus_tag=PFLU_5232;ncbi_feature_key=misc_feature;note=HMMPfam hit to PF00155%2C Aminotransferase class I and II%2C score 2.2e-31;seqid=gnl_MPB_PFLU_1
    13 5 gnl_MPB_PFLU_1	ena_misc_feature	biological_region	5739225	5740274	.	-	.	ID=biological_region_9502;external_name=PFLU_5232%3B AM181176.4:misc_feature:complement(5739365..5740414);frame=.;logic_name=ena_misc_feature;seqid=gnl_MPB_PFLU_1
    13 6 gnl_MPB_PFLU_1	Geneious	regulatory_region	5739462	5739482	.	-	.	regulatory_class=other;locus_tag=PFLU_5232;note=possibly recognised by TAT system for Sec-independent transport;ID=regulatory_region_13


### Save current db to file


```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220203.gff3')
```

    48441it [00:03, 12976.89it/s]


## RS ncRNA


```python
rs_ncrna_updates = find_new_features(rsdb, 'ncRNA', gnldb)
```

    0it [00:00, ?it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	ncRNA	1041057	1041410	.	+	.	Name=rnpB ncRNA;ncRNA_class=RNase_P_RNA;gene=rnpB;locus_tag=PFLU_RS30310;product=RNase P RNA component class A;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00010,COORDINATES: profile:INFERNAL:1.1.1;note=Derived by automated computational analysis using gene prediction method: cmsearch.;db_xref=RFAM:RF00010;NCBI Feature Key=ncRNA. Append to features.
    1it [00:01,  1.18s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	ncRNA	5051694	5051790	.	-	.	Name=ffs ncRNA;ncRNA_class=SRP_RNA;gene=ffs;locus_tag=PFLU_RS31040;product=signal recognition particle sRNA small type;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00169,COORDINATES: profile:INFERNAL:1.1.1;note=Derived by automated computational analysis using gene prediction method: cmsearch.;db_xref=RFAM:RF00169;NCBI Feature Key=ncRNA. Append to features.
    2it [00:02,  1.15s/it]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	ncRNA	6438422	6438600	.	+	.	Name=ssrS ncRNA;ncRNA_class=other;gene=ssrS;locus_tag=PFLU_RS31195;product=6S RNA;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00013,COORDINATES: profile:INFERNAL:1.1.1;note=Derived by automated computational analysis using gene prediction method: cmsearch.;db_xref=RFAM:RF00013;NCBI Feature Key=ncRNA. Append to features.
    3it [00:03,  1.16s/it]



```python
[print(feat) for feat in rs_ncrna_updates];
```

    gnl_MPB_PFLU_1	Geneious	ncRNA	1041057	1041410	.	+	.	Name=rnpB ncRNA;ncRNA_class=RNase_P_RNA;gene=rnpB;locus_tag=PFLU_RS30310;product=RNase P RNA component class A;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00010,COORDINATES: profile:INFERNAL:1.1.1;note=Derived by automated computational analysis using gene prediction method: cmsearch.;NCBI Feature Key=ncRNA;ID=ncRNA_0;Dbxref=RFAM:RF00010
    gnl_MPB_PFLU_1	Geneious	ncRNA	5051694	5051790	.	-	.	Name=ffs ncRNA;ncRNA_class=SRP_RNA;gene=ffs;locus_tag=PFLU_RS31040;product=signal recognition particle sRNA small type;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00169,COORDINATES: profile:INFERNAL:1.1.1;note=Derived by automated computational analysis using gene prediction method: cmsearch.;NCBI Feature Key=ncRNA;ID=ncRNA_1;Dbxref=RFAM:RF00169
    gnl_MPB_PFLU_1	Geneious	ncRNA	6438422	6438600	.	+	.	Name=ssrS ncRNA;ncRNA_class=other;gene=ssrS;locus_tag=PFLU_RS31195;product=6S RNA;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00013,COORDINATES: profile:INFERNAL:1.1.1;note=Derived by automated computational analysis using gene prediction method: cmsearch.;NCBI Feature Key=ncRNA;ID=ncRNA_2;Dbxref=RFAM:RF00013


### Update db


```python
for i,feat in enumerate(rs_ncrna_updates):
    feat.attributes['ID'] = 'ncRNA_{}'.format(i)
    feat.attributes['Dbxref'] = feat.attributes['db_xref']
    del feat.attributes['db_xref']
```


```python
gnldb.update(rs_ncrna_updates)
```




    <gffutils.interface.FeatureDB at 0x7f3201cbcdc0>




```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220204.gff3')
```

    48444it [00:03, 14680.91it/s]



```python
## Feature counts:
gnldb_feature_counts = {}
for ft in gnldb.featuretypes():
    gnldb_feature_counts[ft] = gnldb.count_features_of_type(ft)

gnldb_feature_counts=pandas.DataFrame.from_dict(gnldb_feature_counts, orient='index')

db_feature_counts = pandas.concat([rsdb_feature_counts, ebidb_feature_counts, gbdb_feature_counts, pdcdb_feature_counts, gnldb_feature_counts], axis=1, join='outer')
db_feature_counts.columns = ['RefSeq', 'EBI', 'GenBank', 'Pseudomonas.com', 'MPB']
db_feature_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RefSeq</th>
      <th>EBI</th>
      <th>GenBank</th>
      <th>Pseudomonas.com</th>
      <th>MPB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6069.0</td>
      <td>5921.0</td>
      <td>6044.0</td>
      <td>5921.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6153.0</td>
      <td>5921.0</td>
      <td>6018.0</td>
      <td>6106.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA</th>
      <td>3.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>7.0</td>
      <td>NaN</td>
      <td>14.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
    </tr>
    <tr>
      <th>tmRNA</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>NaN</td>
      <td>11293.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>20629.0</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>NaN</td>
      <td>6098.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6003.0</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>NaN</td>
      <td>5921.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>NaN</td>
      <td>82.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>82.0</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>101.0</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>79.0</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
      <td>15.0</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>misc_feature</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>16929.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>protein_bind</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>3375.0</td>
      <td>NaN</td>
      <td>3389.0</td>
    </tr>
    <tr>
      <th>sig_peptide</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1131.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>pseudogenic_CDS</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>123.0</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>95.0</td>
    </tr>
    <tr>
      <th>regulatory_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>14.0</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>61.0</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
    </tr>
  </tbody>
</table>
</div>



## RS regulatory


```python
rs_regulatory_updates = find_new_features(rsdb, 'regulatory', gnldb)
```

    6it [00:03,  1.35it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	regulatory	6270548	6270654	.	+	.	regulatory_class=riboswitch;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF01057,COORDINATES: profile:INFERNAL:1.1.1;note=S-adenosyl-L-homocysteine riboswitch%3B Derived by automated computational analysis using gene prediction method: cmsearch.;bound_moiety=S-adenosyl-L-homocysteine;db_xref=RFAM:RF01057. Append to features.
    7it [00:04,  1.44it/s]



```python
[print(feat.id, feat) for feat in rs_regulatory_updates];
```

    regulatory_7 gnl_MPB_PFLU_1	Geneious	regulatory	6270548	6270654	.	+	.	regulatory_class=riboswitch;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF01057,COORDINATES: profile:INFERNAL:1.1.1;note=S-adenosyl-L-homocysteine riboswitch%3B Derived by automated computational analysis using gene prediction method: cmsearch.;bound_moiety=S-adenosyl-L-homocysteine;db_xref=RFAM:RF01057



```python
## Look at region in gnl
[print(f.id) for f in gnldb.region(rs_regulatory_updates[0])];
```
-> no features in vicinity

```python
for i, feat in enumerate(rs_regulatory_updates):
    feat.attributes['ID'] = 'regulatory_region_{}'.format(i)
    feat.id = feat.attributes['ID']
    feat.featuretype = 'regulatory_region'
    feat.attributes['Dbxref'] = feat.attributes['db_xref']
    del feat.attributes['db_xref']
    feat.attributes['note'] = ['Copied from RefSeq GCF0009225.2 annotation transferred to new P.flu SBW25 assembly gnl_MPB_PFLU_1']
```


```python
gnldb.update(rs_regulatory_updates)
```


    ---------------------------------------------------------------------------

    IntegrityError                            Traceback (most recent call last)

    /opt/anaconda3/envs/biopy/lib/python3.9/site-packages/gffutils/create.py in _populate_from_lines(self, lines)
        588             try:
    --> 589                 self._insert(f, c)
        590             except sqlite3.IntegrityError:


    /opt/anaconda3/envs/biopy/lib/python3.9/site-packages/gffutils/create.py in _insert(self, feature, cursor)
        529         try:
    --> 530             cursor.execute(constants._INSERT, feature.astuple())
        531         except sqlite3.ProgrammingError:


    IntegrityError: UNIQUE constraint failed: features.id

    
    During handling of the above exception, another exception occurred:


    ValueError                                Traceback (most recent call last)

    /tmp/ipykernel_46539/410203934.py in <module>
    ----> 1 gnldb.update(rs_regulatory_updates)
    

    /opt/anaconda3/envs/biopy/lib/python3.9/site-packages/gffutils/interface.py in update(self, data, make_backup, **kwargs)
        936         if len(peek) == 0: return db  # If the file is empty then do nothing
        937 
    --> 938         db._populate_from_lines(data)
        939         db._update_relations()
        940 


    /opt/anaconda3/envs/biopy/lib/python3.9/site-packages/gffutils/create.py in _populate_from_lines(self, lines)
        589                 self._insert(f, c)
        590             except sqlite3.IntegrityError:
    --> 591                 fixed, final_strategy = self._do_merge(f, self.merge_strategy)
        592                 if final_strategy == 'merge':
        593                     c.execute(


    /opt/anaconda3/envs/biopy/lib/python3.9/site-packages/gffutils/create.py in _do_merge(self, f, merge_strategy, add_duplicate)
        224         """
        225         if merge_strategy == 'error':
    --> 226             raise ValueError("Duplicate ID {0.id}".format(f))
        227 
        228         if merge_strategy == 'warning':


    ValueError: Duplicate ID regulatory_region_0



```python
rs_regulatory_updates[0].attributes['ID'] = ['15']
rs_regulatory_updates[0].id = 15
```


```python
gnldb.update(rs_regulatory_updates)
```




    <gffutils.interface.FeatureDB at 0x7f3201cbcdc0>




```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220204.gff3')
```

    48445it [00:03, 15019.75it/s]


## RS tmRNA


```python
gnldb = gff.create_db('gnl_MPB_PFLU_1-20220204.gff3', 'gnl_MPB_PFLU_1-latest.db')
```


```python
rs_tmrna_updates = find_new_features(rsdb, 'tmRNA', gnldb, logmatches=True)
```

    0it [00:00, ?it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	tmRNA	5798168	5798564	.	+	.	Name=ssrA tmRNA;gene=ssrA;locus_tag=PFLU_RS31105;product=transfer-messenger RNA;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00023,COORDINATES: profile:INFERNAL:1.1.1;note=Derived by automated computational analysis using gene prediction method: cmsearch.;db_xref=RFAM:RF00023;NCBI Feature Key=tmRNA. Append to features.
    1it [00:01,  1.24s/it]



```python
region_matches = gnldb.region(rs_tmrna_updates[0])
```


```python
[print(f.id, f) for f in region_matches];
```

    ReD54 difference gnl_MPB_PFLU_1	Geneious	sequence_difference	5705165	5843931	.	+	.	ID=ReD54 difference;Name=ReD54 difference;external_name=AM181176.4:misc_feature:5705305..5844071;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=sequence_difference;note=ReD54;seqid=gnl_MPB_PFLU_1
    tmRNA gnl_MPB_PFLU_1	Geneious	transcript	5798168	5798561	.	+	.	ID=tmRNA;Name=tmRNA;domain=Rfam:RF00023%3BtmRNA%3BScore%3D135.65%3Bpositions 1 to 366;external_name=Bacterial tmRNA%2C 10Sa RNA or SsrA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Bacterial tmRNA%2C 10Sa RNA or SsrA;seqid=gnl_MPB_PFLU_1
    PFLUGI12 gnl_MPB_PFLU_1	Geneious	biological_region	5798562	5833658	.	+	.	ID=PFLUGI12;Name=PFLUGI12;external_name=PFI-12;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=misc_feature;note=PFLUGI12;seqid=gnl_MPB_PFLU_1


-> This RS feature, a tmRNA is already present in gnl as tmRNA. Will retype it to tmRNA and add name as alternative name.


```python

```


```python
gnl_tmRNA = copy.deepcopy(gnldb['tmRNA'])
```


```python
gnldb.delete('tmRNA')
```




    <gffutils.interface.FeatureDB at 0x7f3201cbcdc0>




```python
gnl_tmRNA.attributes = gff.helpers.merge_attributes(gnl_tmRNA.attributes, rs_tmrna_updates[0].attributes)
```


```python
gnl_tmRNA.attributes
```




    {'ID': ['tmRNA'],
     'Name': ['ssrA tmRNA', 'tmRNA'],
     'domain': ['Rfam:RF00023;tmRNA;Score=135.65;positions 1 to 366'],
     'external_name': ['Bacterial tmRNA, 10Sa RNA or SsrA'],
     'frame': ['.'],
     'logic_name': ['ena_misc_rna'],
     'ncbi_feature_key': ['misc_RNA'],
     'note': ['Bacterial tmRNA, 10Sa RNA or SsrA',
      'Derived by automated computational analysis using gene prediction method: cmsearch.'],
     'seqid': ['gnl_MPB_PFLU_1'],
     'gene': ['ssrA'],
     'locus_tag': ['PFLU_RS31105'],
     'product': ['transfer-messenger RNA'],
     'inference': ['COORDINATES: nucleotide motif:Rfam:14.4:RF00023',
      'COORDINATES: profile:INFERNAL:1.1.1'],
     'db_xref': ['RFAM:RF00023'],
     'NCBI Feature Key': ['tmRNA']}




```python
gnl_tmRNA.featuretype='tmRNA'
gnl_tmRNA.attributes['Alias'] = gnl_tmRNA.attributes['Name'][0]
gnl_tmRNA.attributes['Name'] = gnl_tmRNA.attributes['Name'][1:]
```


```python
gnl_tmRNA.attributes
```




    {'ID': ['tmRNA'],
     'Name': ['tmRNA'],
     'domain': ['Rfam:RF00023;tmRNA;Score=135.65;positions 1 to 366'],
     'external_name': ['Bacterial tmRNA, 10Sa RNA or SsrA'],
     'frame': ['.'],
     'logic_name': ['ena_misc_rna'],
     'ncbi_feature_key': ['misc_RNA'],
     'note': ['Bacterial tmRNA, 10Sa RNA or SsrA',
      'Derived by automated computational analysis using gene prediction method: cmsearch.'],
     'seqid': ['gnl_MPB_PFLU_1'],
     'gene': ['ssrA'],
     'locus_tag': ['PFLU_RS31105'],
     'product': ['transfer-messenger RNA'],
     'inference': ['COORDINATES: nucleotide motif:Rfam:14.4:RF00023',
      'COORDINATES: profile:INFERNAL:1.1.1'],
     'db_xref': ['RFAM:RF00023'],
     'NCBI Feature Key': ['tmRNA'],
     'Alias': 'ssrA tmRNA'}




```python
gnl_tmRNA.attributes['Dbxref'] = gnl_tmRNA.attributes['db_xref']
del gnl_tmRNA.attributes['db_xref']
```

### Delete old feature in gnl


```python
gnldb = gnldb.delete('tmRNA')
```


```python
gnldb = gnldb.update([gnl_tmRNA])
```


```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220204.gff3')
```

    48445it [00:03, 13965.63it/s]


## Pseudomonas.com CDS


```python
pdc_cds_updates = find_new_features(pdcdb, 'CDS', gnldb, 'CDS')
```

    5921it [11:45,  8.39it/s]


-> no new features in pseudomonas.com CDS

## Pseudomonas.com Genes


```python
pdc_gene_updates = find_new_features(pdcdb, 'gene', gnldb, ['gene', 'pseudogene', 'ncRNA_gene'])
```

    523it [00:12, 39.98it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	557910	558015	.	+	.	Name=/locus tag%3D"PFLUs28 gene;gene=/locus_tag%3D"PFLUs28;db_xref=Pseudomonas Genome DB: PGD1459033;NCBI Feature Key=gene. Append to features.
    959it [00:29, 25.74it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1041102	1041394	.	+	.	Name=/locus tag%3D"PFLUs30 gene;gene=/locus_tag%3D"PFLUs30;db_xref=Pseudomonas Genome DB: PGD1459935;NCBI Feature Key=gene. Append to features.
    968it [00:30, 21.44it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1019875	1020016	.	-	.	Name=/locus tag%3D"PFLUs29 gene;gene=/locus_tag%3D"PFLUs29;db_xref=Pseudomonas Genome DB: PGD1459895;NCBI Feature Key=gene. Append to features.
    1211it [00:40, 22.32it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	1299387	1299550	.	+	.	Name=/locus tag%3D"PFLUs31 gene;gene=/locus_tag%3D"PFLUs31;db_xref=Pseudomonas Genome DB: PGD1460379;NCBI Feature Key=gene. Append to features.
    2019it [01:32, 14.01it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2176622	2177269	.	+	.	Name=/locus tag%3D"PFLU2007 gene;gene=/locus_tag%3D"PFLU2007;db_xref=Pseudomonas Genome DB: PGD1462036;NCBI Feature Key=gene. Append to features.
    2655it [02:27, 11.10it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2943310	2943516	.	-	.	Name=/locus tag%3D"PFLUs32 gene;gene=/locus_tag%3D"PFLUs32;db_xref=Pseudomonas Genome DB: PGD1463310;NCBI Feature Key=gene. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	2944392	2944591	.	+	.	Name=/locus tag%3D"PFLUs33 gene;gene=/locus_tag%3D"PFLUs33;db_xref=Pseudomonas Genome DB: PGD1463312;NCBI Feature Key=gene. Append to features.
    3862it [04:36,  8.97it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4296791	4296940	.	+	.	Name=/locus tag%3D"PFLUs34 gene;gene=/locus_tag%3D"PFLUs34;db_xref=Pseudomonas Genome DB: PGD1465721;NCBI Feature Key=gene. Append to features.
    4230it [05:15, 10.29it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4663139	4663334	.	+	.	Name=/locus tag%3D"PFLUs35 gene;gene=/locus_tag%3D"PFLUs35;db_xref=Pseudomonas Genome DB: PGD1466390;NCBI Feature Key=gene. Append to features.
    4474it [05:44,  6.83it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	4958980	4959233	.	-	.	Name=/locus tag%3D"PFLUs36 gene;gene=/locus_tag%3D"PFLUs36;db_xref=Pseudomonas Genome DB: PGD1466917;NCBI Feature Key=gene. Append to features.
    4591it [05:57,  8.94it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5051692	5051791	.	-	.	Name=/locus tag%3D"PFLUs37 gene;gene=/locus_tag%3D"PFLUs37;db_xref=Pseudomonas Genome DB: PGD1467095;NCBI Feature Key=gene. Append to features.
    4721it [06:13,  9.28it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5226129	5226298	.	+	.	Name=/locus tag%3D"PFLUs38 gene;gene=/locus_tag%3D"PFLUs38;db_xref=Pseudomonas Genome DB: PGD1467422;NCBI Feature Key=gene. Append to features.
    5226it [07:16,  8.24it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5735139	5735293	.	+	.	Name=/locus tag%3D"PFLUs39 gene;gene=/locus_tag%3D"PFLUs39;db_xref=Pseudomonas Genome DB: PGD1468372;NCBI Feature Key=gene. Append to features.
    5277it [07:22,  7.73it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	5798168	5798561	.	+	.	Name=/locus tag%3D"PFLUs40 gene;gene=/locus_tag%3D"PFLUs40;db_xref=Pseudomonas Genome DB: PGD1468484;NCBI Feature Key=gene. Append to features.
    5619it [08:08,  7.07it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	6141633	6141750	.	-	.	Name=/locus tag%3D"PFLUs41 gene;gene=/locus_tag%3D"PFLUs41;db_xref=Pseudomonas Genome DB: PGD1469148;NCBI Feature Key=gene. Append to features.
    5868it [08:43,  7.20it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	gene	6438422	6438599	.	+	.	Name=/locus tag%3D"PFLUs42 gene;gene=/locus_tag%3D"PFLUs42;db_xref=Pseudomonas Genome DB: PGD1469692;NCBI Feature Key=gene. Append to features.
    6106it [09:17, 10.96it/s]



```python

```


```python
pdc_gene_updates = copy.deepcopy(backup)
```


```python
match_count = 0
unmatched = []
matched = []
for i,feat in enumerate(pdc_gene_updates):
    match=False
    for j,gnlfeat in enumerate(gnldb.region(feat)):
        print(i, j, feat.id, feat.start, feat.end, feat.strand, gnlfeat.id, gnlfeat.start, gnlfeat.end, gnlfeat.strand)
        if(feat.start == gnlfeat.start and feat.end==gnlfeat.end and feat.strand==gnlfeat.strand):
            logging.info("Identical match: %s matches %s", feat.id, gnlfeat.id)
            match_count += 1
            match=True
            matched.append((feat, gnlfeat))
    if not match:
        unmatched.append(feat)
logging.info("Found %d matches out of %d total.", match_count, i+1)
```

    INFO:root:Identical match: /locus tag="PFLUs28 gene matches THI
    INFO:root:Identical match: /locus tag="PFLUs30 gene matches RNase_P
    INFO:root:Identical match: /locus tag="PFLUs29 gene matches yybP-ykoY
    INFO:root:Identical match: /locus tag="PFLUs31 gene matches PrrB
    INFO:root:Identical match: /locus tag="PFLUs32 gene matches riboswitch
    INFO:root:Identical match: /locus tag="PFLUs33 gene matches riboswitch_1
    INFO:root:Identical match: /locus tag="PFLUs34 gene matches PrrF
    INFO:root:Identical match: /locus tag="PFLUs35 gene matches riboswitch_2
    INFO:root:Identical match: /locus tag="PFLUs36 gene matches riboswitch_3
    INFO:root:Identical match: /locus tag="PFLUs37 gene matches SRP
    INFO:root:Identical match: /locus tag="PFLUs38 gene matches RFN
    INFO:root:Identical match: /locus tag="PFLUs39 gene matches PrrF_1
    INFO:root:Identical match: /locus tag="PFLUs40 gene matches tmRNA
    INFO:root:Identical match: /locus tag="PFLUs41 gene matches RsmY
    INFO:root:Identical match: /locus tag="PFLUs42 gene matches SsrS
    INFO:root:Found 15 matches out of 16 total.


    0 0 /locus tag="PFLUs28 gene 557910 558015 + THI 557910 558015 +
    1 0 /locus tag="PFLUs30 gene 1041102 1041394 + ReD13 difference 1028291 1064675 +
    1 1 /locus tag="PFLUs30 gene 1041102 1041394 + ncRNA_0 1041057 1041410 +
    1 2 /locus tag="PFLUs30 gene 1041102 1041394 + RNase_P 1041102 1041394 +
    2 0 /locus tag="PFLUs29 gene 1019875 1020016 - FLUR10 repeat region_1 1019865 1019887 -
    2 1 /locus tag="PFLUs29 gene 1019875 1020016 - yybP-ykoY 1019875 1020016 -
    3 0 /locus tag="PFLUs31 gene 1299387 1299550 + ReD15 difference 1287864 1319971 +
    3 1 /locus tag="PFLUs31 gene 1299387 1299550 + PrrB 1299387 1299550 +
    4 0 /locus tag="PFLU2007 gene 2176622 2177269 + ReD23 difference 2049099 2200007 +
    4 1 /locus tag="PFLU2007 gene 2176622 2177269 + biological_region_3485 2050710 4749278 +
    4 2 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_39 2176622 2176723 +
    4 3 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogene_5 2176622 2176723 +
    4 4 /locus tag="PFLU2007 gene 2176622 2177269 + region similar to PFLU_1959 repeat region 2176623 2176849 +
    4 5 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_40 2176727 2176789 +
    4 6 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogene_6 2176727 2176789 +
    4 7 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_41 2176793 2176984 +
    4 8 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogene_7 2176793 2177058 +
    4 9 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_42 2176965 2177000 +
    4 10 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_43 2177000 2177041 +
    4 11 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_44 2177041 2177058 +
    4 12 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_45 2177067 2177126 +
    4 13 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogene_8 2177067 2177126 +
    4 14 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogenic_cds_46 2177132 2177269 +
    4 15 /locus tag="PFLU2007 gene 2176622 2177269 + pseudogene_9 2177132 2177269 +
    5 0 /locus tag="PFLUs32 gene 2943310 2943516 - riboswitch 2943310 2943516 -
    6 0 /locus tag="PFLUs33 gene 2944392 2944591 + biological_region_3485 2050710 4749278 +
    6 1 /locus tag="PFLUs33 gene 2944392 2944591 + riboswitch_1 2944392 2944591 +
    7 0 /locus tag="PFLUs34 gene 4296791 4296940 + biological_region_3485 2050710 4749278 +
    7 1 /locus tag="PFLUs34 gene 4296791 4296940 + PrrF 4296791 4296940 +
    8 0 /locus tag="PFLUs35 gene 4663139 4663334 + biological_region_3485 2050710 4749278 +
    8 1 /locus tag="PFLUs35 gene 4663139 4663334 + riboswitch_2 4663139 4663334 +
    9 0 /locus tag="PFLUs36 gene 4958980 4959233 - riboswitch_3 4958980 4959233 -
    10 0 /locus tag="PFLUs37 gene 5051692 5051791 - SRP 5051692 5051791 -
    10 1 /locus tag="PFLUs37 gene 5051692 5051791 - ncRNA_1 5051694 5051790 -
    11 0 /locus tag="PFLUs38 gene 5226129 5226298 + RFN 5226129 5226298 +
    12 0 /locus tag="PFLUs39 gene 5735139 5735293 + ReD54 difference 5705165 5843931 +
    12 1 /locus tag="PFLUs39 gene 5735139 5735293 + PrrF_1 5735139 5735293 +
    13 0 /locus tag="PFLUs40 gene 5798168 5798561 + ReD54 difference 5705165 5843931 +
    13 1 /locus tag="PFLUs40 gene 5798168 5798561 + tmRNA 5798168 5798561 +
    14 0 /locus tag="PFLUs41 gene 6141633 6141750 - RsmY 6141633 6141750 -
    15 0 /locus tag="PFLUs42 gene 6438422 6438599 + SsrS 6438422 6438599 +
    15 1 /locus tag="PFLUs42 gene 6438422 6438599 + ncRNA_2 6438422 6438600 +



```python
unmatched
```




    [<Feature gene (gnl_MPB_PFLU_1:2176622-2177269[+]) at 0x7f31fcb348e0>]




```python
print(unmatched[0])
```

    gnl_MPB_PFLU_1	Geneious	gene	2176622	2177269	.	+	.	Name=/locus tag%3D"PFLU2007 gene;gene=/locus_tag%3D"PFLU2007;db_xref=Pseudomonas Genome DB: PGD1462036;NCBI Feature Key=gene

The only unmatched feature is PFLU_2007. Will add as pseudogene. All others: add dbxref to pdc.

```python
gnldb.count_features_of_type('pseudogene')
```




    101




```python
re.sub?
```


    [0;31mSignature:[0m [0mre[0m[0;34m.[0m[0msub[0m[0;34m([0m[0mpattern[0m[0;34m,[0m [0mrepl[0m[0;34m,[0m [0mstring[0m[0;34m,[0m [0mcount[0m[0;34m=[0m[0;36m0[0m[0;34m,[0m [0mflags[0m[0;34m=[0m[0;36m0[0m[0;34m)[0m[0;34m[0m[0;34m[0m[0m
    [0;31mDocstring:[0m
    Return the string obtained by replacing the leftmost
    non-overlapping occurrences of the pattern in string by the
    replacement repl.  repl can be either a string or a callable;
    if a string, backslash escapes in it are processed.  If it is
    a callable, it's passed the Match object and must return
    a replacement string to be used.
    [0;31mFile:[0m      /opt/anaconda3/envs/biopy/lib/python3.9/re.py
    [0;31mType:[0m      function




```python
feat = unmatched[0]
feat.featuretype = 'pseudogene'
feat.id = 'pseudogene_101'
feat.attributes['old_locus_tag'] = ["PFLU_2007"]
feat.attributes['Dbxref'] = ['PseudoCAP:PGD1462036']
del feat.attributes['Name']
del feat.attributes['gene']
feat.attributes['NCBI Feature Key'] = 'pseudogene'
print( feat.id, feat,  feat.attributes)
```

    pseudogene_101 gnl_MPB_PFLU_1	Geneious	pseudogene	2176622	2177269	.	+	.	db_xref=Pseudomonas Genome DB: PGD1462036;NCBI Feature Key=pseudogene;old_locus_tag=PFLU_2007;Dbxref=PseudoCAP:PGD1462036 db_xref: ['Pseudomonas Genome DB: PGD1462036']
    NCBI Feature Key: ['pseudogene']
    old_locus_tag: ['PFLU_2007']
    Dbxref: ['PseudoCAP:PGD1462036']



```python
del feat.attributes['db_xref']
print(feat.attributes)
```

    NCBI Feature Key: ['pseudogene']
    old_locus_tag: ['PFLU_2007']
    Dbxref: ['PseudoCAP:PGD1462036']



```python
feat.attributes['ID'] = ['pseudogene_101']
print(feat)
```

    gnl_MPB_PFLU_1	Geneious	pseudogene	2176622	2177269	.	+	.	NCBI Feature Key=pseudogene;old_locus_tag=PFLU_2007;Dbxref=PseudoCAP:PGD1462036;ID=pseudogene_101



```python
del feat.attributes['gene']
print(feat.attributes)
```

    db_xref: ['Pseudomonas Genome DB: PGD1462036']
    NCBI Feature Key: ['gene']



```python
to_be_deleted = []
updates = []
for pdcfeat, gnlfeat in matched:
    # print(pdcfeat,'\n', gnlfeat, '\n\n')
    
    # Parse locus tag and pseudocap db id.
    lt = re.sub(r'/locus\stag=\"PFLU(.*)\sgene', r'\1',  pdcfeat.id)
    pseudodb_id = re.sub(r'Pseudomonas\sGenome\sDB:\sPGD([0-9]*)$', r'\1', pdcfeat.attributes['db_xref'][0])
    
    # Add attributions if not present.
    if 'old_locus_tag' not in gnlfeat.attributes.keys(): gnlfeat.attributes['old_locus_tag'] = []
    if 'Dbxref' not in gnlfeat.attributes.keys(): gnlfeat.attributes['Dbxref'] = []

    gnlfeat.attributes['old_locus_tag'].append("PFLU_{}".format(lt))
    gnlfeat.attributes['Dbxref'].append("PseudoCAP:PGD{}".format(pseudodb_id))
    
    updates.append(copy.deepcopy(gnlfeat))
    to_be_deleted.append(gnldb[gnlfeat.id])
#     
    

```


```python
print(updates[0])
```

    gnl_MPB_PFLU_1	Geneious	transcript	557910	558015	.	+	.	ID=THI;Name=THI;domain=Rfam:RF00059%3BTHI%3BScore%3D88.13%3Bpositions 1 to 105;external_name=TPP riboswitch (THI element);frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=TPP riboswitch (THI element);seqid=gnl_MPB_PFLU_1;old_locus_tag=PFLU_s28;Dbxref=PseudoCAP:PGD1459033


### Delete


```python
gnldb = gnldb.delete(to_be_deleted)
```

### Update


```python
gnldb = gnldb.update(updates)
```

### Serialize


```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220204.gff3')
```

    48445it [00:03, 13532.68it/s]


### Update unmatched feature


```python
gnldb = gff.create_db('gnl_MPB_PFLU_1-20220204.gff3', 'gnl_MPB_PFLU_1-latest.db', force=True)
```


```python
gnldb.update([feat])
```




    <gffutils.interface.FeatureDB at 0x7f31fcb26eb0>




```python
gnldb['pseudogene_101']
```




    <Feature pseudogene (gnl_MPB_PFLU_1:2176622-2177269[+]) at 0x7f31fcaac280>




```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220204.gff3')
```

    48446it [00:03, 13658.77it/s]



```python
get_tableview()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RefSeq</th>
      <th>EBI</th>
      <th>GenBank</th>
      <th>Pseudomonas.com</th>
      <th>MPB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6069.0</td>
      <td>5921.0</td>
      <td>6044.0</td>
      <td>5921.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6153.0</td>
      <td>5921.0</td>
      <td>6018.0</td>
      <td>6106.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA</th>
      <td>3.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>7.0</td>
      <td>NaN</td>
      <td>14.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
    </tr>
    <tr>
      <th>tmRNA</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>NaN</td>
      <td>11293.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>20629.0</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>NaN</td>
      <td>6098.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6003.0</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>NaN</td>
      <td>5921.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>NaN</td>
      <td>82.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>82.0</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>102.0</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>79.0</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
      <td>15.0</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>misc_feature</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>16929.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>protein_bind</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>3375.0</td>
      <td>NaN</td>
      <td>3389.0</td>
    </tr>
    <tr>
      <th>sig_peptide</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1131.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>pseudogenic_CDS</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>123.0</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>95.0</td>
    </tr>
    <tr>
      <th>regulatory_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>61.0</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>14.0</td>
    </tr>
  </tbody>
</table>
</div>



## GB misc_RNA


```python
gb_misc_rna_updates = find_new_features(gbdb, 'misc_RNA', gnldb, logmatches=True)
```

    0it [00:00, ?it/s]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	557910	558015	.	+	.	Name=TPP riboswitch (THI element) RNA;note=TPP riboswitch (THI element);NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	557910	558015	.	+	.	Dbxref=PseudoCAP:PGD1459033;ID=THI;Name=THI;domain=Rfam:RF00059%3BTHI%3BScore%3D88.13%3Bpositions 1 to 105;external_name=TPP riboswitch (THI element);frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=TPP riboswitch (THI element);old_locus_tag=PFLU_s28;seqid=gnl_MPB_PFLU_1.
    1it [00:01,  1.21s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	1041102	1041394	.	+	.	Name=Bacterial Ribonuclease P class A RNA;note=Bacterial Ribonuclease P class A;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	1041102	1041394	.	+	.	Dbxref=PseudoCAP:PGD1459935;ID=RNase_P;Name=RNase_P;domain=Rfam:RF00010%3BRNaseP_bact_a%3BScore%3D356.82%3Bposition s 1 to 307;external_name=Bacterial Ribonuclease P class A;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Bacterial Ribonuclease P class A;old_locus_tag=PFLU_s30;seqid=gnl_MPB_PFLU_1.
    2it [00:02,  1.19s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	1019875	1020016	.	-	.	Name=yybP-ykoY element RNA;note=yybP-ykoY element;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	1019875	1020016	.	-	.	Dbxref=PseudoCAP:PGD1459895;ID=yybP-ykoY;Name=yybP-ykoY;domain=Rfam:RF00080%3ByybP-ykoY%3BScore%3D68.65%3Bpositions 1 to124;external_name=yybP-ykoY element;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=yybP-ykoY element;old_locus_tag=PFLU_s29;seqid=gnl_MPB_PFLU_1.
    3it [00:03,  1.17s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	1299387	1299550	.	+	.	Name=PrrB/RsmZ RNA family;note=PrrB/RsmZ RNA family;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	1299387	1299550	.	+	.	Dbxref=PseudoCAP:PGD1460379;ID=PrrB;Name=PrrB;domain=Rfam:RF00166%3BPrrB_RsmZ%3BScore%3D71.47%3Bpositions 1 to163;external_name=PrrB/RsmZ RNA family;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=PrrB/RsmZ RNA family;old_locus_tag=PFLU_s31;seqid=gnl_MPB_PFLU_1.
    4it [00:04,  1.16s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	2943310	2943516	.	-	.	Name=Cobalamin riboswitch RNA;note=Cobalamin riboswitch;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	2943310	2943516	.	-	.	Dbxref=PseudoCAP:PGD1463310;ID=riboswitch;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D120.00%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s32;seqid=gnl_MPB_PFLU_1.
    5it [00:05,  1.16s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	2944392	2944591	.	+	.	Name=Cobalamin riboswitch RNA;note=Cobalamin riboswitch;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	2944392	2944591	.	+	.	Dbxref=PseudoCAP:PGD1463312;ID=riboswitch_1;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D114.43%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s33;seqid=gnl_MPB_PFLU_1.
    6it [00:07,  1.17s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	4296791	4296940	.	+	.	Name=PrrF RNA;note=PrrF RNA;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	4296791	4296940	.	+	.	Dbxref=PseudoCAP:PGD1465721;ID=PrrF;Name=PrrF;domain=Rfam:RF00444%3BPrrF%3BScore%3D93.27%3Bpositions 1 to 148;external_name=PrrF RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=PrrF RNA;old_locus_tag=PFLU_s34;seqid=gnl_MPB_PFLU_1.
    7it [00:08,  1.18s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	4663139	4663334	.	+	.	Name=Cobalamin riboswitch RNA;note=Cobalamin riboswitch;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	4663139	4663334	.	+	.	Dbxref=PseudoCAP:PGD1466390;ID=riboswitch_2;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D120.08%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s35;seqid=gnl_MPB_PFLU_1.
    8it [00:09,  1.18s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	4958980	4959233	.	-	.	Name=Cobalamin riboswitch RNA;note=Cobalamin riboswitch;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	4958980	4959233	.	-	.	Dbxref=PseudoCAP:PGD1466917;ID=riboswitch_3;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D123.92%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s36;seqid=gnl_MPB_PFLU_1.
    9it [00:10,  1.21s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5051692	5051791	.	-	.	Name=RNA;note=Bacterial signal recognition particle RNA;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	5051692	5051791	.	-	.	Dbxref=PseudoCAP:PGD1467095;ID=SRP;Name=SRP;domain=Rfam:RF00169%3BSRP_bact%3BScore%3D88.12%3Bpositions 1 to 100;external_name=Bacterial signal recognition particle RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Bacterial signal recognition particle RNA;old_locus_tag=PFLU_s37;seqid=gnl_MPB_PFLU_1.
    10it [00:11,  1.19s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5226129	5226298	.	+	.	Name=FMN riboswitch (RFN element) RNA;note=FMN riboswitch (RFN element);NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	5226129	5226298	.	+	.	Dbxref=PseudoCAP:PGD1467422;ID=RFN;Name=RFN;domain=Rfam:RF00050%3BRFN%3BScore%3D129.82%3Bpositions 1 to 137;external_name=FMN riboswitch (RFN element);frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=FMN riboswitch (RFN element);old_locus_tag=PFLU_s38;seqid=gnl_MPB_PFLU_1.
    11it [00:12,  1.17s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5735139	5735293	.	+	.	Name=PrrF RNA;note=PrrF RNA;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	5735139	5735293	.	+	.	Dbxref=PseudoCAP:PGD1468372;ID=PrrF_1;Name=PrrF;domain=Rfam:RF00444%3BPrrF%3BScore%3D89.56%3Bpositions 1 to 148;external_name=PrrF RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=PrrF RNA;old_locus_tag=PFLU_s39;seqid=gnl_MPB_PFLU_1.
    12it [00:14,  1.17s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5798168	5798561	.	+	.	Name=Bacterial tmRNA%2C 10Sa RNA or SsrA;note=Bacterial tmRNA%2C 10Sa RNA or SsrA;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	tmRNA	5798168	5798561	.	+	.	Alias=ssrA tmRNA;Dbxref=RFAM:RF00023,PseudoCAP:PGD1468484;ID=tmRNA;NCBI Feature Key=tmRNA;Name=tmRNA;domain=Rfam:RF00023%3BtmRNA%3BScore%3D135.65%3Bpositions 1 to 366;external_name=Bacterial tmRNA%2C 10Sa RNA or SsrA;frame=.;gene=ssrA;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00023,COORDINATES: profile:INFERNAL:1.1.1;locus_tag=PFLU_RS31105;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Bacterial tmRNA%2C 10Sa RNA or SsrA,Derived by automated computational analysis using gene prediction method: cmsearch.;old_locus_tag=PFLU_s40;product=transfer-messenger RNA;seqid=gnl_MPB_PFLU_1.
    13it [00:15,  1.16s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	6141633	6141750	.	-	.	Name=RsmY RNA family;note=RsmY RNA family;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	6141633	6141750	.	-	.	Dbxref=PseudoCAP:PGD1469148;ID=RsmY;Name=RsmY;domain=Rfam:RF00195%3BRsmY%3BScore%3D53.62%3Bpositions 1 to 121;external_name=RsmY RNA family;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=RsmY RNA family;old_locus_tag=PFLU_s41;seqid=gnl_MPB_PFLU_1.
    14it [00:16,  1.16s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	6438422	6438599	.	+	.	Name=6S / SsrS RNA;note=6S / SsrS RNA;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	6438422	6438599	.	+	.	Dbxref=PseudoCAP:PGD1469692;ID=SsrS;Name=SsrS;domain=Rfam:RF00013%3B6S%3BScore%3D58.21%3Bpositions 1 to 184;external_name=6S / SsrS RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=6S / SsrS RNA;old_locus_tag=PFLU_s42;seqid=gnl_MPB_PFLU_1.
    15it [00:17,  1.17s/it]



```python
gb_misc_rna_updates
```




    []



## Pseudomonas.com misc_RNA


```python
pdc_misc_rna_updates = find_new_features(pdcdb, 'misc_RNA', gnldb, logmatches=True)
```

    0it [00:00, ?it/s]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	557910	558015	.	+	.	Name=RNA;locus_tag=PFLUs28;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	557910	558015	.	+	.	Dbxref=PseudoCAP:PGD1459033;ID=THI;Name=THI;domain=Rfam:RF00059%3BTHI%3BScore%3D88.13%3Bpositions 1 to 105;external_name=TPP riboswitch (THI element);frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=TPP riboswitch (THI element);old_locus_tag=PFLU_s28;seqid=gnl_MPB_PFLU_1.
    1it [00:01,  1.20s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	1041102	1041394	.	+	.	Name=RNA;locus_tag=PFLUs30;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	1041102	1041394	.	+	.	Dbxref=PseudoCAP:PGD1459935;ID=RNase_P;Name=RNase_P;domain=Rfam:RF00010%3BRNaseP_bact_a%3BScore%3D356.82%3Bposition s 1 to 307;external_name=Bacterial Ribonuclease P class A;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Bacterial Ribonuclease P class A;old_locus_tag=PFLU_s30;seqid=gnl_MPB_PFLU_1.
    2it [00:02,  1.18s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	1019875	1020016	.	-	.	Name=RNA;locus_tag=PFLUs29;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	1019875	1020016	.	-	.	Dbxref=PseudoCAP:PGD1459895;ID=yybP-ykoY;Name=yybP-ykoY;domain=Rfam:RF00080%3ByybP-ykoY%3BScore%3D68.65%3Bpositions 1 to124;external_name=yybP-ykoY element;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=yybP-ykoY element;old_locus_tag=PFLU_s29;seqid=gnl_MPB_PFLU_1.
    3it [00:03,  1.17s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	1299387	1299550	.	+	.	Name=RNA;locus_tag=PFLUs31;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	1299387	1299550	.	+	.	Dbxref=PseudoCAP:PGD1460379;ID=PrrB;Name=PrrB;domain=Rfam:RF00166%3BPrrB_RsmZ%3BScore%3D71.47%3Bpositions 1 to163;external_name=PrrB/RsmZ RNA family;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=PrrB/RsmZ RNA family;old_locus_tag=PFLU_s31;seqid=gnl_MPB_PFLU_1.
    4it [00:04,  1.15s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	2943310	2943516	.	-	.	Name=RNA;locus_tag=PFLUs32;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	2943310	2943516	.	-	.	Dbxref=PseudoCAP:PGD1463310;ID=riboswitch;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D120.00%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s32;seqid=gnl_MPB_PFLU_1.
    5it [00:05,  1.14s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	2944392	2944591	.	+	.	Name=RNA;locus_tag=PFLUs33;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	2944392	2944591	.	+	.	Dbxref=PseudoCAP:PGD1463312;ID=riboswitch_1;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D114.43%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s33;seqid=gnl_MPB_PFLU_1.
    6it [00:06,  1.14s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	4296791	4296940	.	+	.	Name=RNA;locus_tag=PFLUs34;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	4296791	4296940	.	+	.	Dbxref=PseudoCAP:PGD1465721;ID=PrrF;Name=PrrF;domain=Rfam:RF00444%3BPrrF%3BScore%3D93.27%3Bpositions 1 to 148;external_name=PrrF RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=PrrF RNA;old_locus_tag=PFLU_s34;seqid=gnl_MPB_PFLU_1.
    7it [00:08,  1.14s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	4663139	4663334	.	+	.	Name=RNA;locus_tag=PFLUs35;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	4663139	4663334	.	+	.	Dbxref=PseudoCAP:PGD1466390;ID=riboswitch_2;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D120.08%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s35;seqid=gnl_MPB_PFLU_1.
    8it [00:09,  1.14s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	4958980	4959233	.	-	.	Name=RNA;locus_tag=PFLUs36;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	4958980	4959233	.	-	.	Dbxref=PseudoCAP:PGD1466917;ID=riboswitch_3;Name=riboswitch;domain=Rfam:RF00174%3BCobalamin%3BScore%3D123.92%3Bpositions 1 to 190;external_name=Cobalamin riboswitch;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Cobalamin riboswitch;old_locus_tag=PFLU_s36;seqid=gnl_MPB_PFLU_1.
    9it [00:10,  1.15s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5051692	5051791	.	-	.	Name=RNA;locus_tag=PFLUs37;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	5051692	5051791	.	-	.	Dbxref=PseudoCAP:PGD1467095;ID=SRP;Name=SRP;domain=Rfam:RF00169%3BSRP_bact%3BScore%3D88.12%3Bpositions 1 to 100;external_name=Bacterial signal recognition particle RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Bacterial signal recognition particle RNA;old_locus_tag=PFLU_s37;seqid=gnl_MPB_PFLU_1.
    10it [00:11,  1.14s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5226129	5226298	.	+	.	Name=RNA;locus_tag=PFLUs38;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	5226129	5226298	.	+	.	Dbxref=PseudoCAP:PGD1467422;ID=RFN;Name=RFN;domain=Rfam:RF00050%3BRFN%3BScore%3D129.82%3Bpositions 1 to 137;external_name=FMN riboswitch (RFN element);frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=FMN riboswitch (RFN element);old_locus_tag=PFLU_s38;seqid=gnl_MPB_PFLU_1.
    11it [00:12,  1.13s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5735139	5735293	.	+	.	Name=RNA;locus_tag=PFLUs39;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	5735139	5735293	.	+	.	Dbxref=PseudoCAP:PGD1468372;ID=PrrF_1;Name=PrrF;domain=Rfam:RF00444%3BPrrF%3BScore%3D89.56%3Bpositions 1 to 148;external_name=PrrF RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=PrrF RNA;old_locus_tag=PFLU_s39;seqid=gnl_MPB_PFLU_1.
    12it [00:13,  1.13s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	5798168	5798561	.	+	.	Name=RNA;locus_tag=PFLUs40;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	tmRNA	5798168	5798561	.	+	.	Alias=ssrA tmRNA;Dbxref=RFAM:RF00023,PseudoCAP:PGD1468484;ID=tmRNA;NCBI Feature Key=tmRNA;Name=tmRNA;domain=Rfam:RF00023%3BtmRNA%3BScore%3D135.65%3Bpositions 1 to 366;external_name=Bacterial tmRNA%2C 10Sa RNA or SsrA;frame=.;gene=ssrA;inference=COORDINATES: nucleotide motif:Rfam:14.4:RF00023,COORDINATES: profile:INFERNAL:1.1.1;locus_tag=PFLU_RS31105;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=Bacterial tmRNA%2C 10Sa RNA or SsrA,Derived by automated computational analysis using gene prediction method: cmsearch.;old_locus_tag=PFLU_s40;product=transfer-messenger RNA;seqid=gnl_MPB_PFLU_1.
    13it [00:14,  1.12s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	6141633	6141750	.	-	.	Name=RNA;locus_tag=PFLUs41;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	6141633	6141750	.	-	.	Dbxref=PseudoCAP:PGD1469148;ID=RsmY;Name=RsmY;domain=Rfam:RF00195%3BRsmY%3BScore%3D53.62%3Bpositions 1 to 121;external_name=RsmY RNA family;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=RsmY RNA family;old_locus_tag=PFLU_s41;seqid=gnl_MPB_PFLU_1.
    14it [00:15,  1.13s/it]INFO:root:Reference DB feature gnl_MPB_PFLU_1	Geneious	misc_RNA	6438422	6438599	.	+	.	Name=RNA;locus_tag=PFLUs42;product;NCBI Feature Key=misc_RNA matches Target DB feature gnl_MPB_PFLU_1	Geneious	transcript	6438422	6438599	.	+	.	Dbxref=PseudoCAP:PGD1469692;ID=SsrS;Name=SsrS;domain=Rfam:RF00013%3B6S%3BScore%3D58.21%3Bpositions 1 to 184;external_name=6S / SsrS RNA;frame=.;logic_name=ena_misc_rna;ncbi_feature_key=misc_RNA;note=6S / SsrS RNA;old_locus_tag=PFLU_s42;seqid=gnl_MPB_PFLU_1.
    15it [00:17,  1.14s/it]



```python
pdc_misc_rna_updates
```




    []



$\Rightarrow$  All misc_RNA are already in the gnl annotation

## GB sig_peptide


```python
gb_sig_peptide_updates = find_new_features(gbdb, 'sig_peptide', gnldb)
```

    1131it [10:23,  1.81it/s]



```python
len(gb_sig_peptide_updates)
```




    0




```python
print(next(gbdb.features_of_type('sig_peptide')))
```

    gnl_MPB_PFLU_1	Geneious	sig_peptide	7284	7397	.	+	.	Name=signal peptide;locus_tag=PFLU_0006;note=Signal peptide predicted for PFLU0006 by SignalP 2.0 HMM (Signal peptide probability 0.924) with cleavage site probability 0.396 between residues 38 and 39;NCBI Feature Key=sig_peptide



```python
for feat in gnldb.region(seqid='gnl_MPB_PFLU_1', start=7284, end=7397): print(feat)
```

    gnl_MPB_PFLU_1	Geneious	sequence_difference	1	16700	.	+	.	ID=ReD1 difference;Name=ReD1 difference;external_name=AM181176.4:misc_feature:1..16700;frame=.;logic_name=ena_misc_feature;ncbi_feature_key=sequence_difference;note=ReD1;seqid=gnl_MPB_PFLU_1
    gnl_MPB_PFLU_1	ena	chromosome	1	6722400	.	.	.	ID=chromosome:gnl_MPB_PFLU_1;Name=chromosome:gnl_MPB_PFLU_1;frame=.;seqid=gnl_MPB_PFLU_1
    gnl_MPB_PFLU_1	Geneious	biological_region	7284	7397	.	+	.	ID=misc_2;Name=misc;algorithm=SignalP 2.0 HMM;cleavage=0.396;coord=38%2C39;frame=.;locus_tag=PFLU_0006;ncbi_feature_key=misc_feature;note=Signal peptide predicted for PFLU_0006 by SignalP 2.0 HMM (Signal peptide probability 0.924) with cleavage site probability 0.396 between residues 38 and 39;seqid=gnl_MPB_PFLU_1;signal=0.924;type=signalp
    gnl_MPB_PFLU_1	ena	CDS	7284	8240	.	+	0	Dbxref=EMBL:AM181176,InterPro:IPR006860,KEGG:pfs:PFLU_0006,PRIDE:C3KE36,Pfam:PF04773,STRING:216595.PFLU_0006,eggNOG:COG4254,Pubmed:19432983;ID=CDS:CAY46291;Parent=transcript:CAY46291;codon_start=1;confidence_level=3;description=Putative exported protein;features=Domain (1);frame=0;ft_domain=DOMAIN 91..185%3B  /note%3D"FecR"%3B  /evidence%3D"ECO:0000259|Pfam:PF04773";gene=PFLU_0006;inference=Predicted;locus_tag=PFLU_0006;ncbi_feature_key=CDS;product=putative exported protein;protein_id=CAY46291;seqid=gnl_MPB_PFLU_1;similarity=fasta%3B with%3DUniProt:Q7WRD4 (EMBL:BX640437)%3B Bordetella bronchiseptica (Alcaligenes bronchisepticus).%3B Putative peptidoglycan binding protein.%3B length%3D368%3B id 31.783%25%3B ungapped id 33.469%25%3B E()%3D2e-13%3B 258 aa overlap%3B query 54-305%3B subject 88-338,fasta%3B with%3DUniProt:Q9HWZ2 (EMBL:AE004820)%3B Pseudomonas aeruginosa.%3B Hypothetical protein.%3B length%3D333%3B id 56.627%25%3B ungapped id 59.873%25%3B E()%3D1.9e-35%3B 332 aa overlap%3B query 5-318%3B subject 2-333;uniprot_annotation_score=1 out of 5;uniprot_review_status=unreviewed
    gnl_MPB_PFLU_1	ena	exon	7284	8240	.	+	.	ID=exon_6;Name=CAY46291-1;Parent=transcript:CAY46291;constitutive=1;ensembl_end_phase=0;ensembl_phase=0;exon_id=CAY46291-1;frame=.;rank=1;seqid=gnl_MPB_PFLU_1
    gnl_MPB_PFLU_1	ena	gene	7284	8240	.	+	.	ID=gene:PFLU_0006;biotype=protein_coding;description=putative exported protein;frame=.;gene_id=PFLU_0006;logic_name=ena;seqid=gnl_MPB_PFLU_1
    gnl_MPB_PFLU_1	ena	mRNA	7284	8240	.	+	.	ID=transcript:CAY46291;Parent=gene:PFLU_0006;biotype=protein_coding;frame=.;seqid=gnl_MPB_PFLU_1;transcript_id=CAY46291


$\Rightarrow$ all `sig_peptide`s are already contained. Action to take: retype as `sig_peptide`.


```python
updates = []
to_be_deleted = []
for i,sig_peptide in enumerate(gbdb.features_of_type('sig_peptide')):
    region = gnldb.region(sig_peptide, completely_within=True)
    for candidate in region:
        if candidate.start==sig_peptide.start and candidate.end==sig_peptide.end and candidate.strand==sig_peptide.strand:
            update = copy.deepcopy(candidate)
            update.featuretype = 'sig_peptide'
            update.id = 'sig_peptide_{}'.format(i)
            update.attributes['ID'] = update.id
        
            updates.append(update)    
            to_be_deleted.append(candidate)
```


```python
len(updates)
```




    1131




```python
gnldb = gnldb.delete(to_be_deleted)
```


```python
gnldb = gnldb.update(updates)
```


```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220204.gff3')
```

    48446it [00:03, 12609.38it/s]



```python
get_tableview()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RefSeq</th>
      <th>EBI</th>
      <th>GenBank</th>
      <th>Pseudomonas.com</th>
      <th>MPB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6069.0</td>
      <td>5921.0</td>
      <td>6044.0</td>
      <td>5921.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6153.0</td>
      <td>5921.0</td>
      <td>6018.0</td>
      <td>6106.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA</th>
      <td>3.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>7.0</td>
      <td>NaN</td>
      <td>14.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
    </tr>
    <tr>
      <th>tmRNA</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>NaN</td>
      <td>11293.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>19498.0</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>NaN</td>
      <td>6098.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6003.0</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>NaN</td>
      <td>5921.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>NaN</td>
      <td>82.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>82.0</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>102.0</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>79.0</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
      <td>15.0</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>misc_feature</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>16929.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>protein_bind</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>3375.0</td>
      <td>NaN</td>
      <td>3389.0</td>
    </tr>
    <tr>
      <th>sig_peptide</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1131.0</td>
      <td>NaN</td>
      <td>1131.0</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>pseudogenic_CDS</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>123.0</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>95.0</td>
    </tr>
    <tr>
      <th>regulatory_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>61.0</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>14.0</td>
    </tr>
  </tbody>
</table>
</div>



## GB repeat_region


```python
gb_repeat_updates = find_new_features(gbdb, 'repeat_region', gnldb)
```

    3375it [33:26,  1.68it/s]



```python
len(gb_repeat_updates)
```




    0



Are they all repeat regions?


```python
not_repeat_regions = []
for gbfeat in tqdm(gbdb.features_of_type('repeat_region')):
    region = gnldb.region(gbfeat, completely_within=True)
    
    for gnlfeat in region:
        if any([gbfeat.start != gnlfeat.start, gbfeat.end != gnlfeat.end, gbfeat.strand != gnlfeat.strand]):
            continue
        if gnlfeat.featuretype != 'repeat_region':
            logging.info("Type mismatch: %s-%s vs. %s-%s", gbfeat.featuretype, gbfeat.id, gnlfeat.featuretype, gnlfeat.id)
            not_repeat_regions.append(gnlfeat)
```

    3375it [00:01, 2056.01it/s]



```python
not_repeat_regions
```




    []



$\Rightarrow$ all `repeat_region`s in GB are also `repeat_region` in GNL.

## GB misc_features


```python
gb_misc_updates = find_new_features(gbdb, 'misc_feature', gnldb)
```

    639it [00:14, 23.56it/s] INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	243388	243753	.	+	.	Name=misc;locus_tag=PFLU_0220;inference=protein motif:PFAM:PF00239;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318324.53. Append to features.
    INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	243755	243784	.	+	.	Name=misc;locus_tag=PFLU_0220;inference=protein motif:PFAM:PF00239;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318324.53. Append to features.
    660it [00:17, 14.92it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	243788	243811	.	+	.	Name=misc;locus_tag=PFLU_0220;inference=protein motif:PFAM:PF02796;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318325.55. Append to features.
    663it [00:18,  5.91it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	243811	243924	.	+	.	Name=misc;locus_tag=PFLU_0220;inference=protein motif:PFAM:PF02796;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318325.55. Append to features.
    5391it [16:29,  2.75it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	2133763	2133813	.	+	.	Name=misc;locus_tag=PFLU_1959;inference=protein motif:PFAM:PF01609;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318636.340. Append to features.
    5401it [16:33,  2.78it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	2130586	2130882	.	+	.	Name=misc;locus_tag=PFLU_1956;inference=protein motif:PFAM:PF00106;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318636.342. Append to features.
    5402it [16:34,  1.67it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	2130891	2131052	.	+	.	Name=misc;locus_tag=PFLU_1956;inference=protein motif:PFAM:PF00106;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318636.342. Append to features.
    6921it [27:09,  2.13it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	2716967	2717050	.	+	.	Name=misc;locus_tag=PFLU_2503A;inference=protein motif:PFAM:PF01614;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318741.446. Append to features.
    6922it [27:10,  1.47it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	2717127	2717336	.	+	.	Name=misc;locus_tag=PFLU_2503A;inference=protein motif:PFAM:PF01614;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318741.446. Append to features.
    13202it [1:38:58,  1.12it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	5254009	5254164	.	+	.	Name=misc;locus_tag=PFLU_4770;inference=protein motif:PFAM:PF02498;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319134.836. Append to features.
    13203it [1:38:59,  1.02it/s]INFO:root:No match found for gnl_MPB_PFLU_1	Geneious	misc_feature	5254164	5254265	.	+	.	Name=misc;locus_tag=PFLU_4770;inference=protein motif:PFAM:PF02498;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452319134.836. Append to features.
    16929it [2:42:41,  1.73it/s]



```python
len(gb_misc_updates)
```




    11




```python
print(gb_misc_updates[0])
```

    gnl_MPB_PFLU_1	Geneious	misc_feature	243388	243753	.	+	.	Name=misc;locus_tag=PFLU_0220;inference=protein motif:PFAM:PF00239;pseudo;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=H3vLTfdV07uecYH3tU9j82ABBTs.1642452318324.53



```python
number_of_biological_regions = gnldb.count_features_of_type('biological_region')
```




    19498




```python
for i,feat in enumerate(gb_misc_updates):
    feat.id = 'biological_region_{}'.format(number_of_biological_regions+i)
    feat.attributes['ID'] = feat.id
    feat.featuretype = 'biological_region'
    
    feat.attributes['old_locus_tag'] = feat.attributes['locus_tag']
    del feat.attributes['locus_tag']
    del feat.attributes['pseudo']
    
```


```python
[print(feat) for feat in gb_misc_updates];
```

    gnl_MPB_PFLU_1	Geneious	biological_region	243388	243753	.	+	.	Name=misc;inference=protein motif:PFAM:PF00239;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19498;old_locus_tag=PFLU_0220
    gnl_MPB_PFLU_1	Geneious	biological_region	243755	243784	.	+	.	Name=misc;inference=protein motif:PFAM:PF00239;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19499;old_locus_tag=PFLU_0220
    gnl_MPB_PFLU_1	Geneious	biological_region	243788	243811	.	+	.	Name=misc;inference=protein motif:PFAM:PF02796;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19500;old_locus_tag=PFLU_0220
    gnl_MPB_PFLU_1	Geneious	biological_region	243811	243924	.	+	.	Name=misc;inference=protein motif:PFAM:PF02796;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19501;old_locus_tag=PFLU_0220
    gnl_MPB_PFLU_1	Geneious	biological_region	2133763	2133813	.	+	.	Name=misc;inference=protein motif:PFAM:PF01609;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19502;old_locus_tag=PFLU_1959
    gnl_MPB_PFLU_1	Geneious	biological_region	2130586	2130882	.	+	.	Name=misc;inference=protein motif:PFAM:PF00106;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19503;old_locus_tag=PFLU_1956
    gnl_MPB_PFLU_1	Geneious	biological_region	2130891	2131052	.	+	.	Name=misc;inference=protein motif:PFAM:PF00106;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19504;old_locus_tag=PFLU_1956
    gnl_MPB_PFLU_1	Geneious	biological_region	2716967	2717050	.	+	.	Name=misc;inference=protein motif:PFAM:PF01614;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19505;old_locus_tag=PFLU_2503A
    gnl_MPB_PFLU_1	Geneious	biological_region	2717127	2717336	.	+	.	Name=misc;inference=protein motif:PFAM:PF01614;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19506;old_locus_tag=PFLU_2503A
    gnl_MPB_PFLU_1	Geneious	biological_region	5254009	5254164	.	+	.	Name=misc;inference=protein motif:PFAM:PF02498;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19507;old_locus_tag=PFLU_4770
    gnl_MPB_PFLU_1	Geneious	biological_region	5254164	5254265	.	+	.	Name=misc;inference=protein motif:PFAM:PF02498;NCBI Feature Key=misc_feature;NCBI Join Type=join;ID=biological_region_19508;old_locus_tag=PFLU_4770



```python
gnldb = gnldb.update(gb_misc_updates)
```


```python
serialize(gnldb, 'gnl_MPB_PFLU_1-20220204.gff3')
```

    48457it [00:04, 10650.02it/s]



```python
get_tableview()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RefSeq</th>
      <th>EBI</th>
      <th>GenBank</th>
      <th>Pseudomonas.com</th>
      <th>MPB</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CDS</th>
      <td>6069.0</td>
      <td>5921.0</td>
      <td>6044.0</td>
      <td>5921.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>gene</th>
      <td>6153.0</td>
      <td>5921.0</td>
      <td>6018.0</td>
      <td>6106.0</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA</th>
      <td>3.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>rRNA</th>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
      <td>16.0</td>
    </tr>
    <tr>
      <th>regulatory</th>
      <td>7.0</td>
      <td>NaN</td>
      <td>14.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>source</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>tRNA</th>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
      <td>66.0</td>
    </tr>
    <tr>
      <th>tmRNA</th>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>biological_region</th>
      <td>NaN</td>
      <td>11293.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>19509.0</td>
    </tr>
    <tr>
      <th>exon</th>
      <td>NaN</td>
      <td>6098.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6003.0</td>
    </tr>
    <tr>
      <th>mRNA</th>
      <td>NaN</td>
      <td>5921.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5921.0</td>
    </tr>
    <tr>
      <th>ncRNA_gene</th>
      <td>NaN</td>
      <td>82.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>82.0</td>
    </tr>
    <tr>
      <th>pseudogene</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>102.0</td>
    </tr>
    <tr>
      <th>pseudogenic_transcript</th>
      <td>NaN</td>
      <td>79.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>79.0</td>
    </tr>
    <tr>
      <th>misc_RNA</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
      <td>15.0</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>misc_feature</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>16929.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>protein_bind</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>repeat_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>3375.0</td>
      <td>NaN</td>
      <td>3389.0</td>
    </tr>
    <tr>
      <th>sig_peptide</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1131.0</td>
      <td>NaN</td>
      <td>1131.0</td>
    </tr>
    <tr>
      <th>stem_loop</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>chromosome</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>protein_binding_site</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>pseudogenic_CDS</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>123.0</td>
    </tr>
    <tr>
      <th>pseudogenic_exon</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>95.0</td>
    </tr>
    <tr>
      <th>regulatory_region</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>15.0</td>
    </tr>
    <tr>
      <th>sequence_difference</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>61.0</td>
    </tr>
    <tr>
      <th>transcript</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>14.0</td>
    </tr>
  </tbody>
</table>
</div>



## RS CDS
There are hundreds of CDSs which have a different start and/or end position than their counterparts in GB, PDC, or GNL. I'll skip them for now


```python
## CDS
gnl_update = SeqRecord(seq=sbw25.seq, name="gnl_MPB_PFLU_1", id="gnl_MPB_PFLU_1")
```


```python
# For each CDS in RS, check if there is a CDS at the same coordinates in GNL. If yes, go on, if not, add it to the update list.
for i, rscds in enumerate(rsdb.features_of_type(featuretype)):
    
    
    # if i > 100 break
    new_feat = True
    
    start = rscds.start
    end = rscds.end
    strand = rscds.strand
        
    for j, gnlcds in enumerate(gnldb.features_of_type(featuretype)):
        if (gnlcds.start == start and gnlcds.end == end and gnlcds.strand == strand):
            new_feat = False
            break
        else:
            continue
            
    if new_feat:
        gnl_update.features.append(gffbio.to_seqfeature(rscds))
```

    6069it [12:15,  8.25it/s]



```python
len(gnl_update.features)
```




    1116




```python
gnl_update.features
```




    [SeqFeature(FeatureLocation(ExactPosition(47154), ExactPosition(49254), strand=1), type='CDS', id='prlC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(53081), ExactPosition(54380), strand=1), type='CDS', id='cytochrome c CDS'),
     SeqFeature(FeatureLocation(ExactPosition(7316), ExactPosition(8240), strand=1), type='CDS', id='FecR domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(55747), ExactPosition(56479), strand=1), type='CDS', id='carbonic anhydrase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(49599), ExactPosition(50256), strand=-1), type='CDS', id='tyrosine-protein phosphatase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(62116), ExactPosition(62668), strand=1), type='CDS', id='cytochrome c oxidase assembly protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(32472), ExactPosition(32802), strand=1), type='CDS', id='DOPA 4,5-dioxygenase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(78164), ExactPosition(80942), strand=1), type='CDS', id='polA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(75194), ExactPosition(75689), strand=-1), type='CDS', id='transcriptional repressor CDS'),
     SeqFeature(FeatureLocation(ExactPosition(165565), ExactPosition(173761), strand=1), type='CDS', id='hemagglutinin repeat-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(192885), ExactPosition(195042), strand=1), type='CDS', id='hypothetical protein CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(184267), ExactPosition(185890), strand=1), type='CDS', id='VWA domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(189148), ExactPosition(190405), strand=1), type='CDS', id='SIR2 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(135383), ExactPosition(136409), strand=1), type='CDS', id='ABC transporter substrate-binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(178067), ExactPosition(178856), strand=1), type='CDS', id='lepB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(154539), ExactPosition(155172), strand=1), type='CDS', id='hypothetical protein CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(162951), ExactPosition(163434), strand=1), type='CDS', id='pgaD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(177587), ExactPosition(178028), strand=1), type='CDS', id='SMI1/KNR4 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(149440), ExactPosition(149851), strand=1), type='CDS', id='MarR family transcriptional regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(200895), ExactPosition(201213), strand=-1), type='CDS', id='AlpA family phage regulatory protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(200593), ExactPosition(200899), strand=-1), type='CDS', id='hypothetical protein CDS_19'),
     SeqFeature(FeatureLocation(ExactPosition(187433), ExactPosition(187655), strand=-1), type='CDS', id='hypothetical protein CDS_21'),
     SeqFeature(FeatureLocation(ExactPosition(173747), ExactPosition(173966), strand=1), type='CDS', id='hypothetical protein CDS_22'),
     SeqFeature(FeatureLocation(ExactPosition(192236), ExactPosition(192446), strand=1), type='CDS', id='KTSC domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(195411), ExactPosition(195621), strand=-1), type='CDS', id='hypothetical protein CDS_23'),
     SeqFeature(FeatureLocation(ExactPosition(210475), ExactPosition(212386), strand=1), type='CDS', id='EAL domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(207934), ExactPosition(208936), strand=-1), type='CDS', id='sensor domain-containing diguanylate cyclase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(266118), ExactPosition(266883), strand=-1), type='CDS', id='hypothetical protein CDS_24'),
     SeqFeature(FeatureLocation(ExactPosition(237735), ExactPosition(238458), strand=1), type='CDS', id='MotA/TolQ/ExbB proton channel family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(244536), ExactPosition(245237), strand=1), type='CDS', id='AAA family ATPase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(243384), ExactPosition(243951), strand=1), type='CDS', id='recombinase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(244139), ExactPosition(244551), strand=1), type='CDS', id='IS481 family transposase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(219256), ExactPosition(219622), strand=1), type='CDS', id='response regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(242102), ExactPosition(242381), strand=-1), type='CDS', id='transposase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(242635), ExactPosition(242890), strand=-1), type='CDS', id='type II toxin-antitoxin system RelE/ParE family toxin CDS'),
     SeqFeature(FeatureLocation(ExactPosition(243966), ExactPosition(244170), strand=1), type='CDS', id='TetR family transcriptional regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(272231), ExactPosition(274193), strand=1), type='CDS', id='betT CDS'),
     SeqFeature(FeatureLocation(ExactPosition(274398), ExactPosition(275727), strand=1), type='CDS', id='OprD family outer membrane porin CDS'),
     SeqFeature(FeatureLocation(ExactPosition(270908), ExactPosition(271835), strand=1), type='CDS', id='serine acetyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(288719), ExactPosition(289136), strand=-1), type='CDS', id='ATP-dependent zinc protease CDS'),
     SeqFeature(FeatureLocation(ExactPosition(325173), ExactPosition(325566), strand=1), type='CDS', id='SDR family oxidoreductase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(294470), ExactPosition(294827), strand=-1), type='CDS', id='DUF1493 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(320942), ExactPosition(321158), strand=1), type='CDS', id='hypothetical protein CDS_26'),
     SeqFeature(FeatureLocation(ExactPosition(364512), ExactPosition(366099), strand=-1), type='CDS', id='serine protease CDS'),
     SeqFeature(FeatureLocation(ExactPosition(391014), ExactPosition(392367), strand=1), type='CDS', id='DUF3999 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(343054), ExactPosition(344242), strand=-1), type='CDS', id='methyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(341877), ExactPosition(342813), strand=1), type='CDS', id='yhjQ CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(396089), ExactPosition(396869), strand=1), type='CDS', id='hutC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(371474), ExactPosition(371888), strand=1), type='CDS', id='rhodanese-like domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(414404), ExactPosition(416977), strand=1), type='CDS', id='mdoH CDS'),
     SeqFeature(FeatureLocation(ExactPosition(412732), ExactPosition(414412), strand=1), type='CDS', id='glucan biosynthesis protein G CDS'),
     SeqFeature(FeatureLocation(ExactPosition(403949), ExactPosition(405458), strand=1), type='CDS', id='hutH CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(453657), ExactPosition(454983), strand=1), type='CDS', id='type IV pilus secretin PilQ family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(408669), ExactPosition(409875), strand=1), type='CDS', id='hutI CDS'),
     SeqFeature(FeatureLocation(ExactPosition(446270), ExactPosition(447077), strand=1), type='CDS', id='thermonuclease family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(418181), ExactPosition(418979), strand=1), type='CDS', id='transporter substrate-binding domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(451421), ExactPosition(452075), strand=1), type='CDS', id='type IV pilus assembly protein PilM CDS'),
     SeqFeature(FeatureLocation(ExactPosition(479930), ExactPosition(480866), strand=-1), type='CDS', id='glycosyl transferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(480862), ExactPosition(481597), strand=-1), type='CDS', id='glycosyltransferase family 2 protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(508721), ExactPosition(509405), strand=1), type='CDS', id='DUF4123 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(477289), ExactPosition(477943), strand=-1), type='CDS', id='outer membrane lipoprotein carrier protein LolA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(531568), ExactPosition(531817), strand=1), type='CDS', id='ATP-binding cassette domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(543265), ExactPosition(545128), strand=-1), type='CDS', id='bifunctional O-antigen ligase/aminoglycoside phosphotransferase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(539724), ExactPosition(541125), strand=1), type='CDS', id='PIG-L family deacetylase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(592766), ExactPosition(593633), strand=1), type='CDS', id='hflC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(563582), ExactPosition(564398), strand=1), type='CDS', id='cpdA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(570672), ExactPosition(571383), strand=1), type='CDS', id='ABC-type transport auxiliary lipoprotein family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(667118), ExactPosition(671729), strand=1), type='CDS', id='DUF4329 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(651581), ExactPosition(654779), strand=1), type='CDS', id='hypothetical protein CDS_37'),
     SeqFeature(FeatureLocation(ExactPosition(663997), ExactPosition(665500), strand=-1), type='CDS', id='urtB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(634661), ExactPosition(635567), strand=1), type='CDS', id='cation diffusion facilitator family transporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(619319), ExactPosition(620210), strand=1), type='CDS', id='transglutaminase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(640806), ExactPosition(641259), strand=-1), type='CDS', id='hypothetical protein CDS_41'),
     SeqFeature(FeatureLocation(ExactPosition(620256), ExactPosition(620445), strand=-1), type='CDS', id='hypothetical protein CDS_43'),
     SeqFeature(FeatureLocation(ExactPosition(690811), ExactPosition(693583), strand=-1), type='CDS', id='hybrid sensor histidine kinase/response regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(678235), ExactPosition(680296), strand=-1), type='CDS', id='TonB-dependent copper receptor CDS'),
     SeqFeature(FeatureLocation(ExactPosition(700799), ExactPosition(702161), strand=-1), type='CDS', id='accC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(732104), ExactPosition(732992), strand=-1), type='CDS', id='type II secretion system F family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(732993), ExactPosition(733875), strand=-1), type='CDS', id='type II secretion system F family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(689181), ExactPosition(689961), strand=1), type='CDS', id='hypothetical protein CDS_45'),
     SeqFeature(FeatureLocation(ExactPosition(731353), ExactPosition(732094), strand=-1), type='CDS', id='tetratricopeptide repeat protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(696847), ExactPosition(697168), strand=-1), type='CDS', id='fis CDS'),
     SeqFeature(FeatureLocation(ExactPosition(682283), ExactPosition(682532), strand=1), type='CDS', id='DUF6124 family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(739751), ExactPosition(741434), strand=-1), type='CDS', id='ShlB/FhaC/HecB family hemolysin secretion/activation protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(793993), ExactPosition(795364), strand=-1), type='CDS', id='gluconate:H+ symporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(782644), ExactPosition(783391), strand=1), type='CDS', id='hypothetical protein CDS_49'),
     SeqFeature(FeatureLocation(ExactPosition(783957), ExactPosition(784362), strand=-1), type='CDS', id='hypothetical protein CDS_51'),
     SeqFeature(FeatureLocation(ExactPosition(817799), ExactPosition(818354), strand=1), type='CDS', id='SepL/TyeA/HrpJ family type III secretion system gatekeeper CDS'),
     SeqFeature(FeatureLocation(ExactPosition(842604), ExactPosition(843123), strand=1), type='CDS', id='acyloxyacyl hydrolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(818926), ExactPosition(819385), strand=1), type='CDS', id='type III secretion system HrpP C-terminal domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(873512), ExactPosition(873950), strand=1), type='CDS', id='fkpB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(843259), ExactPosition(843565), strand=-1), type='CDS', id='YkgJ family cysteine cluster protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(865890), ExactPosition(866127), strand=-1), type='CDS', id='DUF3565 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(829062), ExactPosition(829254), strand=-1), type='CDS', id='hypothetical protein CDS_60'),
     SeqFeature(FeatureLocation(ExactPosition(818350), ExactPosition(818515), strand=1), type='CDS', id='EscN/YscN/HrcN family type III secretion system ATPase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(808904), ExactPosition(808976), strand=1), type='CDS', id='lysine transporter LysE CDS'),
     SeqFeature(FeatureLocation(ExactPosition(909078), ExactPosition(910791), strand=1), type='CDS', id='PTS fructose-like transporter subunit IIB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(915660), ExactPosition(917142), strand=1), type='CDS', id='M10 family metallopeptidase C-terminal domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(890885), ExactPosition(892094), strand=-1), type='CDS', id='type II secretion system F family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(927230), ExactPosition(928142), strand=-1), type='CDS', id='ABC transporter permease subunit CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(890022), ExactPosition(890847), strand=-1), type='CDS', id='A24 family peptidase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(875864), ExactPosition(876293), strand=1), type='CDS', id='pilV CDS'),
     SeqFeature(FeatureLocation(ExactPosition(892293), ExactPosition(892710), strand=1), type='CDS', id='pilin CDS'),
     SeqFeature(FeatureLocation(ExactPosition(915287), ExactPosition(915446), strand=1), type='CDS', id='hypothetical protein CDS_65'),
     SeqFeature(FeatureLocation(ExactPosition(961772), ExactPosition(963326), strand=1), type='CDS', id='garD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(984178), ExactPosition(985525), strand=1), type='CDS', id='pmbA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(951933), ExactPosition(953145), strand=1), type='CDS', id='cytochrome bc complex cytochrome b subunit CDS'),
     SeqFeature(FeatureLocation(ExactPosition(946565), ExactPosition(947486), strand=-1), type='CDS', id='GlxA family transcriptional regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(983548), ExactPosition(984070), strand=-1), type='CDS', id='ribosome-associated protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(956059), ExactPosition(956254), strand=-1), type='CDS', id='hypothetical protein CDS_71'),
     SeqFeature(FeatureLocation(ExactPosition(1043453), ExactPosition(1045196), strand=1), type='CDS', id='penicillin-binding protein 2 CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1013441), ExactPosition(1014605), strand=-1), type='CDS', id='1-acyl-sn-glycerol-3-phosphate acyltransferase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(1060404), ExactPosition(1061409), strand=1), type='CDS', id='hypothetical protein CDS_72'),
     SeqFeature(FeatureLocation(ExactPosition(1042222), ExactPosition(1043164), strand=1), type='CDS', id='rsmH CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1040054), ExactPosition(1040960), strand=1), type='CDS', id='rsmI CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1018309), ExactPosition(1019128), strand=1), type='CDS', id='M48 family metallopeptidase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1033359), ExactPosition(1034004), strand=-1), type='CDS', id='paraquat-inducible protein A CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(1022147), ExactPosition(1022780), strand=1), type='CDS', id='LysE family transporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1031145), ExactPosition(1031706), strand=-1), type='CDS', id='PqiC family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1037695), ExactPosition(1038067), strand=-1), type='CDS', id='YraN family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1011598), ExactPosition(1011832), strand=-1), type='CDS', id='hypothetical protein CDS_76'),
     SeqFeature(FeatureLocation(ExactPosition(1119721), ExactPosition(1122193), strand=-1), type='CDS', id='fimbria/pilus outer membrane usher protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1078773), ExactPosition(1080951), strand=1), type='CDS', id='FUSC family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1096443), ExactPosition(1097985), strand=-1), type='CDS', id='algK CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1077251), ExactPosition(1078763), strand=1), type='CDS', id='efflux transporter outer membrane subunit CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(1099197), ExactPosition(1100679), strand=-1), type='CDS', id='alg8 CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1108073), ExactPosition(1109543), strand=-1), type='CDS', id='rhlB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1132844), ExactPosition(1134302), strand=1), type='CDS', id='dacB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1091910), ExactPosition(1093359), strand=-1), type='CDS', id='alginate O-acetyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1090792), ExactPosition(1091902), strand=-1), type='CDS', id='mannuronate-specific alginate lyase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1141060), ExactPosition(1142080), strand=1), type='CDS', id='Glu/Leu/Phe/Val dehydrogenase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1126683), ExactPosition(1127514), strand=1), type='CDS', id='response regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(1122244), ExactPosition(1122970), strand=-1), type='CDS', id='fimbria/pilus periplasmic chaperone CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1082094), ExactPosition(1082322), strand=-1), type='CDS', id='NAD synthetase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1191645), ExactPosition(1192620), strand=1), type='CDS', id='GTP-binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1174151), ExactPosition(1175054), strand=1), type='CDS', id='era CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1199899), ExactPosition(1200763), strand=1), type='CDS', id='DUF3034 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1157230), ExactPosition(1157809), strand=1), type='CDS', id='RNA polymerase sigma factor CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(1208851), ExactPosition(1209361), strand=1), type='CDS', id='type 1 fimbrial protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1178369), ExactPosition(1178867), strand=1), type='CDS', id='tadA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1154363), ExactPosition(1154852), strand=-1), type='CDS', id='sigma-70 family RNA polymerase sigma factor CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1165574), ExactPosition(1165952), strand=-1), type='CDS', id='hypothetical protein CDS_82'),
     SeqFeature(FeatureLocation(ExactPosition(1158783), ExactPosition(1159134), strand=-1), type='CDS', id='glutaredoxin family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1155022), ExactPosition(1155325), strand=1), type='CDS', id='DUF3649 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1191226), ExactPosition(1191520), strand=1), type='CDS', id='hypothetical protein CDS_83'),
     SeqFeature(FeatureLocation(ExactPosition(1269055), ExactPosition(1270957), strand=-1), type='CDS', id='sensor histidine kinase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1243134), ExactPosition(1245009), strand=-1), type='CDS', id='GGDEF and EAL domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1267709), ExactPosition(1268990), strand=-1), type='CDS', id='sigma-54 dependent transcriptional regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(1234557), ExactPosition(1235691), strand=-1), type='CDS', id='OpgC domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1242144), ExactPosition(1243080), strand=-1), type='CDS', id='c-type cytochrome CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1258854), ExactPosition(1259625), strand=-1), type='CDS', id='MipA/OmpV family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1213949), ExactPosition(1214444), strand=1), type='CDS', id='fimbrial protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(1257452), ExactPosition(1257857), strand=1), type='CDS', id='EamA family transporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1307133), ExactPosition(1308129), strand=1), type='CDS', id='baseplate J/gp47 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1306183), ExactPosition(1306795), strand=1), type='CDS', id='phage baseplate assembly protein V CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1316612), ExactPosition(1317170), strand=1), type='CDS', id='glycoside hydrolase family 19 protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1303217), ExactPosition(1303706), strand=-1), type='CDS', id='DUF4065 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1321592), ExactPosition(1322033), strand=-1), type='CDS', id='hypothetical protein CDS_91'),
     SeqFeature(FeatureLocation(ExactPosition(1314982), ExactPosition(1315366), strand=1), type='CDS', id='phage tail protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1323905), ExactPosition(1324274), strand=1), type='CDS', id='diacylglycerol kinase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1304712), ExactPosition(1304961), strand=1), type='CDS', id='hypothetical protein CDS_94'),
     SeqFeature(FeatureLocation(ExactPosition(1311212), ExactPosition(1311404), strand=1), type='CDS', id='hypothetical protein CDS_95'),
     SeqFeature(FeatureLocation(ExactPosition(1330300), ExactPosition(1330435), strand=-1), type='CDS', id='DNA-binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1399394), ExactPosition(1401038), strand=1), type='CDS', id='Na+/H+ antiporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1396482), ExactPosition(1397688), strand=-1), type='CDS', id='cysteine desulfurase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1361596), ExactPosition(1361668), strand=1), type='CDS', id='prfB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1361669), ExactPosition(1362692), strand=1), type='CDS', id='prfB CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1367004), ExactPosition(1367910), strand=1), type='CDS', id='alpha/beta hydrolase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(1364396), ExactPosition(1365113), strand=1), type='CDS', id='TetR family transcriptional regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(1378461), ExactPosition(1379082), strand=-1), type='CDS', id='hypothetical protein CDS_99'),
     SeqFeature(FeatureLocation(ExactPosition(1369993), ExactPosition(1370404), strand=-1), type='CDS', id='DUF4398 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1386159), ExactPosition(1386561), strand=-1), type='CDS', id='YbaY family lipoprotein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1368401), ExactPosition(1368755), strand=1), type='CDS', id='hypothetical protein CDS_102'),
     SeqFeature(FeatureLocation(ExactPosition(1367906), ExactPosition(1368233), strand=1), type='CDS', id='hypothetical protein CDS_104'),
     SeqFeature(FeatureLocation(ExactPosition(1398819), ExactPosition(1399128), strand=-1), type='CDS', id='arsenate reductase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1425839), ExactPosition(1427159), strand=1), type='CDS', id='tilS CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1459386), ExactPosition(1460145), strand=1), type='CDS', id='collagen-like triple helix repeat-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1420366), ExactPosition(1420999), strand=1), type='CDS', id='rnhB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1442242), ExactPosition(1442455), strand=1), type='CDS', id='hypothetical protein CDS_108'),
     SeqFeature(FeatureLocation(ExactPosition(1456320), ExactPosition(1456425), strand=-1), type='CDS', id='ABC transporter ATP-binding protein CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(1487472), ExactPosition(1489737), strand=-1), type='CDS', id='bifunctional diguanylate cyclase/phosphodiesterase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1513145), ExactPosition(1514522), strand=-1), type='CDS', id='M91 family zinc metallopeptidase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1528521), ExactPosition(1529877), strand=1), type='CDS', id='AMP-binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1505465), ExactPosition(1506668), strand=1), type='CDS', id='pcaF CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1512101), ExactPosition(1512986), strand=-1), type='CDS', id='polysaccharide deacetylase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1537386), ExactPosition(1538178), strand=-1), type='CDS', id='1-acyl-sn-glycerol-3-phosphate acyltransferase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(1533082), ExactPosition(1533850), strand=1), type='CDS', id='LuxR family transcriptional regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1529828), ExactPosition(1530428), strand=1), type='CDS', id='alpha/beta hydrolase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(1495025), ExactPosition(1495367), strand=1), type='CDS', id='hypothetical protein CDS_111'),
     SeqFeature(FeatureLocation(ExactPosition(1500475), ExactPosition(1500676), strand=-1), type='CDS', id='hypothetical protein CDS_113'),
     SeqFeature(FeatureLocation(ExactPosition(1498099), ExactPosition(1498261), strand=1), type='CDS', id='hypothetical protein CDS_114'),
     SeqFeature(FeatureLocation(ExactPosition(1588407), ExactPosition(1590597), strand=1), type='CDS', id='autotransporter outer membrane beta-barrel domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1586084), ExactPosition(1588103), strand=1), type='CDS', id='hypothetical protein CDS_116'),
     SeqFeature(FeatureLocation(ExactPosition(1583813), ExactPosition(1585769), strand=1), type='CDS', id='hypothetical protein CDS_117'),
     SeqFeature(FeatureLocation(ExactPosition(1548884), ExactPosition(1550087), strand=1), type='CDS', id='SAM-dependent methyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1597578), ExactPosition(1598568), strand=-1), type='CDS', id='tripartite tricarboxylate transporter substrate binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1567314), ExactPosition(1568232), strand=1), type='CDS', id='LysR family transcriptional regulator CDS_17'),
     SeqFeature(FeatureLocation(ExactPosition(1546658), ExactPosition(1547270), strand=1), type='CDS', id='LemA family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1569666), ExactPosition(1570194), strand=1), type='CDS', id='DUF4142 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1596990), ExactPosition(1597449), strand=-1), type='CDS', id='tripartite tricarboxylate transporter TctB family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1609196), ExactPosition(1609625), strand=1), type='CDS', id='hypothetical protein CDS_120'),
     SeqFeature(FeatureLocation(ExactPosition(1547399), ExactPosition(1547693), strand=1), type='CDS', id='TPM domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1604766), ExactPosition(1604958), strand=-1), type='CDS', id='L-aspartate oxidase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1610868), ExactPosition(1611033), strand=1), type='CDS', id='hypothetical protein CDS_124'),
     SeqFeature(FeatureLocation(ExactPosition(1547891), ExactPosition(1548017), strand=1), type='CDS', id='hypothetical protein CDS_125'),
     SeqFeature(FeatureLocation(ExactPosition(1656429), ExactPosition(1657947), strand=1), type='CDS', id='MFS transporter CDS_14'),
     SeqFeature(FeatureLocation(ExactPosition(1640841), ExactPosition(1642167), strand=1), type='CDS', id='MFS transporter CDS_15'),
     SeqFeature(FeatureLocation(ExactPosition(1639463), ExactPosition(1640786), strand=1), type='CDS', id='MFS transporter CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(1644961), ExactPosition(1646149), strand=-1), type='CDS', id='cyanate transporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1655178), ExactPosition(1656363), strand=1), type='CDS', id='pobA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1669266), ExactPosition(1670187), strand=1), type='CDS', id='ParB/Srx family N-terminal domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1658463), ExactPosition(1659303), strand=-1), type='CDS', id='cache domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1659353), ExactPosition(1660124), strand=-1), type='CDS', id='transporter substrate-binding domain-containing protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(1674515), ExactPosition(1675052), strand=1), type='CDS', id='hypothetical protein CDS_128'),
     SeqFeature(FeatureLocation(ExactPosition(1631982), ExactPosition(1632412), strand=-1), type='CDS', id='hypothetical protein CDS_129'),
     SeqFeature(FeatureLocation(ExactPosition(1668793), ExactPosition(1669168), strand=-1), type='CDS', id='YajD family HNH nuclease CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1671629), ExactPosition(1671908), strand=1), type='CDS', id='hypothetical protein CDS_130'),
     SeqFeature(FeatureLocation(ExactPosition(1625817), ExactPosition(1626087), strand=1), type='CDS', id='hypothetical protein CDS_131'),
     SeqFeature(FeatureLocation(ExactPosition(1642121), ExactPosition(1642367), strand=-1), type='CDS', id='hypothetical protein CDS_133'),
     SeqFeature(FeatureLocation(ExactPosition(1615515), ExactPosition(1615695), strand=-1), type='CDS', id='hypothetical protein CDS_134'),
     SeqFeature(FeatureLocation(ExactPosition(1628072), ExactPosition(1628252), strand=1), type='CDS', id='hypothetical protein CDS_135'),
     SeqFeature(FeatureLocation(ExactPosition(1693210), ExactPosition(1694998), strand=1), type='CDS', id='ABC transporter ATP-binding protein/permease CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1730430), ExactPosition(1730943), strand=1), type='CDS', id='cupredoxin family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1747326), ExactPosition(1747665), strand=-1), type='CDS', id='hypothetical protein CDS_143'),
     SeqFeature(FeatureLocation(ExactPosition(1708203), ExactPosition(1708437), strand=1), type='CDS', id='hypothetical protein CDS_144'),
     SeqFeature(FeatureLocation(ExactPosition(1692791), ExactPosition(1692965), strand=-1), type='CDS', id='hypothetical protein CDS_145'),
     SeqFeature(FeatureLocation(ExactPosition(1739872), ExactPosition(1739962), strand=-1), type='CDS', id='Pyocin activator protein PrtN CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1760939), ExactPosition(1763852), strand=1), type='CDS', id='ATP-binding protein CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(1808149), ExactPosition(1810360), strand=1), type='CDS', id='bifunctional prephenate dehydrogenase/3-phosphoshikimate 1-carboxyvinyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1749198), ExactPosition(1750575), strand=-1), type='CDS', id='PIN-like domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1814880), ExactPosition(1816164), strand=1), type='CDS', id='flippase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1779646), ExactPosition(1780888), strand=-1), type='CDS', id='adenylate cyclase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1753489), ExactPosition(1753933), strand=1), type='CDS', id='IS3 family transposase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1774282), ExactPosition(1774612), strand=1), type='CDS', id='YXWGXW repeat-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1775193), ExactPosition(1775451), strand=-1), type='CDS', id='AbrB/MazE/SpoVT family DNA-binding domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1754005), ExactPosition(1754227), strand=1), type='CDS', id='hypothetical protein CDS_151'),
     SeqFeature(FeatureLocation(ExactPosition(1750810), ExactPosition(1751026), strand=-1), type='CDS', id='hypothetical protein CDS_152'),
     SeqFeature(FeatureLocation(ExactPosition(1840127), ExactPosition(1842185), strand=1), type='CDS', id='kdpB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1818288), ExactPosition(1819515), strand=1), type='CDS', id='hypothetical protein CDS_153'),
     SeqFeature(FeatureLocation(ExactPosition(1873317), ExactPosition(1874394), strand=-1), type='CDS', id='3-deoxy-7-phosphoheptulonate synthase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1845681), ExactPosition(1846371), strand=1), type='CDS', id='response regulator CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(1824286), ExactPosition(1824931), strand=1), type='CDS', id='sugar transferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1834689), ExactPosition(1835334), strand=1), type='CDS', id='GntR family transcriptional regulator CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(1842323), ExactPosition(1842869), strand=1), type='CDS', id='kdpC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1850576), ExactPosition(1850984), strand=-1), type='CDS', id='immunity 50 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1851031), ExactPosition(1851433), strand=-1), type='CDS', id='immunity 50 family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1879413), ExactPosition(1879782), strand=-1), type='CDS', id='hypothetical protein CDS_157'),
     SeqFeature(FeatureLocation(ExactPosition(1833624), ExactPosition(1833957), strand=1), type='CDS', id='ComEA family DNA-binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1861176), ExactPosition(1861485), strand=1), type='CDS', id='hypothetical protein CDS_158'),
     SeqFeature(FeatureLocation(ExactPosition(1870589), ExactPosition(1870883), strand=1), type='CDS', id='hypothetical protein CDS_159'),
     SeqFeature(FeatureLocation(ExactPosition(1872971), ExactPosition(1873253), strand=1), type='CDS', id='N-acetyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1824930), ExactPosition(1825137), strand=1), type='CDS', id='acyl carrier protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1838318), ExactPosition(1838408), strand=1), type='CDS', id='kdpF CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1905679), ExactPosition(1909315), strand=-1), type='CDS', id='transporter substrate-binding domain-containing protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(1901810), ExactPosition(1904120), strand=-1), type='CDS', id='response regulator CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(1933116), ExactPosition(1934664), strand=-1), type='CDS', id='MFS transporter CDS_20'),
     SeqFeature(FeatureLocation(ExactPosition(1946141), ExactPosition(1946579), strand=1), type='CDS', id='phnG CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1937658), ExactPosition(1938078), strand=-1), type='CDS', id='chorismate mutase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(1918299), ExactPosition(1918635), strand=1), type='CDS', id='hypothetical protein CDS_163'),
     SeqFeature(FeatureLocation(ExactPosition(1941951), ExactPosition(1942263), strand=1), type='CDS', id='histidine phosphatase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1932282), ExactPosition(1932558), strand=-1), type='CDS', id='DUF6124 family protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(1886717), ExactPosition(1886924), strand=1), type='CDS', id='DUF1127 domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2006868), ExactPosition(2008950), strand=-1), type='CDS', id='TonB-dependent receptor CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(1996570), ExactPosition(1998475), strand=1), type='CDS', id='htpG CDS'),
     SeqFeature(FeatureLocation(ExactPosition(1974124), ExactPosition(1975678), strand=-1), type='CDS', id='glycosyl hydrolase family 17 protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2017084), ExactPosition(2018122), strand=-1), type='CDS', id='ATP-binding protein CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(2005937), ExactPosition(2006867), strand=-1), type='CDS', id='ABC transporter substrate-binding protein CDS_14'),
     SeqFeature(FeatureLocation(ExactPosition(1998541), ExactPosition(1999270), strand=1), type='CDS', id='dienelactone hydrolase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2036153), ExactPosition(2038601), strand=-1), type='CDS', id='acyl-CoA dehydrogenase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2062090), ExactPosition(2063641), strand=1), type='CDS', id='istA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2066843), ExactPosition(2068301), strand=-1), type='CDS', id='cbb3-type cytochrome c oxidase subunit I CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2052908), ExactPosition(2054252), strand=1), type='CDS', id='metallophosphoesterase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2069160), ExactPosition(2070396), strand=-1), type='CDS', id='molybdopterin-dependent oxidoreductase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2042733), ExactPosition(2043927), strand=1), type='CDS', id='MFS transporter CDS_23'),
     SeqFeature(FeatureLocation(ExactPosition(2055336), ExactPosition(2056338), strand=1), type='CDS', id='hypothetical protein CDS_170'),
     SeqFeature(FeatureLocation(ExactPosition(2018121), ExactPosition(2018886), strand=-1), type='CDS', id='response regulator transcription factor CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(2027732), ExactPosition(2028491), strand=-1), type='CDS', id='hypothetical protein CDS_172'),
     SeqFeature(FeatureLocation(ExactPosition(2073796), ExactPosition(2074399), strand=-1), type='CDS', id='uracil-DNA glycosylase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2033236), ExactPosition(2033704), strand=-1), type='CDS', id='hypothetical protein CDS_175'),
     SeqFeature(FeatureLocation(ExactPosition(2082622), ExactPosition(2083018), strand=1), type='CDS', id='tnpB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2068796), ExactPosition(2069159), strand=-1), type='CDS', id='hypothetical protein CDS_179'),
     SeqFeature(FeatureLocation(ExactPosition(2079043), ExactPosition(2079385), strand=-1), type='CDS', id='hypothetical protein CDS_180'),
     SeqFeature(FeatureLocation(ExactPosition(2065352), ExactPosition(2065547), strand=-1), type='CDS', id='hypothetical protein CDS_183'),
     SeqFeature(FeatureLocation(ExactPosition(2111953), ExactPosition(2113396), strand=-1), type='CDS', id='NAD-dependent succinate-semialdehyde dehydrogenase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2132462), ExactPosition(2133873), strand=1), type='CDS', id='IS1182 family transposase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2113482), ExactPosition(2114493), strand=-1), type='CDS', id='dihydrodipicolinate synthase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2094410), ExactPosition(2095370), strand=-1), type='CDS', id='ssDNA-binding domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2099918), ExactPosition(2100794), strand=-1), type='CDS', id='TIGR03756 family integrating conjugative elementprotein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2128200), ExactPosition(2128989), strand=1), type='CDS', id='SDR family oxidoreductase CDS_8'),
     SeqFeature(FeatureLocation(ExactPosition(2124143), ExactPosition(2124896), strand=1), type='CDS', id='SDR family oxidoreductase CDS_9'),
     SeqFeature(FeatureLocation(ExactPosition(2131374), ExactPosition(2132124), strand=1), type='CDS', id='FadR family transcriptional regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2140366), ExactPosition(2141113), strand=-1), type='CDS', id='ABC transporter substrate-binding protein CDS_15'),
     SeqFeature(FeatureLocation(ExactPosition(2115787), ExactPosition(2116447), strand=1), type='CDS', id='FCD domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2107654), ExactPosition(2108296), strand=-1), type='CDS', id='TIGR03746 family integrating conjugative elementprotein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2139717), ExactPosition(2140260), strand=-1), type='CDS', id='PilL N-terminal domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2130914), ExactPosition(2131274), strand=1), type='CDS', id='SDR family oxidoreductase CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(2137287), ExactPosition(2137644), strand=-1), type='CDS', id='integrating conjugative element protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2095500), ExactPosition(2095830), strand=-1), type='CDS', id='hypothetical protein CDS_188'),
     SeqFeature(FeatureLocation(ExactPosition(2130567), ExactPosition(2130885), strand=1), type='CDS', id='SDR family oxidoreductase CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(2110259), ExactPosition(2110559), strand=-1), type='CDS', id='transposase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2101808), ExactPosition(2102086), strand=-1), type='CDS', id='HU family DNA-binding protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2101404), ExactPosition(2101677), strand=1), type='CDS', id='hypothetical protein CDS_190'),
     SeqFeature(FeatureLocation(ExactPosition(2141760), ExactPosition(2142018), strand=-1), type='CDS', id='hypothetical protein CDS_191'),
     SeqFeature(FeatureLocation(ExactPosition(2151052), ExactPosition(2151262), strand=-1), type='CDS', id='hypothetical protein CDS_192'),
     SeqFeature(FeatureLocation(ExactPosition(2094075), ExactPosition(2094273), strand=1), type='CDS', id='hypothetical protein CDS_193'),
     SeqFeature(FeatureLocation(ExactPosition(2110558), ExactPosition(2110633), strand=1), type='CDS', id='integrase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2187044), ExactPosition(2188661), strand=1), type='CDS', id='M81 family metallopeptidase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2191061), ExactPosition(2192645), strand=1), type='CDS', id='BCCT family transporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2194825), ExactPosition(2195776), strand=1), type='CDS', id='LysR substrate-binding domain-containing protein CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(2184632), ExactPosition(2185541), strand=-1), type='CDS', id='LysR family transcriptional regulator CDS_35'),
     SeqFeature(FeatureLocation(ExactPosition(2210100), ExactPosition(2210907), strand=-1), type='CDS', id='alkaline phosphatase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2212457), ExactPosition(2213171), strand=1), type='CDS', id='UTRA domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2174235), ExactPosition(2174907), strand=1), type='CDS', id='PAS domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2176609), ExactPosition(2177269), strand=1), type='CDS', id='transposase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2175417), ExactPosition(2175789), strand=-1), type='CDS', id='hypothetical protein CDS_197'),
     SeqFeature(FeatureLocation(ExactPosition(2217377), ExactPosition(2217698), strand=-1), type='CDS', id='hypothetical protein CDS_199'),
     SeqFeature(FeatureLocation(ExactPosition(2175915), ExactPosition(2176221), strand=-1), type='CDS', id='TIGR00366 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2189075), ExactPosition(2189315), strand=1), type='CDS', id='hypothetical protein CDS_201'),
     SeqFeature(FeatureLocation(ExactPosition(2188930), ExactPosition(2189152), strand=1), type='CDS', id='aminotransferase class III-fold pyridoxal phosphate-dependent enzyme CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2176383), ExactPosition(2176595), strand=-1), type='CDS', id='IS66 family transposase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2175027), ExactPosition(2175225), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_13'),
     SeqFeature(FeatureLocation(ExactPosition(2184412), ExactPosition(2184571), strand=1), type='CDS', id='IS3 family transposase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2176211), ExactPosition(2176307), strand=-1), type='CDS', id='hypothetical protein CDS_202'),
     SeqFeature(FeatureLocation(ExactPosition(2268167), ExactPosition(2269994), strand=-1), type='CDS', id='glycoside hydrolase family 15 protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2250578), ExactPosition(2252006), strand=-1), type='CDS', id='O-antigen ligase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2271770), ExactPosition(2272994), strand=1), type='CDS', id='transaldolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2248097), ExactPosition(2249126), strand=-1), type='CDS', id='acyltransferase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2266190), ExactPosition(2267195), strand=1), type='CDS', id='GlxA family transcriptional regulator CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2283169), ExactPosition(2284156), strand=1), type='CDS', id='DeoR family transcriptional regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2221883), ExactPosition(2222798), strand=-1), type='CDS', id='LysR substrate-binding domain-containing protein CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(2222852), ExactPosition(2223728), strand=-1), type='CDS', id='alpha/beta hydrolase CDS_9'),
     SeqFeature(FeatureLocation(ExactPosition(2275595), ExactPosition(2276366), strand=1), type='CDS', id='class I SAM-dependent methyltransferase CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(2271040), ExactPosition(2271745), strand=1), type='CDS', id='HAD-IA family hydrolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2247462), ExactPosition(2248038), strand=1), type='CDS', id='DUF4174 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2242548), ExactPosition(2242950), strand=-1), type='CDS', id='GFA family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2224613), ExactPosition(2224979), strand=1), type='CDS', id='antibiotic biosynthesis monooxygenase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2229423), ExactPosition(2229693), strand=1), type='CDS', id='hypothetical protein CDS_207'),
     SeqFeature(FeatureLocation(ExactPosition(2263907), ExactPosition(2264039), strand=-1), type='CDS', id='TonB-dependent receptor CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(2349265), ExactPosition(2350756), strand=-1), type='CDS', id='helix-turn-helix transcriptional regulator CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2303766), ExactPosition(2305086), strand=1), type='CDS', id='TRAP transporter large permease subunit CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2309093), ExactPosition(2310302), strand=1), type='CDS', id='cytochrome c biogenesis protein DipZ CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2338270), ExactPosition(2339395), strand=-1), type='CDS', id='integrase core domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2350953), ExactPosition(2351922), strand=1), type='CDS', id='hypothetical protein CDS_210'),
     SeqFeature(FeatureLocation(ExactPosition(2324140), ExactPosition(2324842), strand=-1), type='CDS', id='hypothetical protein CDS_212'),
     SeqFeature(FeatureLocation(ExactPosition(2347317), ExactPosition(2347776), strand=1), type='CDS', id='transposase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(2294811), ExactPosition(2295258), strand=-1), type='CDS', id='GNAT family N-acetyltransferase CDS_9'),
     SeqFeature(FeatureLocation(ExactPosition(2295478), ExactPosition(2295853), strand=-1), type='CDS', id='hypothetical protein CDS_214'),
     SeqFeature(FeatureLocation(ExactPosition(2347920), ExactPosition(2348004), strand=1), type='CDS', id='transposase CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(2414159), ExactPosition(2416220), strand=1), type='CDS', id='ATP-binding cassette domain-containing protein CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(2390596), ExactPosition(2392075), strand=1), type='CDS', id='acyl--CoA ligase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2400911), ExactPosition(2402246), strand=-1), type='CDS', id='LLM class flavin-dependent oxidoreductase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2384777), ExactPosition(2386091), strand=-1), type='CDS', id='MFS transporter CDS_30'),
     SeqFeature(FeatureLocation(ExactPosition(2380481), ExactPosition(2381735), strand=-1), type='CDS', id='ectB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2383572), ExactPosition(2384757), strand=-1), type='CDS', id='acyl-CoA dehydrogenase family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2394838), ExactPosition(2395918), strand=1), type='CDS', id='sigma-54 dependent transcriptional regulator CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(2392956), ExactPosition(2393790), strand=-1), type='CDS', id='hypothetical protein CDS_220'),
     SeqFeature(FeatureLocation(ExactPosition(2370834), ExactPosition(2371584), strand=1), type='CDS', id='carbon-nitrogen hydrolase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2358450), ExactPosition(2359056), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(2367660), ExactPosition(2368083), strand=1), type='CDS', id='chemotaxis protein CheY CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2376311), ExactPosition(2376644), strand=1), type='CDS', id='Arm DNA-binding domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2377718), ExactPosition(2378018), strand=-1), type='CDS', id='hypothetical protein CDS_221'),
     SeqFeature(FeatureLocation(ExactPosition(2372051), ExactPosition(2372291), strand=-1), type='CDS', id='DNA-binding protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2357413), ExactPosition(2357599), strand=1), type='CDS', id='type II toxin-antitoxin system HicA family toxin CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2377166), ExactPosition(2377328), strand=-1), type='CDS', id='histidine phosphatase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2413713), ExactPosition(2413836), strand=-1), type='CDS', id='XRE family transcriptional regulator CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2439772), ExactPosition(2441374), strand=1), type='CDS', id='hybrid sensor histidine kinase/response regulator CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2485107), ExactPosition(2486475), strand=1), type='CDS', id='MFS transporter CDS_31'),
     SeqFeature(FeatureLocation(ExactPosition(2463581), ExactPosition(2464814), strand=-1), type='CDS', id='pectate lyase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2457129), ExactPosition(2458215), strand=1), type='CDS', id='ABC transporter substrate-binding protein CDS_21'),
     SeqFeature(FeatureLocation(ExactPosition(2432998), ExactPosition(2434024), strand=-1), type='CDS', id='OmpA family protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(2447473), ExactPosition(2448457), strand=1), type='CDS', id='GlxA family transcriptional regulator CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2430629), ExactPosition(2431325), strand=-1), type='CDS', id='GntR family transcriptional regulator CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(2445376), ExactPosition(2446048), strand=-1), type='CDS', id='Crp/Fnr family transcriptional regulator CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2439029), ExactPosition(2439668), strand=-1), type='CDS', id='response regulator transcription factor CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(2450142), ExactPosition(2450721), strand=1), type='CDS', id='DUF1349 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2448460), ExactPosition(2448986), strand=-1), type='CDS', id='site-specific integrase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2446144), ExactPosition(2446537), strand=1), type='CDS', id='CoA transferase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2441536), ExactPosition(2441824), strand=1), type='CDS', id='hypothetical protein CDS_227'),
     SeqFeature(FeatureLocation(ExactPosition(2431886), ExactPosition(2432141), strand=1), type='CDS', id='Arm DNA-binding domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2431289), ExactPosition(2431379), strand=1), type='CDS', id='ABC transporter ATP-binding protein CDS_21'),
     SeqFeature(FeatureLocation(ExactPosition(2539616), ExactPosition(2541926), strand=-1), type='CDS', id='aminomethyltransferase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2517041), ExactPosition(2518988), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_18'),
     SeqFeature(FeatureLocation(ExactPosition(2534011), ExactPosition(2535337), strand=1), type='CDS', id='ammonium transporter CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2547959), ExactPosition(2549072), strand=1), type='CDS', id='ATP-binding protein CDS_14'),
     SeqFeature(FeatureLocation(ExactPosition(2545374), ExactPosition(2546391), strand=-1), type='CDS', id='DUF3445 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2536853), ExactPosition(2537789), strand=-1), type='CDS', id='folD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2528659), ExactPosition(2529259), strand=-1), type='CDS', id='helix-turn-helix domain-containing protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(2598753), ExactPosition(2600715), strand=1), type='CDS', id='sigma-54-dependent Fis family transcriptional regulator CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2593607), ExactPosition(2594834), strand=1), type='CDS', id='MFS transporter CDS_35'),
     SeqFeature(FeatureLocation(ExactPosition(2605317), ExactPosition(2606517), strand=1), type='CDS', id='FAD-dependent oxidoreductase CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(2592543), ExactPosition(2593542), strand=-1), type='CDS', id='AraC family transcriptional regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2621071), ExactPosition(2621968), strand=-1), type='CDS', id='hypothetical protein CDS_232'),
     SeqFeature(FeatureLocation(ExactPosition(2559762), ExactPosition(2560596), strand=-1), type='CDS', id='alpha/beta hydrolase CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(2613976), ExactPosition(2614807), strand=1), type='CDS', id='IS3 family transposase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2566998), ExactPosition(2567820), strand=-1), type='CDS', id='DeoR/GlpR family DNA-binding transcription regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2618153), ExactPosition(2618540), strand=1), type='CDS', id='DoxX family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2591037), ExactPosition(2591154), strand=-1), type='CDS', id='glutathione S-transferase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2652668), ExactPosition(2654252), strand=1), type='CDS', id='ShlB/FhaC/HecB family hemolysin secretion/activation protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2675261), ExactPosition(2676815), strand=1), type='CDS', id='DUF726 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2673274), ExactPosition(2674060), strand=-1), type='CDS', id='crotonase/enoyl-CoA hydratase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2683123), ExactPosition(2683908), strand=1), type='CDS', id='helix-turn-helix transcriptional regulator CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(2623864), ExactPosition(2624509), strand=1), type='CDS', id='TonB C-terminal domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2680147), ExactPosition(2680636), strand=1), type='CDS', id='DMT family transporter CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2682474), ExactPosition(2682747), strand=-1), type='CDS', id='hypothetical protein CDS_238'),
     SeqFeature(FeatureLocation(ExactPosition(2652276), ExactPosition(2652420), strand=1), type='CDS', id='transposase CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(2755635), ExactPosition(2769675), strand=1), type='CDS', id='non-ribosomal peptide synthase/polyketide synthase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2699836), ExactPosition(2701753), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_20'),
     SeqFeature(FeatureLocation(ExactPosition(2724948), ExactPosition(2725300), strand=1), type='CDS', id='IS3 family transposase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2725299), ExactPosition(2726174), strand=1), type='CDS', id='IS3 family transposase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(2702121), ExactPosition(2703219), strand=-1), type='CDS', id='hypothetical protein CDS_241'),
     SeqFeature(FeatureLocation(ExactPosition(2695698), ExactPosition(2696646), strand=-1), type='CDS', id='aromatic alcohol reductase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2720248), ExactPosition(2721187), strand=-1), type='CDS', id='helix-turn-helix domain-containing protein CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(2745306), ExactPosition(2746038), strand=1), type='CDS', id='GntR family transcriptional regulator CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(2728834), ExactPosition(2729539), strand=-1), type='CDS', id='hypothetical protein CDS_242'),
     SeqFeature(FeatureLocation(ExactPosition(2733910), ExactPosition(2734558), strand=-1), type='CDS', id='site-specific integrase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2716007), ExactPosition(2716553), strand=-1), type='CDS', id='FUSC family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2717504), ExactPosition(2718047), strand=-1), type='CDS', id='cysteine hydrolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2691658), ExactPosition(2692156), strand=-1), type='CDS', id='transcriptional regulator CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2709911), ExactPosition(2710370), strand=1), type='CDS', id='hypothetical protein CDS_244'),
     SeqFeature(FeatureLocation(ExactPosition(2707540), ExactPosition(2707966), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_24'),
     SeqFeature(FeatureLocation(ExactPosition(2692414), ExactPosition(2692810), strand=1), type='CDS', id='hypothetical protein CDS_247'),
     SeqFeature(FeatureLocation(ExactPosition(2693394), ExactPosition(2693778), strand=1), type='CDS', id='hypothetical protein CDS_248'),
     SeqFeature(FeatureLocation(ExactPosition(2694087), ExactPosition(2694438), strand=-1), type='CDS', id='hypothetical protein CDS_249'),
     SeqFeature(FeatureLocation(ExactPosition(2694586), ExactPosition(2694931), strand=-1), type='CDS', id='hypothetical protein CDS_250'),
     SeqFeature(FeatureLocation(ExactPosition(2715644), ExactPosition(2715986), strand=-1), type='CDS', id='carboxymuconolactone decarboxylase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2699159), ExactPosition(2699499), strand=1), type='CDS', id='transcriptional regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2740060), ExactPosition(2740351), strand=-1), type='CDS', id='(2Fe-2S)-binding protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2714972), ExactPosition(2715245), strand=1), type='CDS', id='DUF1652 domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2695125), ExactPosition(2695386), strand=1), type='CDS', id='nuclear transport factor 2 family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2744950), ExactPosition(2745193), strand=-1), type='CDS', id='3-carboxy-cis,cis-muconate cycloisomerase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2717117), ExactPosition(2717354), strand=1), type='CDS', id='hypothetical protein CDS_251'),
     SeqFeature(FeatureLocation(ExactPosition(2727017), ExactPosition(2727227), strand=-1), type='CDS', id='hypothetical protein CDS_252'),
     SeqFeature(FeatureLocation(ExactPosition(2693828), ExactPosition(2694026), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_14'),
     SeqFeature(FeatureLocation(ExactPosition(2698829), ExactPosition(2699018), strand=-1), type='CDS', id='IS66 family insertion sequence element accessoryprotein TnpB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2707393), ExactPosition(2707539), strand=-1), type='CDS', id='potassium transporter TrkH CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2726211), ExactPosition(2726328), strand=-1), type='CDS', id='transcriptional regulator CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2793979), ExactPosition(2806840), strand=1), type='CDS', id='non-ribosomal peptide synthetase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2786085), ExactPosition(2786916), strand=1), type='CDS', id='N(5)-hydroxyornithine transformylase PvdF CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2822399), ExactPosition(2823071), strand=-1), type='CDS', id='helix-turn-helix transcriptional regulator CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(2863768), ExactPosition(2868403), strand=-1), type='CDS', id='RHS repeat protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2860268), ExactPosition(2862395), strand=-1), type='CDS', id='TonB-dependent receptor CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(2855382), ExactPosition(2856972), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_25'),
     SeqFeature(FeatureLocation(ExactPosition(2851769), ExactPosition(2853323), strand=1), type='CDS', id='sugar ABC transporter ATP-binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2853350), ExactPosition(2854373), strand=1), type='CDS', id='ABC transporter permease CDS_28'),
     SeqFeature(FeatureLocation(ExactPosition(2832096), ExactPosition(2833080), strand=-1), type='CDS', id='hypothetical protein CDS_254'),
     SeqFeature(FeatureLocation(ExactPosition(2876215), ExactPosition(2877160), strand=-1), type='CDS', id='diguanylate cyclase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2843745), ExactPosition(2844639), strand=1), type='CDS', id='iolE CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2828181), ExactPosition(2829069), strand=-1), type='CDS', id='helix-turn-helix domain-containing protein CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(2835471), ExactPosition(2836320), strand=1), type='CDS', id='SDR family oxidoreductase CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(2858531), ExactPosition(2858981), strand=1), type='CDS', id='hypothetical protein CDS_256'),
     SeqFeature(FeatureLocation(ExactPosition(2862490), ExactPosition(2862880), strand=-1), type='CDS', id='DUF2946 domain-containing protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2871249), ExactPosition(2871579), strand=1), type='CDS', id='hypothetical protein CDS_259'),
     SeqFeature(FeatureLocation(ExactPosition(2877749), ExactPosition(2878061), strand=1), type='CDS', id='YXWGXW repeat-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2857193), ExactPosition(2857430), strand=-1), type='CDS', id='DUF2789 domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2857497), ExactPosition(2857728), strand=-1), type='CDS', id='hypothetical protein CDS_263'),
     SeqFeature(FeatureLocation(ExactPosition(2875429), ExactPosition(2875651), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_15'),
     SeqFeature(FeatureLocation(ExactPosition(2841387), ExactPosition(2841591), strand=-1), type='CDS', id='hypothetical protein CDS_264'),
     SeqFeature(FeatureLocation(ExactPosition(2884163), ExactPosition(2884331), strand=-1), type='CDS', id='thioredoxin CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2877572), ExactPosition(2877737), strand=-1), type='CDS', id='hypothetical protein CDS_266'),
     SeqFeature(FeatureLocation(ExactPosition(2877251), ExactPosition(2877401), strand=1), type='CDS', id='DUF1652 domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2877397), ExactPosition(2877538), strand=-1), type='CDS', id='hypothetical protein CDS_267'),
     SeqFeature(FeatureLocation(ExactPosition(2882125), ExactPosition(2882236), strand=-1), type='CDS', id='DAK2 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2958392), ExactPosition(2961050), strand=1), type='CDS', id='pepN CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2910769), ExactPosition(2912608), strand=-1), type='CDS', id='extracellular solute-binding protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(2931314), ExactPosition(2932544), strand=1), type='CDS', id='hypothetical protein CDS_268'),
     SeqFeature(FeatureLocation(ExactPosition(2944671), ExactPosition(2945745), strand=1), type='CDS', id='cobW CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2932797), ExactPosition(2933718), strand=1), type='CDS', id='hypothetical protein CDS_269'),
     SeqFeature(FeatureLocation(ExactPosition(2917097), ExactPosition(2917868), strand=1), type='CDS', id='methyltransferase domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2918421), ExactPosition(2919162), strand=1), type='CDS', id='dnaQ CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2942308), ExactPosition(2943013), strand=-1), type='CDS', id='CbtA family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2921566), ExactPosition(2922148), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(2927846), ExactPosition(2928260), strand=1), type='CDS', id='MerR family transcriptional regulator CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(2923550), ExactPosition(2923787), strand=-1), type='CDS', id='hypothetical protein CDS_272'),
     SeqFeature(FeatureLocation(ExactPosition(2966996), ExactPosition(2968745), strand=-1), type='CDS', id='phosphoethanolamine transferase CptA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3015570), ExactPosition(3017157), strand=1), type='CDS', id='RimK family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3018547), ExactPosition(3019570), strand=1), type='CDS', id='ilvA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2979827), ExactPosition(2980823), strand=-1), type='CDS', id='FecR family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2989087), ExactPosition(2990014), strand=1), type='CDS', id='carbon-nitrogen hydrolase family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(2988076), ExactPosition(2988997), strand=-1), type='CDS', id='LysR family transcriptional regulator CDS_45'),
     SeqFeature(FeatureLocation(ExactPosition(3017309), ExactPosition(3018071), strand=-1), type='CDS', id='map CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3006613), ExactPosition(3007315), strand=1), type='CDS', id='response regulator transcription factor CDS_13'),
     SeqFeature(FeatureLocation(ExactPosition(3015029), ExactPosition(3015476), strand=-1), type='CDS', id='GNAT family N-acetyltransferase CDS_18'),
     SeqFeature(FeatureLocation(ExactPosition(3019804), ExactPosition(3020074), strand=1), type='CDS', id='DUF1652 domain-containing protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3020547), ExactPosition(3020817), strand=-1), type='CDS', id='DUF2790 domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3018070), ExactPosition(3018271), strand=-1), type='CDS', id='ParD-like family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(2968905), ExactPosition(2969070), strand=-1), type='CDS', id='DUF6026 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3091156), ExactPosition(3094684), strand=1), type='CDS', id='molybdopterin-dependent oxidoreductase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3040212), ExactPosition(3041625), strand=1), type='CDS', id='efflux transporter outer membrane subunit CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3030370), ExactPosition(3031660), strand=-1), type='CDS', id='glycosyltransferase family 4 protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(3066261), ExactPosition(3067518), strand=-1), type='CDS', id='3-phosphoshikimate 1-carboxyvinyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3061216), ExactPosition(3062431), strand=-1), type='CDS', id='MFS transporter CDS_39'),
     SeqFeature(FeatureLocation(ExactPosition(3080606), ExactPosition(3081563), strand=-1), type='CDS', id='aliphatic sulfonate ABC transporter substrate-binding protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3077969), ExactPosition(3078890), strand=1), type='CDS', id='DUF4880 domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3072028), ExactPosition(3072943), strand=-1), type='CDS', id='class I SAM-dependent methyltransferase CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(3026906), ExactPosition(3027797), strand=-1), type='CDS', id='sugar ABC transporter permease CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3079762), ExactPosition(3080584), strand=-1), type='CDS', id='ABC transporter permease CDS_31'),
     SeqFeature(FeatureLocation(ExactPosition(3085143), ExactPosition(3085776), strand=-1), type='CDS', id='isochorismatase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3067715), ExactPosition(3068279), strand=-1), type='CDS', id='YecA family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3065842), ExactPosition(3066229), strand=-1), type='CDS', id='hypothetical protein CDS_283'),
     SeqFeature(FeatureLocation(ExactPosition(3048742), ExactPosition(3048919), strand=-1), type='CDS', id='hypothetical protein CDS_285'),
     SeqFeature(FeatureLocation(ExactPosition(3114774), ExactPosition(3116640), strand=-1), type='CDS', id='DNA cytosine methyltransferase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3106450), ExactPosition(3108037), strand=-1), type='CDS', id='ABC-F family ATPase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3108057), ExactPosition(3109251), strand=-1), type='CDS', id='site-specific integrase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3105015), ExactPosition(3106191), strand=-1), type='CDS', id='MFS transporter CDS_40'),
     SeqFeature(FeatureLocation(ExactPosition(3140075), ExactPosition(3141170), strand=1), type='CDS', id='minor capsid protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3152956), ExactPosition(3154039), strand=-1), type='CDS', id='type I restriction enzyme HsdR N-terminal domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3125801), ExactPosition(3126857), strand=-1), type='CDS', id='ImmA/IrrE family metallo-endopeptidase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3129752), ExactPosition(3130730), strand=1), type='CDS', id='phage replication protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3130716), ExactPosition(3131514), strand=1), type='CDS', id='hypothetical protein CDS_288'),
     SeqFeature(FeatureLocation(ExactPosition(3112621), ExactPosition(3113398), strand=1), type='CDS', id='hypothetical protein CDS_289'),
     SeqFeature(FeatureLocation(ExactPosition(3127925), ExactPosition(3128570), strand=-1), type='CDS', id='LexA family transcriptional regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3094680), ExactPosition(3095319), strand=-1), type='CDS', id='TetR family transcriptional regulator CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3122713), ExactPosition(3123346), strand=1), type='CDS', id='hypothetical protein CDS_294'),
     SeqFeature(FeatureLocation(ExactPosition(3111525), ExactPosition(3112131), strand=-1), type='CDS', id='hypothetical protein CDS_297'),
     SeqFeature(FeatureLocation(ExactPosition(3154989), ExactPosition(3155583), strand=1), type='CDS', id='tail assembly protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3125338), ExactPosition(3125815), strand=-1), type='CDS', id='DUF4411 family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3122054), ExactPosition(3122492), strand=-1), type='CDS', id='hypothetical protein CDS_305'),
     SeqFeature(FeatureLocation(ExactPosition(3127296), ExactPosition(3127605), strand=-1), type='CDS', id='hypothetical protein CDS_317'),
     SeqFeature(FeatureLocation(ExactPosition(3127055), ExactPosition(3127274), strand=-1), type='CDS', id='hypothetical protein CDS_325'),
     SeqFeature(FeatureLocation(ExactPosition(3154221), ExactPosition(3154422), strand=1), type='CDS', id='hypothetical protein CDS_328'),
     SeqFeature(FeatureLocation(ExactPosition(3134064), ExactPosition(3134256), strand=1), type='CDS', id='Com family DNA-binding transcriptional regulator CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3181035), ExactPosition(3182340), strand=-1), type='CDS', id='efeB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3207007), ExactPosition(3208192), strand=-1), type='CDS', id='sensor histidine kinase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(3187208), ExactPosition(3188363), strand=-1), type='CDS', id='MFS transporter CDS_42'),
     SeqFeature(FeatureLocation(ExactPosition(3202561), ExactPosition(3203530), strand=1), type='CDS', id='ABC transporter substrate-binding protein CDS_27'),
     SeqFeature(FeatureLocation(ExactPosition(3162920), ExactPosition(3163601), strand=-1), type='CDS', id='SOS response-associated peptidase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3221918), ExactPosition(3222425), strand=1), type='CDS', id='hypothetical protein CDS_333'),
     SeqFeature(FeatureLocation(ExactPosition(3162266), ExactPosition(3162767), strand=1), type='CDS', id='lysis system i-spanin subunit Rz CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3161841), ExactPosition(3162267), strand=1), type='CDS', id='cell wall hydrolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3225066), ExactPosition(3225348), strand=-1), type='CDS', id='hypothetical protein CDS_337'),
     SeqFeature(FeatureLocation(ExactPosition(3166272), ExactPosition(3166482), strand=-1), type='CDS', id='hypothetical protein CDS_340'),
     SeqFeature(FeatureLocation(ExactPosition(3222612), ExactPosition(3222810), strand=1), type='CDS', id='hypothetical protein CDS_341'),
     SeqFeature(FeatureLocation(ExactPosition(3222925), ExactPosition(3223105), strand=1), type='CDS', id='hypothetical protein CDS_342'),
     SeqFeature(FeatureLocation(ExactPosition(3193390), ExactPosition(3193531), strand=-1), type='CDS', id='hypothetical protein CDS_343'),
     SeqFeature(FeatureLocation(ExactPosition(3292192), ExactPosition(3296470), strand=-1), type='CDS', id='hypothetical protein CDS_344'),
     SeqFeature(FeatureLocation(ExactPosition(3285514), ExactPosition(3289012), strand=1), type='CDS', id='response regulator CDS_13'),
     SeqFeature(FeatureLocation(ExactPosition(3228884), ExactPosition(3231599), strand=-1), type='CDS', id='mgtA CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3261537), ExactPosition(3263079), strand=-1), type='CDS', id='EAL domain-containing protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(3241460), ExactPosition(3242540), strand=-1), type='CDS', id='modC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3246899), ExactPosition(3247946), strand=-1), type='CDS', id='ada CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3264122), ExactPosition(3265046), strand=1), type='CDS', id='DMT family transporter CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3266426), ExactPosition(3267308), strand=-1), type='CDS', id='rarD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3289027), ExactPosition(3289843), strand=1), type='CDS', id='protein-glutamate O-methyltransferase CheR CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3282415), ExactPosition(3283087), strand=1), type='CDS', id='DUF3313 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3249925), ExactPosition(3250573), strand=1), type='CDS', id='c-type cytochrome CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3281777), ExactPosition(3282272), strand=1), type='CDS', id='hypothetical protein CDS_346'),
     SeqFeature(FeatureLocation(ExactPosition(3237326), ExactPosition(3237680), strand=1), type='CDS', id='hypothetical protein CDS_347'),
     SeqFeature(FeatureLocation(ExactPosition(3228471), ExactPosition(3228819), strand=-1), type='CDS', id='SLOG family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3235861), ExactPosition(3236077), strand=1), type='CDS', id='hypothetical protein CDS_351'),
     SeqFeature(FeatureLocation(ExactPosition(3337507), ExactPosition(3339181), strand=-1), type='CDS', id='methyl-accepting chemotaxis protein CDS_28'),
     SeqFeature(FeatureLocation(ExactPosition(3328203), ExactPosition(3329007), strand=-1), type='CDS', id='hypothetical protein CDS_357'),
     SeqFeature(FeatureLocation(ExactPosition(3308578), ExactPosition(3309289), strand=-1), type='CDS', id='HPP family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3334730), ExactPosition(3335324), strand=-1), type='CDS', id='ATP-binding protein CDS_21'),
     SeqFeature(FeatureLocation(ExactPosition(3334066), ExactPosition(3334636), strand=-1), type='CDS', id='hypothetical protein CDS_358'),
     SeqFeature(FeatureLocation(ExactPosition(3329883), ExactPosition(3330171), strand=1), type='CDS', id='hypothetical protein CDS_360'),
     SeqFeature(FeatureLocation(ExactPosition(3336834), ExactPosition(3337116), strand=-1), type='CDS', id='MGMT family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3309463), ExactPosition(3309607), strand=-1), type='CDS', id='hypothetical protein CDS_362'),
     SeqFeature(FeatureLocation(ExactPosition(3372525), ExactPosition(3373974), strand=-1), type='CDS', id='APC family permease CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3380753), ExactPosition(3382130), strand=1), type='CDS', id='aspartate aminotransferase family protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3418299), ExactPosition(3419649), strand=1), type='CDS', id='ATP-binding protein CDS_23'),
     SeqFeature(FeatureLocation(ExactPosition(3363749), ExactPosition(3364790), strand=-1), type='CDS', id='glycosyl hydrolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3426451), ExactPosition(3427393), strand=-1), type='CDS', id='alpha/beta hydrolase CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(3368905), ExactPosition(3369664), strand=1), type='CDS', id='IclR family transcriptional regulator CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(3384610), ExactPosition(3385315), strand=-1), type='CDS', id='hypothetical protein CDS_364'),
     SeqFeature(FeatureLocation(ExactPosition(3397396), ExactPosition(3398026), strand=-1), type='CDS', id='PAS domain-containing protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(3362666), ExactPosition(3363269), strand=-1), type='CDS', id='amino acid synthesis family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3396760), ExactPosition(3397345), strand=-1), type='CDS', id='methyl-accepting chemotaxis protein CDS_31'),
     SeqFeature(FeatureLocation(ExactPosition(3410890), ExactPosition(3411433), strand=-1), type='CDS', id='cytochrome b CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3495725), ExactPosition(3498410), strand=-1), type='CDS', id='aceE CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3475664), ExactPosition(3477329), strand=-1), type='CDS', id='ggt CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3433355), ExactPosition(3434684), strand=-1), type='CDS', id='TolC family outer membrane protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3480314), ExactPosition(3481052), strand=-1), type='CDS', id='hypothetical protein CDS_369'),
     SeqFeature(FeatureLocation(ExactPosition(3460565), ExactPosition(3461204), strand=1), type='CDS', id='VTT domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3466577), ExactPosition(3467210), strand=1), type='CDS', id='TetR/AcrR family transcriptional regulator CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(3470071), ExactPosition(3470653), strand=-1), type='CDS', id='TetR/AcrR family transcriptional regulator CDS_12'),
     SeqFeature(FeatureLocation(ExactPosition(3471755), ExactPosition(3472193), strand=-1), type='CDS', id='nuclear transport factor 2 family protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3437851), ExactPosition(3438220), strand=-1), type='CDS', id='protease inhibitor Inh/omp19 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3466132), ExactPosition(3466468), strand=1), type='CDS', id='LysR family transcriptional regulator CDS_54'),
     SeqFeature(FeatureLocation(ExactPosition(3459998), ExactPosition(3460265), strand=-1), type='CDS', id='hypothetical protein CDS_370'),
     SeqFeature(FeatureLocation(ExactPosition(3479539), ExactPosition(3479749), strand=-1), type='CDS', id='hypothetical protein CDS_371'),
     SeqFeature(FeatureLocation(ExactPosition(3550502), ExactPosition(3560567), strand=1), type='CDS', id='non-ribosomal peptide synthetase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3517595), ExactPosition(3519236), strand=1), type='CDS', id='AMP-binding protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3524173), ExactPosition(3525589), strand=-1), type='CDS', id='diaminobutyrate--2-oxoglutarate transaminase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3503871), ExactPosition(3505089), strand=-1), type='CDS', id='beta-ketoacyl-[acyl-carrier-protein] synthase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3532398), ExactPosition(3533445), strand=1), type='CDS', id='TauD/TfdA family dioxygenase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3528023), ExactPosition(3529001), strand=1), type='CDS', id='TauD/TfdA family dioxygenase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3506450), ExactPosition(3507344), strand=-1), type='CDS', id='DMT family transporter CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(3511001), ExactPosition(3511868), strand=1), type='CDS', id='transporter CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3498479), ExactPosition(3499238), strand=-1), type='CDS', id='fabG CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3516608), ExactPosition(3517184), strand=1), type='CDS', id='PAS domain-containing protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3519247), ExactPosition(3519823), strand=1), type='CDS', id='TetR/AcrR family transcriptional regulator CDS_13'),
     SeqFeature(FeatureLocation(ExactPosition(3505088), ExactPosition(3505319), strand=-1), type='CDS', id='acyl carrier protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3517292), ExactPosition(3517502), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_32'),
     SeqFeature(FeatureLocation(ExactPosition(3509655), ExactPosition(3509817), strand=1), type='CDS', id='hypothetical protein CDS_372'),
     SeqFeature(FeatureLocation(ExactPosition(3608214), ExactPosition(3610023), strand=-1), type='CDS', id='ABC transporter ATP-binding protein/permease CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3599613), ExactPosition(3601101), strand=1), type='CDS', id='hxsB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3576262), ExactPosition(3577435), strand=-1), type='CDS', id='gspL CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3629915), ExactPosition(3630737), strand=1), type='CDS', id='molybdenum cofactor biosynthesis F family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3565932), ExactPosition(3566607), strand=-1), type='CDS', id='fhuF CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3575724), ExactPosition(3576270), strand=-1), type='CDS', id='type II secretion system protein M CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3617399), ExactPosition(3617822), strand=-1), type='CDS', id='hpaR CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3568591), ExactPosition(3568879), strand=-1), type='CDS', id='hypothetical protein CDS_377'),
     SeqFeature(FeatureLocation(ExactPosition(3610484), ExactPosition(3610598), strand=1), type='CDS', id='hypothetical protein CDS_380'),
     SeqFeature(FeatureLocation(ExactPosition(3650179), ExactPosition(3652054), strand=-1), type='CDS', id='feruloyl-CoA synthase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3647162), ExactPosition(3648905), strand=-1), type='CDS', id='acyl-CoA dehydrogenase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(3665438), ExactPosition(3666776), strand=-1), type='CDS', id='hypothetical protein CDS_381'),
     SeqFeature(FeatureLocation(ExactPosition(3666891), ExactPosition(3668154), strand=-1), type='CDS', id='serine hydrolase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3643093), ExactPosition(3644089), strand=-1), type='CDS', id='NAD-dependent epimerase/dehydratase family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3685790), ExactPosition(3686771), strand=-1), type='CDS', id='GlxA family transcriptional regulator CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3695562), ExactPosition(3696375), strand=1), type='CDS', id='oxidoreductase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3631522), ExactPosition(3632293), strand=1), type='CDS', id='SDR family oxidoreductase CDS_27'),
     SeqFeature(FeatureLocation(ExactPosition(3670381), ExactPosition(3671143), strand=-1), type='CDS', id='pstB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3669651), ExactPosition(3670350), strand=-1), type='CDS', id='hypothetical protein CDS_383'),
     SeqFeature(FeatureLocation(ExactPosition(3696916), ExactPosition(3697585), strand=-1), type='CDS', id='N-acetyltransferase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3697634), ExactPosition(3698282), strand=-1), type='CDS', id='hypothetical protein CDS_384'),
     SeqFeature(FeatureLocation(ExactPosition(3687112), ExactPosition(3687721), strand=-1), type='CDS', id='LysE family translocator CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3675453), ExactPosition(3675903), strand=1), type='CDS', id='hypothetical protein CDS_386'),
     SeqFeature(FeatureLocation(ExactPosition(3642723), ExactPosition(3643170), strand=1), type='CDS', id='MerR family transcriptional regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3638507), ExactPosition(3638579), strand=-1), type='CDS', id='methyl-accepting chemotaxis protein CDS_33'),
     SeqFeature(FeatureLocation(ExactPosition(3749712), ExactPosition(3751365), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_35'),
     SeqFeature(FeatureLocation(ExactPosition(3706275), ExactPosition(3707901), strand=-1), type='CDS', id='methyl-accepting chemotaxis protein CDS_36'),
     SeqFeature(FeatureLocation(ExactPosition(3720522), ExactPosition(3722076), strand=1), type='CDS', id='glgA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3735294), ExactPosition(3736722), strand=-1), type='CDS', id='amino acid permease CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3731246), ExactPosition(3732245), strand=1), type='CDS', id='acyl-CoA/acyl-ACP dehydrogenase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3760099), ExactPosition(3760987), strand=1), type='CDS', id='serine hydrolase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3744802), ExactPosition(3745663), strand=1), type='CDS', id='DMT family transporter CDS_9'),
     SeqFeature(FeatureLocation(ExactPosition(3703001), ExactPosition(3703850), strand=1), type='CDS', id='tyrosine-type recombinase/integrase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3734429), ExactPosition(3735191), strand=1), type='CDS', id='PIG-L family deacetylase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3704666), ExactPosition(3705302), strand=-1), type='CDS', id='sigma-70 family RNA polymerase sigma factor CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(3757392), ExactPosition(3758004), strand=-1), type='CDS', id='hypothetical protein CDS_391'),
     SeqFeature(FeatureLocation(ExactPosition(3701869), ExactPosition(3702460), strand=-1), type='CDS', id='hypothetical protein CDS_392'),
     SeqFeature(FeatureLocation(ExactPosition(3759527), ExactPosition(3759974), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_21'),
     SeqFeature(FeatureLocation(ExactPosition(3748670), ExactPosition(3749096), strand=1), type='CDS', id='hypothetical protein CDS_395'),
     SeqFeature(FeatureLocation(ExactPosition(3758720), ExactPosition(3759140), strand=1), type='CDS', id='hypothetical protein CDS_397'),
     SeqFeature(FeatureLocation(ExactPosition(3701092), ExactPosition(3701503), strand=-1), type='CDS', id='DUF4113 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3701499), ExactPosition(3701868), strand=1), type='CDS', id='SOS response-associated peptidase family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(3751762), ExactPosition(3752128), strand=1), type='CDS', id='hypothetical protein CDS_400'),
     SeqFeature(FeatureLocation(ExactPosition(3702526), ExactPosition(3702853), strand=-1), type='CDS', id='DNA adenine methylase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3748305), ExactPosition(3748593), strand=1), type='CDS', id='hypothetical protein CDS_401'),
     SeqFeature(FeatureLocation(ExactPosition(3728693), ExactPosition(3728966), strand=1), type='CDS', id='DUF2934 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3759176), ExactPosition(3759392), strand=1), type='CDS', id='hypothetical protein CDS_403'),
     SeqFeature(FeatureLocation(ExactPosition(3708180), ExactPosition(3708336), strand=-1), type='CDS', id='transposase CDS_8'),
     SeqFeature(FeatureLocation(ExactPosition(3789926), ExactPosition(3793910), strand=1), type='CDS', id='molybdopterin-dependent oxidoreductase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3786799), ExactPosition(3789340), strand=-1), type='CDS', id='nirB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3811062), ExactPosition(3812223), strand=1), type='CDS', id='MFS transporter CDS_51'),
     SeqFeature(FeatureLocation(ExactPosition(3808912), ExactPosition(3809836), strand=-1), type='CDS', id='AraC family transcriptional regulator CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(3815172), ExactPosition(3815994), strand=-1), type='CDS', id='helix-turn-helix transcriptional regulator CDS_12'),
     SeqFeature(FeatureLocation(ExactPosition(3797214), ExactPosition(3797991), strand=-1), type='CDS', id='nucleotidyltransferase domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3829929), ExactPosition(3830484), strand=-1), type='CDS', id='YceI family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3821183), ExactPosition(3821489), strand=-1), type='CDS', id='DUF2834 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3778677), ExactPosition(3778866), strand=1), type='CDS', id='hypothetical protein CDS_409'),
     SeqFeature(FeatureLocation(ExactPosition(3845367), ExactPosition(3848244), strand=-1), type='CDS', id='hypothetical protein CDS_411'),
     SeqFeature(FeatureLocation(ExactPosition(3870782), ExactPosition(3872108), strand=1), type='CDS', id='C4-dicarboxylate transporter DctA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3832108), ExactPosition(3833293), strand=-1), type='CDS', id='DUF2252 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3881912), ExactPosition(3882845), strand=1), type='CDS', id='DUF58 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3857394), ExactPosition(3857865), strand=-1), type='CDS', id='DUF1289 domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3895456), ExactPosition(3895744), strand=-1), type='CDS', id='DUF5629 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3950499), ExactPosition(3952932), strand=-1), type='CDS', id='TonB-dependent receptor CDS_15'),
     SeqFeature(FeatureLocation(ExactPosition(3925624), ExactPosition(3927700), strand=-1), type='CDS', id='EAL domain-containing protein CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(3944162), ExactPosition(3945746), strand=-1), type='CDS', id='L,D-transpeptidase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3922353), ExactPosition(3923892), strand=1), type='CDS', id='NAD(P)/FAD-dependent oxidoreductase CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(3910001), ExactPosition(3911345), strand=-1), type='CDS', id='3-oxoacyl-ACP reductase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(3960202), ExactPosition(3961522), strand=-1), type='CDS', id='OprD family porin CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(3954599), ExactPosition(3955754), strand=1), type='CDS', id='DUF1624 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3904568), ExactPosition(3905684), strand=1), type='CDS', id='Gfo/Idh/MocA family oxidoreductase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(3903221), ExactPosition(3904262), strand=-1), type='CDS', id='YCF48-related protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3965010), ExactPosition(3966015), strand=-1), type='CDS', id='ABC transporter substrate-binding protein CDS_30'),
     SeqFeature(FeatureLocation(ExactPosition(3921088), ExactPosition(3922087), strand=-1), type='CDS', id='alpha/beta hydrolase CDS_20'),
     SeqFeature(FeatureLocation(ExactPosition(3932143), ExactPosition(3933001), strand=-1), type='CDS', id='icmH CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3929129), ExactPosition(3929873), strand=-1), type='CDS', id='serine/threonine-protein phosphatase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3953972), ExactPosition(3954470), strand=-1), type='CDS', id='sigma-70 family RNA polymerase sigma factor CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(3933000), ExactPosition(3933360), strand=-1), type='CDS', id='tssE CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3905816), ExactPosition(3905948), strand=1), type='CDS', id='gfo/Idh/MocA family oxidoreductase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(3980431), ExactPosition(3983071), strand=-1), type='CDS', id='glucosidase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4021192), ExactPosition(4023655), strand=1), type='CDS', id='TonB-dependent receptor CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(4026382), ExactPosition(4027267), strand=-1), type='CDS', id='NAD(P)/FAD-dependent oxidoreductase CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(3998436), ExactPosition(3999054), strand=-1), type='CDS', id='lexA CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4004566), ExactPosition(4005154), strand=-1), type='CDS', id='NUDIX domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4019635), ExactPosition(4020148), strand=1), type='CDS', id='sigma-70 family RNA polymerase sigma factor CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(4019022), ExactPosition(4019448), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_27'),
     SeqFeature(FeatureLocation(ExactPosition(4018685), ExactPosition(4018949), strand=1), type='CDS', id='hypothetical protein CDS_429'),
     SeqFeature(FeatureLocation(ExactPosition(3974842), ExactPosition(3975082), strand=1), type='CDS', id='DUF6124 family protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(4089417), ExactPosition(4091283), strand=-1), type='CDS', id='phytase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4050729), ExactPosition(4052121), strand=1), type='CDS', id='undecaprenyl-phosphate glucose phosphotransferase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4046711), ExactPosition(4048016), strand=1), type='CDS', id='HAMP domain-containing histidine kinase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4041203), ExactPosition(4042433), strand=-1), type='CDS', id='M20/M25/M40 family metallo-hydrolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4034497), ExactPosition(4035703), strand=-1), type='CDS', id='multidrug effflux MFS transporter CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4048375), ExactPosition(4049104), strand=1), type='CDS', id='winged helix-turn-helix domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4049823), ExactPosition(4050483), strand=1), type='CDS', id='winged helix-turn-helix domain-containing protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(4097323), ExactPosition(4097947), strand=-1), type='CDS', id='hydrolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4074868), ExactPosition(4075489), strand=-1), type='CDS', id='hypothetical protein CDS_436'),
     SeqFeature(FeatureLocation(ExactPosition(4055140), ExactPosition(4055728), strand=-1), type='CDS', id='YjbF family lipoprotein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4063635), ExactPosition(4064205), strand=-1), type='CDS', id='acetyltransferase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4040095), ExactPosition(4040527), strand=1), type='CDS', id='hypothetical protein CDS_437'),
     SeqFeature(FeatureLocation(ExactPosition(4040538), ExactPosition(4040871), strand=1), type='CDS', id='hypothetical protein CDS_438'),
     SeqFeature(FeatureLocation(ExactPosition(4074279), ExactPosition(4074588), strand=1), type='CDS', id='phosphomannomutase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4067912), ExactPosition(4068158), strand=-1), type='CDS', id='hypothetical protein CDS_440'),
     SeqFeature(FeatureLocation(ExactPosition(4057007), ExactPosition(4057247), strand=-1), type='CDS', id='hypothetical protein CDS_441'),
     SeqFeature(FeatureLocation(ExactPosition(4149346), ExactPosition(4151464), strand=-1), type='CDS', id='TonB-dependent receptor CDS_19'),
     SeqFeature(FeatureLocation(ExactPosition(4125623), ExactPosition(4127135), strand=1), type='CDS', id='sugar ABC transporter ATP-binding protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4135487), ExactPosition(4136738), strand=1), type='CDS', id='MFS transporter CDS_53'),
     SeqFeature(FeatureLocation(ExactPosition(4112807), ExactPosition(4113797), strand=-1), type='CDS', id='ABC transporter permease subunit CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(4107581), ExactPosition(4108532), strand=-1), type='CDS', id='GlxA family transcriptional regulator CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(4117564), ExactPosition(4118509), strand=1), type='CDS', id='aliphatic sulfonate ABC transporter substrate-binding protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4156347), ExactPosition(4157220), strand=1), type='CDS', id='sensor domain-containing diguanylate cyclase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(4134519), ExactPosition(4135308), strand=1), type='CDS', id='helix-turn-helix transcriptional regulator CDS_14'),
     SeqFeature(FeatureLocation(ExactPosition(4153767), ExactPosition(4154517), strand=-1), type='CDS', id='hypothetical protein CDS_443'),
     SeqFeature(FeatureLocation(ExactPosition(4130867), ExactPosition(4131581), strand=-1), type='CDS', id='aqpZ CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4152527), ExactPosition(4153049), strand=-1), type='CDS', id='hypothetical protein CDS_445'),
     SeqFeature(FeatureLocation(ExactPosition(4157533), ExactPosition(4158031), strand=-1), type='CDS', id='hypothetical protein CDS_446'),
     SeqFeature(FeatureLocation(ExactPosition(4155805), ExactPosition(4156237), strand=1), type='CDS', id='hypothetical protein CDS_448'),
     SeqFeature(FeatureLocation(ExactPosition(4109580), ExactPosition(4109967), strand=1), type='CDS', id='hypothetical protein CDS_450'),
     SeqFeature(FeatureLocation(ExactPosition(4102797), ExactPosition(4103196), strand=1), type='CDS', id='AraC family transcriptional regulator CDS_15'),
     SeqFeature(FeatureLocation(ExactPosition(4160516), ExactPosition(4160846), strand=-1), type='CDS', id='hypothetical protein CDS_452'),
     SeqFeature(FeatureLocation(ExactPosition(4154998), ExactPosition(4155316), strand=-1), type='CDS', id='hypothetical protein CDS_453'),
     SeqFeature(FeatureLocation(ExactPosition(4140341), ExactPosition(4140614), strand=-1), type='CDS', id='DUF2160 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4232184), ExactPosition(4232706), strand=1), type='CDS', id='DUF3087 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4209839), ExactPosition(4210262), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_28'),
     SeqFeature(FeatureLocation(ExactPosition(4228792), ExactPosition(4229143), strand=1), type='CDS', id='toxin CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4200291), ExactPosition(4200504), strand=1), type='CDS', id='cspD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4231894), ExactPosition(4232071), strand=-1), type='CDS', id='hypothetical protein CDS_458'),
     SeqFeature(FeatureLocation(ExactPosition(4171975), ExactPosition(4172148), strand=-1), type='CDS', id='helix-turn-helix domain-containing protein CDS_20'),
     SeqFeature(FeatureLocation(ExactPosition(4275191), ExactPosition(4278863), strand=-1), type='CDS', id='translocation/assembly module TamB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4258462), ExactPosition(4261383), strand=1), type='CDS', id='monovalent cation/H+ antiporter subunit A CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4261723), ExactPosition(4263406), strand=1), type='CDS', id='monovalent cation/H+ antiporter subunit D CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4253571), ExactPosition(4255005), strand=-1), type='CDS', id='LLM class flavin-dependent oxidoreductase CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(4255344), ExactPosition(4256541), strand=-1), type='CDS', id='SfnB family sulfur acquisition oxidoreductase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4290110), ExactPosition(4291274), strand=1), type='CDS', id='isovaleryl-CoA dehydrogenase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4297215), ExactPosition(4298367), strand=1), type='CDS', id='M14-type cytosolic carboxypeptidase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4301519), ExactPosition(4302482), strand=-1), type='CDS', id='spore coat U domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4241651), ExactPosition(4242356), strand=1), type='CDS', id='ABC transporter ATP-binding protein CDS_35'),
     SeqFeature(FeatureLocation(ExactPosition(4264525), ExactPosition(4265092), strand=1), type='CDS', id='dihydrofolate reductase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4256696), ExactPosition(4257260), strand=-1), type='CDS', id='XRE family transcriptional regulator CDS_8'),
     SeqFeature(FeatureLocation(ExactPosition(4300282), ExactPosition(4300789), strand=1), type='CDS', id='sigma-70 family RNA polymerase sigma factor CDS_8'),
     SeqFeature(FeatureLocation(ExactPosition(4268976), ExactPosition(4269480), strand=1), type='CDS', id='peptidyl-prolyl cis-trans isomerase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4236038), ExactPosition(4236437), strand=-1), type='CDS', id='hypothetical protein CDS_459'),
     SeqFeature(FeatureLocation(ExactPosition(4264162), ExactPosition(4264501), strand=1), type='CDS', id='Na+/H+ antiporter subunit G CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4235652), ExactPosition(4235805), strand=-1), type='CDS', id='DUF2282 domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4302487), ExactPosition(4304845), strand=-1), type='CDS', id='fimbria/pilus outer membrane usher protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4309165), ExactPosition(4310458), strand=1), type='CDS', id='protein kinase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4355978), ExactPosition(4357256), strand=1), type='CDS', id='preA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4321488), ExactPosition(4322493), strand=1), type='CDS', id='hypothetical protein CDS_460'),
     SeqFeature(FeatureLocation(ExactPosition(4365611), ExactPosition(4366505), strand=1), type='CDS', id='aldose 1-epimerase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4357376), ExactPosition(4357997), strand=-1), type='CDS', id='TetR family transcriptional regulator C-terminaldomain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4306221), ExactPosition(4306764), strand=-1), type='CDS', id='spore coat protein U domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4329027), ExactPosition(4329552), strand=-1), type='CDS', id='GNAT family N-acetyltransferase CDS_30'),
     SeqFeature(FeatureLocation(ExactPosition(4307318), ExactPosition(4307807), strand=-1), type='CDS', id='spore coat U domain-containing protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(4342263), ExactPosition(4342425), strand=1), type='CDS', id='hypothetical protein CDS_462'),
     SeqFeature(FeatureLocation(ExactPosition(4428719), ExactPosition(4434941), strand=-1), type='CDS', id='non-ribosomal peptide synthetase CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(4369561), ExactPosition(4371040), strand=-1), type='CDS', id='DASS family sodium-coupled anion symporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4436221), ExactPosition(4437679), strand=1), type='CDS', id='adeC CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4394747), ExactPosition(4396130), strand=1), type='CDS', id='efflux transporter outer membrane subunit CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(4419093), ExactPosition(4420158), strand=-1), type='CDS', id='iron ABC transporter permease CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4378082), ExactPosition(4379141), strand=1), type='CDS', id='alpha-ketoacid dehydrogenase subunit beta CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4386051), ExactPosition(4386984), strand=-1), type='CDS', id='IS110 family transposase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4387234), ExactPosition(4388068), strand=-1), type='CDS', id='TIGR03915 family putative DNA repair protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4423554), ExactPosition(4424229), strand=1), type='CDS', id='histidine phosphatase family protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(4385412), ExactPosition(4385910), strand=1), type='CDS', id='Lrp/AsnC family transcriptional regulator CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(4390852), ExactPosition(4391332), strand=-1), type='CDS', id='sigma-70 family RNA polymerase sigma factor CDS_9'),
     SeqFeature(FeatureLocation(ExactPosition(4498613), ExactPosition(4501007), strand=-1), type='CDS', id='TonB-dependent receptor CDS_20'),
     SeqFeature(FeatureLocation(ExactPosition(4459377), ExactPosition(4460751), strand=1), type='CDS', id='LLM class flavin-dependent oxidoreductase CDS_12'),
     SeqFeature(FeatureLocation(ExactPosition(4462101), ExactPosition(4463394), strand=-1), type='CDS', id='gabT CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4484317), ExactPosition(4485502), strand=1), type='CDS', id='FAD-binding oxidoreductase CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(4497631), ExactPosition(4498597), strand=-1), type='CDS', id='amino acid ABC transporter permease CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(4456770), ExactPosition(4457592), strand=1), type='CDS', id='acid phosphatase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4457695), ExactPosition(4458514), strand=1), type='CDS', id='transporter substrate-binding domain-containing protein CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(4443055), ExactPosition(4443784), strand=-1), type='CDS', id='amino acid ABC transporter ATP-binding protein CDS_8'),
     SeqFeature(FeatureLocation(ExactPosition(4492733), ExactPosition(4493126), strand=-1), type='CDS', id='DUF4398 domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4512951), ExactPosition(4515702), strand=1), type='CDS', id='glycoside hydrolase family 3 C-terminal domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4566165), ExactPosition(4567791), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_41'),
     SeqFeature(FeatureLocation(ExactPosition(4554171), ExactPosition(4555704), strand=1), type='CDS', id='sensor domain-containing diguanylate cyclase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(4542323), ExactPosition(4543505), strand=1), type='CDS', id='MFS transporter CDS_59'),
     SeqFeature(FeatureLocation(ExactPosition(4552865), ExactPosition(4553906), strand=1), type='CDS', id='zinc-dependent alcohol dehydrogenase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4541315), ExactPosition(4542308), strand=1), type='CDS', id='transporter CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(4524037), ExactPosition(4525006), strand=-1), type='CDS', id='fecC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4548579), ExactPosition(4549428), strand=1), type='CDS', id='VOC family protein CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(4569929), ExactPosition(4570682), strand=1), type='CDS', id='type II toxin-antitoxin system MqsA family antitoxin CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4537816), ExactPosition(4538500), strand=-1), type='CDS', id='response regulator transcription factor CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(4555803), ExactPosition(4556439), strand=-1), type='CDS', id='VacJ family lipoprotein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4570659), ExactPosition(4571151), strand=1), type='CDS', id='hypothetical protein CDS_470'),
     SeqFeature(FeatureLocation(ExactPosition(4536038), ExactPosition(4536479), strand=-1), type='CDS', id='hypothetical protein CDS_471'),
     SeqFeature(FeatureLocation(ExactPosition(4569492), ExactPosition(4569933), strand=1), type='CDS', id='hypothetical protein CDS_472'),
     SeqFeature(FeatureLocation(ExactPosition(4512219), ExactPosition(4512444), strand=-1), type='CDS', id='hypothetical protein CDS_473'),
     SeqFeature(FeatureLocation(ExactPosition(4565302), ExactPosition(4565506), strand=-1), type='CDS', id='hypothetical protein CDS_474'),
     SeqFeature(FeatureLocation(ExactPosition(4637017), ExactPosition(4640314), strand=1), type='CDS', id='EAL domain-containing protein CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(4579358), ExactPosition(4580861), strand=-1), type='CDS', id='hypothetical protein CDS_475'),
     SeqFeature(FeatureLocation(ExactPosition(4613424), ExactPosition(4614765), strand=1), type='CDS', id='MFS transporter CDS_60'),
     SeqFeature(FeatureLocation(ExactPosition(4629375), ExactPosition(4630386), strand=-1), type='CDS', id='aspartate-semialdehyde dehydrogenase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4625545), ExactPosition(4626370), strand=-1), type='CDS', id='truA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4606328), ExactPosition(4607111), strand=1), type='CDS', id='alpha/beta hydrolase CDS_24'),
     SeqFeature(FeatureLocation(ExactPosition(4617318), ExactPosition(4618083), strand=-1), type='CDS', id='SDR family oxidoreductase CDS_36'),
     SeqFeature(FeatureLocation(ExactPosition(4605444), ExactPosition(4606122), strand=-1), type='CDS', id='HAD family hydrolase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4581462), ExactPosition(4582134), strand=-1), type='CDS', id='baseplate J/gp47 family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4621733), ExactPosition(4622396), strand=-1), type='CDS', id='SPOR domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4582349), ExactPosition(4582841), strand=-1), type='CDS', id='terminase small subunit CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4603751), ExactPosition(4604117), strand=-1), type='CDS', id='hypothetical protein CDS_476'),
     SeqFeature(FeatureLocation(ExactPosition(4571865), ExactPosition(4572222), strand=1), type='CDS', id='hypothetical protein CDS_477'),
     SeqFeature(FeatureLocation(ExactPosition(4575295), ExactPosition(4575595), strand=-1), type='CDS', id='hypothetical protein CDS_482'),
     SeqFeature(FeatureLocation(ExactPosition(4579011), ExactPosition(4579290), strand=-1), type='CDS', id='tail fiber assembly protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4582073), ExactPosition(4582349), strand=-1), type='CDS', id='hypothetical protein CDS_484'),
     SeqFeature(FeatureLocation(ExactPosition(4572974), ExactPosition(4573244), strand=1), type='CDS', id='superinfection immunity protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4574342), ExactPosition(4574603), strand=1), type='CDS', id='hypothetical protein CDS_485'),
     SeqFeature(FeatureLocation(ExactPosition(4574026), ExactPosition(4574284), strand=1), type='CDS', id='hypothetical protein CDS_486'),
     SeqFeature(FeatureLocation(ExactPosition(4583519), ExactPosition(4583765), strand=-1), type='CDS', id='hypothetical protein CDS_488'),
     SeqFeature(FeatureLocation(ExactPosition(4607225), ExactPosition(4607471), strand=1), type='CDS', id='DUF2790 domain-containing protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(4571528), ExactPosition(4571771), strand=1), type='CDS', id='hypothetical protein CDS_489'),
     SeqFeature(FeatureLocation(ExactPosition(4573787), ExactPosition(4574006), strand=1), type='CDS', id='hypothetical protein CDS_491'),
     SeqFeature(FeatureLocation(ExactPosition(4583763), ExactPosition(4583973), strand=1), type='CDS', id='integrase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4604379), ExactPosition(4604574), strand=1), type='CDS', id='csrA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4614795), ExactPosition(4614990), strand=-1), type='CDS', id='hypothetical protein CDS_493'),
     SeqFeature(FeatureLocation(ExactPosition(4572202), ExactPosition(4572325), strand=-1), type='CDS', id='lysis protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4643344), ExactPosition(4644922), strand=-1), type='CDS', id='hypothetical protein CDS_494'),
     SeqFeature(FeatureLocation(ExactPosition(4698406), ExactPosition(4699528), strand=1), type='CDS', id='lactonase family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4667972), ExactPosition(4668899), strand=-1), type='CDS', id='alpha/beta hydrolase fold domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4679133), ExactPosition(4680030), strand=-1), type='CDS', id='AraC family transcriptional regulator CDS_17'),
     SeqFeature(FeatureLocation(ExactPosition(4654986), ExactPosition(4655859), strand=-1), type='CDS', id='LysR family transcriptional regulator CDS_68'),
     SeqFeature(FeatureLocation(ExactPosition(4646993), ExactPosition(4647827), strand=-1), type='CDS', id='alpha/beta fold hydrolase CDS_8'),
     SeqFeature(FeatureLocation(ExactPosition(4642314), ExactPosition(4643082), strand=-1), type='CDS', id='hypothetical protein CDS_498'),
     SeqFeature(FeatureLocation(ExactPosition(4680168), ExactPosition(4680912), strand=1), type='CDS', id='SDR family oxidoreductase CDS_37'),
     SeqFeature(FeatureLocation(ExactPosition(4669404), ExactPosition(4669965), strand=-1), type='CDS', id='nucleotidyltransferase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4647841), ExactPosition(4648381), strand=-1), type='CDS', id='TetR/AcrR family transcriptional regulator CDS_19'),
     SeqFeature(FeatureLocation(ExactPosition(4687325), ExactPosition(4687862), strand=1), type='CDS', id='hypothetical protein CDS_501'),
     SeqFeature(FeatureLocation(ExactPosition(4676122), ExactPosition(4676611), strand=1), type='CDS', id='heme-binding protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4667349), ExactPosition(4667784), strand=1), type='CDS', id='hypothetical protein CDS_503'),
     SeqFeature(FeatureLocation(ExactPosition(4673828), ExactPosition(4674068), strand=1), type='CDS', id='DUF6124 family protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(4666911), ExactPosition(4667136), strand=-1), type='CDS', id='hypothetical protein CDS_510'),
     SeqFeature(FeatureLocation(ExactPosition(4654759), ExactPosition(4654969), strand=1), type='CDS', id='hypothetical protein CDS_511'),
     SeqFeature(FeatureLocation(ExactPosition(4674935), ExactPosition(4675028), strand=1), type='CDS', id='SDR family NAD(P)-dependent oxidoreductase CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(4765062), ExactPosition(4769880), strand=-1), type='CDS', id='hypothetical protein CDS_512'),
     SeqFeature(FeatureLocation(ExactPosition(4742367), ExactPosition(4744779), strand=-1), type='CDS', id='DUF637 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4757295), ExactPosition(4758678), strand=1), type='CDS', id='class II fumarate hydratase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4746910), ExactPosition(4748053), strand=1), type='CDS', id='lldD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4706973), ExactPosition(4707900), strand=-1), type='CDS', id='ABC transporter substrate-binding protein CDS_43'),
     SeqFeature(FeatureLocation(ExactPosition(4748355), ExactPosition(4749282), strand=1), type='CDS', id='GGDEF domain-containing protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(4744942), ExactPosition(4745656), strand=-1), type='CDS', id='methyl-accepting chemotaxis protein CDS_44'),
     SeqFeature(FeatureLocation(ExactPosition(4739850), ExactPosition(4740393), strand=-1), type='CDS', id='hypothetical protein CDS_513'),
     SeqFeature(FeatureLocation(ExactPosition(4722414), ExactPosition(4722921), strand=-1), type='CDS', id='msrA CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4741912), ExactPosition(4742371), strand=-1), type='CDS', id='DUF4279 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4741075), ExactPosition(4741369), strand=1), type='CDS', id='IS66 family transposase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(4706490), ExactPosition(4706764), strand=-1), type='CDS', id='putative addiction module antidote protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4744673), ExactPosition(4744916), strand=-1), type='CDS', id='hypothetical protein CDS_515'),
     SeqFeature(FeatureLocation(ExactPosition(4734031), ExactPosition(4734226), strand=-1), type='CDS', id='hypothetical protein CDS_516'),
     SeqFeature(FeatureLocation(ExactPosition(4734222), ExactPosition(4734417), strand=-1), type='CDS', id='hypothetical protein CDS_517'),
     SeqFeature(FeatureLocation(ExactPosition(4724216), ExactPosition(4724378), strand=1), type='CDS', id='DUF2986 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4740911), ExactPosition(4741040), strand=-1), type='CDS', id='hypothetical protein CDS_518'),
     SeqFeature(FeatureLocation(ExactPosition(4739719), ExactPosition(4739818), strand=1), type='CDS', id='LysR family transcriptional regulator CDS_73'),
     SeqFeature(FeatureLocation(ExactPosition(4802580), ExactPosition(4807716), strand=-1), type='CDS', id='hypothetical protein CDS_519'),
     SeqFeature(FeatureLocation(ExactPosition(4837897), ExactPosition(4840639), strand=-1), type='CDS', id='hypothetical protein CDS_520'),
     SeqFeature(FeatureLocation(ExactPosition(4792772), ExactPosition(4793897), strand=-1), type='CDS', id='integrase core domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(4814790), ExactPosition(4815159), strand=1), type='CDS', id='hypothetical protein CDS_524'),
     SeqFeature(FeatureLocation(ExactPosition(4895947), ExactPosition(4897666), strand=-1), type='CDS', id='fused response regulator/phosphatase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4905457), ExactPosition(4906669), strand=-1), type='CDS', id='ATP-binding protein CDS_29'),
     SeqFeature(FeatureLocation(ExactPosition(4869618), ExactPosition(4870512), strand=1), type='CDS', id='sensor histidine kinase CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(4875064), ExactPosition(4875952), strand=-1), type='CDS', id='motD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4855486), ExactPosition(4856275), strand=-1), type='CDS', id='transporter substrate-binding domain-containing protein CDS_11'),
     SeqFeature(FeatureLocation(ExactPosition(4889347), ExactPosition(4890133), strand=-1), type='CDS', id='fliR CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4868803), ExactPosition(4869250), strand=1), type='CDS', id='hypothetical protein CDS_528'),
     SeqFeature(FeatureLocation(ExactPosition(4887862), ExactPosition(4888069), strand=-1), type='CDS', id='cold shock domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4887643), ExactPosition(4887844), strand=-1), type='CDS', id='cold-shock protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(4930968), ExactPosition(4933158), strand=1), type='CDS', id='patatin-like phospholipase family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4940924), ExactPosition(4942289), strand=-1), type='CDS', id='rmuC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4918002), ExactPosition(4919274), strand=-1), type='CDS', id='flgJ CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4947186), ExactPosition(4948386), strand=-1), type='CDS', id='MFS transporter CDS_67'),
     SeqFeature(FeatureLocation(ExactPosition(4961496), ExactPosition(4962126), strand=-1), type='CDS', id='NlpC/P60 family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4936926), ExactPosition(4937310), strand=1), type='CDS', id='YbaN family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4943994), ExactPosition(4944216), strand=1), type='CDS', id='hypothetical protein CDS_533'),
     SeqFeature(FeatureLocation(ExactPosition(4978363), ExactPosition(4979812), strand=1), type='CDS', id='cytosine permease CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4990538), ExactPosition(4991885), strand=-1), type='CDS', id='DEAD/DEAH box helicase CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(4988754), ExactPosition(4989621), strand=1), type='CDS', id='DUF6279 family lipoprotein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5017539), ExactPosition(5018331), strand=1), type='CDS', id='CPBP family intramembrane metalloprotease CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5009688), ExactPosition(5010339), strand=1), type='CDS', id='hypothetical protein CDS_536'),
     SeqFeature(FeatureLocation(ExactPosition(5022220), ExactPosition(5022829), strand=1), type='CDS', id='ccoO CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4977919), ExactPosition(4978342), strand=1), type='CDS', id='nuclear transport factor 2 family protein CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(4981432), ExactPosition(4981846), strand=-1), type='CDS', id='hypothetical protein CDS_539'),
     SeqFeature(FeatureLocation(ExactPosition(4995195), ExactPosition(4995510), strand=-1), type='CDS', id='3-phosphoglycerate kinase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(4987580), ExactPosition(4987760), strand=1), type='CDS', id='hypothetical protein CDS_542'),
     SeqFeature(FeatureLocation(ExactPosition(5072145), ExactPosition(5074542), strand=-1), type='CDS', id='GGDEF domain-containing protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(5074683), ExactPosition(5076948), strand=-1), type='CDS', id='rlmKL CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5099124), ExactPosition(5100987), strand=1), type='CDS', id='moeB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5081690), ExactPosition(5082917), strand=1), type='CDS', id='ABC transporter substrate-binding protein CDS_45'),
     SeqFeature(FeatureLocation(ExactPosition(5083715), ExactPosition(5084927), strand=1), type='CDS', id='NarK/NasA family nitrate transporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5101002), ExactPosition(5101917), strand=1), type='CDS', id='cysteine synthase family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5098189), ExactPosition(5099083), strand=1), type='CDS', id='EamA family transporter CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5046277), ExactPosition(5047144), strand=1), type='CDS', id='LysR substrate-binding domain-containing protein CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(5051136), ExactPosition(5051568), strand=-1), type='CDS', id='hypothetical protein CDS_544'),
     SeqFeature(FeatureLocation(ExactPosition(5078940), ExactPosition(5079360), strand=-1), type='CDS', id='transglycosylase SLT domain-containing protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(5079394), ExactPosition(5079814), strand=-1), type='CDS', id='winged helix-turn-helix domain-containing protein CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(5077480), ExactPosition(5077696), strand=1), type='CDS', id='ribosome modulation factor CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5161746), ExactPosition(5163246), strand=-1), type='CDS', id='araG CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5133160), ExactPosition(5134657), strand=1), type='CDS', id='serine protease CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5130717), ExactPosition(5131833), strand=-1), type='CDS', id='TauD/TfdA family dioxygenase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(5163242), ExactPosition(5164223), strand=-1), type='CDS', id='substrate-binding domain-containing protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(5160763), ExactPosition(5161729), strand=-1), type='CDS', id='araH CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5164255), ExactPosition(5165077), strand=-1), type='CDS', id='SDR family oxidoreductase CDS_41'),
     SeqFeature(FeatureLocation(ExactPosition(5170143), ExactPosition(5170857), strand=-1), type='CDS', id='GntR family transcriptional regulator CDS_14'),
     SeqFeature(FeatureLocation(ExactPosition(5130084), ExactPosition(5130693), strand=-1), type='CDS', id='HD domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5126051), ExactPosition(5126417), strand=1), type='CDS', id='DUF2784 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5117426), ExactPosition(5117783), strand=1), type='CDS', id='transcriptional regulator CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(5235196), ExactPosition(5241127), strand=1), type='CDS', id='hypothetical protein CDS_551'),
     SeqFeature(FeatureLocation(ExactPosition(5188566), ExactPosition(5191746), strand=1), type='CDS', id='rne CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5205741), ExactPosition(5206833), strand=-1), type='CDS', id='hypothetical protein CDS_552'),
     SeqFeature(FeatureLocation(ExactPosition(5182695), ExactPosition(5183676), strand=-1), type='CDS', id='plsX CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5178239), ExactPosition(5179055), strand=-1), type='CDS', id='pabC CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5212142), ExactPosition(5212907), strand=1), type='CDS', id='flgA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5198835), ExactPosition(5199552), strand=1), type='CDS', id="4'-phosphopantetheinyl transferase superfamily protein CDS"),
     SeqFeature(FeatureLocation(ExactPosition(5198389), ExactPosition(5198698), strand=-1), type='CDS', id='hypothetical protein CDS_553'),
     SeqFeature(FeatureLocation(ExactPosition(5292869), ExactPosition(5293901), strand=1), type='CDS', id='LacI family DNA-binding transcriptional regulator CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(5257873), ExactPosition(5258821), strand=1), type='CDS', id='hypothetical protein CDS_557'),
     SeqFeature(FeatureLocation(ExactPosition(5278364), ExactPosition(5279243), strand=-1), type='CDS', id='allophanate hydrolase subunit 1 CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5300990), ExactPosition(5301860), strand=-1), type='CDS', id='LysR family transcriptional regulator CDS_82'),
     SeqFeature(FeatureLocation(ExactPosition(5267969), ExactPosition(5268671), strand=1), type='CDS', id='CTP synthase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5255381), ExactPosition(5256026), strand=1), type='CDS', id='HAD-IB family hydrolase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5287988), ExactPosition(5288594), strand=1), type='CDS', id="pyridoxamine 5'-phosphate oxidase family protein CDS_1"),
     SeqFeature(FeatureLocation(ExactPosition(5260864), ExactPosition(5261419), strand=-1), type='CDS', id='hypothetical protein CDS_558'),
     SeqFeature(FeatureLocation(ExactPosition(5253963), ExactPosition(5254496), strand=1), type='CDS', id='Bro-N domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5282965), ExactPosition(5283478), strand=-1), type='CDS', id='multidrug/biocide efflux PACE transporter CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5298182), ExactPosition(5298668), strand=1), type='CDS', id='hypothetical protein CDS_560'),
     SeqFeature(FeatureLocation(ExactPosition(5288637), ExactPosition(5289120), strand=-1), type='CDS', id='GyrI-like domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5257369), ExactPosition(5257819), strand=1), type='CDS', id='MFS transporter CDS_71'),
     SeqFeature(FeatureLocation(ExactPosition(5302923), ExactPosition(5303361), strand=1), type='CDS', id='GNAT family N-acetyltransferase CDS_37'),
     SeqFeature(FeatureLocation(ExactPosition(5294671), ExactPosition(5295097), strand=1), type='CDS', id='hypothetical protein CDS_561'),
     SeqFeature(FeatureLocation(ExactPosition(5256051), ExactPosition(5256456), strand=-1), type='CDS', id='type II toxin-antitoxin system VapC family toxin CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5287704), ExactPosition(5287959), strand=1), type='CDS', id='helix-turn-helix domain-containing protein CDS_25'),
     SeqFeature(FeatureLocation(ExactPosition(5257063), ExactPosition(5257312), strand=-1), type='CDS', id='substrate-binding domain-containing protein CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(5256448), ExactPosition(5256688), strand=-1), type='CDS', id='AbrB/MazE/SpoVT family DNA-binding domain-containing protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5294216), ExactPosition(5294447), strand=1), type='CDS', id='DUF6124 family protein CDS_7'),
     SeqFeature(FeatureLocation(ExactPosition(5305987), ExactPosition(5306173), strand=-1), type='CDS', id='CsbD family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5367616), ExactPosition(5369044), strand=1), type='CDS', id='arcD CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5363417), ExactPosition(5364767), strand=1), type='CDS', id='CoA transferase CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(5352928), ExactPosition(5353948), strand=-1), type='CDS', id='integrase core domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5317712), ExactPosition(5318573), strand=1), type='CDS', id='MurR/RpiR family transcriptional regulator CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5328735), ExactPosition(5329563), strand=-1), type='CDS', id='ATPase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5326890), ExactPosition(5327661), strand=-1), type='CDS', id='hypothetical protein CDS_567'),
     SeqFeature(FeatureLocation(ExactPosition(5315327), ExactPosition(5316041), strand=-1), type='CDS', id='pgl CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5356815), ExactPosition(5357514), strand=-1), type='CDS', id='segregation/condensation protein A CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5314644), ExactPosition(5315310), strand=-1), type='CDS', id='bifunctional 4-hydroxy-2-oxoglutarate aldolase/2-dehydro-3-deoxy-phosphogluconate aldolase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5346883), ExactPosition(5347297), strand=-1), type='CDS', id='arfB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5333652), ExactPosition(5333841), strand=1), type='CDS', id='hypothetical protein CDS_569'),
     SeqFeature(FeatureLocation(ExactPosition(5351505), ExactPosition(5351643), strand=1), type='CDS', id='hypothetical protein CDS_571'),
     SeqFeature(FeatureLocation(ExactPosition(5410834), ExactPosition(5413570), strand=-1), type='CDS', id='secA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5388207), ExactPosition(5389488), strand=-1), type='CDS', id='tolB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5421438), ExactPosition(5422131), strand=1), type='CDS', id='RDD family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5390578), ExactPosition(5391031), strand=-1), type='CDS', id='tolR CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5441038), ExactPosition(5441284), strand=-1), type='CDS', id='hypothetical protein CDS_573'),
     SeqFeature(FeatureLocation(ExactPosition(5455195), ExactPosition(5457334), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_50'),
     SeqFeature(FeatureLocation(ExactPosition(5484393), ExactPosition(5486109), strand=1), type='CDS', id='proline--tRNA ligase CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5466429), ExactPosition(5467560), strand=1), type='CDS', id='efflux RND transporter periplasmic adaptor subunit CDS_16'),
     SeqFeature(FeatureLocation(ExactPosition(5481093), ExactPosition(5482101), strand=-1), type='CDS', id='DUF481 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5507862), ExactPosition(5508666), strand=1), type='CDS', id='ccsA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5504442), ExactPosition(5505216), strand=-1), type='CDS', id='trmD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5471598), ExactPosition(5472279), strand=-1), type='CDS', id='class I SAM-dependent methyltransferase CDS_10'),
     SeqFeature(FeatureLocation(ExactPosition(5482302), ExactPosition(5482617), strand=1), type='CDS', id='MGMT family protein CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5498101), ExactPosition(5498386), strand=-1), type='CDS', id='DUF3509 domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5498703), ExactPosition(5498904), strand=1), type='CDS', id='hypothetical protein CDS_578'),
     SeqFeature(FeatureLocation(ExactPosition(5519473), ExactPosition(5521987), strand=1), type='CDS', id='ptsP CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5572366), ExactPosition(5574235), strand=-1), type='CDS', id='secD CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5552825), ExactPosition(5554295), strand=-1), type='CDS', id='der CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5577208), ExactPosition(5578450), strand=1), type='CDS', id='tyrosine-type recombinase/integrase CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(5574679), ExactPosition(5575795), strand=-1), type='CDS', id='tgt CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5550783), ExactPosition(5551575), strand=-1), type='CDS', id='amidohydrolase CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(5544624), ExactPosition(5545392), strand=-1), type='CDS', id='sulfite exporter TauE/SafE family protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(5515206), ExactPosition(5515755), strand=1), type='CDS', id='NUDIX hydrolase CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(5567240), ExactPosition(5567732), strand=-1), type='CDS', id='iscR CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5531717), ExactPosition(5531996), strand=-1), type='CDS', id='PTS transporter subunit EIIB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5562131), ExactPosition(5562332), strand=-1), type='CDS', id='iscX CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5642277), ExactPosition(5644779), strand=-1), type='CDS', id='LuxR C-terminal-related transcriptional regulator CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5613313), ExactPosition(5615335), strand=1), type='CDS', id='LTA synthase family protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5587004), ExactPosition(5588858), strand=-1), type='CDS', id='hypothetical protein CDS_581'),
     SeqFeature(FeatureLocation(ExactPosition(5623143), ExactPosition(5624154), strand=1), type='CDS', id='sensor domain-containing diguanylate cyclase CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(5583836), ExactPosition(5584820), strand=1), type='CDS', id='DUF4435 domain-containing protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5634907), ExactPosition(5635849), strand=-1), type='CDS', id='cyoA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5640364), ExactPosition(5641267), strand=-1), type='CDS', id='DMT family transporter CDS_18'),
     SeqFeature(FeatureLocation(ExactPosition(5625338), ExactPosition(5626232), strand=1), type='CDS', id='LysR substrate-binding domain-containing protein CDS_18'),
     SeqFeature(FeatureLocation(ExactPosition(5580916), ExactPosition(5581708), strand=-1), type='CDS', id='potassium channel family protein CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5612634), ExactPosition(5613246), strand=1), type='CDS', id='hypothetical protein CDS_585'),
     SeqFeature(FeatureLocation(ExactPosition(5603848), ExactPosition(5604436), strand=1), type='CDS', id='hypothetical protein CDS_586'),
     SeqFeature(FeatureLocation(ExactPosition(5607242), ExactPosition(5607713), strand=1), type='CDS', id='hypothetical protein CDS_589'),
     SeqFeature(FeatureLocation(ExactPosition(5601558), ExactPosition(5601888), strand=1), type='CDS', id='hypothetical protein CDS_590'),
     SeqFeature(FeatureLocation(ExactPosition(5627858), ExactPosition(5628113), strand=1), type='CDS', id='type II toxin-antitoxin system prevent-host-death family antitoxin CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5619598), ExactPosition(5619760), strand=1), type='CDS', id='VacJ family lipoprotein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(5693377), ExactPosition(5694781), strand=1), type='CDS', id='amino acid permease CDS_6'),
     SeqFeature(FeatureLocation(ExactPosition(5663306), ExactPosition(5664425), strand=-1), type='CDS', id='proB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5688892), ExactPosition(5689879), strand=-1), type='CDS', id='helix-turn-helix domain-containing protein CDS_29'),
     SeqFeature(FeatureLocation(ExactPosition(5678247), ExactPosition(5679141), strand=-1), type='CDS', id='NAD-dependent epimerase/dehydratase family protein CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(5653427), ExactPosition(5654297), strand=-1), type='CDS', id='MerR family transcriptional regulator CDS_4'),
     SeqFeature(FeatureLocation(ExactPosition(5680248), ExactPosition(5681016), strand=-1), type='CDS', id='sulfite exporter TauE/SafE family protein CDS_5'),
     SeqFeature(FeatureLocation(ExactPosition(5697173), ExactPosition(5697923), strand=1), type='CDS', id='hypothetical protein CDS_594'),
     SeqFeature(FeatureLocation(ExactPosition(5662242), ExactPosition(5662725), strand=-1), type='CDS', id='glutathione peroxidase CDS_1'),
     SeqFeature(FeatureLocation(ExactPosition(5656510), ExactPosition(5656888), strand=1), type='CDS', id='folX CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5671760), ExactPosition(5672129), strand=-1), type='CDS', id='VOC family protein CDS_9'),
     SeqFeature(FeatureLocation(ExactPosition(5701732), ExactPosition(5702023), strand=1), type='CDS', id='DUF3077 domain-containing protein CDS_3'),
     SeqFeature(FeatureLocation(ExactPosition(5648275), ExactPosition(5648518), strand=1), type='CDS', id='hypothetical protein CDS_597'),
     SeqFeature(FeatureLocation(ExactPosition(5680171), ExactPosition(5680252), strand=-1), type='CDS', id='NADPH quinone reductase MdaB CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5773841), ExactPosition(5775752), strand=-1), type='CDS', id='ftsH CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5769537), ExactPosition(5769996), strand=-1), type='CDS', id='rimP CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5740687), ExactPosition(5741128), strand=1), type='CDS', id='dksA CDS'),
     SeqFeature(FeatureLocation(ExactPosition(5777038), ExactPosition(5777443), strand=-1), type='CDS', id='hypothetical protein CDS_598'),
     SeqFeature(FeatureLocation(ExactPosition(5817814), ExactPosition(5820247), strand=1), type='CDS', id='alpha/beta hydrolase fold domain-containing protein CDS_2'),
     SeqFeature(FeatureLocation(ExactPosition(5827780), ExactPosition(5829697), strand=1), type='CDS', id='methyl-accepting chemotaxis protein CDS_55'),
     SeqFeature(FeatureLocation(ExactPosition(5816166), ExactPosition(5817828), strand=1), type='CDS', id='GMC family oxidoreductase N-terminal domain-containing protein CDS_1'),
     ...]




```python

```


```python
testrec.features[0].qualifiers
```


    ---------------------------------------------------------------------------

    IndexError                                Traceback (most recent call last)

    /tmp/ipykernel_46539/2519109628.py in <module>
    ----> 1 testrec.features[0].qualifiers
    

    IndexError: list index out of range



```python
rsdb['gyrB CDS'].attributes.__dict__
```




    {'_d': {'Name': ['gyrB CDS'],
      'gene': ['gyrB'],
      'locus_tag': ['PFLU_RS00020'],
      'old_locus_tag': ['PFLU0004', 'PFLU_0004'],
      'EC_number': ['5.6.2.2'],
      'inference': ['COORDINATES: similar to AA sequence:RefSeq:WP_002551319.1'],
      'note': ['Derived by automated computational analysis using gene prediction method: Protein Homology.'],
      'codon_start': ['1'],
      'transl_table': ['11'],
      'product': ['DNA topoisomerase (ATP-hydrolyzing) subunit B'],
      'protein_id': ['WP_012721453.1'],
      'NCBI Feature Key': ['CDS']}}



### Differences across reference annotations in prlC


```python
gbdb['prlC CDS']
```




    <Feature CDS (gnl_MPB_PFLU_1:47194-49254[+]) at 0x7f320b177e50>




```python
pdcdb['prlC CDS']
```




    <Feature CDS (gnl_MPB_PFLU_1:47194-49254[+]) at 0x7f320b177940>




```python
rsdb['prlC CDS']
```




    <Feature CDS (gnl_MPB_PFLU_1:47155-49254[+]) at 0x7f320b177850>




```python
gnldb['CDS:CAY46333']
```




    <Feature CDS (gnl_MPB_PFLU_1:47194-49254[+]) at 0x7f320b177f70>



I checked the original reference annotations (i.e. before transferring them to the new assembly). Same difference, Refseq reports 47155 (47154 for old genome), all others 47194 (47193 for old genome) as start position.



```python
gnl_update.features[1]
```




    SeqFeature(FeatureLocation(ExactPosition(53081), ExactPosition(54380), strand=1), type='CDS', id='cytochrome c CDS')




```python
rsdb[gnl_update.features[1].qualifiers['Name'][0]]
```




    <Feature CDS (gnl_MPB_PFLU_1:53082-54380[+]) at 0x7f320b17ba90>




```python
gnldb["gene:PFLU_0053"]
```




    <Feature gene (gnl_MPB_PFLU_1:53076-54380[+]) at 0x7f320b17b730>




```python
gnl_update.features[1].qualifiers['Name']
```




    ['cytochrome c CDS']




```python

```
