#! /bin/bash

bin/prokka \
    --outdir Pseudomonas_fluorescens_SBW25_28C \
    --addgenes \
    --locustag PFLU \
    --prefix Pseudomonas_fluorescens_SBW25_28C \
    --increment 1 \
    --compliant \
    --centre MPB \
    --genus Pseudomonas \
    --species fluorescens \
    --strain SBW25 \
    --gcode 11 \
    --gram neg \
    --proteins /home/grotec/data/sync/sbw25/ncbi/ncbi-genomes-2020-12-16/GCA_000009225.1_ASM922v1_genomic.gbff \
    --addmrna \
    --cpus 14 \
    --rfam \
    $1
