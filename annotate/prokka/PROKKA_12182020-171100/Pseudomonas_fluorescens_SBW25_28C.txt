organism: Pseudomonas fluorescens SBW25 
contigs: 1
bases: 6722400
CDS: 6019
gene: 6221
mRNA: 6221
misc_RNA: 115
rRNA: 16
tRNA: 70
tmRNA: 1
