#! /bin/bash

/home/grotec/Repositories/prokka/bin/prokka \
    --outdir PROKKA_12182020-115100 \
    --addgenes \
    --locustag PFLU \
    --prefix Pseudomonas_fluorescens_SBW25_28C \
    --increment 1 \
    --compliant \
    --centre MPB \
    --genus Pseudomonas \
    --species fluorescens \
    --strain SBW25 \
    --gcode 11 \
    --gram neg \
    --proteins /home/grotec/data/sync/sbw25/genbank2020/GCA_000009225.1_ASM922v1_genomic.gbff \
    --addmrna \
    --cpus 14 \
    --rfam \
    $1
