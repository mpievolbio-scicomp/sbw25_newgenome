#! /bin/bash

PROKKA_PATH=$HOME/Repositories/prokka

$PROKKA_PATH/bin/prokka \
    --outdir PROKKA \
    --locustag PFLU \
    --prefix Pseudomonas_fluorescens_SBW25_28C \
    --increment 1 \
    --compliant \
    --centre MPB \
    --genus Pseudomonas \
    --species fluorescens \
    --strain SBW25 \
    --gcode 11 \
    --proteins /home/grotec/data/sync/sbw25/genbank2020/GCA_000009225.1_ASM922v1_translated_cds.faa \
    --cpus 32 \
    --norrna \
    --notrna \
    --fast \
    --debug \
    $1

