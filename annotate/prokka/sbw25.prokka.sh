#! /bin/bash


FASTA="../geneious/gnl_MPB_PFLU_1.fasta"
OUT=PROKKA_current

if [ -d $OUT ]; then
    rm -r $OUT
fi

prokka \
    --outdir $OUT \
    --locustag MPB \
    --prefix Pseudomonas_fluorescens_SBW25 \
    --increment 1 \
    --addgenes \
    --compliant \
    --centre MPIEvolBio \
    --genus Pseudomonas \
    --species fluorescens \
    --strain SBW25 \
    --gcode 11 \
    --gram neg \
    --proteins ../../fasta/GCA_000009225.1_ASM922v1_translated_cds.faa \
    --addmrna \
    --rfam \
    --cpus 32 \
    $FASTA
    
