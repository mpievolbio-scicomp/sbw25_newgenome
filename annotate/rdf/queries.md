---
title: "Example queries with the Pseudomonas fluorescens SBW25 knowledge graph."
author: "Carsten"
date: 2022-03-31
tags: [SPARQL, RDF, Linked Data, Semantic Web, Knowledge Graph, queries, UniProt, Pseudomonas fluorescens, SBW25]
---


## Introduction
In this post I will present example SPARQL queries against the Pseudomonas fluorescens SBW25 knowledge graph (SBW25KG). The knowlegde graph
was derived from the manually created annotation in gff3 format, as explained in a [previous post](https://mpievolbio-scicomp.pages.gwdg.de/blog/post/2022-03-31_gff3_to_rdf/).

The queries are run against a local instance of the apache-jena-fuseki triplestore. First, I set the endpoint URL and the maximum number of returned records:


```sparql
%endpoint http://localhost:3030/plu/
%show 50
```


<div class="krn-spql"><div class="magic">Endpoint set to: http://localhost:3030/plu/</div><div class="magic">Result maximum size: 50</div></div>


## Retrieve 10 CDS's from the SBW25KG.
In this first example, I query for CDS features, list their primary name,  locus tag, and protein ID.


```sparql
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX gffo: <https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#>
PREFIX so: <http://purl.obolibrary.org/obo/SO_>

SELECT DISTINCT  *  WHERE {
    ?cds a so:0000316 ,
           gffo:Feature ;
           
        gffo:primary_name ?primary_name ;
        gffo:locus_tag ?locus_tag ;
        gffo:protein_id ?protein .
        
}

ORDER BY ?locus_tag
LIMIT 10
```


<div class="krn-spql"><table><tr class=hdr><th>cds</th>
<th>primary_name</th>
<th>locus_tag</th>
<th>protein</th></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14291" target="_other">http://pflu.evolbio.mpg.de/bio_data/14291</a></td>
<td class=val>macB</td>
<td class=val>PFLU_2556</td>
<td class=val>CAY48791.1</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14293" target="_other">http://pflu.evolbio.mpg.de/bio_data/14293</a></td>
<td class=val>mdeA</td>
<td class=val>PFLU_2558</td>
<td class=val>CAY48793.1</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14298" target="_other">http://pflu.evolbio.mpg.de/bio_data/14298</a></td>
<td class=val>potF</td>
<td class=val>PFLU_2564</td>
<td class=val>CAY48798.1</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14314" target="_other">http://pflu.evolbio.mpg.de/bio_data/14314</a></td>
<td class=val>idh</td>
<td class=val>PFLU_2580</td>
<td class=val>CAY48814.1</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14326" target="_other">http://pflu.evolbio.mpg.de/bio_data/14326</a></td>
<td class=val>proX</td>
<td class=val>PFLU_2592</td>
<td class=val>CAY48826.1</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14327" target="_other">http://pflu.evolbio.mpg.de/bio_data/14327</a></td>
<td class=val>bfrG</td>
<td class=val>PFLU_2593</td>
<td class=val>CAY48827.1</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14332" target="_other">http://pflu.evolbio.mpg.de/bio_data/14332</a></td>
<td class=val>bfrH</td>
<td class=val>PFLU_2598</td>
<td class=val>CAY48832.1</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14334" target="_other">http://pflu.evolbio.mpg.de/bio_data/14334</a></td>
<td class=val>cynR</td>
<td class=val>PFLU_2600</td>
<td class=val>CAY48834.1</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14342" target="_other">http://pflu.evolbio.mpg.de/bio_data/14342</a></td>
<td class=val>yaaX</td>
<td class=val>PFLU_2608</td>
<td class=val>CAY48842.1</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/14348" target="_other">http://pflu.evolbio.mpg.de/bio_data/14348</a></td>
<td class=val>sohB</td>
<td class=val>PFLU_2614</td>
<td class=val>CAY48848.1</td></tr></table><div class="tinfo">Total: 10, Shown: 10</div></div>


A click on the CDS URL would open the respective page on the [Pseudomonas fluorescens SBW25 genome database](https://pflu.evolbio.mpg.de)

## Which properties are listed for CDS features?
One of my first exploratory queries against new (to me) SPARQL endpoints is to list the porperties of a given node type. Here, I list all properties of the `gffo:Feature` subject class.


```sparql
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX gffo: <https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#>
PREFIX so: <http://purl.obolibrary.org/obo/SO_>

SELECT DISTINCT  ?prop  WHERE {
    ?cds a so:0000316 ,
           gffo:Feature ;
           
        ?prop ?val .
        
}

```


<div class="krn-spql"><table><tr class=hdr><th>prop</th></tr><tr class=odd><td class=val><a href="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" target="_other">http://www.w3.org/1999/02/22-rdf-syntax-ns#type</a></td></tr><tr class=even><td class=val><a href="http://www.w3.org/2000/01/rdf-schema#seeAlso" target="_other">http://www.w3.org/2000/01/rdf-schema#seeAlso</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#ID" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#ID</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#Parent" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#Parent</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#end" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#end</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#frame" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#frame</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#locus_tag" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#locus_tag</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#score" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#score</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#seqid" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#seqid</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#source" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#source</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#start" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#start</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#strand" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#strand</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#product" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#product</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/reviewed" target="_other">http://purl.uniprot.org/core/reviewed</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#Ontology_term" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#Ontology_term</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#annotated_protein_regions" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#annotated_protein_regions</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#codon_start" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#codon_start</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#confidence_level" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#confidence_level</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#features" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#features</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#gene" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#gene</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#inference" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#inference</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#protein_families" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#protein_families</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#protein_id" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#protein_id</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#similarity" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#similarity</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#uniprot_annotation_score" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#uniprot_annotation_score</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#ft_domain" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#ft_domain</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#note" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#note</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#pathway" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#pathway</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#cc_domain" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#cc_domain</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#motif" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#motif</a></td></tr><tr class=odd><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#ortholog" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#ortholog</a></td></tr><tr class=even><td class=val><a href="https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#primary_name" target="_other">https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#primary_name</a></td></tr></table><div class="tinfo">Total: 32, Shown: 32</div></div>


## Retrieve uniprot entry via federated query using protein id to get hold of the uniprot graph.
Here we use the protein ID to link a node on the SBW25KG to the UniProt KG. On the UniProt KG, the protein ID is linked to the protein (type `up:Protein`) node through the property `rdfs:seeAlso` and it's value is a URI anchored on the `emblcds` prefix <http://purl.uniprot.org/embl-cds/>. Hence, we need to construct the full `emblcds` URI from the variable `?protein_id`. This is achieved by concatenating the `emblcds:` prefix (converted to `str`) and `?protein_id`, casting the resulting string to a URI and bind that URI to the variable `?emblcdsuri`. The query than matches against the UniProt node that has `emblcdsuri` as a `rdfs:seeAlso` property value. To limit the search on the UniProt side, we also demand that the UniProt node is connected to the `up:organism` resource that designates *Pseudomonas fluorescens* SBW25 (`taxon:216595`). We list the first 25 CDSs.


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX gffo: <https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#>
PREFIX so: <http://purl.obolibrary.org/obo/SO_>
PREFIX emblcds: <http://purl.uniprot.org/embl-cds/>
PREFIX upbase: <http://purl.uniprot.org/uniprot/>
PREFIX taxon: <http://purl.uniprot.org/taxonomy/>

SELECT DISTINCT ?cds ?id ?protein_id ?protein
WHERE {
  ?cds a so:0000316 ;
       gffo:ID ?id ;
       gffo:protein_id ?protein_id .
  BIND(IRI(CONCAT(STR(emblcds:), ?protein_id)) AS ?emblcdsuri)

  SERVICE <http://sparql.uniprot.org/sparql> {
    ?protein a up:Protein .
    ?protein up:organism taxon:216595 .
    
    ?protein  rdfs:seeAlso ?emblcdsuri .
    ?protein  rdfs:seeAlso ?xref .
    
  }
}
LIMIT 25
```


<div class="krn-spql"><table><tr class=hdr><th>cds</th>
<th>id</th>
<th>protein_id</th>
<th>protein</th></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11845" target="_other">http://pflu.evolbio.mpg.de/bio_data/11845</a></td>
<td class=val>CDS:PFLU_0001-0</td>
<td class=val>CAJ57270.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/B0B0A5" target="_other">http://purl.uniprot.org/uniprot/B0B0A5</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11846" target="_other">http://pflu.evolbio.mpg.de/bio_data/11846</a></td>
<td class=val>CDS:PFLU_0002-0</td>
<td class=val>CAY46287.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU3" target="_other">http://purl.uniprot.org/uniprot/C3KDU3</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11847" target="_other">http://pflu.evolbio.mpg.de/bio_data/11847</a></td>
<td class=val>CDS:PFLU_0003-0</td>
<td class=val>CAY46288.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU4" target="_other">http://purl.uniprot.org/uniprot/C3KDU4</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11848" target="_other">http://pflu.evolbio.mpg.de/bio_data/11848</a></td>
<td class=val>CDS:PFLU_0004-0</td>
<td class=val>CAY46289.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU5" target="_other">http://purl.uniprot.org/uniprot/C3KDU5</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11849" target="_other">http://pflu.evolbio.mpg.de/bio_data/11849</a></td>
<td class=val>CDS:PFLU_0005-0</td>
<td class=val>CAY46290.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU6" target="_other">http://purl.uniprot.org/uniprot/C3KDU6</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11850" target="_other">http://pflu.evolbio.mpg.de/bio_data/11850</a></td>
<td class=val>CDS:PFLU_0006-0</td>
<td class=val>CAY46291.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE36" target="_other">http://purl.uniprot.org/uniprot/C3KE36</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11851" target="_other">http://pflu.evolbio.mpg.de/bio_data/11851</a></td>
<td class=val>CDS:PFLU_0007-0</td>
<td class=val>CAY46292.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE37" target="_other">http://purl.uniprot.org/uniprot/C3KE37</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11852" target="_other">http://pflu.evolbio.mpg.de/bio_data/11852</a></td>
<td class=val>CDS:PFLU_0008-0</td>
<td class=val>CAY46293.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE38" target="_other">http://purl.uniprot.org/uniprot/C3KE38</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11853" target="_other">http://pflu.evolbio.mpg.de/bio_data/11853</a></td>
<td class=val>CDS:PFLU_0009-0</td>
<td class=val>CAY46294.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE39" target="_other">http://purl.uniprot.org/uniprot/C3KE39</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11854" target="_other">http://pflu.evolbio.mpg.de/bio_data/11854</a></td>
<td class=val>CDS:PFLU_0010-0</td>
<td class=val>CAY46295.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE40" target="_other">http://purl.uniprot.org/uniprot/C3KE40</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11855" target="_other">http://pflu.evolbio.mpg.de/bio_data/11855</a></td>
<td class=val>CDS:PFLU_0011-0</td>
<td class=val>CAY46296.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE41" target="_other">http://purl.uniprot.org/uniprot/C3KE41</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11856" target="_other">http://pflu.evolbio.mpg.de/bio_data/11856</a></td>
<td class=val>CDS:PFLU_0012-0</td>
<td class=val>CAY46297.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE42" target="_other">http://purl.uniprot.org/uniprot/C3KE42</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11857" target="_other">http://pflu.evolbio.mpg.de/bio_data/11857</a></td>
<td class=val>CDS:PFLU_0013-0</td>
<td class=val>CAY46298.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE43" target="_other">http://purl.uniprot.org/uniprot/C3KE43</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11858" target="_other">http://pflu.evolbio.mpg.de/bio_data/11858</a></td>
<td class=val>CDS:PFLU_0014-0</td>
<td class=val>CAY46299.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE44" target="_other">http://purl.uniprot.org/uniprot/C3KE44</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11859" target="_other">http://pflu.evolbio.mpg.de/bio_data/11859</a></td>
<td class=val>CDS:PFLU_0015-0</td>
<td class=val>CAY46300.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE45" target="_other">http://purl.uniprot.org/uniprot/C3KE45</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11860" target="_other">http://pflu.evolbio.mpg.de/bio_data/11860</a></td>
<td class=val>CDS:PFLU_0016-0</td>
<td class=val>CAY46301.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE46" target="_other">http://purl.uniprot.org/uniprot/C3KE46</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11861" target="_other">http://pflu.evolbio.mpg.de/bio_data/11861</a></td>
<td class=val>CDS:PFLU_0017-0</td>
<td class=val>CAY46302.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE47" target="_other">http://purl.uniprot.org/uniprot/C3KE47</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11862" target="_other">http://pflu.evolbio.mpg.de/bio_data/11862</a></td>
<td class=val>CDS:PFLU_0018-0</td>
<td class=val>CAY46303.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE48" target="_other">http://purl.uniprot.org/uniprot/C3KE48</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11863" target="_other">http://pflu.evolbio.mpg.de/bio_data/11863</a></td>
<td class=val>CDS:PFLU_0019-0</td>
<td class=val>CAY46304.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KE49" target="_other">http://purl.uniprot.org/uniprot/C3KE49</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11864" target="_other">http://pflu.evolbio.mpg.de/bio_data/11864</a></td>
<td class=val>CDS:PFLU_0020-0</td>
<td class=val>CAY46305.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K4H7" target="_other">http://purl.uniprot.org/uniprot/C3K4H7</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11865" target="_other">http://pflu.evolbio.mpg.de/bio_data/11865</a></td>
<td class=val>CDS:PFLU_0021-0</td>
<td class=val>CAY46306.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K4H8" target="_other">http://purl.uniprot.org/uniprot/C3K4H8</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11866" target="_other">http://pflu.evolbio.mpg.de/bio_data/11866</a></td>
<td class=val>CDS:PFLU_0022-0</td>
<td class=val>CAY46307.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K4H9" target="_other">http://purl.uniprot.org/uniprot/C3K4H9</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11867" target="_other">http://pflu.evolbio.mpg.de/bio_data/11867</a></td>
<td class=val>CDS:PFLU_0023-0</td>
<td class=val>CAY46308.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K4I0" target="_other">http://purl.uniprot.org/uniprot/C3K4I0</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11868" target="_other">http://pflu.evolbio.mpg.de/bio_data/11868</a></td>
<td class=val>CDS:PFLU_0024-0</td>
<td class=val>CAY46309.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K4I1" target="_other">http://purl.uniprot.org/uniprot/C3K4I1</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11869" target="_other">http://pflu.evolbio.mpg.de/bio_data/11869</a></td>
<td class=val>CDS:PFLU_0025-0</td>
<td class=val>CAY46310.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K4I2" target="_other">http://purl.uniprot.org/uniprot/C3K4I2</a></td></tr></table><div class="tinfo">Total: 25, Shown: 25</div></div>


## Query the UniProt annotations for a given PFLU SBW25 CDS.
Using the same strategy as before, we perform a federated query on the UniProt KG and list the `rdfs:comment` property values for all `up:annotation` property values. 


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX gffo: <https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#>
PREFIX so: <http://purl.obolibrary.org/obo/SO_>
PREFIX emblcds: <http://purl.uniprot.org/embl-cds/>
PREFIX taxon: <http://purl.uniprot.org/taxonomy/>

SELECT DISTINCT ?cds ?protein ?comment 
WHERE {
  ?cds a so:0000316 ;
       gffo:ID ?id ;
       gffo:protein_id ?protein_id .
  BIND(IRI(CONCAT(STR(emblcds:), ?protein_id)) AS ?emblcdsuri)

  SERVICE <http://sparql.uniprot.org/sparql> {
    ?protein a up:Protein .
    ?protein up:organism taxon:216595 .
      
   
    ?protein  rdfs:seeAlso ?emblcdsuri .
    
    ?protein up:annotation ?annotation .
    ?annotation rdfs:comment ?comment
                
  }
}
LIMIT 25
```


<div class="krn-spql"><table><tr class=hdr><th>cds</th>
<th>protein</th>
<th>comment</th></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11845" target="_other">http://pflu.evolbio.mpg.de/bio_data/11845</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/B0B0A5" target="_other">http://purl.uniprot.org/uniprot/B0B0A5</a></td>
<td class=val>Polar residues</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11845" target="_other">http://pflu.evolbio.mpg.de/bio_data/11845</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/B0B0A5" target="_other">http://purl.uniprot.org/uniprot/B0B0A5</a></td>
<td class=val>ATP</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11845" target="_other">http://pflu.evolbio.mpg.de/bio_data/11845</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/B0B0A5" target="_other">http://purl.uniprot.org/uniprot/B0B0A5</a></td>
<td class=val>Disordered</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11845" target="_other">http://pflu.evolbio.mpg.de/bio_data/11845</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/B0B0A5" target="_other">http://purl.uniprot.org/uniprot/B0B0A5</a></td>
<td class=val>Plays an important role in the initiation and regulation of chromosomal replication. Binds to the origin of replication; it binds specifically double-stranded DNA at a 9 bp consensus (dnaA box): 5'-TTATC[CA]A[CA]A-3'. DnaA binds to ATP and to acidic phospholipids.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11845" target="_other">http://pflu.evolbio.mpg.de/bio_data/11845</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/B0B0A5" target="_other">http://purl.uniprot.org/uniprot/B0B0A5</a></td>
<td class=val>Belongs to the DnaA family.</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11845" target="_other">http://pflu.evolbio.mpg.de/bio_data/11845</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/B0B0A5" target="_other">http://purl.uniprot.org/uniprot/B0B0A5</a></td>
<td class=val>Chromosomal replication initiator protein DnaA</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11846" target="_other">http://pflu.evolbio.mpg.de/bio_data/11846</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU3" target="_other">http://purl.uniprot.org/uniprot/C3KDU3</a></td>
<td class=val>Confers DNA tethering and processivity to DNA polymerases and other proteins. Acts as a clamp, forming a ring around DNA (a reaction catalyzed by the clamp-loading complex) which diffuses in an ATP-independent manner freely and bidirectionally along dsDNA. Initially characterized for its ability to contact the catalytic subunit of DNA polymerase III (Pol III), a complex, multichain enzyme responsible for most of the replicative synthesis in bacteria; Pol III exhibits 3'-5' exonuclease proofreading activity. The beta chain is required for initiation of replication as well as for processivity of DNA replication.</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11846" target="_other">http://pflu.evolbio.mpg.de/bio_data/11846</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU3" target="_other">http://purl.uniprot.org/uniprot/C3KDU3</a></td>
<td class=val>Forms a ring-shaped head-to-tail homodimer around DNA.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11846" target="_other">http://pflu.evolbio.mpg.de/bio_data/11846</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU3" target="_other">http://purl.uniprot.org/uniprot/C3KDU3</a></td>
<td class=val>DNA_pol3_beta_3</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11846" target="_other">http://pflu.evolbio.mpg.de/bio_data/11846</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU3" target="_other">http://purl.uniprot.org/uniprot/C3KDU3</a></td>
<td class=val>DNA_pol3_beta_2</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11846" target="_other">http://pflu.evolbio.mpg.de/bio_data/11846</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU3" target="_other">http://purl.uniprot.org/uniprot/C3KDU3</a></td>
<td class=val>DNA_pol3_beta</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11846" target="_other">http://pflu.evolbio.mpg.de/bio_data/11846</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU3" target="_other">http://purl.uniprot.org/uniprot/C3KDU3</a></td>
<td class=val>Belongs to the beta sliding clamp family.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11847" target="_other">http://pflu.evolbio.mpg.de/bio_data/11847</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU4" target="_other">http://purl.uniprot.org/uniprot/C3KDU4</a></td>
<td class=val>DNA replication and repair protein RecF</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11847" target="_other">http://pflu.evolbio.mpg.de/bio_data/11847</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU4" target="_other">http://purl.uniprot.org/uniprot/C3KDU4</a></td>
<td class=val>Belongs to the RecF family.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11847" target="_other">http://pflu.evolbio.mpg.de/bio_data/11847</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU4" target="_other">http://purl.uniprot.org/uniprot/C3KDU4</a></td>
<td class=val>ATP</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11847" target="_other">http://pflu.evolbio.mpg.de/bio_data/11847</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU4" target="_other">http://purl.uniprot.org/uniprot/C3KDU4</a></td>
<td class=val>The RecF protein is involved in DNA metabolism; it is required for DNA replication and normal SOS inducibility. RecF binds preferentially to single-stranded, linear DNA. It also seems to bind ATP.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11848" target="_other">http://pflu.evolbio.mpg.de/bio_data/11848</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU5" target="_other">http://purl.uniprot.org/uniprot/C3KDU5</a></td>
<td class=val>Toprim</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11848" target="_other">http://pflu.evolbio.mpg.de/bio_data/11848</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU5" target="_other">http://purl.uniprot.org/uniprot/C3KDU5</a></td>
<td class=val>Heterotetramer, composed of two GyrA and two GyrB chains. In the heterotetramer, GyrA contains the active site tyrosine that forms a transient covalent intermediate with DNA, while GyrB binds cofactors and catalyzes ATP hydrolysis.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11848" target="_other">http://pflu.evolbio.mpg.de/bio_data/11848</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU5" target="_other">http://purl.uniprot.org/uniprot/C3KDU5</a></td>
<td class=val>A type II topoisomerase that negatively supercoils closed circular double-stranded (ds) DNA in an ATP-dependent manner to modulate DNA topology and maintain chromosomes in an underwound state. Negative supercoiling favors strand separation, and DNA replication, transcription, recombination and repair, all of which involve strand separation. Also able to catalyze the interconversion of other topological isomers of dsDNA rings, including catenanes and knotted rings. Type II topoisomerases break and join 2 DNA strands simultaneously in an ATP-dependent manner.</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11848" target="_other">http://pflu.evolbio.mpg.de/bio_data/11848</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU5" target="_other">http://purl.uniprot.org/uniprot/C3KDU5</a></td>
<td class=val>Belongs to the type II topoisomerase family.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11848" target="_other">http://pflu.evolbio.mpg.de/bio_data/11848</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU5" target="_other">http://purl.uniprot.org/uniprot/C3KDU5</a></td>
<td class=val>Belongs to the type II topoisomerase GyrB family.</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11848" target="_other">http://pflu.evolbio.mpg.de/bio_data/11848</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU5" target="_other">http://purl.uniprot.org/uniprot/C3KDU5</a></td>
<td class=val>Few gyrases are as efficient as E.coli at forming negative supercoils. Not all organisms have 2 type II topoisomerases; in organisms with a single type II topoisomerase this enzyme also has to decatenate newly replicated chromosomes.</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11849" target="_other">http://pflu.evolbio.mpg.de/bio_data/11849</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU6" target="_other">http://purl.uniprot.org/uniprot/C3KDU6</a></td>
<td class=val>4-aspartylphosphate</td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11849" target="_other">http://pflu.evolbio.mpg.de/bio_data/11849</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU6" target="_other">http://purl.uniprot.org/uniprot/C3KDU6</a></td>
<td class=val>OmpR/PhoB-type</td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/11849" target="_other">http://pflu.evolbio.mpg.de/bio_data/11849</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3KDU6" target="_other">http://purl.uniprot.org/uniprot/C3KDU6</a></td>
<td class=val>Response regulatory</td></tr></table><div class="tinfo">Total: 25, Shown: 25</div></div>


## Find proteins that have the same annotation as wssH
Next, we try to find proteins that may have a similar function as a focus gene in Pflu SBW25. Here, we connect to other proteins through the `up:annotation` property.


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX gffo: <https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#>
PREFIX so: <http://purl.obolibrary.org/obo/SO_>
PREFIX emblcds: <http://purl.uniprot.org/embl-cds/>
PREFIX taxon: <http://purl.uniprot.org/taxonomy/>

SELECT DISTINCT ?id ?protein_id ?protein ?annotation ?protein2 ?comment
WHERE {
  ?cds a so:0000316 ;
       gffo:ID ?id .
       FILTER(REGEX(?id, 'CDS:PFLU_0307-0'))
  ?cds gffo:protein_id ?protein_id .
  BIND(IRI(CONCAT(STR(emblcds:), ?protein_id)) AS ?emblcdsuri)

  SERVICE <http://sparql.uniprot.org/sparql> {
    ?protein a up:Protein .
    ?protein up:organism taxon:216595 .
    
    ?protein  rdfs:seeAlso ?emblcdsuri .
    
    ?protein up:annotation ?annotation .
    ?protein2 up:annotation ?annotation .
      
    ?annotation rdfs:comment ?comment .
  }
}
LIMIT 25
```


<div class="krn-spql"><table><tr class=hdr><th>id</th>
<th>protein_id</th>
<th>protein</th>
<th>annotation</th>
<th>protein2</th>
<th>comment</th></tr><tr class=odd><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIP016432E29D5FA475" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIP016432E29D5FA475</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr><tr class=even><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIP2D0C3C41F62290BE" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIP2D0C3C41F62290BE</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr><tr class=odd><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIP5876DA8DDDFD7798" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIP5876DA8DDDFD7798</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Belongs to the membrane-bound acyltransferase family.</td></tr><tr class=even><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIP5E60A310F8922ADA" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIP5E60A310F8922ADA</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr><tr class=odd><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIP65059D303A47C9AA" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIP65059D303A47C9AA</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr><tr class=even><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIPD5D9816EB7014D97" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIPD5D9816EB7014D97</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr><tr class=odd><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIPD76729EFF3A86A35" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIPD76729EFF3A86A35</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr><tr class=even><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIPE3AEBFE73B122FEE" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIPE3AEBFE73B122FEE</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr><tr class=odd><td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6#SIPEAFA32F39A331071" target="_other">http://purl.uniprot.org/uniprot/C3K6B6#SIPEAFA32F39A331071</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val>Helical</td></tr></table><div class="tinfo">Total: 9, Shown: 9</div></div>


This is not exactly what I wanted, since the queried "other" protein (`?protein2`) is identical to the focal protein. 
It seems that the annotation ID encodes the protein ID itself, so every annotation is unique to a given protein. 

Now, instead of matching the `up:annotation` property value, we try to match the annotation comment. All but one annotation comments say "Helical", which is probably 
too generic to serve as a matching criterion. So let's focus on the comment "Belongs to the membrane-bound acyltransferase family" and see if we find other proteins that share this annotation comment. The query below selects proteins that share the annotation comment with our focal protein and belong to the Pseudomonas genus. We list only the scientific name of the organism and the protein ID of the selected protein on the UniProt KG.


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX gffo: <https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#>
PREFIX so: <http://purl.obolibrary.org/obo/SO_>
PREFIX emblcds: <http://purl.uniprot.org/embl-cds/>
PREFIX upbase: <http://purl.uniprot.org/uniprot/>
PREFIX taxon: <http://purl.uniprot.org/taxonomy/>

SELECT DISTINCT ?protein2 ?scName
WHERE {
  ?cds a so:0000316 ;
       gffo:ID ?id .
       FILTER(REGEX(?id, 'CDS:PFLU_0307-0'))
  ?cds gffo:protein_id ?protein_id .
  BIND(IRI(CONCAT(STR(emblcds:), ?protein_id)) AS ?emblcdsuri)

  SERVICE <http://sparql.uniprot.org/sparql> {
    ?protein a up:Protein .
    ?protein up:organism taxon:216595 .
    
    ?protein  rdfs:seeAlso ?emblcdsuri .
    
    ?protein up:annotation ?annotation .
    ?annotation rdfs:comment ?antncmnt .
      
    FILTER(REGEX(?antncmnt, "^Belongs"))
      
    ?protein2 up:annotation ?annotation2 .
    ?annotation2 rdfs:comment ?antncmnt .
      
    ?protein2 up:organism ?taxon .
    ?taxon up:scientificName ?scName .
      
    FILTER(REGEX(?scName, "Pseudomonas"))
           
    
  }
}
LIMIT 25
```


<div class="krn-spql"><table><tr class=hdr><th>protein2</th>
<th>scName</th></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/L8MBF1" target="_other">http://purl.uniprot.org/uniprot/L8MBF1</a></td>
<td class=val>Pseudomonas furukawaii</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/L8MM14" target="_other">http://purl.uniprot.org/uniprot/L8MM14</a></td>
<td class=val>Pseudomonas furukawaii</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/Q88ND2" target="_other">http://purl.uniprot.org/uniprot/Q88ND2</a></td>
<td class=val>Pseudomonas putida (strain ATCC 47054 / DSM 6125 / CFBP 8728 / NCIMB 11950 / KT2440)</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/Q4ZXL1" target="_other">http://purl.uniprot.org/uniprot/Q4ZXL1</a></td>
<td class=val>Pseudomonas syringae pv. syringae (strain B728a)</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/Q3K778" target="_other">http://purl.uniprot.org/uniprot/Q3K778</a></td>
<td class=val>Pseudomonas fluorescens (strain Pf0-1)</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/Q3KHR1" target="_other">http://purl.uniprot.org/uniprot/Q3KHR1</a></td>
<td class=val>Pseudomonas fluorescens (strain Pf0-1)</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/Q51392" target="_other">http://purl.uniprot.org/uniprot/Q51392</a></td>
<td class=val>Pseudomonas aeruginosa (strain ATCC 15692 / DSM 22644 / CIP 104116 / JCM 14847 / LMG 12228 / 1C / PRS 101 / PAO1)</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/Q887Q6" target="_other">http://purl.uniprot.org/uniprot/Q887Q6</a></td>
<td class=val>Pseudomonas syringae pv. tomato (strain ATCC BAA-871 / DC3000)</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A072ZRH8" target="_other">http://purl.uniprot.org/uniprot/A0A072ZRH8</a></td>
<td class=val>Pseudomonas aeruginosa</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A1Y3LHY0" target="_other">http://purl.uniprot.org/uniprot/A0A1Y3LHY0</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A7U6M686" target="_other">http://purl.uniprot.org/uniprot/A0A7U6M686</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A1Q9QVW4" target="_other">http://purl.uniprot.org/uniprot/A0A1Q9QVW4</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A2S3W6C3" target="_other">http://purl.uniprot.org/uniprot/A0A2S3W6C3</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A2S3WIC5" target="_other">http://purl.uniprot.org/uniprot/A0A2S3WIC5</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A0D1PC37" target="_other">http://purl.uniprot.org/uniprot/A0A0D1PC37</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A177SSH9" target="_other">http://purl.uniprot.org/uniprot/A0A177SSH9</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A2S3WW87" target="_other">http://purl.uniprot.org/uniprot/A0A2S3WW87</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A3M8TKL8" target="_other">http://purl.uniprot.org/uniprot/A0A3M8TKL8</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A0N8HF79" target="_other">http://purl.uniprot.org/uniprot/A0A0N8HF79</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A5P1R2W6" target="_other">http://purl.uniprot.org/uniprot/A0A5P1R2W6</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A1B2FDE2" target="_other">http://purl.uniprot.org/uniprot/A0A1B2FDE2</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A0M3CIH1" target="_other">http://purl.uniprot.org/uniprot/A0A0M3CIH1</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A7W2QI71" target="_other">http://purl.uniprot.org/uniprot/A0A7W2QI71</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/uniprot/A0A6I6XTZ1" target="_other">http://purl.uniprot.org/uniprot/A0A6I6XTZ1</a></td>
<td class=val>Pseudomonas putida</td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/uniprot/A0A3M8SAS1" target="_other">http://purl.uniprot.org/uniprot/A0A3M8SAS1</a></td>
<td class=val>Pseudomonas putida</td></tr></table><div class="tinfo">Total: 25, Shown: 25</div></div>


## Find citations from UniProt KB.
The same could also be done by searching proteins that share the GO term of our focal protein. GO terms are connected to protein subjects through the `up:classifiedWith` property.


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX gffo: <https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#>
PREFIX so: <http://purl.obolibrary.org/obo/SO_>
PREFIX emblcds: <http://purl.uniprot.org/embl-cds/>
PREFIX upbase: <http://purl.uniprot.org/uniprot/>
PREFIX taxon: <http://purl.uniprot.org/taxonomy/>

SELECT DISTINCT *
WHERE {
  ?cds a so:0000316 ;
       gffo:ID ?id .
       FILTER(REGEX(?id, 'CDS:PFLU_0307-0'))
  ?cds gffo:protein_id ?protein_id .
  BIND(IRI(CONCAT(STR(emblcds:), ?protein_id)) AS ?emblcdsuri)

  SERVICE <http://sparql.uniprot.org/sparql> {
    ?protein a up:Protein .
    ?protein up:organism taxon:216595 .
    
    # Connect to up protein via protein id (as uri).
    ?protein  rdfs:seeAlso ?emblcdsuri .
    
    # Get GO terms.
    ?protein up:classifiedWith ?keyword .
     
  }
}
LIMIT 10
```


<div class="krn-spql"><table><tr class=hdr><th>cds</th>
<th>id</th>
<th>protein_id</th>
<th>emblcdsuri</th>
<th>protein</th>
<th>keyword</th></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.obolibrary.org/obo/GO_0005886" target="_other">http://purl.obolibrary.org/obo/GO_0005886</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.obolibrary.org/obo/GO_0016021" target="_other">http://purl.obolibrary.org/obo/GO_0016021</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.obolibrary.org/obo/GO_0016746" target="_other">http://purl.obolibrary.org/obo/GO_0016746</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.obolibrary.org/obo/GO_0042121" target="_other">http://purl.obolibrary.org/obo/GO_0042121</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/keywords/1133" target="_other">http://purl.uniprot.org/keywords/1133</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/keywords/1185" target="_other">http://purl.uniprot.org/keywords/1185</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/keywords/12" target="_other">http://purl.uniprot.org/keywords/12</a></td></tr><tr class=even><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/keywords/16" target="_other">http://purl.uniprot.org/keywords/16</a></td></tr><tr class=odd><td class=val><a href="http://pflu.evolbio.mpg.de/bio_data/12135" target="_other">http://pflu.evolbio.mpg.de/bio_data/12135</a></td>
<td class=val>CDS:PFLU_0307-0</td>
<td class=val>CAY46584.1</td>
<td class=val><a href="http://purl.uniprot.org/embl-cds/CAY46584.1" target="_other">http://purl.uniprot.org/embl-cds/CAY46584.1</a></td>
<td class=val><a href="http://purl.uniprot.org/uniprot/C3K6B6" target="_other">http://purl.uniprot.org/uniprot/C3K6B6</a></td>
<td class=val><a href="http://purl.uniprot.org/keywords/997" target="_other">http://purl.uniprot.org/keywords/997</a></td></tr></table><div class="tinfo">Total: 9, Shown: 9</div></div>


## Find papers referenced by proteins that share a GO or keyword with wssH.
Since we now know the Uniprot ID of our focal protein, we may as well submit our query directly to the Uniprot SPARQL endpoint.


```sparql
%endpoint https://sparql.uniprot.org/sparql
%show 100
```


<div class="krn-spql"><div class="magic">Endpoint set to: https://sparql.uniprot.org/sparql</div><div class="magic">Result maximum size: 100</div></div>


In this query, we list all citations of proteins that share a GO term with C3K6B6 (the WssH protein). In addition, we also list the encoding gene's name and locus tag.


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX uniprotkb: <http://purl.uniprot.org/uniprot/>
PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dc: <http://purl.org/dc/terms/>

SELECT DISTINCT ?scName ?title ?protein2_recName ?gene2_label ?gene2_locus ?url WHERE {
  uniprotkb:C3K6B6 up:classifiedWith ?annotation .
  ?annotation rdfs:comment ?comment .
  
  ?protein2 a up:Protein .
  ?protein2 rdfs:label ?protein2_recName .
  ?protein2 up:encodedBy ?gene2 .
  ?gene2 skos:prefLabel ?gene2_label ;
         up:locusName ?gene2_locus.
  ?protein2 up:classifiedWith ?annotation .
    

  ?protein2 up:organism ?taxon .
  ?taxon up:scientificName ?scName .
  
  FILTER(REGEX(?scName, "^Pseudomonas"))
  
  ?protein2 up:citation ?pub .
  ?pub up:title ?title .
  ?pub dc:identifier ?doi .
  
  BIND(IRI(CONCAT("https://dx.doi.org/", ?doi)) as ?url )
  
}
LIMIT 10
```


<div class="krn-spql"><table><tr class=hdr><th>scName</th>
<th>title</th>
<th>protein2_recName</th>
<th>gene2_label</th>
<th>gene2_locus</th>
<th>url</th></tr><tr class=odd><td class=val>Pseudomonas aeruginosa (strain ATCC 15692 / DSM 22644 / CIP 104116 / JCM 14847 / LMG 12228 / 1C / PRS 101 / PAO1)</td>
<td class=val>Pseudomonas aeruginosa Genome Database and PseudoCAP: facilitating community-based, continually updated, genome annotation.</td>
<td class=val>Coat protein B of bacteriophage Pf1</td>
<td class=val>coaB</td>
<td class=val>PA0723</td>
<td class=val><a href="https://dx.doi.org/doi:10.1093/nar/gki047" target="_other">https://dx.doi.org/doi:10.1093/nar/gki047</a></td></tr><tr class=even><td class=val>Pseudomonas aeruginosa (strain ATCC 15692 / DSM 22644 / CIP 104116 / JCM 14847 / LMG 12228 / 1C / PRS 101 / PAO1)</td>
<td class=val>Complete genome sequence of Pseudomonas aeruginosa PAO1, an opportunistic pathogen.</td>
<td class=val>Coat protein B of bacteriophage Pf1</td>
<td class=val>coaB</td>
<td class=val>PA0723</td>
<td class=val><a href="https://dx.doi.org/doi:10.1038/35023079" target="_other">https://dx.doi.org/doi:10.1038/35023079</a></td></tr><tr class=odd><td class=val>Pseudomonas fluorescens (strain SBW25)</td>
<td class=val>Genomic and genetic analyses of diversity and plant interactions of Pseudomonas fluorescens.</td>
<td class=val>Cellulose synthase catalytic subunit [UDP-forming]</td>
<td class=val>bcsA</td>
<td class=val>PFLU_0301</td>
<td class=val><a href="https://dx.doi.org/doi:10.1186/gb-2009-10-5-r51" target="_other">https://dx.doi.org/doi:10.1186/gb-2009-10-5-r51</a></td></tr><tr class=even><td class=val>Pseudomonas fluorescens (strain SBW25)</td>
<td class=val>Genomic and genetic analyses of diversity and plant interactions of Pseudomonas fluorescens.</td>
<td class=val>Cyclic di-GMP-binding protein</td>
<td class=val>bcsB</td>
<td class=val>PFLU_0302</td>
<td class=val><a href="https://dx.doi.org/doi:10.1186/gb-2009-10-5-r51" target="_other">https://dx.doi.org/doi:10.1186/gb-2009-10-5-r51</a></td></tr><tr class=odd><td class=val>Pseudomonas fluorescens (strain SBW25)</td>
<td class=val>Adaptive divergence in experimental populations of Pseudomonas fluorescens. I. Genetic and phenotypic bases of wrinkly spreader fitness.</td>
<td class=val>Cellulose synthase catalytic subunit [UDP-forming]</td>
<td class=val>bcsA</td>
<td class=val>PFLU_0301</td>
<td class=val><a href="https://dx.doi.org/doi:10.1093/genetics/161.1.33" target="_other">https://dx.doi.org/doi:10.1093/genetics/161.1.33</a></td></tr><tr class=even><td class=val>Pseudomonas fluorescens (strain SBW25)</td>
<td class=val>Adaptive divergence in experimental populations of Pseudomonas fluorescens. I. Genetic and phenotypic bases of wrinkly spreader fitness.</td>
<td class=val>Cyclic di-GMP-binding protein</td>
<td class=val>bcsB</td>
<td class=val>PFLU_0302</td>
<td class=val><a href="https://dx.doi.org/doi:10.1093/genetics/161.1.33" target="_other">https://dx.doi.org/doi:10.1093/genetics/161.1.33</a></td></tr><tr class=odd><td class=val>Pseudomonas aeruginosa (strain ATCC 15692 / DSM 22644 / CIP 104116 / JCM 14847 / LMG 12228 / 1C / PRS 101 / PAO1)</td>
<td class=val>Genome diversity of Pseudomonas aeruginosa PAO1 laboratory strains.</td>
<td class=val>Coat protein B of bacteriophage Pf1</td>
<td class=val>coaB</td>
<td class=val>PA0723</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.01515-09" target="_other">https://dx.doi.org/doi:10.1128/jb.01515-09</a></td></tr><tr class=even><td class=val>Pseudomonas stutzeri (strain ATCC 17588 / DSM 5190 / CCUG 11256 / JCM 5965 / LMG 11199 / NBRC 14165 / NCIMB 11358 / Stanier 221)</td>
<td class=val>Complete genome sequence of the type strain Pseudomonas stutzeri CGMCC 1.1803.</td>
<td class=val>Sulfate transporter CysZ</td>
<td class=val>cysZ</td>
<td class=val>PSTAB_1036</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.06061-11" target="_other">https://dx.doi.org/doi:10.1128/jb.06061-11</a></td></tr><tr class=odd><td class=val>Pseudomonas stutzeri (strain ATCC 17588 / DSM 5190 / CCUG 11256 / JCM 5965 / LMG 11199 / NBRC 14165 / NCIMB 11358 / Stanier 221)</td>
<td class=val>Complete Genome Sequence of the Type Strain Pseudomonas stutzeri CGMCC 1.1803.</td>
<td class=val>Sulfate transporter CysZ</td>
<td class=val>cysZ</td>
<td class=val>PSTAB_1036</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.06061-11" target="_other">https://dx.doi.org/doi:10.1128/jb.06061-11</a></td></tr><tr class=even><td class=val>Pseudomonas stutzeri (strain ATCC 17588 / DSM 5190 / CCUG 11256 / JCM 5965 / LMG 11199 / NBRC 14165 / NCIMB 11358 / Stanier 221)</td>
<td class=val>Complete genome sequence of the type strain Pseudomonas stutzeri CGMCC 1.1803.</td>
<td class=val>Amino acid transporter LysE</td>
<td class=val>lysE</td>
<td class=val>PSTAB_3885</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.06061-11" target="_other">https://dx.doi.org/doi:10.1128/jb.06061-11</a></td></tr></table><div class="tinfo">Total: 10, Shown: 10</div></div>


Now this **is interesting**: The UniProt protein encoded by Pflu SBW252's wssH gene shares GO terms with the genes bcsA and bcsB which have the same locus tag as wssA and wssB. In other words, we have just found alternative names for wssA and wssB.

## All properties of a protein
For further endeavors with the Uniprot SPARQL endpoint, let's find out all the properties of a Uniprot protein node.


```sparql
PREFIX uniprotkb: <http://purl.uniprot.org/uniprot/>


select distinct ?prop where {
  uniprotkb:C3K6B6 ?prop ?val
 
}
limit 100
```


<div class="krn-spql"><table><tr class=hdr><th>prop</th></tr><tr class=odd><td class=val><a href="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" target="_other">http://www.w3.org/1999/02/22-rdf-syntax-ns#type</a></td></tr><tr class=even><td class=val><a href="http://www.w3.org/2000/01/rdf-schema#seeAlso" target="_other">http://www.w3.org/2000/01/rdf-schema#seeAlso</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/citation" target="_other">http://purl.uniprot.org/core/citation</a></td></tr><tr class=even><td class=val><a href="http://www.w3.org/2000/01/rdf-schema#label" target="_other">http://www.w3.org/2000/01/rdf-schema#label</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/mnemonic" target="_other">http://purl.uniprot.org/core/mnemonic</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/annotation" target="_other">http://purl.uniprot.org/core/annotation</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/attribution" target="_other">http://purl.uniprot.org/core/attribution</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/classifiedWith" target="_other">http://purl.uniprot.org/core/classifiedWith</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/created" target="_other">http://purl.uniprot.org/core/created</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/encodedBy" target="_other">http://purl.uniprot.org/core/encodedBy</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/enzyme" target="_other">http://purl.uniprot.org/core/enzyme</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/existence" target="_other">http://purl.uniprot.org/core/existence</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/modified" target="_other">http://purl.uniprot.org/core/modified</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/organism" target="_other">http://purl.uniprot.org/core/organism</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/recommendedName" target="_other">http://purl.uniprot.org/core/recommendedName</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/representativeFor" target="_other">http://purl.uniprot.org/core/representativeFor</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/reviewed" target="_other">http://purl.uniprot.org/core/reviewed</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/seedFor" target="_other">http://purl.uniprot.org/core/seedFor</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/sequence" target="_other">http://purl.uniprot.org/core/sequence</a></td></tr><tr class=even><td class=val><a href="http://purl.uniprot.org/core/version" target="_other">http://purl.uniprot.org/core/version</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/proteome" target="_other">http://purl.uniprot.org/core/proteome</a></td></tr></table><div class="tinfo">Total: 21, Shown: 21</div></div>


## Gene properties
... and the same for gene properties.


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX uniprotkb: <http://purl.uniprot.org/uniprot/>


select distinct ?prop where {
  uniprotkb:C3K6B6 up:encodedBy ?gene .
  ?gene ?prop ?val .
 
}
limit 100
```


<div class="krn-spql"><table><tr class=hdr><th>prop</th></tr><tr class=odd><td class=val><a href="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" target="_other">http://www.w3.org/1999/02/22-rdf-syntax-ns#type</a></td></tr><tr class=even><td class=val><a href="http://www.w3.org/2004/02/skos/core#prefLabel" target="_other">http://www.w3.org/2004/02/skos/core#prefLabel</a></td></tr><tr class=odd><td class=val><a href="http://purl.uniprot.org/core/locusName" target="_other">http://purl.uniprot.org/core/locusName</a></td></tr></table><div class="tinfo">Total: 3, Shown: 3</div></div>


## UniRef
Uniprot provides *UniRef* clusters, i.e. groups of proteins that share a certain percentage of similarity with a given focal protein. The three cluster groups *UniRef100*, *UniRef90*, and *UniRef50* contain protein sequences that are 100%, over 90% or over 50% identical to the focal protein, respectively.
Here, we utilize these clusters to find literature citations linked to proteins in a given similarity cluster. 

Proteins are linked to a given *UniRef* cluster via their *UniParc* sequence. Hence, we need to query those *UniParc* nodes that contain our focus protein, find the *UniRef* cluster of that *UniParc*, get the other *UniParc* sequences in that cluster and finally traverse to the *other* proteins in those *UniParc* sequences. The returned data is sorted in descending order of the *UniRef* matching percentage.


```sparql
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX uniprotkb: <http://purl.uniprot.org/uniprot/>
PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dc: <http://purl.org/dc/terms/>

SELECT DISTINCT ?pct ?protein ?scName ?title ?url WHERE {
    # Find uniref cluster and uniparcs.
    BIND(uniprotkb:C3K6B6 as ?focus_protein)
    ?uniref a up:Cluster ;
              up:member ?uniparc .
    ?uniparc a up:Sequence ;
               up:sequenceFor ?focus_protein .
    BIND(STRBEFORE(STRAFTER(STR(?uniref), "UniRef"), "_") AS ?pct) 
    # For each uniparc, find all members and list their references.
    ?uniref up:member ?uniparc2 .
    FILTER(?uniparc2 NOT IN (?uniparc))
    
    ?uniparc2 up:sequenceFor ?protein .
    ?protein a up:Protein .
    FILTER(?protein NOT IN (?focus_protein))
    
    ?protein up:organism ?taxon .
    ?taxon up:scientificName ?scName .

    FILTER(REGEX(?scName, "^Pseudomonas"))

    ?protein up:citation ?pub .
    ?pub up:title ?title .
    ?pub dc:identifier ?doi .

    BIND(IRI(CONCAT("https://dx.doi.org/", ?doi)) AS ?url )

  
}
ORDER BY DESC(?pct)
LIMIT 100

```


<div class="krn-spql"><table><tr class=hdr><th>pct</th>
<th>protein</th>
<th>scName</th>
<th>title</th>
<th>url</th></tr><tr class=odd><td class=val>90</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/Q8RSY5" target="_other">http://purl.uniprot.org/uniprot/Q8RSY5</a></td>
<td class=val>Pseudomonas fluorescens</td>
<td class=val>Adaptive divergence in experimental populations of Pseudomonas fluorescens. I. Genetic and phenotypic bases of wrinkly spreader fitness.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1093/genetics/161.1.33" target="_other">https://dx.doi.org/doi:10.1093/genetics/161.1.33</a></td></tr><tr class=even><td class=val>90</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A5M9ICB8" target="_other">http://purl.uniprot.org/uniprot/A0A5M9ICB8</a></td>
<td class=val>Pseudomonas panacis</td>
<td class=val>Genetic Organization of the &lt;i&gt;aprX-lipA2&lt;/i&gt; Operon Affects the Proteolytic Potential of &lt;i&gt;Pseudomonas&lt;/i&gt; Species in Milk.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2020.01190" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2020.01190</a></td></tr><tr class=odd><td class=val>90</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A5M9ICB8" target="_other">http://purl.uniprot.org/uniprot/A0A5M9ICB8</a></td>
<td class=val>Pseudomonas panacis</td>
<td class=val>Genetic Organization of the aprX-lipA2 Operon Affects the Proteolytic Potential of Pseudomonas Species in Milk.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2020.01190" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2020.01190</a></td></tr><tr class=even><td class=val>90</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A0H5ARJ4" target="_other">http://purl.uniprot.org/uniprot/A0A0H5ARJ4</a></td>
<td class=val>Pseudomonas trivialis</td>
<td class=val>Complete Genome Sequence of the Rhizobacterium Pseudomonas trivialis Strain IHBB745 with Multiple Plant Growth-Promoting Activities and Tolerance to Desiccation and Alkalinity.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/genomea.00943-15" target="_other">https://dx.doi.org/doi:10.1128/genomea.00943-15</a></td></tr><tr class=odd><td class=val>90</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/W2DZG2" target="_other">http://purl.uniprot.org/uniprot/W2DZG2</a></td>
<td class=val>Pseudomonas sp. FH1</td>
<td class=val>The rulB gene of plasmid pWW0 is a hotspot for the site-specific insertion of integron-like elements found in the chromosomes of environmental Pseudomonas fluorescens group bacteria.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1111/1462-2920.12345" target="_other">https://dx.doi.org/doi:10.1111/1462-2920.12345</a></td></tr><tr class=even><td class=val>90</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A829MQ51" target="_other">http://purl.uniprot.org/uniprot/A0A829MQ51</a></td>
<td class=val>Pseudomonas fluorescens BBc6R8</td>
<td class=val>Genome Sequence of the Mycorrhizal Helper Bacterium Pseudomonas fluorescens BBc6R8.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/genomea.01152-13" target="_other">https://dx.doi.org/doi:10.1128/genomea.01152-13</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/Q888J1" target="_other">http://purl.uniprot.org/uniprot/Q888J1</a></td>
<td class=val>Pseudomonas syringae pv. tomato (strain ATCC BAA-871 / DC3000)</td>
<td class=val>The complete genome sequence of the Arabidopsis and tomato pathogen Pseudomonas syringae pv. tomato DC3000.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1073/pnas.1731982100" target="_other">https://dx.doi.org/doi:10.1073/pnas.1731982100</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/J2WKT6" target="_other">http://purl.uniprot.org/uniprot/J2WKT6</a></td>
<td class=val>Pseudomonas sp. GM79</td>
<td class=val>Twenty-one genome sequences from Pseudomonas species and 19 genome sequences from diverse bacteria isolated from the rhizosphere and endosphere of Populus deltoides.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.01243-12" target="_other">https://dx.doi.org/doi:10.1128/jb.01243-12</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/J2RZY5" target="_other">http://purl.uniprot.org/uniprot/J2RZY5</a></td>
<td class=val>Pseudomonas sp. GM48</td>
<td class=val>Twenty-one genome sequences from Pseudomonas species and 19 genome sequences from diverse bacteria isolated from the rhizosphere and endosphere of Populus deltoides.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.01243-12" target="_other">https://dx.doi.org/doi:10.1128/jb.01243-12</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/J3GHR5" target="_other">http://purl.uniprot.org/uniprot/J3GHR5</a></td>
<td class=val>Pseudomonas sp. GM50</td>
<td class=val>Twenty-one genome sequences from Pseudomonas species and 19 genome sequences from diverse bacteria isolated from the rhizosphere and endosphere of Populus deltoides.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.01243-12" target="_other">https://dx.doi.org/doi:10.1128/jb.01243-12</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/J2NRJ0" target="_other">http://purl.uniprot.org/uniprot/J2NRJ0</a></td>
<td class=val>Pseudomonas sp. GM18</td>
<td class=val>Twenty-one genome sequences from Pseudomonas species and 19 genome sequences from diverse bacteria isolated from the rhizosphere and endosphere of Populus deltoides.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.01243-12" target="_other">https://dx.doi.org/doi:10.1128/jb.01243-12</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/J3BN19" target="_other">http://purl.uniprot.org/uniprot/J3BN19</a></td>
<td class=val>Pseudomonas sp. GM67</td>
<td class=val>Twenty-one genome sequences from Pseudomonas species and 19 genome sequences from diverse bacteria isolated from the rhizosphere and endosphere of Populus deltoides.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.01243-12" target="_other">https://dx.doi.org/doi:10.1128/jb.01243-12</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/J2M3Y6" target="_other">http://purl.uniprot.org/uniprot/J2M3Y6</a></td>
<td class=val>Pseudomonas sp. GM102</td>
<td class=val>Twenty-one genome sequences from Pseudomonas species and 19 genome sequences from diverse bacteria isolated from the rhizosphere and endosphere of Populus deltoides.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/jb.01243-12" target="_other">https://dx.doi.org/doi:10.1128/jb.01243-12</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/Q8RSY5" target="_other">http://purl.uniprot.org/uniprot/Q8RSY5</a></td>
<td class=val>Pseudomonas fluorescens</td>
<td class=val>Adaptive divergence in experimental populations of Pseudomonas fluorescens. I. Genetic and phenotypic bases of wrinkly spreader fitness.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1093/genetics/161.1.33" target="_other">https://dx.doi.org/doi:10.1093/genetics/161.1.33</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A161ZG69" target="_other">http://purl.uniprot.org/uniprot/A0A161ZG69</a></td>
<td class=val>Pseudomonas fluorescens</td>
<td class=val>Mutant phenotypes for thousands of bacterial genes of unknown function.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1038/s41586-018-0124-0" target="_other">https://dx.doi.org/doi:10.1038/s41586-018-0124-0</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A147GXL8" target="_other">http://purl.uniprot.org/uniprot/A0A147GXL8</a></td>
<td class=val>Pseudomonas psychrotolerans</td>
<td class=val>Genomic Resource of Rice Seed Associated Bacteria.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2015.01551" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2015.01551</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A2W0EWR7" target="_other">http://purl.uniprot.org/uniprot/A0A2W0EWR7</a></td>
<td class=val>Pseudomonas jessenii</td>
<td class=val>Characterization of the caprolactam degradation pathway in Pseudomonas jessenii using mass spectrometry-based proteomics.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1007/s00253-018-9073-7" target="_other">https://dx.doi.org/doi:10.1007/s00253-018-9073-7</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A8E6ERY0" target="_other">http://purl.uniprot.org/uniprot/A0A8E6ERY0</a></td>
<td class=val>Pseudomonas qingdaonensis</td>
<td class=val>A newly isolated Pseudomonas putida S-1 strain for batch-mode-propanethiol degradation and continuous treatment of propanethiol-containing waste gas.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1016/j.jhazmat.2015.09.063" target="_other">https://dx.doi.org/doi:10.1016/j.jhazmat.2015.09.063</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A7Y1N448" target="_other">http://purl.uniprot.org/uniprot/A0A7Y1N448</a></td>
<td class=val>Pseudomonas oryzihabitans</td>
<td class=val>Genetic Organization of the &lt;i&gt;aprX-lipA2&lt;/i&gt; Operon Affects the Proteolytic Potential of &lt;i&gt;Pseudomonas&lt;/i&gt; Species in Milk.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2020.01190" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2020.01190</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A7Y1N448" target="_other">http://purl.uniprot.org/uniprot/A0A7Y1N448</a></td>
<td class=val>Pseudomonas oryzihabitans</td>
<td class=val>Genetic Organization of the aprX-lipA2 Operon Affects the Proteolytic Potential of Pseudomonas Species in Milk.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2020.01190" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2020.01190</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A5M9ICB8" target="_other">http://purl.uniprot.org/uniprot/A0A5M9ICB8</a></td>
<td class=val>Pseudomonas panacis</td>
<td class=val>Genetic Organization of the &lt;i&gt;aprX-lipA2&lt;/i&gt; Operon Affects the Proteolytic Potential of &lt;i&gt;Pseudomonas&lt;/i&gt; Species in Milk.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2020.01190" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2020.01190</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A5M9ICB8" target="_other">http://purl.uniprot.org/uniprot/A0A5M9ICB8</a></td>
<td class=val>Pseudomonas panacis</td>
<td class=val>Genetic Organization of the aprX-lipA2 Operon Affects the Proteolytic Potential of Pseudomonas Species in Milk.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2020.01190" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2020.01190</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A077LSB0" target="_other">http://purl.uniprot.org/uniprot/A0A077LSB0</a></td>
<td class=val>Pseudomonas sp. StFLB209</td>
<td class=val>Complete Genome Sequence of N-Acylhomoserine Lactone-Producing Pseudomonas sp. Strain StFLB209, Isolated from Potato Phyllosphere.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/genomea.01037-14" target="_other">https://dx.doi.org/doi:10.1128/genomea.01037-14</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/F3IQG6" target="_other">http://purl.uniprot.org/uniprot/F3IQG6</a></td>
<td class=val>Pseudomonas amygdali pv. lachrymans str. M302278</td>
<td class=val>Dynamic evolution of pathogenicity revealed by sequencing and comparative genomics of 19 Pseudomonas syringae isolates.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1371/journal.ppat.1002132" target="_other">https://dx.doi.org/doi:10.1371/journal.ppat.1002132</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/S6SJ46" target="_other">http://purl.uniprot.org/uniprot/S6SJ46</a></td>
<td class=val>Pseudomonas syringae pv. actinidiae ICMP 18807</td>
<td class=val>Genomic analysis of the Kiwifruit pathogen Pseudomonas syringae pv. actinidiae provides insight into the origins of an emergent plant disease.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1371/journal.ppat.1003503" target="_other">https://dx.doi.org/doi:10.1371/journal.ppat.1003503</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/S6SPB9" target="_other">http://purl.uniprot.org/uniprot/S6SPB9</a></td>
<td class=val>Pseudomonas syringae pv. actinidiae ICMP 18807</td>
<td class=val>Genomic analysis of the Kiwifruit pathogen Pseudomonas syringae pv. actinidiae provides insight into the origins of an emergent plant disease.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1371/journal.ppat.1003503" target="_other">https://dx.doi.org/doi:10.1371/journal.ppat.1003503</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A261WJQ2" target="_other">http://purl.uniprot.org/uniprot/A0A261WJQ2</a></td>
<td class=val>Pseudomonas avellanae</td>
<td class=val>Genome analysis of the kiwifruit canker pathogen Pseudomonas syringae pv. actinidiae biovar 5.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1038/srep21399" target="_other">https://dx.doi.org/doi:10.1038/srep21399</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A4P6QNS5" target="_other">http://purl.uniprot.org/uniprot/A0A4P6QNS5</a></td>
<td class=val>Pseudomonas syringae</td>
<td class=val>The &lt;i&gt;Ptr1&lt;/i&gt; Locus of &lt;i&gt;Solanum lycopersicoides&lt;/i&gt; Confers Resistance to Race 1 Strains of &lt;i&gt;Pseudomonas syringae&lt;/i&gt; pv. &lt;i&gt;tomato&lt;/i&gt; and to &lt;i&gt;Ralstonia pseudosolanacearum&lt;/i&gt; by Recognizing the Type III Effectors AvrRpt2 and RipBN.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1094/mpmi-01-19-0018-r" target="_other">https://dx.doi.org/doi:10.1094/mpmi-01-19-0018-r</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A4P6QNS5" target="_other">http://purl.uniprot.org/uniprot/A0A4P6QNS5</a></td>
<td class=val>Pseudomonas syringae</td>
<td class=val>The Ptr1 locus of Solanum lycopersicoides confers resistance to race 1 strains of Pseudomonas syringae pv. tomato and to Ralstonia pseudosolanacearum by recognizing the type III effectors AvrRpt2/RipBN.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1094/mpmi-01-19-0018-r" target="_other">https://dx.doi.org/doi:10.1094/mpmi-01-19-0018-r</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A0H5ARJ4" target="_other">http://purl.uniprot.org/uniprot/A0A0H5ARJ4</a></td>
<td class=val>Pseudomonas trivialis</td>
<td class=val>Complete Genome Sequence of the Rhizobacterium Pseudomonas trivialis Strain IHBB745 with Multiple Plant Growth-Promoting Activities and Tolerance to Desiccation and Alkalinity.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/genomea.00943-15" target="_other">https://dx.doi.org/doi:10.1128/genomea.00943-15</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A5B7VT39" target="_other">http://purl.uniprot.org/uniprot/A0A5B7VT39</a></td>
<td class=val>Pseudomonas sp. MPC6</td>
<td class=val>Exploiting the natural poly(3-hydroxyalkanoates) production capacity of Antarctic Pseudomonas strains: from unique phenotypes to novel biopolymers.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1007/s10295-019-02186-2" target="_other">https://dx.doi.org/doi:10.1007/s10295-019-02186-2</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A5B7VT39" target="_other">http://purl.uniprot.org/uniprot/A0A5B7VT39</a></td>
<td class=val>Pseudomonas sp. MPC6</td>
<td class=val>In-Depth Genomic and Phenotypic Characterization of the Antarctic Psychrotolerant Strain &lt;i&gt;Pseudomonas&lt;/i&gt; sp. MPC6 Reveals Unique Metabolic Features, Plasticity, and Biotechnological Potential.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2019.01154" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2019.01154</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A5B7VT39" target="_other">http://purl.uniprot.org/uniprot/A0A5B7VT39</a></td>
<td class=val>Pseudomonas sp. MPC6</td>
<td class=val>In-Depth Genomic and Phenotypic Characterization of the Antarctic Psychrotolerant Strain Pseudomonas sp. MPC6 Reveals Unique Metabolic Features, Plasticity, and Biotechnological Potential.</td>
<td class=val><a href="https://dx.doi.org/doi:10.3389/fmicb.2019.01154" target="_other">https://dx.doi.org/doi:10.3389/fmicb.2019.01154</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/W2DZG2" target="_other">http://purl.uniprot.org/uniprot/W2DZG2</a></td>
<td class=val>Pseudomonas sp. FH1</td>
<td class=val>The rulB gene of plasmid pWW0 is a hotspot for the site-specific insertion of integron-like elements found in the chromosomes of environmental Pseudomonas fluorescens group bacteria.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1111/1462-2920.12345" target="_other">https://dx.doi.org/doi:10.1111/1462-2920.12345</a></td></tr><tr class=odd><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A829MQ51" target="_other">http://purl.uniprot.org/uniprot/A0A829MQ51</a></td>
<td class=val>Pseudomonas fluorescens BBc6R8</td>
<td class=val>Genome Sequence of the Mycorrhizal Helper Bacterium Pseudomonas fluorescens BBc6R8.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1128/genomea.01152-13" target="_other">https://dx.doi.org/doi:10.1128/genomea.01152-13</a></td></tr><tr class=even><td class=val>50</td>
<td class=val><a href="http://purl.uniprot.org/uniprot/A0A0N0VKM3" target="_other">http://purl.uniprot.org/uniprot/A0A0N0VKM3</a></td>
<td class=val>Pseudomonas fuscovaginae</td>
<td class=val>Rice-Infecting Pseudomonas Genomes Are Highly Accessorized and Harbor Multiple Putative Virulence Mechanisms to Cause Sheath Brown Rot.</td>
<td class=val><a href="https://dx.doi.org/doi:10.1371/journal.pone.0139256" target="_other">https://dx.doi.org/doi:10.1371/journal.pone.0139256</a></td></tr></table><div class="tinfo">Total: 36, Shown: 36</div></div>


From here, we could now try to extract additional information on our focal protein (gene) from the cited papers. 
Recently, Yu et al (http://arxiv.org/abs/2203.09975) described a promising approach to automated knowledge extraction from scientific papers and knowledge graph construction. Unfortunately, their online service (https://bios.idea.edu.cn/) does not provide a SPARQL endpoint.



```sparql

```
