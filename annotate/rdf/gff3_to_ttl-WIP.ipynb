{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Turning a genome annotation into a knowledge graph"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "In this article, I will describe the steps taken to generate a RDF (Resource Description Format) datastructure starting from a gff3 formatted genome annotation file. The annotation file in question is the new reference annotation for *Pseudomonas fluorescens* strain SBW25.\n",
    "\n",
    "## Required packages\n",
    "I will make use of the following python packages:\n",
    "* gffutils to read the gff3 file into a sqlite database.\n",
    "* rdflib to construct the rdf graph.\n",
    "* requests to fetch data (e.g. ontology files)\n",
    "\n",
    "All packages can be installed via conda from the conda-forge channel or pypi."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import gffutils\n",
    "from tqdm import tqdm\n",
    "from SPARQLWrapper import SPARQLWrapper as sw\n",
    "from SPARQLWrapper import TURTLE\n",
    "from rdflib import URIRef, Literal, Namespace, Graph, BNode\n",
    "from rdflib.namespace import CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, \\\n",
    "                           PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, \\\n",
    "                           VOID, XMLNS, XSD\n",
    "\n",
    "import requests\n",
    "import re\n",
    "from rdflib.store import Store\n",
    "from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore\n",
    "from rdflib.graph import DATASET_DEFAULT_GRAPH_ID as default"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "rdflib.term.URIRef('urn:x-rdflib:default')"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "default"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last import statement loads the *magic* url for the default graph in the rdf triple store into which we will eventually load the rdf graph."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the gff3 file\n",
    "To load the gff3 file for the first time, I run\n",
    "```python\n",
    "db = gffutils.create_db(MPBAS00001.gff3, MPBAS00001.gff3.db)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This creates the sqlite database file *MPBAS00001.gff3.db* and loads the database into memory.\n",
    "Subsequently, I can load the database directly from the database file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "db = gffutils.FeatureDB(\"MPBAS00001.gff3.db\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each genome feature can now be accessed via it's feature ID, e.g."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 70,
   "metadata": {},
   "outputs": [],
   "source": [
    "feat = db['CDS:PFLU_0409-0']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "would load the CDS in the locus tagged as \"PFLU_0409\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the feature object, I can query all information given in the gff3 file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 71,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'CDS'"
      ]
     },
     "execution_count": 71,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat.featuretype"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 72,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "452623"
      ]
     },
     "execution_count": 72,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat.start"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "453588"
      ]
     },
     "execution_count": 73,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat.end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'+'"
      ]
     },
     "execution_count": 74,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat.strand"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'CDS:PFLU_0409-0'"
      ]
     },
     "execution_count": 75,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat.id"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'MPBAS00001'"
      ]
     },
     "execution_count": 76,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# The chromosome on which this feature is located.\n",
    "feat.chrom"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'0'"
      ]
     },
     "execution_count": 81,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# CDS coding frame.\n",
    "feat.frame"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'ena'"
      ]
     },
     "execution_count": 82,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat.source"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Further annotations that would be found in the 9th column of the gff3 file are accessible via the `attributes` member of the `feat` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Dbxref: ['EMBL:AM181176', 'InterPro:IPR007445', 'InterPro:IPR007446', 'InterPro:IPR014717', 'KEGG:pfs:PFLU_0409', 'OrthoDB:1803493at2', 'Pfam:PF04350', 'Pfam:PF04351', 'RefSeq:WP_012721815.1', 'STRING:216595.PFLU_0409', 'eggNOG:COG3167', 'eggNOG:COG3168', 'Pubmed:19432983']\n",
      "ID: ['CDS:PFLU_0409-0']\n",
      "Ontology_term: ['GO:0043107', 'GO:0043683']\n",
      "Parent: ['transcript:PFLU_0409-0']\n",
      "codon_start: ['1']\n",
      "confidence_level: ['3']\n",
      "frame: ['0']\n",
      "gene: ['PFLU_0409']\n",
      "inference: ['Predicted']\n",
      "locus_tag: ['PFLU_0409']\n",
      "note: ['N-terminus similar to Pseudomonas syringae PilO andC-terminus similar to Pseudomonas syringae PilP']\n",
      "product: ['Putative fimbriae biogenesis-related fusion protein']\n",
      "protein_id: ['CAY46686.1']\n",
      "seqid: ['MPBAS00001']\n",
      "similarity: ['fasta; with=UniProt:Q52542; Pseudomonas syringae.; pilP; Pilus expression protein.; length=175; id 41.781%; ungapped id 43.571%; E()=6.9e-14; 146 aa overlap; query 181-321; subject 31-175', 'fasta; with=UniProt:Q87V11; Pseudomonas syringae (pv. tomato).; pilO; Type IV pilus biogenesis protein PilO.; length=207; id 36.735%; ungapped id 37.696%; E()=1.6e-17; 196 aa overlap; query 3-193; subject 10-205']\n",
      "uniprot_annotation_score: ['1 out of 5']\n",
      "uniprot_review_status: ['unreviewed']\n"
     ]
    }
   ],
   "source": [
    "print(feat.attributes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`feat.attributes` returns a dictionary, each item is a list (iterable)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For further information about working with `gffutils` data structures and functions, refer to the [reference manual](https://gffutils.rtfd.io)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The gff3 file had a number of issues, e.g. some Dbxref entries were not formatted correctly. The next three cells clean up these issues. Note that each alteration\n",
    "is committed back to the database file by first deleting all features that had to be fixed and then updating the database with the altered features."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "# debug pubmed.\n",
    "updates = []\n",
    "for feat in db.all_features():\n",
    "    dbxrefs = feat.attributes.get(\"Dbxref\", [])\n",
    "    \n",
    "    indices_to_remove = []\n",
    "    for i,dbxref in enumerate(dbxrefs):\n",
    "        if dbxref.startswith(\"Pubmed\"):\n",
    "            if \";\" in dbxref:\n",
    "                new_dbxrefs = []\n",
    "                indices_to_remove.append(i)\n",
    "                pubmed_ids = dbxref.split(\":\")[1].replace(\" \",\"\").split(\";\")\n",
    "                pubmed_ids = [pmid for pmid in pubmed_ids if pmid != '']\n",
    "            \n",
    "                for pmid in pubmed_ids:\n",
    "                    dbxrefs.append(\"Pubmed:{}\".format(pmid))\n",
    "                    \n",
    "    if len(indices_to_remove) > 0:\n",
    "        for i in indices_to_remove:\n",
    "            dbxrefs.pop(i)\n",
    "\n",
    "        updates.append(feat)\n",
    "    else:\n",
    "        continue\n",
    "        \n",
    "    \n",
    "                "
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "# split dbxrefs with ;.\n",
    "updates = []\n",
    "for feat in db.all_features():\n",
    "    dbxrefs = feat.attributes.get(\"Dbxref\", [])\n",
    "    \n",
    "    indices_to_remove = []\n",
    "    for i,dbxref in enumerate(dbxrefs):\n",
    "        if \";\" in dbxref:\n",
    "            new_dbxrefs = []\n",
    "            indices_to_remove.append(i)\n",
    "            splt = dbxref.split(\":\")\n",
    "            dbkey = splt[0]\n",
    "            ids = \":\".join(splt[1:]).replace(\" \",\"\").split(\";\")\n",
    "            ids = [idx for idx in ids if idx != '']\n",
    "\n",
    "            for idx in ids:\n",
    "                dbxrefs.append(\"{}:{}\".format(dbkey, idx))\n",
    "                    \n",
    "    if len(indices_to_remove) > 0:\n",
    "        for i in indices_to_remove:\n",
    "            dbxrefs.pop(i)\n",
    "\n",
    "        updates.append(feat)\n",
    "    else:\n",
    "        continue\n",
    "        \n",
    "    \n",
    "                "
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "# split GO/EC terms with ;.\n",
    "updates = []\n",
    "for feat in db.all_features():\n",
    "    ontos = feat.attributes.get(\"Ontology_term\", [])\n",
    "    \n",
    "    indices_to_remove = []\n",
    "    for i,dbxref in enumerate(ontos):\n",
    "        if \";\" in dbxref:\n",
    "            new_ontos = []\n",
    "            indices_to_remove.append(i)\n",
    "            splt = dbxref.split(\":\")\n",
    "            dbkey = splt[0]\n",
    "            ids = \":\".join(splt[1:]).replace(\" \",\"\").split(\";\")\n",
    "            ids = [idx for idx in ids if idx != '']\n",
    "\n",
    "            for idx in ids:\n",
    "                ontos.append(\"{}:{}\".format(dbkey, idx))\n",
    "                    \n",
    "    if len(indices_to_remove) > 0:\n",
    "        for i in indices_to_remove:\n",
    "            ontos.pop(i)\n",
    "\n",
    "        updates.append(feat)\n",
    "    else:\n",
    "        continue\n",
    "        \n",
    "    \n",
    "                "
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "db = db.delete(updates)\n",
    "db = db.update(updates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dbxref keys\n",
    "In the rdf graph, I want to express database cross-references as rdf objects through their unique resource identifier (URI). First, I need to find all dbxref keys (the part before the \":\" in a dbxref entry) in my gff3 file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get all dbxref keys\n",
    "dbxref_keys = []\n",
    "for feat in db.all_features():\n",
    "    dbxrefs = feat.attributes.get('Dbxref', [])\n",
    "    for dbxref in dbxrefs:\n",
    "        key = dbxref.split(':')[0]\n",
    "        if not key in dbxref_keys:\n",
    "            dbxref_keys.append(key)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['EMBL',\n",
       " 'GeneID',\n",
       " 'HAMAP',\n",
       " 'InterPro',\n",
       " 'KEGG',\n",
       " 'OrthoDB',\n",
       " 'PANTHER',\n",
       " 'PRIDE',\n",
       " 'PRINTS',\n",
       " 'Pfam',\n",
       " 'RefSeq',\n",
       " 'STRING',\n",
       " 'SUPFAM',\n",
       " 'TIGRFAMs',\n",
       " 'eggNOG',\n",
       " 'Pubmed',\n",
       " 'UniPathway',\n",
       " 'PSEUDO',\n",
       " 'PseudoCAP',\n",
       " 'RFAM',\n",
       " 'EC']"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dbxref_keys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, I will define the generic part of the database URI as a `rdflib.Namespace`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "dbxref_prefixes = {\n",
    "    'GeneID': Namespace(\"https://www.ncbi.nlm.nih.gov/gene/\"),\n",
    "    'InterPro': Namespace(\"http://purl.uniprot.org/interpro/\"),\n",
    "    'KEGG': Namespace(\"http://purl.uniprot.org/kegg/\"),\n",
    "    'OrthoDB': Namespace(\"http://purl.orthodb.org/odbgroup/\"),\n",
    "    'PANTHER': Namespace(\"http://purl.uniprot.org/panther/\"),\n",
    "    'PRIDE': Namespace(\"http://purl.uniprot.org/pride\"),\n",
    "    'PRINTS': Namespace(\"http://purl.uniprot.org/prints/\"),\n",
    "    'Pfam': Namespace(\"http://purl.uniprot.org/pfam/\"),\n",
    "    'RefSeq': Namespace(\"http://purl.uniprot.org/refseq/\"),\n",
    "    'STRING': Namespace(\"http://purl.uniprot.org/string/\"),\n",
    "    'SUPFAM': Namespace(\"http://purl.uniprot.org/supfam\"),\n",
    "    'TIGRFAMs': Namespace(\"http://purl.uniprot.org/tigrfams/\"),\n",
    "    'eggNOG': Namespace(\"http://purl.uniprot.org/eggnog/\"),\n",
    "    'Pubmed': Namespace(\"https://pubmed.ncbi.nlm.nih.gov/\"),\n",
    "    'PseudoCAP': Namespace(\"https://www.pseudomonas.com/feature/show?id=\"),\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the sequence ontology\n",
    "Featuretypes are to be expressed via their sequence ontology (SO) identifier. Here, I load the sequence ontology file as a json dictionary by issuing a GET request."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load SO\n",
    "so_json = requests.get('https://raw.githubusercontent.com/The-Sequence-Ontology/SO-Ontologies/master/Ontology_Files/so.json').json()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The loaded json structure exposes its data under the 'graph' item. The 'graph' item is a list of graphs, here it has only one element. From this graph element, I need\n",
    "the 'nodes' item."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "metadata": {},
   "outputs": [],
   "source": [
    "so_nodes = so_json['graphs'][0]['nodes']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now I define a function that returns the SO feature ID for a given feature type as reported by the gff3 feature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_so_id(featuretype):\n",
    "    hits = filter(lambda x:x.get('lbl', \"\")==featuretype, so_nodes)\n",
    "    \n",
    "    return hits"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I will build the rdf graph by utilizing established domain specific ontologies. Here, I define the ontology namespaces. The `pflu` namespace is defined such\n",
    "that `pflu:<ID>` with a valid feature ID will resolve to the corresponding feature website at the [Pseudomonas fluorescens SBW25 genome database](https://pflu.evolbio.mpg.de)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "metadata": {},
   "outputs": [],
   "source": [
    "pflu = Namespace(r'http://pflu.evolbio.mpg.de/bio_data/')\n",
    "so = Namespace(r'http://purl.obolibrary.org/obo/so/')\n",
    "up = Namespace(r'http://purl.uniprot.org/core/')\n",
    "gffo = Namespace(r'https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#')\n",
    "obo = Namespace(r'http://purl.obolibrary.org/obo/')\n",
    "go = Namespace(r'http://amigo.geneontology.org/amigo/term/')\n",
    "ec = Namespace(r'https://www.brenda-enzymes.org/enzyme.php?ecno=')\n",
    "faldo = Namespace(r'http://biohackathon.org/resource/faldo#')\n",
    "gfvo = Namespace(r'http://github.com/BioInterchange/Ontologies/gfvo#')\n",
    "ogo = Namespace(r'http://protozoadb.biowebdb.org/22/ogroup#')\n",
    "edam = Namespace('http://edamontology.org/')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The aforementioned Pflu SBW25 database exposes its data through a public API at https://pflu.evolbio.mpg.de/web-services. E.g. the next code cell fetches all features\n",
    "of type `biological_region` as a json object. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 86,
   "metadata": {},
   "outputs": [],
   "source": [
    "biological_region_json = requests.get('https://pflu.evolbio.mpg.de/web-services/content/v0.1/Biological_Region').json()\n",
    "# ncrna_gene_json = requests.get('https://pflu.evolbio.mpg.de/web-services/content/v0.1/NcRNA_Gene/').json()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, I define a function that returns a given features json object. If `featuretype` is `biological_region`, I return the prefetched object that I just defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 87,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_pflu_json(feat=None, feat_id=None):\n",
    "    if feat is None:\n",
    "        feat = db[feat_id]\n",
    "        \n",
    "    feat_type = feat.featuretype\n",
    "    \n",
    "    pflu_search = 'https://pflu.evolbio.mpg.de/web-services/content/v0.1/{}?identifier={}'.format(feat_type, feat.id)\n",
    "    \n",
    "    if feat_type in ['ncRNA_gene', 'chromosome', 'pseudogenic_CDS']:\n",
    "        return {}\n",
    "        \n",
    "    elif re.match('biological_region', feat_type):\n",
    "        return biological_region_json\n",
    "        \n",
    "    response = requests.get(pflu_search).json()\n",
    "    \n",
    "    return response"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, I code up a function that returns for a given gff3 feature its representation as a list of rdf nodes. This function works as follows:\n",
    "* Retrieve the feature's json object from the Pflu SBW25 database.\n",
    "* Extract the Pflu SBW25 DB ID from the json object.\n",
    "* Define the subject as the URI `pflu:ID` which resolves to the Pflu SBW25 DB site for the given feature.\n",
    "* Define the subject's type through the SO term of the feature's type.\n",
    "* Add the Pflu SBW25 DB URI as a rdfs:seeAlso property value.\n",
    "* Iterate over top level feature attributes (source, seqid, start, end, strand, frame, score) and define a node for each one. The property is anchored at the\n",
    "newly defined `gffo` ontology, with the appropriate namespace defined above\n",
    "* Add GO and EC annotations as nodes with property values anchored at the respective ontologies namespaces.\n",
    "* Add database crossreferences (Dbxref) as rdfs:seeAlso property values.\n",
    "* Add remaining attributes from column 9 of the gff3 files via the properties of th `gffo` namespace.\n",
    "* Uniprot review status is given as a `up:reviewed` property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [],
   "source": [
    "def feature_to_rdf(feat=None):\n",
    "    \"\"\" Returns a serialized rdf string for the given feature\"\"\"\n",
    "    nodes = []\n",
    "    # Get feature id in database.\n",
    "\n",
    "    response = get_pflu_json(feat)\n",
    "    pflu_db_id = response['member'][-1]['ItemPage'].split(\"/\")[-1]\n",
    "    so_id = next(get_so_id(feat.featuretype))['id']\n",
    "    \n",
    "    # pflu db id\n",
    "    subject = pflu.term(pflu_db_id)\n",
    "    nodes.append((subject, RDF.type, gffo.Feature))\n",
    "    nodes.append((subject, RDF.type, URIRef(so_id)) )\n",
    "    nodes.append((subject, RDFS.seeAlso, pflu.term(pflu_db_id)))\n",
    "    \n",
    "    # source\n",
    "    nodes.append((subject, gffo.source, Literal(feat.source)))\n",
    "    \n",
    "    # seqid\n",
    "    nodes.append((subject, gffo.seqid, Literal(feat.seqid)))\n",
    "    \n",
    "    # position\n",
    "    nodes.append((subject, gffo.start, Literal(feat.start)))\n",
    "    nodes.append((subject, gffo.end, Literal(feat.end)))\n",
    "    nodes.append((subject, gffo.strand, Literal(feat.strand)))\n",
    "    \n",
    "#     position = BNode()\n",
    "#     nodes.append((position, RDF.type, {'+': faldo.ForwardStrandPosition,\n",
    "#                                        '-': faldo.ReverseStrandPosition}[feat.strand]))\n",
    "#     nodes.append((position, faldo.begin, Literal(feat.start)))\n",
    "#     nodes.append((position, faldo.end, Literal(feat.end)))\n",
    "#     nodes.append((subject, faldo.position, position))\n",
    "    \n",
    "    # score\n",
    "    nodes.append((subject, gffo.score, Literal(feat.score)))\n",
    "    \n",
    "    # phase/frame\n",
    "    nodes.append((subject, gffo.frame, Literal(feat.frame)))\n",
    "   \n",
    "    for key, vals in feat.attributes.items():\n",
    "        if re.match('Ontology_term', key) is not None:\n",
    "            for ot in vals:\n",
    "                prefix = ot.split(\":\")[0]\n",
    "                if re.match(\"EC\", prefix) is not None: \n",
    "                    obj = ec.term(ot.split(\":\")[1])\n",
    "                elif re.match(\"GO\", prefix) is not None: \n",
    "                    obj = go.term(ot)\n",
    "                go_node = (\n",
    "                                subject,\n",
    "                                gffo.Ontology_term,\n",
    "                                obj\n",
    "                )\n",
    "                \n",
    "                nodes.append(go_node)\n",
    "                \n",
    "        elif re.match('Dbxref', key) is not None:\n",
    "            for val in vals:\n",
    "                splt = val.split(\":\")\n",
    "                dbkey = splt[0]\n",
    "                dbval = \":\".join(splt[1:])\n",
    "                if dbkey not in dbxref_prefixes.keys():\n",
    "                    continue\n",
    "                    \n",
    "                target = URIRef(dbxref_prefixes[dbkey].term(dbval))\n",
    "                node = (\n",
    "                                subject,\n",
    "                                RDFS.seeAlso,\n",
    "                                target\n",
    "                )\n",
    "                nodes.append(node)\n",
    "        # Add protein id as cross ref to embl-cds\n",
    "        elif re.match('protein_id', key) is not None:\n",
    "            for val in vals:\n",
    "                nodes.append((\n",
    "                                subject,\n",
    "                                gffo.term(key),\n",
    "                                Literal(val)\n",
    "                ))\n",
    "                nodes.append((subject,\n",
    "                              RDFS.seeAlso,\n",
    "                              URIRef(val, base=\"http://purl.uniprot.org/embl-cds/\")\n",
    "                             ))\n",
    "            \n",
    "            # Add pfludb id as crossref.\n",
    "                \n",
    "        elif re.match('uniprot_review_status', key) is not None:\n",
    "            nodes.append ((\n",
    "                    subject,\n",
    "                    up.reviewed,\n",
    "                    {\"reviewed\": Literal(True), \"unreviewed\": Literal(False)}[vals[0]]\n",
    "            ))\n",
    "        else:\n",
    "            for val in vals:\n",
    "                nodes.append((\n",
    "                                subject,\n",
    "                                gffo.term(key),\n",
    "                                Literal(val)\n",
    "                ))\n",
    "            \n",
    "    return nodes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define rdf nodes for each feature.\n",
    "Finally, I iterate over all features (using the `all_features()` function that returns an iterator). Some featuretypes cause problems when pulling their json representation from the Pflu SBW25 database, hence I skip those. All nodes are added to the previously defined empty graph (`rdflib.Graph`)."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "for feat in tqdm(db.all_features()):\n",
    "    if feat.featuretype in skipped_features:\n",
    "        continue\n",
    "    try:\n",
    "        nodes = feature_to_rdf(feat)\n",
    "    except:\n",
    "        print(feat)\n",
    "    for node in nodes:\n",
    "        graph.add(node)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The initial run took approximately 14 hours on a single core. Subsequently, I serialize the graph to *turtle* formatted rdf file."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "graph.serialize('MPBAS00001.ttl')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph = Graph(identifier=default)\n",
    "with open('MPBAS00001.ttl', 'r') as ifh:\n",
    "    graph.parse(file=ifh, format='ttl')\n",
    "# graph.bind('pflu', str(pflu))\n",
    "# graph.bind('so', str(so))\n",
    "# graph.bind('up', str(up))\n",
    "# graph.bind('gffo', str(gffo))\n",
    "# graph.bind('obo', str(obo))\n",
    "# graph.bind('faldo', str(faldo))\n",
    "# graph.bind('gfvo', str(gfvo))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "rdflib.term.URIRef('urn:x-rdflib:default')"
      ]
     },
     "execution_count": 58,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "graph.identifier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "19500it [00:09, 2106.14it/s]\n"
     ]
    }
   ],
   "source": [
    "for feat in tqdm(db.features_of_type('biological_region')):\n",
    "    if feat.featuretype in skipped_features:\n",
    "        continue\n",
    "    try:\n",
    "        nodes = feature_to_rdf(feat)\n",
    "    except:\n",
    "        print(feat)\n",
    "    for node in nodes:\n",
    "        graph.add(node)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<Graph identifier=urn:x-rdflib:default (<class 'rdflib.graph.Graph'>)>"
      ]
     },
     "execution_count": 61,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "graph.serialize('MPBAS00001.ttl')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    store.close()\n",
    "    del store\n",
    "except:\n",
    "    pass\n",
    "finally:\n",
    "    store = SPARQLUpdateStore()\n",
    "\n",
    "query_endpoint = 'http://localhost:3030/pflu/query'\n",
    "update_endpoint = 'http://localhost:3030/pflu/update'\n",
    "\n",
    "store.open((query_endpoint, update_endpoint))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [],
   "source": [
    "store.add_graph(graph)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jena",
   "language": "python",
   "name": "jena"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
