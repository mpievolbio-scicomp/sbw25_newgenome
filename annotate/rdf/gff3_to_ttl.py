#!/usr/bin/env python
# coding: utf-8

# In[1]:


import gffutils
from tqdm import tqdm
from SPARQLWrapper import SPARQLWrapper as sw
from SPARQLWrapper import TURTLE
from rdflib import URIRef, Literal, Namespace, Graph, BNode
from rdflib.namespace import CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL,                            PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME,                            VOID, XMLNS, XSD

import requests
import re
from rdflib.store import Store
from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore
from rdflib.graph import DATASET_DEFAULT_GRAPH_ID as default


db = gffutils.FeatureDB("MPBAS00001.gff3.db")

pflu = Namespace(r'http://pflu.evolbio.mpg.de/bio_data/')
so = Namespace(r'http://purl.obolibrary.org/obo/so/')
up = Namespace(r'http://purl.uniprot.org/core/')
gffo = Namespace(r'https://raw.githubusercontent.com/mpievolbio-scicomp/GenomeFeatureFormatOntology/main/gffo#')
obo = Namespace(r'http://purl.obolibrary.org/obo/')
go = Namespace(r'http://amigo.geneontology.org/amigo/term/')
ec = Namespace(r'https://www.brenda-enzymes.org/enzyme.php?ecno=')
faldo = Namespace(r'http://biohackathon.org/resource/faldo#')
gfvo = Namespace(r'http://github.com/BioInterchange/Ontologies/gfvo#')
ogo = Namespace(r'http://protozoadb.biowebdb.org/22/ogroup#')
edam = Namespace('http://edamontology.org/')


dbxref_prefixes = {
    'GeneID': Namespace("https://www.ncbi.nlm.nih.gov/gene/"),
    'InterPro': Namespace("http://purl.uniprot.org/interpro/"),
    'KEGG': Namespace("http://purl.uniprot.org/kegg/"),
    'OrthoDB': Namespace("http://purl.orthodb.org/odbgroup/"),
    'PANTHER': Namespace("http://purl.uniprot.org/panther/"),
    'PRIDE': Namespace("http://purl.uniprot.org/pride"),
    'PRINTS': Namespace("http://purl.uniprot.org/prints/"),
    'Pfam': Namespace("http://purl.uniprot.org/pfam/"),
    'RefSeq': Namespace("http://purl.uniprot.org/refseq/"),
    'STRING': Namespace("http://purl.uniprot.org/string/"),
    'SUPFAM': Namespace("http://purl.uniprot.org/supfam"),
    'TIGRFAMs': Namespace("http://purl.uniprot.org/tigrfams/"),
    'eggNOG': Namespace("http://purl.uniprot.org/eggnog/"),
    'Pubmed': Namespace("https://pubmed.ncbi.nlm.nih.gov/"),
    'PseudoCAP': Namespace("https://www.pseudomonas.com/feature/show?id="),
}

# Load SO
so_json = requests.get('https://raw.githubusercontent.com/The-Sequence-Ontology/SO-Ontologies/master/Ontology_Files/so.json').json()


so_nodes = so_json['graphs'][0]['nodes']

def get_so_id(featuretype):
    hits = filter(lambda x:x.get('lbl', "")==featuretype, so_nodes)
    
    return hits



def get_pflu_json(feat=None, feat_id=None):
    if feat is None:
        feat = db[feat_id]
        
    feat_type = feat.featuretype
    
    if feat_type in ['NcRNA_Gene', 'chromosome', 'pseudogenic_CDS']:
        return {}
        
    pflu_search = 'https://pflu.evolbio.mpg.de/web-services/content/v0.1/{}?identifier={}'.format(feat_type,
                                                                                            feat.id)
    response = requests.get(pflu_search).json()
    
    return response


def feature_to_rdf(feat=None):
    """ Returns a serialized rdf string for the given feature"""
    nodes = []
    # Get feature id in database.

    response = get_pflu_json(feat)
    pflu_db_id = response['member'][-1]['ItemPage'].split("/")[-1]
    so_id = next(get_so_id(feat.featuretype))['id']
    
    # pflu db id
    subject = pflu.term(pflu_db_id)
    nodes.append((subject, RDF.type, gffo.Feature))
    nodes.append((subject, RDF.type, URIRef(so_id)) )
    nodes.append((subject, RDFS.seeAlso, pflu.term(pflu_db_id)))
    
    # source
    nodes.append((subject, gffo.source, Literal(feat.source)))
    
    # seqid
    nodes.append((subject, gffo.seqid, Literal(feat.seqid)))
    
    # position
    nodes.append((subject, gffo.start, Literal(feat.start)))
    nodes.append((subject, gffo.end, Literal(feat.end)))
    nodes.append((subject, gffo.strand, Literal(feat.strand)))
    
#     position = BNode()
#     nodes.append((position, RDF.type, {'+': faldo.ForwardStrandPosition,
#                                        '-': faldo.ReverseStrandPosition}[feat.strand]))
#     nodes.append((position, faldo.begin, Literal(feat.start)))
#     nodes.append((position, faldo.end, Literal(feat.end)))
#     nodes.append((subject, faldo.position, position))
    
    # score
    nodes.append((subject, gffo.score, Literal(feat.score)))
    
    # phase/frame
    nodes.append((subject, gffo.frame, Literal(feat.frame)))
   
    for key, vals in feat.attributes.items():
        if re.match('Ontology_term', key) is not None:
            for ot in vals:
                prefix = ot.split(":")[0]
                if re.match("EC", prefix) is not None: 
                    obj = ec.term(ot.split(":")[1])
                elif re.match("GO", prefix) is not None: 
                    obj = go.term(ot)
                go_node = (
                                subject,
                                gffo.Ontology_term,
                                obj
                )
                
                nodes.append(go_node)
                
        elif re.match('Dbxref', key) is not None:
            for val in vals:
                splt = val.split(":")
                dbkey = splt[0]
                dbval = ":".join(splt[1:])
                if dbkey not in dbxref_prefixes.keys():
                    continue
                    
                target = URIRef(dbxref_prefixes[dbkey].term(dbval))
                node = (
                                subject,
                                RDFS.seeAlso,
                                target
                )
                nodes.append(node)
        # Add protein id as cross ref to embl-cds
        elif re.match('protein_id', key) is not None:
            for val in vals:
                nodes.append((
                                subject,
                                gffo.term(key),
                                Literal(val)
                ))
                nodes.append((subject,
                              RDFS.seeAlso,
                              URIRef(val, base="http://purl.uniprot.org/embl-cds/")
                             ))
            
            # Add pfludb id as crossref.
                
        elif re.match('uniprot_review_status', key) is not None:
            nodes.append ((
                    subject,
                    up.reviewed,
                    {"reviewed": Literal(True), "unreviewed": Literal(False)}[vals[0]]
            ))
        else:
            for val in vals:
                nodes.append((
                                subject,
                                gffo.term(key),
                                Literal(val)
                ))
            
    return nodes


graph = Graph()
graph.bind('pflu', str(pflu))
graph.bind('so', str(so))
graph.bind('up', str(up))
graph.bind('gffo', str(gffo))
graph.bind('obo', str(obo))
graph.bind('faldo', str(faldo))
graph.bind('gfvo', str(gfvo))




skipped_features = ['chromosome', 'ncRNA_gene', 'pseudogenic_CDS']


for feat in tqdm(db.all_features()):
    if feat.featuretype in skipped_features:
        continue
    try:
        nodes = feature_to_rdf(feat)
    except:
        print(feat)
    for node in nodes:
        graph.add(node)


graph.serialize("MPBAS00001.ttl")
