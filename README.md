# Latest version
## The latest version of the genome assembly and annotation is kept in ENASubmission/AssemblyUpdate/data

# Generated Annotations
| Code                                                     | Run (outdir)                                                                                                                                                                                                                                       | prokka version                 | Reference annotation                                                                                                                                                                                                    | comments                                                                                                                                           |
|----------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| prokka                                                   | [PROKKA_10082020-115800](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_10082020-115800)                                                                                                              | 1.14.5                         | GCA_000009225.1_ASM922v1_genomic.gbff (2015)                                                                                                                                                                            | Eric's run, with `--rfam` but barrnap/rnammer not installed; signalp v5                                                                            |
| prokka                                                   | [PROKKA_12172020-164400](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_12172020-164400)                                                                                                              | 1.12                           | None                                                                                                                                                                                                                    | no `--rfam`, first prokka "test" run                                                                                                               |
| prokka                                                   | [PROKKA_12172020-170600](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_12172020-170600)                                                                                                              | 1.12                           | None                                                                                                                                                                                                                    | with RNA flags                                                                                                                                     |
| prokka                                                   | [PROKKA_12182020-105900](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_12182020-105900)                                                                                                              | 1.14.5                         | ~~GCA_000009225.1_ASM922v1_genomic.gbff~~                                                                                                                                                                               | with `--rfam`, barrnap present, ref. ann. file not found at specified path                                                                         |
| prokka                                                   | [PROKKA_12182020-115100](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_12182020-115100)                                                                                                              | 1.14.5                         | GCA_000009225.1_ASM922v1_genomic.gbff (2015)                                                                                                                                                                            | with `--rfam` and barrnap present, finds same genes and ann. as 10082020                                                                           |
| prokka                                                   | [PROKKA_12182020-171100](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_12182020-171100)                                                                                                              | 1.14.5                         | ~~GCA_000009225.1_ASM922v1_genomic.gbff~~                                                                                                                                                                               | with `--rfam`, ref. ann. not found (wrong path), after rebuilding sprot database. no diff to 12182020-105900                                       |
| prokka                                                   | [PROKKA_12182020-213900](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_12182020-213900)                                                                                                              | 1.14.5                         | GCF_000009225.2_ASM922v1_genomic.gbff (2020)                                                                                                                                                                            | with `--rfam`, after rebuilding sprot database                                                                                                     |
| prokka                                                   | [PROKKA_03312021-140400](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_03312021-140400)                                                                                                              | 1.14.5                         | translated CDS                                                                                                                                                                                                          | no rrna, no trna, fast mode (transfer proteins only)                                                                                               |
| prokka                                                   | [PROKKA_03312021-144400](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_03312021-144400)                                                                                                              | 1.14.5                         | translated CDS                                                                                                                                                                                                          | --rfam --rnammer (no rrnas found)                                                                                                                  |
| pgap                                                     | [pgap_12212020-155500 (out)](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/pgap/sbw25_out.1) [pgap_12212020-155500 (in)](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/pgap/sbw25) | ncbi/pgap:2020-09-24.build4894 | None                                                                                                                                                                                                                    | No mRNA in output gff.                                                                                                                             |
| copy from GCA_000009225.1_ASM922v                        | [copy_from_GCA_9225v1_ASM922v1](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/copy_from_GCA_9225v1_ASM922v1)                                                                                                       | N/A                            | [Pseudomonas_fluorescens_sbw25_gca_000009225.ASM922v1.49.gff3](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/blob/master/reference_annotations/Pseudomonas_fluorescens_sbw25_gca_000009225.ASM922v1.49.gff3) | [notebook](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/blob/master/annotate/copy_from_GCA_9225v1_ASM922v1/extract_ref_features.ipynb) |
| copy from GCA_000009225.1_ASM922v with added uniprot ids | [copy_from_GCA_9225v1_ASM922v1](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/copy_from_GCA_9225v1_ASM922v1/gnl_MPB_PFLU_1-20210409.gff3)                                                                          | N/A                            | [Pseudomonas_fluorescens_sbw25_gca_000009225.ASM922v1.49.gff3](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/blob/master/reference_annotations/Pseudomonas_fluorescens_sbw25_gca_000009225.ASM922v1.49.gff3) | [notebook](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/blob/master/annotate/copy_from_GCA_9225v1_ASM922v1/add_dbxrefs.ipynb)          |
| prokka | [PROKKA_20211209](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/tree/master/annotate/prokka/PROKKA_20211209) | 1.14.5 | with --rfam, barrnap |

## Manual annotation work
We decided that the most appropriate route to a new genome annotation for the new genome assembly is to
1. Copy the features from the old manually annotated genome to the new assembly. This requires adjusting the feature coordinates.
2. Query the swissprot db for all features that are described as "putative" or "hypothetical" and check if new experimental evidence is present.
3. Optionally, refine the annotations by looking up protein families and orthologs.
### Copying features from old reference annotation.
We start from the ensembl refererence genome annotation. The file was retrieved from genbank and deposited in this repository: https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/blob/master/reference_annotations/GCA_000009225.1_ASM922v1_genomic.gff.gz .

The genome assembly (sequence) GCA_000009225.1_ASM922v1_genomic.fna.gz was retrieved from genbank and deposited [in this repository](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/blob/master/fasta/GCA_000009225.1_ASM922v1_genomic.fna.gz).

Both data are loaded into a `biopython` pipeline documented [here](https://gitlab.gwdg.de/c.fortmanngrote/sbw25_newgenome/-/blob/master/annotate/copy_from_GCA_9225v1_ASM922v1/extract_ref_features.ipynb) .
### dbxrefs
In a second step, cross reference ids were added to the new annotation. The corresponding notebook is 


# 2021-04-08
* Convert gff3 and fa files to genome assembly hub with makeHub.
## Workflow:
```shell
cd MakeHub
cp <fasta> .
cp <ref_annotation>.gff3 .
# Adjust fasta headers if needed (replace | by _, ...)
agat_convert_sp_gff2gtf.pl --gff ../annotate/copy_from_GCA_9225v1_ASM922v1/MPB_ASM001_20210401.gff3 -o gnl_MPB_PFLU_1.gtf
python make_hub.py -e computing@evolbio.mpg.de -l "gnl_MPB_PFLU_1" -L "Pseudomonas fluorescens SBW25 Assembly gnl_MPB_PFLU_1" -g gnl_MPB_PFLU_1.fa -a gnl_MPB_PFLU_1.gtf -N "Pseudomonas fluorescens SBW25" -V "gnl_MPB_PFLU_1" -o out
```
Then upload to server and add entry for new assembly to "genomes.txt" in top level hub directory.

## sparql endpoint
* installed askomics on the server (via docker-compose)
* can be accessed at 141.5.101.176:8079
* add remote endpoints via `abstractor`: 
  - nextprot
  - uniprot
  - kegg
  - rfam ?
  - pfam
  - interpro
  - dbpedia
  - wikidata
  - ensembl
  - ncbi
  - pubmed
# 2021-04-01
* Had to split up into plus and minus strands
* Removed "off-diagonal" hits by retaining only the first in each set of duplicates. 
* on minus strand: swap start and end because blast returns coordinates on minus
  strand if run with --strand minus
* Very short reads did not yield any hits, reran with blastn-short and evalue
  cutoff=1. still 9 missing.
* Load gff as db with gffutils
* create empty new db.
* iterate over genes
* for each gene and its children (mRNA, exon, CDS):
    *  fix start, end position
    * update in newdb
# 2021-03-31
* Generated prokka annotation with GCA_922v1 translated CDS as protein reference
  (from ensembl release 49).
* newly assigned pflu numbers do not match to old pflus.
* todo:
    * run again with rna activated
* loaded GCA_ASM922v1 reference dna seq and annotation into biopython notebook
* for each feature in the annotation file:
    * extract sequence from ref. sequence
    * save as fasta file -> 26k fastas
* on each fasta file:
    * blastn against new genome assembly, save positions and scores in csv.
* TODO: 
    * assemble new gff file from the generated csv, copy over the corresponding
      annotations from ref. annotation.
    * save all hypothetical proteins separately and blastn against swissprot db.

# 2021-01-07
## PGAP
* Rerun with locus_tag_prefix="PFLU" to achieve same PFLUs as in the assembly (??)

# 2020-12-22
## Megannotator (https://github.com/GabrieleAndrea/MEGAnnotator)
### Installation
* get megannotator repo:
```
git clone git@github.com:mpievolbio-scicomp/MEGAnnotator
cd MEGAnnotator
```
* Install rnammer from dtu
```
wget https://services.healthtech.dtu.dk/download/4f854d06-a53b-43c4-b2b4-e446907d1718/rnammer-1.2.src.tar.gz
tar xzvf rnammer-1.2.src.tar.gz
mv core-rnammer rnammer xml2* bin
```

* Install remaining dependencies with `conda`
```
conda create -n megannotator
conda activate megannotator
conda install readseq bwa samtools tabix hmmer emboss abyss trnascan-se gatk -c bioconda -c conda-forge
```

* got message about cuda-mpi: 
```
For Linux 64, Open MPI is built with CUDA awareness but this support is disabled by default.\nTo enable it, please set the environmental variable OMPI_MCA_opal_cuda_support=true before\nlaunching your MPI processes. Equivalently, you can set the MCA parameter in the command line:\nmpiexec --mca opal_cuda_support 1 
```

* DL EBI hmmer data
```
wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz
```

* DL NCBI data
```
 wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz  
```

* DL refseq data
`wget` command does not work, use midnight commander instead.

# 2020-12-21
## PGAP (the NCBI Prokaryotic Genome Annotation Pipeline)
* on server deepbio:

```
cd Projects/SBW25_Annotation
./pgap.py -n -o sbw25_out.1 -c32 sbw25/input.yaml
```

started 2020-12-21 at 16:30.

# 2020-12-18
### PROKKA Annotation
#### Installation

```
 conda create -n prokka1.14_perl5.32 perl=5.32 -c bioconda -c conda-forge -c anaconda
 conda activate prokka1.14_perl5.32
 conda install perl-bioperl -c bioconda
 conda install -c bioconda perl-xml-simple barrnap
```
Must export the location of `XML/Simple.pm`:
```
 find /opt/anaconda3/envs/prokka1.14_perl5.32 -name Simple.pm
 export PERL5LIB=/opt/anaconda3/envs/prokka1.14_perl5.32/lib/perl5/site_perl/5.22.0
```

##### Signalp dependency
Needed for the `-gram` option.
```
 wget https://services.healthtech.dtu.dk/download/<SECRET_TOKEN>/signalp-3.0.Linux.tar.Z
 conda install -c predector signalp3
 signalp3-register signalp-3.0.Linux.tar.Z
```


#### Building uniprot db locally
* install swissknife:
```
conda install perl-app-cpanminus -c bioconda
cpanm https://sourceforge.net/projects/swissknife/files/swissknife/1.80/swissknife_1.80.tar.gz -n
```
`-n` for no testing.

* get uniprot data:
```
wget
ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/taxonomic_divisions/uniprot_sprot_bacteria.dat.gz
```

### Genome browser
http://172.16.255.28:8080/apollo/annotator/index

### Aligning new to old (reference) genome

* Use mummer4
* dnadiff report: 
```
                               [REF]                [QRY]
[Sequences]
TotalSeqs                          1                    1
AlignedSeqs              1(100.0000%)          1(100.0000%)
UnalignedSeqs             0(0.0000%)           0(0.0000%)

[Bases]
TotalBases                   6722539              6722400
AlignedBases       6722539(100.0000%)    6722399(100.0000%)
UnalignedBases            0(0.0000%)           1(0.0000%)

[Alignments]
1-to-1                             3                    3
TotalLength                  6723236              6723224
AvgLength               2241078.6667         2241074.6667
AvgIdentity                  99.9900              99.9900

M-to-M                             4                    4
TotalLength                  6723755              6723743
AvgLength               1680938.7500         1680935.7500
AvgIdentity                  99.9900              99.9900

[Feature Estimates]
Breakpoints                        6                    7
Relocations                        0                    1
Translocations                     0                    0
Inversions                         0                    0

Insertions                         1                    2
InsertionSum                     519                  221
InsertionAvg                519.0000             110.5000

TandemIns                          0                    1
TandemInsSum                       0                  220
TandemInsAvg                  0.0000             220.0000

[SNPs]
TotalSNPs                          3                    3
GA                        0(0.0000%)           0(0.0000%)
GC                        0(0.0000%)           0(0.0000%)
GT                        0(0.0000%)           0(0.0000%)
AG                        0(0.0000%)           0(0.0000%)
AC                        0(0.0000%)           0(0.0000%)
AT                        0(0.0000%)           0(0.0000%)
CT                        0(0.0000%)          3(100.0000%)
CA                        0(0.0000%)           0(0.0000%)
CG                        0(0.0000%)           0(0.0000%)
TG                        0(0.0000%)           0(0.0000%)
TA                        0(0.0000%)           0(0.0000%)
TC                       3(100.0000%)           0(0.0000%)

TotalGSNPs                         1                    1
GC                        0(0.0000%)           0(0.0000%)
GT                        0(0.0000%)           0(0.0000%)
GA                        0(0.0000%)           0(0.0000%)
AC                        0(0.0000%)           0(0.0000%)
AT                        0(0.0000%)           0(0.0000%)
AG                        0(0.0000%)           0(0.0000%)
CT                        0(0.0000%)          1(100.0000%)
CA                        0(0.0000%)           0(0.0000%)
CG                        0(0.0000%)           0(0.0000%)
TA                        0(0.0000%)           0(0.0000%)
TG                        0(0.0000%)           0(0.0000%)
TC                       1(100.0000%)           0(0.0000%)

TotalIndels                        4                    4
G.                        0(0.0000%)          1(25.0000%)
A.                        0(0.0000%)           0(0.0000%)
C.                       2(50.0000%)          1(25.0000%)
T.                        0(0.0000%)           0(0.0000%)
.A                        0(0.0000%)           0(0.0000%)
.G                       1(25.0000%)           0(0.0000%)
.T                        0(0.0000%)           0(0.0000%)
.C                       1(25.0000%)          2(50.0000%)

TotalGIndels                       2                    2
G.                        0(0.0000%)          1(50.0000%)
A.                        0(0.0000%)           0(0.0000%)
C.                        0(0.0000%)          1(50.0000%)
T.                        0(0.0000%)           0(0.0000%)
.G                       1(50.0000%)           0(0.0000%)
.A                        0(0.0000%)           0(0.0000%)
.C                       1(50.0000%)           0(0.0000%)
.T                        0(0.0000%)           0(0.0000%)
```

* Displaying mummer delta files in jbrowse is not so straight forward, will skip
  that for now.

## Further annotation todos:
* rerun prokka with ncRNA and other finders activated
* ncbi annotation pipeline: https://github.com/ncbi/pgap
* glimmer: http://ccb.jhu.edu/software.shtml#genefinding
* augustus ab-inito: will require training
* is there an ensembl annotation pipeline similar to pgap?


