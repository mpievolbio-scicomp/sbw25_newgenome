#! /bin/bash

patch MPBAS00001.embl reference.patch

cat MPBAS00001.embl | sed -e "s/\"\/product=//" | sed -e "s/LOCUS/misc/" | sed -e "s/PFLU_PFLU/PFLU/" |  grep -v "\/anticodon=" tmp.embl

mv tmp.embl MPBAS00001.embl
python fix_embl_regulatory.py

cat MPBAS00001.embl | gzip - > MPBAS00001.embl.gz
