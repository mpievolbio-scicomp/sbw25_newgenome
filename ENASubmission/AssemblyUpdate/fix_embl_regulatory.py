#!/usr/bin/env python


import os, sys
import re


# Read in data.
with open("MPBAS00001.embl", 'r') as ifh:
    embl_lines = ifh.readlines()

# Pattern for regulatory feature
reg_start_pattern = re.compile("^FT\s{3}regulatory")

# List of indices where reg. features start.
regulatory_feature_starts = []
for i, line in enumerate(embl_lines):
    if reg_start_pattern.match(line) is not None:
        
        regulatory_feature_starts.append(i)
   

# List of lines that contain notes to filter out.
lines_to_remove = []

# Regex patterns to filter out.
note_filter_patterns = [
    r'FT\s{19}/note="source',
    r'FT\s{19}/note="frame',
    r'FT\s{19}/note="ID',
    r'FT\s{19}/note="Copied',
]

# Go over every reg. feature.
for line_idx in regulatory_feature_starts:
    idx = line_idx + 1
    line = embl_lines[idx]
    
    # Check if this is still in the same feature.
    while re.match(r'^FT\s{3}[a-z,A-Z]', line) is None:
        
        # Check if this is a note line
        if re.match(r'^FT\s{19}/note', line):
           
            # If one of the patterns matches: filter out.
            if any([re.match(pattern, line) is not None for pattern in note_filter_patterns]):
                lines_to_remove.append(idx)
            # If last pattern matched: remove next line as well.
            if re.match(note_filter_patterns[-1], line):
                lines_to_remove.append(idx+1)
            else:
        else:
            pass
        idx += 1
        line = embl_lines[idx]

fixed_lines = [line for i,line in enumerate(embl_lines) if i not in lines_to_remove]


with open('MPBAS00001.embl', 'w') as ofh:
    ofh.writelines(fixed_lines)
    




