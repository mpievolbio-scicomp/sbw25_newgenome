# New SBW25 Genome data
This directory contains the latest version of the Pseudomonas fluorescens SBW25 genome.

| Filename | Filetype | Description |
|----------|----------|-------------|
| MPBAS00001.fasta | fasta | Fasta formatted genome sequence (6722400 bp) |
| MPBAS00001.gff3  | genome feature format v3 (gff3) | Annotated genome features merged from previous reference annotations and the Sanger2009 manual annotation |
| MPBAS00001.gff3.db | sqlite database files | Contains the same information as MPBAS00001.gff3 as a relational database. This file can be opened with sqlite3 or with `gffutils.FeatureDB` in python. |
| MPBAS00001.embl.gz | gzip | Converted from MPBAS00001.gff3. The conversion assigned locus tags for misc_features that are not present in the gff3 version. Some attributes in gff3 have no corresponding qualifier in the embl format and have either been moved to a /note or stripped. |
| chromosomes.txt.gz | gzip, text | Compressed text file listing the name of the submitted chromosome (MPBAS00001) |
| genome | Directory | Contains temporary and log files from the submission process |
| manifest.txt | text | Manifest file listing the files submitted to ENA |
| gff3_to_embl.sh | Shell script | Converts MPBAS00001.gff3 to MPBAS00001.embl |
|translation_gff_attribute_to_embl_qualifier.json | json | Maps gff3 attributes
to embl qualifiers, used by gff3_to_embl.sh |
| translation_gff_feature_to_embl_feature.json | Maps gff3 feature types to embl  feature types | 
| translation_gff_other_to_embl_qualifier.json | Maps less frequently used gff3 attributes to embl qualifiers. |
| gff3_to_embl.log | text log file | Log file from the gff3 to embl conversion |
| reference.patch | UNIX patch file | Applying this patch to MPBAS00001.embl inserts the author information for this record. |
| fix_embl_regulatory.py | python script | Applies smaller fixes to MPBAS00001.embl: Removes duplicate /note's from regulatory features, where only a single note is allowed according to the embl flat file specification. |
| cleanup_embl.sh  | Shell script | Fixes malformed locus tags and other smaller issues found in MPBAS00001.embl, applies "reference.patch" and runs "fix_embl_regulatory.py" |
| validate.sh | Shell script | Validates manifest.txt before submission |
| submit.sh | Shell script | Submits the genome to ENA |
| webin-cli.report | Text | Report from the submission process |
| README.md | markdown | This document |

