[QueryItem="feature"]
PREFIX : <http://purl.org/net/chado/schema/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX chado: <http://purl.org/net/chado/schema/>
PREFIX sbw25: <http://micropop046.evolbio.mpg.de/sbw25/>

select * where {
?feature a chado:Feature .
filter(?feature_id=121199)
  ?feature chado:organism_id ?organism_id .
  ?organism a chado:Organism;
chado:organism_id ?organism_id .
  filter(?organism_id=4)
  ?subj chado:feature_id ?feature_id .
  ?subj ?prop ?value .
} 
LIMIT 10
[QueryItem="featureloc"]
PREFIX : <http://purl.org/net/chado/schema/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obda: <https://w3id.org/obda/vocabulary#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX chado: <http://purl.org/net/chado/schema/>
PREFIX sbw25: <http://micropop046.evolbio.mpg.de/sbw25/>

select ?prop ?val where {
?feature a chado:Feature .
?feature chado:feature_id ?id .
?loc a chado:Featureloc .
?loc chado:feature_id ?id .
?loc ?prop ?val .
#filter(regex(?loc, "107926"))
} 
LIMIT 10