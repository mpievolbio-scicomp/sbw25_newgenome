#!/bin/bash

######################################################################
# ILLUMINA POLISH
# using pilon
######################################################################

# QC ILLUMINA SEQS
# using fastp
cd Illumina
mkdir QC
cd QC

f=''; 
find .. -maxdepth 1 -mindepth 1 -type f | sort | while read each; do 
	if [ ! $f ]; then 
		f="$each"; 
		continue; 
	fi; 
	r="$each"; 
	bash ../../illumina_QC.sh $f $r; 
	f=''; 
	r=''; 
done


## MERGE LANES
# merge paired
for i in 1 2; do 
	find . -type f -iname "*.gz" | cut -d '_' -f 1-2 | sort -u | while read each; do 
		name=$(basename $each); 
		echo $name; 
		find . -type f -iname "$name*R${i}*trimmed*.gz" | sort | while read fastq; do 
			cat $fastq >> ${name}_R${i}_trimmed_mergedlanes.fastq.gz; 
		done;
      	done;
done

# merge unpaired
find . -type f -iname "*.gz" | cut -d '_' -f 1-2 | sort -u | while read each; do 
	name=$(basename $each); 
	echo $name; 
	find . -type f -iname "$name*unpaired*.gz" | sort | while read fastq; do 
		cat $fastq >> ${name}_unpaired_mergedlanes.fastq.gz; 
	done;  
done;

# FIND INSERT SIZES
cd /fast

# only SBW for now
grep "WTSBW" /seq/databases/genomic_sources/SBW25_pacbio/samples.csv | while read each; do 
	cd $(awk -F '\t' '{print $1}' <<< "$each"); 
	mkdir -p pilon_polish; 
	cd pilon_polish; 
	bash /seq/databases/genomic_sources/SBW25_pacbio/find_ins_size.sh $(awk -F '\t' '{print $4,$5,$3}' <<< "$each"); 
	cd ..; 
	cd ..; 
done

## PILON POLISH
# still only SBW for now
grep "WTSBW" /seq/databases/genomic_sources/SBW25_pacbio/samples.csv | while read each; do 
	cd $(awk -F '\t' '{print $1}' <<< "$each"); 
	cd pilon_polish; bash /seq/databases/genomic_sources/SBW25_pacbio/pilon_polish.sh $(awk -F '\t' '{print $4,$5,$6,$3}' <<< "$each"); 
	cd ..; 
	cd ..; 
done


## PROKKA ANNOTATIONS
# only SBW25 for now
grep "WTSBW" /seq/databases/genomic_sources/SBW25_pacbio/samples.csv | while read each; do 
	cd $(awk -F '\t' '{print $1}' <<< "$each");
	mkdir prokka
	cd prokka
	prokka --outdir Pseudomonas_fluorescens_SBW25_28C --addgenes --locustag PFLU --prefix Pseudomonas_fluorescens_SBW25_28C --increment 1 --compliant --centre MPB --genus Pseudomonas --species fluorescens --strain SBW25 --gcode 11 --gram neg --proteins /seq/databases/genomic_sources/SBW25/GCA_000009225.1_ASM922v1_genomic.gbff --addmrna  --cpus 14 --rfam ../pilon_polish/round_1.fasta;
	cd ..;
	cd ..;
done
