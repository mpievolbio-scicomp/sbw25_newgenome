#!/bin/bash

######################################################################
# WRAPPER for pacbio assembly pipe
######################################################################

find . -maxdepth 2 -mindepth 1 -type f -iname "L*.fastq.gz" | while read each; do i
	fastq=$(basename $each); 
	fdir=$(cut -d '.' -f 1 <<< "$fastq"); 
	mkdir -p /fast/$fdir; 
	cp $each /fast/$fdir; 
	cd /fast/$fdir; 
	mkdir -p assemblies; 
	../assembly_pipe.sh $fastq; 
	cd /seq/databases/genomic_sources/SBW25_pacbio; 
done


