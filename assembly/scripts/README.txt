In the path /Volumes/micropop/New Genome/pacbio/Loukas_Raw_PACBIO/ there are 8 folders:
1. raw/ contains all the raw data compressed into one file
2. reference/ contains an alignment for each one of the samples [34837,34838,34839,34840,34841, 34842]
3. L34837/ refers to the sample s1_TSS-f6_28C
4. L34838/ refers to the sample s2_TSS-f6_20C
5. L34839/ refers to the sample s3_SBW25_28C
6. L34840/ refers to the sample s4_SBW25_20C
7. L34841/ refers to the sample s5_f2_28C
8. L34842/ refers to the sample s6_f2_20C

In order to assemble the new reference genome we have to work with the folders L34839 and L34840. 
Especially we need to focus on the folder L34839.
