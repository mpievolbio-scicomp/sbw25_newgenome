#!/bin/bash

set -e -u

F=$1
R=$2
U=$3
before=$4 #consensus/polished sequence
after=round_1
threads=14

ins_sizes=$(python /seq/databases/genomic_sources/SBW25_pacbio/find_ins_size.py)
insert_min=$(awk '{print $1}' <<< $ins_sizes)
insert_max=$(awk '{print $2}' <<< $ins_sizes)

bowtie2-build "$before" "$before"
bowtie2 -1 $F -2 $R -x "$before" --threads "$threads" -I "$insert_min" -X "$insert_max" --local --very-sensitive-local | samtools sort > illumina_alignments.bam
bowtie2 -U $U -x "$before" --threads "$threads" --local --very-sensitive-local | samtools sort > illumina_alignments_u.bam
samtools index illumina_alignments.bam
samtools index illumina_alignments_u.bam

java -Xmx16G -jar /home/eric/bin/pilon-1.23.jar --genome "$before" --frags illumina_alignments.bam --unpaired illumina_alignments_u.bam --output "$after" --changes

# clean up
rm *.bam *.bam.bai *.bt2
# remove "_pilon" from the FASTA headers
sed -i 's/_pilon//' "$after".fasta
