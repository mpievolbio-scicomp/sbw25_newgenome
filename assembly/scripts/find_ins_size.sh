#!/bin/bash

F=$1
R=$2
consensus=$3

threads=14

bowtie2-build $consensus $consensus
bowtie2 -1 $F -2 $R -x $consensus --fast --threads $threads -I 0 -X 1000 -S insert_size_test.sam
