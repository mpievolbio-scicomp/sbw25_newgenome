#!/bin/bash

######################################################################
# LONG READ ASSEMBLY
# employing multiple assemblers and consensus by trycycler
######################################################################

set -eu

fastq=$1
threads=15

# filter short and very bad reads
# we have a lot of reads, therefore quite aggressive
filtlong --min_length 1000 --keep_percent 90 $fastq > reads.fastq

# setup to randomly subsample
target_depth=50
genome_size=6722539
mean_length=$(seqtk comp reads.fastq | awk '{count++; bases += $2} END{print bases/count}')
read_count=$(echo $target_depth"*"$genome_size"/"$mean_length | bc)

# random subsampling into 12 samples
for i in {00..11}; do
    seqtk sample -s "$i" reads.fastq "$read_count" | paste - - - - | shuf | tr '\t' '\n' > sample_"$i".fastq
done

# assemble the subsamples with three different, good longread assemblers
for i in {00..03}; do
        flye --pacbio-raw sample_$i.fastq --genome-size "$genome_size" --threads "$threads" --plasmids --out-dir assembly_$i; 
	cp assembly_0$i/assembly.fasta assemblies/assembly_$i.fasta; 
	rm -r assembly_$i &
done
for i in {04..07}; do
        miniasm_and_minipolish.sh sample_$i.fastq "$threads" > assembly_$i.gfa; 
	any2fasta assembly_0$i.gfa > assemblies/assembly_$i.fasta; 
	rm assembly_$i.gfa
done
for i in {08..11}; do
        raven --threads "$threads" sample_$i.fastq > assemblies/assembly_$i.fasta; 
	rm raven.cereal;
done

# save some space by removing the random subsamples
rm sample_*.fastq

# run trycycler to cluster assemblies
trycycler cluster --threads $threads --assemblies assemblies/*.fasta --reads reads.fastq --out_dir trycycler

# presume only cluster_001 is what we want
# prior manual analysis of the clusterings suggest that only the first cluster is useful
# can still go back and look later
trycycler reconcile --threads $threads --reads reads.fastq --cluster_dir trycycler/cluster_001
