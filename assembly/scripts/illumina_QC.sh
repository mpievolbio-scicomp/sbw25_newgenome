#!/bin/bash

set -e

F=$1
R=$2
Fname=$(basename $F | cut -d '.' -f 1)
Rname=$(basename $R | cut -d '.' -f 1)

threads=15

fastp --in1 $F --in2 $R --out1 ${Fname}_trimmed.fastq.gz --out2 ${Rname}_trimmed.fastq.gz --unpaired1 ${Fname}_unpaired.fastq.gz --unpaired2 ${Rname}_unpaired.fastq.gz --thread 15
